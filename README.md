jdhv_robotics
===============

ROS metapackage that includes a mapping and path planning framework for AUVs.

It is composed of different ROS packages:

-jdhv_dev contains some development tools including a driver to read the vehicle echousonders, script for repairing bag files and make them compatible with recent ROS versions, among others.

-jdhv_mp contains several nodes for motion planning that uses or are built on the Open Motion Planning Library (OMPL). These nodes have been coded throughout my PhD thesis and represent an incremental and progressive development, starting from offline path-planning (map is available) approach where the C-Space is assume R2 (X-Y), to a more elaborated online mapping and path-planning approach for 3D workspaces (i.e., C-Space: SE(2)xR = X-Y-Z-Yaw)

-jdhv_scenes contains the launch files for different test scenarios, including simulated and real-world setups.

-laser_octomap contains an alternative for the octomap_server. This approach uses directly the laser_scan from profiling sonars, multibeam sonar, and ranges from echosounders. This package has been developed in collaboration with Guillem Vallicrosa.

-tests_octomaps_ompl_fcl contains different source files in C++ and Python commonly used to test specific functionalities of octomaps, OMPL, and FCL. This package is used to check compatibility issues when upgrading any of the mentioned libraries and frameworks.