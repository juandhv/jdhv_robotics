/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2010, Rice University
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Rice University nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/config.h>
#include <iostream>

namespace ob = ompl::base;
namespace og = ompl::geometric;

int main(int, char **)
{
    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;

    // *** creating a state from the state space SE2
    ob::StateSpacePtr space_se2(new ob::SE2StateSpace());
    ob::ScopedState<ob::SE2StateSpace> state_se2(space_se2);

    state_se2->setX(1.0);
    std::cout << "state_se2: " << state_se2 << std::endl;

    // *** creating a state from the space information SE2
    ob::SpaceInformationPtr si_se2(new ob::SpaceInformation(space_se2));
    ob::ScopedState<ob::SE2StateSpace> another_state_se2(si_se2);

    another_state_se2->setXY(1.0, 2.3);
    std::cout << "another_state_se2: " << another_state_se2 << std::endl;

    // *** creating a state manually
    ob::State* manual_state = si_se2->allocState();
    std::cout << "manual_state: " << manual_state << std::endl <<  std::endl;
    si_se2->freeState(manual_state);

    // *** creating a compound space state
    ob::CompoundStateSpace *cs = new ob::CompoundStateSpace();
    cs->addSubspace(ob::StateSpacePtr(new ob::SO2StateSpace()), 1.0);
    cs->addSubspace(ob::StateSpacePtr(new ob::SO3StateSpace()), 1.0);

    // put the pointer to the state space in a shared pointer
    ob::StateSpacePtr compound_space(cs);

    ob::ScopedState<ob::CompoundStateSpace> state_cs(compound_space);
    state_cs->as<ob::SO2StateSpace::StateType>(0)->setIdentity();

    std::cout << "state_cs: " << state_cs << std::endl;

    // *** alternative way to create a compound space state
    // define the individual state spaces
    ob::StateSpacePtr space_so2(new ob::SO2StateSpace());
    ob::StateSpacePtr space_so3(new ob::SO3StateSpace());

    // construct a compound state space using the overloaded operator+
    ob::StateSpacePtr compound_space2 = space_so2 + space_so3;

    ob::ScopedState<ob::CompoundStateSpace> state_cs2(compound_space2);

    std::cout << "state_cs2: " << state_cs2 << std::endl;

    // *** manipulating states of a compound space state
    ob::ScopedState<ob::SE2StateSpace> full_state_se2(space_se2);
    full_state_se2.random();

    std::cout << "full_state_se2: " << full_state_se2 << std::endl;

    ob::ScopedState<> pos(space_se2->as<ob::SE2StateSpace>()->getSubspace(0));
    ob::ScopedState<> ori(space_se2->as<ob::SE2StateSpace>()->getSubspace(1));
    pos << full_state_se2;
    ori << full_state_se2;

    std::cout << "full_state_se2: " << full_state_se2 << std::endl;
    std::cout << "pos: " << pos << std::endl;
    std::cout << "ori: " << ori << std::endl;

    ori->as<ob::SO2StateSpace::StateType>()->value = 1.3;
    pos->as<ob::RealVectorStateSpace::StateType>()->values[0] = 0.5;
    pos->as<ob::RealVectorStateSpace::StateType>()->values[1] = 2.5;

    ori >> full_state_se2;

    std::cout << "full_state_se2: " << full_state_se2 << std::endl;
    std::cout << "pos: " << pos << std::endl;
    std::cout << "ori: " << ori << std::endl;

    return 0;
}
