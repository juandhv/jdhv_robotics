#include <fcl/shape/geometric_shapes.h>
#include <fcl/shape/geometric_shapes_utility.h>
#include <fcl/narrowphase/narrowphase.h>
#include <fcl/octree.h>
#include <fcl/traversal/traversal_node_octree.h>
#include <fcl/broadphase/broadphase.h>
#include <fcl/shape/geometric_shape_to_BVH_model.h>
#include <fcl/math/transform.h>
#include <iostream>
#include <fcl/collision.h>
#include <boost/foreach.hpp>
//#include <boost/test/unit_test.hpp>

#include "test_fcl_utility.h"
#include "fcl_resources/config.h"

using namespace std;
using namespace fcl;

struct TStruct
{
  std::vector<double> records;
  double overall_time;

  TStruct() { overall_time = 0; }

  void push_back(double t)
  {
    records.push_back(t);
    overall_time += t;
  }
};

/// @brief Generate environment with 3 * n objects: n spheres, n boxes and n cylinders
void generateEnvironments(std::vector<CollisionObject*>& env, double env_scale, std::size_t n);

/// @brief Generate environment with 3 * n objects: n spheres, n boxes and n cylinders, all in mesh
void generateEnvironmentsMesh(std::vector<CollisionObject*>& env, double env_scale, std::size_t n);

/// @brief Generate boxes from the octomap
void generateBoxesFromOctomap(std::vector<CollisionObject*>& env, OcTree& tree);

/// @brief Generate boxes from the octomap
void generateBoxesFromOctomapMesh(std::vector<CollisionObject*>& env, OcTree& tree);

/// @brief Octomap collision with an environment with 3 * env_size objects
void octomap_collision_test(double env_scale, std::size_t env_size, bool exhaustive, std::size_t num_max_contacts, bool use_mesh, bool use_mesh_octomap);

void octomap_collision_test2();

/// @brief Generate an octree
octomap::OcTree* generateOcTree();

int main(int argc, char** argv) 
{
//	std::shared_ptr<Box> box0(new Box(1,1,1));
//	std::shared_ptr<Box> box1(new Box(1,1,1));
//	//  GJKSolver_indep solver;
//	GJKSolver_libccd solver;
//	Vec3f contact_points;
//	FCL_REAL penetration_depth;
//	Vec3f normal;
//
//	Transform3f tf0, tf1;
//	tf0.setIdentity();
//	tf0.setTranslation(Vec3f(.9,0,0));
//	tf0.setQuatRotation(Quaternion3f(.6, .8, 0, 0));
//	tf1.setIdentity();
//
//
//
//	bool res = solver.shapeIntersect(*box0, tf0, *box1, tf1, &contact_points, &penetration_depth, &normal);
//
//	cout << "contact points: " << contact_points << endl;
//	cout << "pen depth: " << penetration_depth << endl;
//	cout << "normal: " << normal << endl;
//	cout << "result: " << res << endl;
//
//	static const int num_max_contacts = std::numeric_limits<int>::max();
//	static const bool enable_contact = true;
//	fcl::CollisionResult result;
//	fcl::CollisionRequest request(num_max_contacts,
//			enable_contact);
//
//
//	CollisionObject co0(box0, tf0);
//	CollisionObject co1(box1, tf1);
//
//	fcl::collide(&co0, &co1, request, result);
//	vector<Contact> contacts;
//	result.getContacts(contacts);
//
//	cout << contacts.size() << " contacts found" << endl;
//	BOOST_FOREACH(Contact& contact, contacts) {
//		cout << "position: " << contact.pos << endl;
//	}

	//octomap_collision_test(200, 100, false, 10, false, false);
	octomap_collision_test2();
}

void octomap_collision_test2()
{
	Transform3f tf0;
	tf0.setIdentity();
	tf0.setTranslation(Vec3f(6.0,2.5,0));
	std::shared_ptr<Box> box0(new Box(1.0,1.0,1.0));
	CollisionObject co0(box0, tf0);

	Transform3f tf1;
	tf1.setIdentity();
	tf1.setTranslation(Vec3f(6.0,2.8,0.0));
	Quaternion3f qt0;
	qt0.fromEuler(0.0, 0.0, 1.57);
	tf1.setQuatRotation(qt0);
	std::shared_ptr<Box> box1(new Box(1.6,0.5,0.5));
	CollisionObject co1(box1, tf1);

	octomap::OcTree* octomap_tree = new octomap::OcTree("/home/juandhv/Dropbox/PhD/3D_ModelsMaps/Iros1.bt");
	OcTree* tree = new OcTree(std::shared_ptr<const octomap::OcTree>(octomap_tree));
	CollisionObject tree_obj((std::shared_ptr<CollisionGeometry>(tree)));

	fcl::CollisionRequest collisionRequest;
	fcl::CollisionResult collisionResult;
//	fcl::Quaternion3f rot;
//	fcl::Vec3f pos;
//	fcl::Transform3f transform;

//	if (environment_.num_tris > 0)
//	{
//		// Performing collision checking with environment.
//		for (std::size_t i = 0; i < robotParts_.size(); ++i)
//		{
//			poseFromStateCallback_(pos, rot, extractState_(state, i));
//			transform.setTransform(rot, pos);
//			fcl::collide()
			fcl::collide(&tree_obj, &co0, collisionRequest, collisionResult);
			vector<Contact> contacts;
			collisionResult.getContacts(contacts);
			cout << "collision: " << collisionResult.isCollision() << endl;

			collisionResult.clear();
			fcl::collide(&tree_obj, &co1, collisionRequest, collisionResult);
			cout << "collision: " << collisionResult.isCollision() << endl;
//			if (fcl::collide(robotParts_[i], transform, &environment_, fcl::Transform3f(), collisionRequest, collisionResult) > 0)
//				return false;
//		}
//	}
}

void octomap_collision_test(double env_scale, std::size_t env_size, bool exhaustive, std::size_t num_max_contacts, bool use_mesh, bool use_mesh_octomap)
{
  // srand(1);
  std::vector<CollisionObject*> env;
  if(use_mesh)
    generateEnvironmentsMesh(env, env_scale, env_size);
  else
    generateEnvironments(env, env_scale, env_size);

  OcTree* tree = new OcTree(std::shared_ptr<const octomap::OcTree>(generateOcTree()));
  CollisionObject tree_obj((std::shared_ptr<CollisionGeometry>(tree)));

  DynamicAABBTreeCollisionManager* manager = new DynamicAABBTreeCollisionManager();
  manager->registerObjects(env);
  manager->setup();

  CollisionData cdata;
  if(exhaustive) cdata.request.num_max_contacts = 100000;
  else cdata.request.num_max_contacts = num_max_contacts;

  TStruct t1;
  Timer timer1;
  timer1.start();
  manager->octree_as_geometry_collide = false;
  manager->octree_as_geometry_distance = false;
  manager->collide(&tree_obj, &cdata, defaultCollisionFunction);
  timer1.stop();
  t1.push_back(timer1.getElapsedTime());

  CollisionData cdata3;
  if(exhaustive) cdata3.request.num_max_contacts = 100000;
  else cdata3.request.num_max_contacts = num_max_contacts;

  TStruct t3;
  Timer timer3;
  timer3.start();
  manager->octree_as_geometry_collide = true;
  manager->octree_as_geometry_distance = true;
  manager->collide(&tree_obj, &cdata3, defaultCollisionFunction);
  timer3.stop();
  t3.push_back(timer3.getElapsedTime());

  TStruct t2;
  Timer timer2;
  timer2.start();
  std::vector<CollisionObject*> boxes;
  if(use_mesh_octomap)
    generateBoxesFromOctomapMesh(boxes, *tree);
  else
    generateBoxesFromOctomap(boxes, *tree);
  timer2.stop();
  t2.push_back(timer2.getElapsedTime());

  timer2.start();
  DynamicAABBTreeCollisionManager* manager2 = new DynamicAABBTreeCollisionManager();
  manager2->registerObjects(boxes);
  manager2->setup();
  timer2.stop();
  t2.push_back(timer2.getElapsedTime());


  CollisionData cdata2;
  if(exhaustive) cdata2.request.num_max_contacts = 100000;
  else cdata2.request.num_max_contacts = num_max_contacts;

  timer2.start();
  manager->collide(manager2, &cdata2, defaultCollisionFunction);
  timer2.stop();
  t2.push_back(timer2.getElapsedTime());

  std::cout << cdata.result.numContacts() << " " << cdata3.result.numContacts() << " " << cdata2.result.numContacts() << std::endl;
//  if(exhaustive)
//  {
//    if(use_mesh) BOOST_CHECK((cdata.result.numContacts() > 0) >= (cdata2.result.numContacts() > 0));
//    else BOOST_CHECK(cdata.result.numContacts() == cdata2.result.numContacts());
//  }
//  else
//  {
//    if(use_mesh) BOOST_CHECK((cdata.result.numContacts() > 0) >= (cdata2.result.numContacts() > 0));
//    else BOOST_CHECK((cdata.result.numContacts() > 0) >= (cdata2.result.numContacts() > 0)); // because AABB return collision when two boxes contact
//  }

  delete manager;
  delete manager2;
  for(size_t i = 0; i < boxes.size(); ++i)
    delete boxes[i];

  if(exhaustive) std::cout << "exhaustive collision" << std::endl;
  else std::cout << "non exhaustive collision" << std::endl;
  std::cout << "1) octomap overall time: " << t1.overall_time << std::endl;
  std::cout << "1') octomap overall time (as geometry): " << t3.overall_time << std::endl;
  std::cout << "2) boxes overall time: " << t2.overall_time << std::endl;
  std::cout << "  a) to boxes: " << t2.records[0] << std::endl;
  std::cout << "  b) structure init: " << t2.records[1] << std::endl;
  std::cout << "  c) collision: " << t2.records[2] << std::endl;
  std::cout << "Note: octomap may need more collides when using mesh, because octomap collision uses box primitive inside" << std::endl;
}

void generateEnvironments(std::vector<CollisionObject*>& env, double env_scale, std::size_t n)
{
  FCL_REAL extents[] = {-env_scale, env_scale, -env_scale, env_scale, -env_scale, env_scale};
  std::vector<Transform3f> transforms;

  generateRandomTransforms(extents, transforms, n);
  for(std::size_t i = 0; i < n; ++i)
  {
    Box* box = new Box(5, 10, 20);
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(box), transforms[i]));
  }

  generateRandomTransforms(extents, transforms, n);
  for(std::size_t i = 0; i < n; ++i)
  {
    Sphere* sphere = new Sphere(30);
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(sphere), transforms[i]));
  }

  generateRandomTransforms(extents, transforms, n);
  for(std::size_t i = 0; i < n; ++i)
  {
    Cylinder* cylinder = new Cylinder(10, 40);
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(cylinder), transforms[i]));
  }

}


void generateBoxesFromOctomapMesh(std::vector<CollisionObject*>& boxes, OcTree& tree)
{
  std::vector<std::array<FCL_REAL, 6> > boxes_ = tree.toBoxes();

  for(std::size_t i = 0; i < boxes_.size(); ++i)
  {
    FCL_REAL x = boxes_[i][0];
    FCL_REAL y = boxes_[i][1];
    FCL_REAL z = boxes_[i][2];
    FCL_REAL size = boxes_[i][3];
    FCL_REAL cost = boxes_[i][4];
    FCL_REAL threshold = boxes_[i][5];

    Box box(size, size, size);
    BVHModel<OBBRSS>* model = new BVHModel<OBBRSS>();
    generateBVHModel(*model, box, Transform3f());
    model->cost_density = cost;
    model->threshold_occupied = threshold;
    CollisionObject* obj = new CollisionObject(std::shared_ptr<CollisionGeometry>(model), Transform3f(Vec3f(x, y, z)));
    boxes.push_back(obj);
  }

  std::cout << "boxes size: " << boxes.size() << std::endl;
}

void generateEnvironmentsMesh(std::vector<CollisionObject*>& env, double env_scale, std::size_t n)
{
  FCL_REAL extents[] = {-env_scale, env_scale, -env_scale, env_scale, -env_scale, env_scale};
  std::vector<Transform3f> transforms;

  generateRandomTransforms(extents, transforms, n);
  Box box(5, 10, 20);
  for(std::size_t i = 0; i < n; ++i)
  {
    BVHModel<OBBRSS>* model = new BVHModel<OBBRSS>();
    generateBVHModel(*model, box, Transform3f());
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(model), transforms[i]));
  }

  generateRandomTransforms(extents, transforms, n);
  Sphere sphere(30);
  for(std::size_t i = 0; i < n; ++i)
  {
    BVHModel<OBBRSS>* model = new BVHModel<OBBRSS>();
    generateBVHModel(*model, sphere, Transform3f(), 16, 16);
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(model), transforms[i]));
  }

  generateRandomTransforms(extents, transforms, n);
  Cylinder cylinder(10, 40);
  for(std::size_t i = 0; i < n; ++i)
  {
    BVHModel<OBBRSS>* model = new BVHModel<OBBRSS>();
    generateBVHModel(*model, cylinder, Transform3f(), 16, 16);
    env.push_back(new CollisionObject(std::shared_ptr<CollisionGeometry>(model), transforms[i]));
  }
}

octomap::OcTree* generateOcTree()
{
  octomap::OcTree* tree = new octomap::OcTree(0.1);

  // insert some measurements of occupied cells
  for(int x = -20; x < 20; x++)
  {
    for(int y = -20; y < 20; y++)
    {
      for(int z = -20; z < 20; z++)
      {
        tree->updateNode(octomap::point3d(x * 0.05, y * 0.05, z * 0.05), true);
      }
    }
  }

  // insert some measurements of free cells
  for(int x = -30; x < 30; x++)
  {
    for(int y = -30; y < 30; y++)
    {
      for(int z = -30; z < 30; z++)
      {
        tree->updateNode(octomap::point3d(x*0.02 -1.0, y*0.02-1.0, z*0.02-1.0), false);
      }
    }
  }

  return tree;
}

void generateBoxesFromOctomap(std::vector<CollisionObject*>& boxes, OcTree& tree)
{
  std::vector<std::array<FCL_REAL, 6> > boxes_ = tree.toBoxes();

  for(std::size_t i = 0; i < boxes_.size(); ++i)
  {
    FCL_REAL x = boxes_[i][0];
    FCL_REAL y = boxes_[i][1];
    FCL_REAL z = boxes_[i][2];
    FCL_REAL size = boxes_[i][3];
    FCL_REAL cost = boxes_[i][4];
    FCL_REAL threshold = boxes_[i][5];

    Box* box = new Box(size, size, size);
    box->cost_density = cost;
    box->threshold_occupied = threshold;
    CollisionObject* obj = new CollisionObject(std::shared_ptr<CollisionGeometry>(box), Transform3f(Vec3f(x, y, z)));
    boxes.push_back(obj);
  }

  std::cout << "boxes size: " << boxes.size() << std::endl;

}
