/*
 * OctoMap - An Efficient Probabilistic 3D Mapping Framework Based on Octrees
 * http://octomap.github.com/
 *
 * Copyright (c) 2009-2013, K.M. Wurm and A. Hornung, University of Freiburg
 * All rights reserved.
 * License: New BSD
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Freiburg nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Modified by juadhv. Example of 2D planning of a rigid body base on
 * RigiBodyPlanning3D.cpp (OMPL example)
 */

#include <octomap/octomap.h>
#include <octomap/OcTree.h>

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/config.h>
#include <iostream>

using namespace std;
using namespace octomap;

namespace ob = ompl::base;
namespace og = ompl::geometric;

bool isStateValid(const ob::State *state)
{
	// cast the abstract state type to the type we expect
	const ob::SE2StateSpace::StateType *se2state = state->as<ob::SE2StateSpace::StateType>();

	// extract the first component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = se2state->as<ob::RealVectorStateSpace::StateType>(0);

	// extract the second component of the state and cast it to what we expect
	const ob::SO2StateSpace::StateType *rot = se2state->as<ob::SO2StateSpace::StateType>(1);

//	std::cout << "posX: " << pos->values[0] << std::endl;
//	std::cout << "posY: " << pos->values[1] << std::endl;
//	std::cout << "goal orien: " << rot->value << std::endl;

	return pos->values[0] < 0.6;
}

void planWithSimpleSetup(void)
{
	// construct the state space we are planning in
	ob::StateSpacePtr space(new ob::SE2StateSpace());

	// set the bounds for the R^2 part of SE(2)
	ob::RealVectorBounds bounds(2);
	bounds.setLow(-1);
	bounds.setHigh(1);

	space->as<ob::SE2StateSpace>()->setBounds(bounds);

	// define a simple setup class
	og::SimpleSetup ss(space);

	// set state validity checking for this space
	ss.setStateValidityChecker(std::bind(&isStateValid, std::placeholders::_1));

	// create a random start state
	ob::ScopedState<> start(space);
	start.random();

	// ... or set specific values
	// cast the abstract state type to the type we expect
	const ob::SE2StateSpace::StateType *se2state = start->as<ob::SE2StateSpace::StateType>();
	// extract the first component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = se2state->as<ob::RealVectorStateSpace::StateType>(0);
	pos->values[0] = 0.5;//start.setX(.5);

	// create a random goal state
	ob::ScopedState<> goal(space);
	goal.random();

	// ... or set specific values
	// cast the abstract state type to the type we expect
	se2state = goal->as<ob::SE2StateSpace::StateType>();
	// extract the first component of the state and cast it to what we expect
	pos = se2state->as<ob::RealVectorStateSpace::StateType>(0);
	pos->values[0] = 0.5;//goal().setX(-.5)

	// set the start and goal states
	ss.setStartAndGoalStates(start, goal);

	// this call is optional, but we put it in to get more output information
	ss.setup();
	//ss.print();

	// attempt to solve the problem within one second of planning time
	ob::PlannerStatus solved = ss.solve(1.0);

	if (solved)
	{
		std::cout << "Found solution:" << std::endl;
		// print the path to screen
		ss.simplifySolution();
		ss.getSolutionPath().print(std::cout);
	}
	else
		std::cout << "No solution found" << std::endl;
}

void planTheHardWay(void)
{
	// construct the state space we are planning in
	ob::StateSpacePtr space(new ob::SE2StateSpace());

	// set the bounds for the R^2 part of SE(2)
	ob::RealVectorBounds bounds(2);
	bounds.setLow(-1);
	bounds.setHigh(1);

	space->as<ob::SE2StateSpace>()->setBounds(bounds);

	// construct an instance of  space information from this state space
	ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

	// set state validity checking for this space
	si->setStateValidityChecker(std::bind(&isStateValid, std::placeholders::_1));

	// create a random start state
	ob::ScopedState<> start(space);
	start.random();

	// create a random goal state
	ob::ScopedState<> goal(space);
	goal.random();

	// create a problem instance
	ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

	// set the start and goal states
	pdef->setStartAndGoalStates(start, goal);

	// create a planner for the defined space
	ob::PlannerPtr planner(new og::RRTConnect(si));

	// set the problem we are trying to solve for the planner
	planner->setProblemDefinition(pdef);

	// perform setup steps for the planner
	planner->setup();

	// print the settings for this space
	si->printSettings(std::cout);

	// print the problem settings
	pdef->print(std::cout);

	// attempt to solve the problem within one second of planning time
	ob::PlannerStatus solved = planner->solve(1.0);

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		ob::PathPtr path = pdef->getSolutionPath();
		std::cout << "Found solution:" << std::endl;

		// print the path to screen
		path->print(std::cout);
	}
	else
		std::cout << "No solution found" << std::endl;
}

int main(int, char **)
{
	std::cout << "OMPL version: " << OMPL_VERSION << std::endl;

	std::cout << std::endl << "*** RigidBodyPlanning2D (C++) with simple setup: ***" << std::endl;
	planWithSimpleSetup();

	std::cout << std::endl << "*** RigidBodyPlanning2D (C++) without simple setup: ***" << std::endl;
	planTheHardWay();

	return 0;
}
