#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import pylab

import select
import socket
import sys

import rospy
from cola2_lib import NED
from visualization_msgs.msg import Marker

# Interactive plot
plt.ion()

# Vehicle database
colors = dict()
colors['myellow'] = ('g', 1.0)
colors['gcv'] = ('g', 0.7)
colors['mblack'] = ('m', 1.0)
colors['ssv'] = ('m', 0.7)
colors['mred'] = ('r', 1.0)
colors['sparus'] = ('r', 1.0)
colors['cvl'] = ('k', 0.7)
colors['G500'] = ('y', 1.0)
colors['G500gps'] = ('r', 0.7)
colors['lsv'] = ('y', 0.7)
colors['CAT'] = ('k', 1.0)
colors['cvr'] = ('r', 0.7)
colors['gateway'] = ('k', 0.7)
colors['VORTEX'] = ('r', 1.0)
minx = -50
maxx = +40
miny = -10
maxy = +60

#41.772805, 3.032595

olat = 41.772805
olon = 3.032595


# Converts degreeminute format (DDMM.MM) to degree (DD.DD)
def degmin2deg(val):
    '''
    Converts degreeminute format (DDMM.MM) to degree (DD.DD)
    '''
    sign = val / abs(val)
    tmp = str(abs(val))
    tmp = tmp.split('.')
    deg = float(tmp[0][:-2])
    min = float(tmp[0][-2:] + '.' + tmp[1])
    return sign * (deg + min / 60.0)


# First map plot
def prepare_empty_map():
    '''
    Just an empty map
    '''
    fig = plt.figure()
    ax = fig.add_axes((0, 0, 1, 1))
    ax.plot([minx, maxx, maxx, minx, minx],
            [miny, miny, maxy, maxy, miny])
    ax.axis((minx, maxx, miny, maxy))
    ax.axis('equal')
    pylab.draw()
    return fig, ax


def prepare_map(fname, ned):
    '''
    Do the first plot with the map.
    '''
    # Read the map data obtained from Open Street Maps export tool
    fh = file(fname, 'r')
    lines = fh.readlines()
    fh.close()

    # Process all data in the map
    points = dict()
    shapes = list()
    for line in lines:

        # GPS coordinates
        if line.startswith(' <node'):
            lat = float(line.split('lat')[1].split('"')[1])
            lon = float(line.split('lon')[1].split('"')[1])
            idx = int(line.split('id')[1].split('"')[1])
            temp = ned.geodetic2ned([lat, lon, 0.0])
            lat = temp[0]
            lon = temp[1]
            points[idx] = (lat, lon)

        # Bounding limits
        elif line.startswith(' <bounds'):
            NED.NED(olat, olon, 0.0)
            minlat = float(line.split('minlat')[1].split('"')[1])
            maxlat = float(line.split('maxlat')[1].split('"')[1])
            minlon = float(line.split('minlon')[1].split('"')[1])
            maxlon = float(line.split('maxlon')[1].split('"')[1])
            temp = ned.geodetic2ned([minlat, minlon, 0.0])
            minlat = temp[0]
            minlon = temp[1]
            temp = ned.geodetic2ned([maxlat, maxlon, 0.0])
            maxlat = temp[0]
            maxlon = temp[1]

        # Shapes
        elif line.startswith(' <way'):
            shape = list()
        elif line.startswith(' </way'):
            shapes.append(shape)
        elif line.startswith('  <nd'):
            ref = int(line.split('ref')[1].split('"')[1])
            shape.append(points[ref])
        elif line.startswith('  <tag'):
            temp = line.split('v="')[1].split('"/>')[0]
            if temp == 'coastline':
                print('coastline')
                for p in shape:
                    print("{:.3f}, {:.3f}".format(p[0], p[1]))

    # Print statistics data
    print("points: {:d}".format(len(points)))
    print("shapes: {:d}".format(len(shapes)))

    # Start a figure with no axis
    fig = plt.figure()
    ax = fig.add_axes((0, 0, 1, 1))

    # Plot the map
    for shape in shapes:
        if len(shape) > 1:
            lats = list()
            lons = list()
            for i in range(len(shape)):
                lats.append(shape[i][0])
                lons.append(shape[i][1])

            ax.plot(lons, lats)
            if lats[0] == lats[-1] and lons[0] == lons[-1]:
                ax.fill(lons, lats, alpha=0.2)
        else:
            ax.plot(shape[0][0], shape[0][1], '*')

    # Drawing limits
    ax.axis('off')
    ax.axis('equal')
    print("lons {:.1f},{:.1f},{:.1f}".format(minlon, maxlon, maxlon - minlon))
    print("lats {:.1f},{:.1f},{:.1f}".format(minlat, maxlat, maxlat - minlat))

    # Zoom a bit
    ax.axis((minlon, maxlon, minlat, maxlat))

    # Save figure and draw
    name = "{}{}".format(fname[:-3], "svg")
    fig.savefig(name)
    pylab.draw()

    return fig, ax


def plot_position(ax, lon, lat, dyaw, color):
    '''
    Plot arrow in correct orientation or a circle.
    '''
    # Has orientation
    if color[1] == 1.0:
        pt = .25 * color[1] * np.array([[0.0, 3.0, 0.0, -3.0, 0.0],
                                        [0.0, -4.0, 10.0, -4.0, 0.0]])
        cs = np.cos(np.deg2rad(-dyaw))
        sn = np.sin(np.deg2rad(-dyaw))
        rot = np.array([[cs, -sn], [sn, cs]])
        pp = np.dot(rot, pt)

    # No orientation
    else:
        a = np.arange(0, 2*np.pi, 0.4)
        pp = .9 * np.array([np.cos(a), np.sin(a)])

    # Translation and plot
    pp += np.array([[lon], [lat]])
    return ax.fill(pp[0], pp[1], color[0], alpha=0.5)


class OnlineMissionView(object):
    '''
    Shows vehicles and mission.
    '''
    def __init__(self, fname):
        '''
        Init everything.
        '''
        # Init plot
        self.ned = NED.NED(olat, olon, 0.0)
        if fname is None:
            self.fig, self.ax = prepare_empty_map()
        else:
            self.fig, self.ax = prepare_map(fname, self.ned)

        # UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(0.005)
        self.sock.bind(('', 2805))

        # Init where to save data
        self.udpauv = dict()
        self.auvyaws = dict()
        self.noudpauv = list()
        self.auvlines = dict()
        self.auvpoint = dict()

        # Subscribers
        self.markers = dict()
        self.plotmarkers = dict()
        self.sub_olines = rospy.Subscriber('/wall_following/olines',
                                           Marker,
                                           callback=self.cbk_olines,
                                           queue_size=2)
        self.sub_clines = rospy.Subscriber('/wall_following/clines',
                                           Marker,
                                           callback=self.cbk_clines,
                                           queue_size=2)
        self.sub_section = rospy.Subscriber('/wall_following/onlinesection',
                                            Marker, callback=self.cbk_section,
                                            queue_size=2)
        self.sub_mission = rospy.Subscriber('/wall_following/onlinemission',
                                            Marker,
                                            callback=self.cbk_mission,
                                            queue_size=2)
        self.sub_points = rospy.Subscriber('/wall_following/points',
                                           Marker,
                                           callback=self.cbk_points,
                                           queue_size=2)
        self.sub_data = rospy.Subscriber('/xmlmission',
                                         Marker,
                                         callback=self.cbk_mission,
                                         queue_size=2)

    def cbk_olines(self, msg):
        self.process_markers(msg)

    def cbk_clines(self, msg):
        self.process_markers(msg)

    def cbk_section(self, msg):
        self.process_markers(msg)

    def cbk_mission(self, msg):
        self.process_markers(msg)

    def cbk_points(self, msg):
        self.process_markers(msg)

    def process_markers(self, msg):
        if msg.ns not in self.markers.keys():
            print("new marker {}".format(msg.ns))
        self.markers[msg.ns] = msg

    def plot_markers(self):
        for name in self.markers.keys():
            msg = self.markers[name]

            # Retrieve all points
            l = len(msg.points)
            n = np.zeros(l)
            e = np.zeros(l)
            for i in range(l):
                n[i] = msg.points[i].x
                e[i] = msg.points[i].y
            col = msg.color
            c = (col.r, col.g, col.b, col.a)

            # Points
            if msg.type == 8:
                self.plotmarkers[name] = self.ax.plot(e, n, 'k.')

            # Line strip
            if msg.type == 4:
                self.plotmarkers[name] = self.ax.plot(e, n, color=c,
                                                      linewidth=2)

            # Line list
            elif msg.type == 5:
                self.plotmarkers[name] = list()
                for i in range(l/2):
                    x = [n[2*i], n[2*i+1]]
                    y = [e[2*i], e[2*i+1]]
                    line = self.ax.plot(y, x, color=c, linewidth=3)
                    self.plotmarkers[name].append(line[0])

    def read_udp(self):
        '''
        Reads robots position from UDP broadcast
        '''
        # Read if available
        rlist, dummyw, dummyx = select.select([self.sock], [], [], 0.05)
        while rlist:

            try:
                # Read data from UDP
                data = self.sock.recv(80)
                data = data.split(',')
                if not data[0] == '$PISE':
                    continue
                name = data[1]
                lat = float(data[2])
                lon = float(data[3])
                dyaw = float(data[8])

                # Project into NED
                temp = self.ned.geodetic2ned([lat, lon, 0.0])
                lat = temp[0]
                lon = temp[1]

                # New vehicle
                if name not in self.auvpoint.keys():
                    print("new vehicle {}".format(name))

                # Check no color defined (new vehicles, no plot)
                if name not in colors.keys():
                    if name not in self.noudpauv:
                        print("{} not defined".format(name))
                        self.noudpauv.append(name)
                    continue

                # Start the list of points
                if name not in self.auvpoint.keys():
                    self.auvpoint[name] = [[lon], [lat]]

                # Add last point
                self.auvpoint[name][0].append(lon)
                self.auvpoint[name][1].append(lat)
                self.auvyaws[name] = dyaw

                # Remove too much points
                while len(self.auvpoint[name][0]) > 200:
                    self.auvpoint[name][0].pop(0)
                    self.auvpoint[name][1].pop(0)

                # Next loop
                rlist, dummyw, dummyx = select.select([self.sock], [], [], 0.1)
            except:
                pass

    def iterate(self):
        '''
        Plots everything.
        '''

        # Remove all plots
        for name in self.udpauv.keys():
            self.udpauv[name][0].remove()
        for name in self.auvlines.keys():
            self.auvlines[name][0].remove()
        for name in self.plotmarkers.keys():
            for plot in self.plotmarkers[name]:
                plot.remove()

        # Update UDP data
        self.read_udp()

        # Plot mission
        self.plot_markers()

        # Plot vehicles
        for name in self.auvpoint.keys():

            # Vehicle tail
            if colors[name][1] == 1.0:
                line = self.ax.plot(self.auvpoint[name][0],
                                    self.auvpoint[name][1],
                                    '-' + colors[name][0])
            else:
                line = self.ax.plot(self.auvpoint[name][0],
                                    self.auvpoint[name][1],
                                    '--' + colors[name][0])

            # Vehicle position
            lon = self.auvpoint[name][0][-1]
            lat = self.auvpoint[name][1][-1]
            dyaw = self.auvyaws[name]
            arrow = plot_position(self.ax, lon, lat, dyaw, colors[name])

            # Save plots to be able to delete them
            self.auvlines[name] = line
            self.udpauv[name] = arrow

        # Draw everythin
        self.ax.axis((minx, maxx, miny, maxy))
        pylab.draw()

# ============================================================================
if __name__ == '__main__':
    # Init node
    rospy.init_node('plot_mission')

    # Check input
    fname = None
    if len(sys.argv) > 1:
        fname = sys.argv[1]
    else:
        print("You could load a map with:\n    {} mapfile".format(sys.argv[0]))

    # Everything else
    node = OnlineMissionView(fname)
    r = rospy.Rate(0.5)
    while not rospy.is_shutdown():
        node.iterate()
        r.sleep()
