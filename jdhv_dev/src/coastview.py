#!/usr/bin/env python

import rospy

from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker, MarkerArray


class CoastView(object):
    '''
    Class to view Porto Pim coastline.
    '''

    def __init__(self):
        '''
        Constructor coastline.
        '''
        # Get file
        fname = "/home/juandhv/catkin_ws/src/jdhv_robotics/jdhv_dev/config/map.txt" #rospy.get_param('/shorefile')
        

        fh = file(fname)
        line = fh.readline()
        num = 0
        self.mkrs = MarkerArray()
        mkr = None
        while line:
            if line.startswith('coastline'):
                if mkr is not None:
                    self.mkrs.markers.append(mkr)
                mkr = Marker()
                mkr.header.frame_id = '/world'
                mkr.ns = 'coastline{:d}'.format(num)
                num += 1
                mkr.id = 0
                mkr.type = mkr.LINE_STRIP
                mkr.action = mkr.ADD
                mkr.pose.position.x = 0
                mkr.pose.position.y = 0
                mkr.pose.position.z = 0
                mkr.pose.orientation.x = 0
                mkr.pose.orientation.y = 0
                mkr.pose.orientation.z = 0
                mkr.pose.orientation.w = 1
                mkr.scale.x = 1
                mkr.color.r = 0.8
                mkr.color.g = 0.0
                mkr.color.b = 0.0
                mkr.color.a = 1.0
            else:
                data = line.split(', ')
                pt = Point()
                pt.x = float(data[0])
                pt.y = float(data[1])
                pt.z = 0.0
                mkr.points.append(pt)

            # Next loop
            line = fh.readline()

        # Copy the last opened one
        self.mkrs.markers.append(mkr)
        print('Coastline markers total {:d}'.format(len(self.mkrs.markers)))

        self.pub = rospy.Publisher("/shoreline", MarkerArray, latch=True, queue_size=2)
        self.pub.publish(self.mkrs)
        self.tim = rospy.Timer(rospy.Duration(5.0), self.callback_timer)

    def callback_timer(self, event):
        '''
        Continue publishing.
        '''
        self.pub.publish(self.mkrs)


if __name__ == '__main__':
    rospy.init_node('shoreline')
    node = CoastView()
    rospy.spin()
