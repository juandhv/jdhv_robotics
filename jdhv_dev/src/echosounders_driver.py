#!/usr/bin/env python
"""
Created on May 29, 2014

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Driver for getting information of echosounders.
"""

# ROS imports
import roslib; roslib.load_manifest('jdhv_mp')
import rospy

import serial
import numpy as np

from sensor_msgs.msg import Range

def dec2chr(dec_num):
    return chr(dec_num)

def hex2chr(hex_num):
    return chr(int(hex_num, 16))

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "%X" % n

class EchosounderDriver():
    """
    Driver to get information from echosounders
    """

    def __init__(self):
        """
        Constructor
        """

        #=======================================================================
        # Driver parameters
        #=======================================================================
        self.range_ = rospy.get_param('echosounders_driver/range')
        self.auto_range_ = rospy.get_param('echosounders_driver/auto_range')
        self.gain_ = rospy.get_param('echosounders_driver/gain')
        self.auto_gain_ = rospy.get_param('echosounders_driver/auto_gain')
        self.pulse_length_ = rospy.get_param('echosounders_driver/pulse_length')
        self.min_range_ = rospy.get_param('echosounders_driver/min_range')
        self.data_points_ = rospy.get_param('echosounders_driver/data_points')
        self.profile_ = rospy.get_param('echosounders_driver/profile')
        self.switch_delay_ = rospy.get_param('echosounders_driver/switch_delay')
        self.echosounders_order_ = rospy.get_param('echosounders_driver/echosounders_order')
        self.serial_port_ = rospy.get_param('echosounders_driver/serial_port')

        self.serialPortEchos_ = serial.Serial(self.serial_port_, 115200, timeout=1)

        #=======================================================================
        # Publishers
        #=======================================================================
        self.echosounders_e11_pub_ = rospy.Publisher("/echosounders/e11", Range)
        self.echosounders_e12_pub_ = rospy.Publisher("/echosounders/e12", Range)
        self.echosounders_e13_pub_ = rospy.Publisher("/echosounders/e13", Range)
        self.echosounders_e14_pub_ = rospy.Publisher("/echosounders/e14", Range)
        self.echosounders_e15_pub_ = rospy.Publisher("/echosounders/e15", Range)

        #=======================================================================
        # Register handler to be called when rospy process begins shutdown.
        #=======================================================================
        rospy.on_shutdown(self.shutdownHook)

        return

    def configEchosounders(self, echos_num, range):

        #=======================================================================
        # Configuration command
        #=======================================================================
        command_msg = ""
        command_msg = command_msg + hex2chr('0x43')#header
        command_msg = command_msg + dec2chr(range)#range
        command_msg = command_msg + dec2chr(self.auto_range_)#auto-range
        command_msg = command_msg + dec2chr(self.gain_)#gain
        command_msg = command_msg + dec2chr(self.auto_gain_)#auto-gain
        command_msg = command_msg + dec2chr(self.pulse_length_)#pulse length
        command_msg = command_msg + dec2chr(self.min_range_)#min range
        command_msg = command_msg + dec2chr(self.data_points_)#data points
        command_msg = command_msg + dec2chr(self.profile_)#profile
        command_msg = command_msg + dec2chr(self.switch_delay_)#swicth delay

        command_msg = command_msg + dec2chr(echos_num)#hex2chr('0x11')#echosounder

        command_msg = command_msg + hex2chr('0xFF')#end of message

        self.serialPortEchos_.write(command_msg)

        r = rospy.Rate(5)
        while not rospy.is_shutdown():
            line = self.serialPortEchos_.readline()   # read a '\n' terminated line
            if len(line)>0:
                if "CONFIGURED" in line:
                    rospy.loginfo('%s: %s', rospy.get_name(), line[0:-1])
                    break
            r.sleep()

    def run(self):

        # ++++ start +++++
        command_msg = ""
        command_msg = command_msg + hex2chr('0x42')#header
        command_msg = command_msg + hex2chr('0x46')#mode
        command_msg = command_msg + hex2chr('0x11')#echo_num
        command_msg = command_msg + hex2chr('0x12')#echo_num
        command_msg = command_msg + hex2chr('0x13')#echo_num
        command_msg = command_msg + hex2chr('0x14')#echo_num
        command_msg = command_msg + hex2chr('0x14')#echo_num
        command_msg = command_msg + hex2chr('0xff')#end of message

        self.serialPortEchos_.write(command_msg)
        
        # ++++ start +++++

        r = rospy.Rate(5)
        while not rospy.is_shutdown():

            try:
                line = self.serialPortEchos_.readline()   # read a '\n' terminated line
            except:
                return False

            if len(line)>0:
                if "STARTED" in line:
                    rospy.loginfo('%s: STARTED msg received', rospy.get_name())
                elif "Header ID echosounder852 error" in line :
                    rospy.logwarn('%s: Header ID echosounder852 error', rospy.get_name())
                elif "Data header echosounder852 error" in line :
                    rospy.logwarn('%s: Data header echosounder852 error', rospy.get_name())
                elif "Range echosounder852 error" in line:
                    rospy.logwarn('%s: Range echosounder852 error', rospy.get_name())
                elif "BAD COMMAND RS232" in line:
                    rospy.logwarn('%s: BAD COMMAND RS232', rospy.get_name())
                elif "not found" in line:
                    rospy.logwarn('%s: '+line)
                else:
                    #print line
                    echosounder_data = line.split(',')
                    echosounder_max_range = float(echosounder_data[1])
                    measurement = float(echosounder_data[3])/100

                    range_msg = Range()
                    range_msg.header.stamp = rospy.Time.now()
                    #range_msg.ULTRASOUND = 1
                    #range_msg.INFRARED = 0
                    range_msg.field_of_view = np.radians(0.5)
                    range_msg.min_range = 0.1
                    range_msg.max_range = echosounder_max_range
                    
                    if measurement == 0.0:
                        range_msg.range = range_msg.max_range 
                    else:
                        range_msg.range = measurement

                    if echosounder_data[0] == "11":
                        range_msg.header.frame_id = "/echosounder11"
                        self.echosounders_e11_pub_.publish(range_msg)
                    elif echosounder_data[0] == "12":
                        range_msg.header.frame_id = "/echosounder12"
                        self.echosounders_e12_pub_.publish(range_msg)
                    elif echosounder_data[0] == "13":
                        range_msg.header.frame_id = "/echosounder13"
                        self.echosounders_e13_pub_.publish(range_msg)
                    elif echosounder_data[0] == "14":
                        range_msg.header.frame_id = "/echosounder14"
                        self.echosounders_e14_pub_.publish(range_msg)
                    elif echosounder_data[0] == "15":
                        range_msg.header.frame_id = "/echosounder15"
                        self.echosounders_e15_pub_.publish(range_msg)

        return

    def shutdownHook(self):
        rospy.loginfo('%s: shutting down driver...', rospy.get_name())

        # ++++ stop +++++
        command_msg = ""
        command_msg = command_msg + hex2chr('0x45')#header
        command_msg = command_msg + hex2chr('0xff')#end of message

        self.serialPortEchos_.write(command_msg)
        # ++++ stop +++++

        r = rospy.Rate(5)
        while not rospy.is_shutdown():
            line = self.serialPortEchos_.readline()   # read a '\n' terminated line
            if len(line)>0:
                if "STOPPED" in line:
                    rospy.loginfo('%s: STOPPED msg received', rospy.get_name())
                    break
            r.sleep()

        self.serialPortEchos_.close()
        return

if __name__ == "__main__":

    # Initialize ROS node
    rospy.init_node('echosounders_driver', log_level=rospy.INFO) #log_level=rospy.INFO, log_level=rospy.DEBUG
    rospy.loginfo('%s: starting driver...', rospy.get_name())

    # Start node
    echosounder_driver = EchosounderDriver()
    echosounder_driver.configEchosounders(17, 5)
    echosounder_driver.configEchosounders(18, 10)
    echosounder_driver.configEchosounders(19, 30)
    echosounder_driver.configEchosounders(20, 40)
    echosounder_driver.configEchosounders(21, 40)
    echosounder_driver.run()
 
    r = rospy.Rate(5)
    while not rospy.is_shutdown():
        r.sleep()
