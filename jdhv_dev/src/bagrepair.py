#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Repairs bags from common errors.

visualization_msgs::Marker different md5sum in jade
visualization_msgs::MarkerArray same as previous"""

import rosbag
import sys

from visualization_msgs.msg import Marker, MarkerArray


def convert_marker(msg):
    """Converts to a new Marker message."""
    new = Marker()
    new.action = msg.action
    new.color = msg.color
    new.colors = msg.colors
    new.frame_locked = msg.frame_locked
    new.header = msg.header
    new.id = msg.id
    new.lifetime = msg.lifetime
    new.mesh_resource = msg.mesh_resource
    new.mesh_use_embedded_materials = msg.mesh_use_embedded_materials
    new.ns = msg.ns
    new.points = msg.points
    new.pose = msg.pose
    new.scale = msg.scale
    new.text = msg.text
    new.type = msg.type
    return new


def convert_markerarray(msg):
    """Converts to a new MarkerArray message."""
    new = MarkerArray()
    for m in msg.markers:
        n = convert_marker(m)
        new.markers.append(n)
    return new


if __name__ == '__main__':
    # Check arguments
    if len(sys.argv) < 2:
        print('usage:\n  {:s} bagname'.format(sys.argv[0]))
        exit(1)

    # Process the bag
    assert(sys.argv[1][-4:] == '.bag')
    inbag = rosbag.Bag(sys.argv[1], 'r')
    fname = '{:s}-repared.bag'.format(sys.argv[1][:-4])
    outbag = rosbag.Bag(fname, 'w')
    for topic, msg, t in inbag.read_messages():
        typ = str(type(msg))
        if 'MarkerArray' in typ:
            new = convert_markerarray(msg)
        elif 'Marker' in typ:
            new = convert_marker(msg)
        else:
            new = msg
        outbag.write(topic, new, t)
    inbag.close()
    outbag.close()
