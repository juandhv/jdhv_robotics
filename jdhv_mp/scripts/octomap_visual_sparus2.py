#!/usr/bin/env python
"""
Created on Jun 17, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
"""

import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from std_msgs.msg import String

from sensor_msgs.msg import PointCloud2, PointField, LaserScan
from auv_msgs.msg import NavSts

import tf

from math import radians



class StartOctomapVisual(object):
    
    def __init__(self):
    
        return
    """
    Start octomap visualization by publishing an occupied point
    """
    def startOctomapVisual(self):
        rospy.loginfo("+++ Activating octomap visualization +++")
            
        pub_octomap = rospy.Publisher('revolving_profiler_scan', LaserScan)
            
        tf_broadcaster = tf.TransformBroadcaster()
        msg = LaserScan()
        # timestamp in the header is the acquisition time of 
        # the first ray in the scan.
        #
        # in frame frame_id, angles are measured around 
        # the positive Z axis (counterclockwise, if Z is up)
        # with zero angle being forward along the x axis
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = 'seaking_profiler'
                                                     
        msg.angle_min = radians(-2.0)
        msg.angle_max = radians(2.0)
        msg.angle_increment = radians(1.0)
         
        msg.time_increment = 1.0/14  
        msg.scan_time = 0.2
                             
        msg.range_min = 0       # minimum range value [m]
        msg.range_max = 0.5     # maximum range value [m]                
                             
        msg.ranges = [0.1]
        msg.intensities = []
        
        #=======================================================================
        # Subscribers 
        #=======================================================================
        #Navigation data (feedback)
        self.nav_sts_available = False
        sub_nav_sts = rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.navStsSubCallback)
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        print "Waiting for nav sts..."
        r = rospy.Rate(5)
        while not self.nav_sts_available:
            r.sleep()
            
        rospy.sleep(2.0)
            
        msg.header.stamp = rospy.Time.now()
        tf_broadcaster.sendTransform((0, 0, 0),                   
                                     tf.transformations.quaternion_from_euler(0, 0, 0),
                                     msg.header.stamp,
                                     'seaking_profiler',
                                     'sparus2')
        pub_octomap.publish(msg)
        return pub_octomap
    
    """
    Callback to receive navSts message
    """
    def navStsSubCallback(self,nav_sts_msg):
            
        self.nav_sts_data = nav_sts_msg
        if self.nav_sts_available == False:
            self.nav_sts_available = True
        
        return

if __name__ == '__main__':
    try:
        rospy.init_node('octomap_visual')
        visual_oct = StartOctomapVisual()
        visual_oct.startOctomapVisual()
    except rospy.ROSInterruptException:
        pass