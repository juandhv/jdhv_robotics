#!/usr/bin/env python
"""
Created on May 15, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
"""

# Debug control variables
DEBUG_OCTOMAP_VIS = False

# ROS imports
import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String
from std_srvs.srv import Empty
from visualization_msgs.msg import Marker
import tf

from auv_msgs.msg import NavSts, WorldWaypointReq, BodyVelocityReq, GoalDescriptor

# cola2 and related imports
from cola2_lib import cola2_ros_lib
from cola2_msg.msg import WorldWaypointReqGoal, WorldWaypointReqAction

# Octomap visualization
if DEBUG_OCTOMAP_VIS:
    import OctomapVisual

# Standard Python imports
from math import *
import numpy as np
import actionlib

STATE_ZERO = 0
STATE_INIT = 1
STATE_TRAJ_INIT_ORI = 2
STATE_TRAJ_POS = 3
STATE_TRAJ_FINAL_ORI = 4

virtFactorPos = 5
virtFactorOri = 1 #2

"""
Path follower controller.
"""
class PathFollowerController(object):

    """
    Constructor
        
    Setup of the initial parameters and create the necessary subscribers and publishers.
    First of all, the robot will descend to the specified depth, if provided, otherwise
    will consider 2 meters.
    """
    def __init__(self):
        
        rospy.loginfo("+++ Setting up controller params +++")
        
        #=======================================================================
        # Initial conditions
        #=======================================================================
        self.virt_targ_updater = -1
        
        #=======================================================================
        # Default waypoint tolerance values
        #=======================================================================
        self.wp_toler = {}
        self.wp_toler_pos = 1.0
        self.wp_toler_ori = radians(7.0)
        
        #=======================================================================
        # Waypoint message and its tolerances
        #=======================================================================
        self.wld_wp_req_msg = WorldWaypointReq()

        self.wld_wp_req_msg.position_tolerance.x = self.wp_toler_pos
        self.wld_wp_req_msg.position_tolerance.y = self.wp_toler_pos
        self.wld_wp_req_msg.position_tolerance.z = self.wp_toler_pos
                
        self.wld_wp_req_msg.orientation_tolerance.roll = self.wp_toler_ori
        self.wld_wp_req_msg.orientation_tolerance.pitch = self.wp_toler_ori
        self.wld_wp_req_msg.orientation_tolerance.yaw = self.wp_toler_ori
        
        #=======================================================================
        # Body velocity message
        #=======================================================================
        self.bdy_vel_req_msg = BodyVelocityReq()
        
        self.bdy_vel_req_msg.goal = GoalDescriptor()
        self.bdy_vel_req_msg.header.frame_id = "girona500"
        
        self.bdy_vel_req_msg.disable_axis.x = False# SURGE
        self.bdy_vel_req_msg.disable_axis.y =   False# SWAY
        self.bdy_vel_req_msg.disable_axis.z = False# HEAVE
        self.bdy_vel_req_msg.disable_axis.roll = True# ROLL
        self.bdy_vel_req_msg.disable_axis.pitch = True# PITCH
        self.bdy_vel_req_msg.disable_axis.yaw = False# YAW
        
        self.bdy_vel_req_msg.goal.priority = 30#GoalDescriptor.PRIORITY_AVOID_OBSTACLE
        self.bdy_vel_req_msg.goal.requester = rospy.get_name() + '_virtTargetControl'

        #=======================================================================
        # Action lib params
        #=======================================================================
        self.wp_act_lib_conf = cola2_ros_lib.Config()
        self.wp_act_lib_conf.trajectory_step = 0.25
        self.wp_act_lib_goal_id = 36000
        
        rospy.loginfo("+++ Creating action client +++")
        #self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('world_waypoint_req', WorldWaypointReqAction)
        self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('absolute_movement', WorldWaypointReqAction)
        rospy.loginfo("+++ Waiting for action server +++")
        self.wp_act_lib_action_client.wait_for_server()
        rospy.loginfo("+++ Ready to use action client +++")
        
        #=======================================================================
        # Subscribers 
        #=======================================================================
        #Navigation data (feedback)
        self.sub_nav_sts = rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.navStsSubCallback)
        self.nav_sts_available = False
        
        #=======================================================================
        # Publishers
        #=======================================================================
        self.pub_wld_wp_req = rospy.Publisher('/cola2_control/world_waypoint_req',WorldWaypointReq)
        self.pub_bdy_vel_req = rospy.Publisher("/cola2_control/body_velocity_req",BodyVelocityReq)
        
        self.pub_real_wp = rospy.Publisher("/mp_jdhv/real_wp", Marker)
        
        #=======================================================================
        # TF 
        #=======================================================================
        self.tfVirtTargBroadcaster = tf.TransformBroadcaster()
        self.tfVirtTargListener = tf.TransformListener()
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        print "Waiting for nav sts..."
        r = rospy.Rate(5)
        while not self.nav_sts_available:
            r.sleep()
        
        #=======================================================================
        # State Machine
        #=======================================================================
        rospy.loginfo("+++ Setting --> state to zero +++")
        self.curr_state = STATE_ZERO
        
        #=======================================================================
        # Register handler to be called when rospy process begins shutdown.
        #=======================================================================
        rospy.on_shutdown(self.shutdownHook)
        
        return
    
    def updateVirtualTarget(self):
        
        if abs(self.nav_sts_data.position.north - self.virtTargPos[0]) < self.wp_toler_pos and abs(self.nav_sts_data.position.east - self.virtTargPos[1]) < self.wp_toler_pos:
            self.traj['count'] = self.traj['count'] + 1
            
            if self.traj['count'] == len(self.traj['list']): 
                self.traj['count'] = 0

            WP = self.traj['list'][self.traj['count']]

            self.virtTargPos = np.array([WP['x'],WP['y'],WP['z']])# is the distance from E to P
            self.virtTargYaw = WP['yaw']
        
        self.pub_real_wp.publish(self.traj_marker)
        self.tfVirtTargBroadcaster.sendTransform((self.virtTargPos[0], self.virtTargPos[1], self.virtTargPos[2]),
                                                 tf.transformations.quaternion_from_euler(0, 0, self.virtTargYaw),
                                                 rospy.Time.now(),
                                                 'virtual_target',
                                                 'world')
        return
    
    def pubVelReq(self):
        
        #=======================================================================
        # Getting virtual target information (pos and ori)
        #=======================================================================
#         t = self.tfVirtTargListener.getLatestCommonTime("world","virtual_target")
#         (trans,rot) = self.tfVirtTargListener.lookupTransform("world","virtual_target", t)
#         roll, pitch, psi_F = tf.transformations.euler_from_quaternion(rot)
        p = np.ones((1,4))
        p[0,0:3] =  self.virtTargPos#np.array(trans)
        psi_F = self.virtTargYaw
        if psi_F<0.0:
            psi_F = psi_F + 2*pi
        
        #=======================================================================
        # Getting robot information (position and velocity)
        #=======================================================================
        x = np.array([self.nav_sts_data.position.north,self.nav_sts_data.position.east,self.nav_sts_data.position.depth,1])
        x_d = np.array([self.nav_sts_data.body_velocity.x,self.nav_sts_data.body_velocity.y,self.nav_sts_data.body_velocity.z])

        T =  np.eye(3)
#         T[:,3] = x
        T[0,0] = T[1,1] = cos(self.nav_sts_data.orientation.yaw)
        T[0,1] = -sin(self.nav_sts_data.orientation.yaw)
        T[1,0] = -T[0,1]
        x_d_ = np.dot(T,x_d)
        psi_E = atan2(x_d_[1],x_d_[0])
        if psi_E<0.0:
            psi_E = psi_E + 2*pi
        U = sqrt(pow(x_d_[1],2)+pow(x_d_[0],2))
        
        #=======================================================================
        # Robot position from virtual target frame
        #=======================================================================
        T =  np.eye(4)
        T[:,3] = p
        T[0,0] = T[1,1] = cos(psi_F)
        T[0,1] = -sin(psi_F)
        T[1,0] = -T[0,1]
        d = np.dot(np.linalg.inv(T),x)
        
        y1 = d[1] #distance of the robot from the target frame
        s1 = d[0] #distance of the robot from the target frame
        
        #=======================================================================
        # Yaw velocity, law of control
        #=======================================================================
        k1 = 0.2
        k_phi =1
        psi_a = radians(45.0)
        Cc = 0.0
        beta = psi_E - psi_F
        
        if beta > (pi):
            beta = beta - (2*pi)
        
        """ incomplete"""
        y1_d = U * sin(beta) 
        
        phi_y1 =  -psi_a*tanh(k_phi*y1)
        phi_y1_d = -psi_a*k_phi*pow(1/cosh(k_phi*y1),2)*y1_d 
        
        """ incomplete"""
        self.bdy_vel_req_msg.twist.angular.z = phi_y1_d - k1*(beta-phi_y1)
        
        #=======================================================================
        # Surge velocity, law of control
        #=======================================================================
        k2 = 0.15
        self.bdy_vel_req_msg.twist.linear.x = 0.3#U*cos(beta) + k2*s1
        self.bdy_vel_req_msg.twist.linear.y = 0.0
        self.bdy_vel_req_msg.twist.linear.z = 0.0
        
        print "T:",T
        print "p:",p
        print "x:",x
        print "d:",d
        print "y1:",y1
        print "s1:",s1
        print "x_d_:",x_d_
        print "phi_y1:",phi_y1
        print "psi_E:",psi_E
        print "psi_F:",psi_F
        print "beta:",beta
        print "U:",U
        print "self.bdy_vel_req_msg.twist.angular.z:",self.bdy_vel_req_msg.twist.angular.z
        print "self.bdy_vel_req_msg.twist.linear.x:",self.bdy_vel_req_msg.twist.linear.x
        print "*****"
  
        self.bdy_vel_req_msg.header.stamp = rospy.Time.now()      
        self.pub_bdy_vel_req.publish(self.bdy_vel_req_msg)
        
#         if self.bdy_vel_req_msg.disable_axis.x:    
#             self.bdy_vel_req_msg.twist.linear.x = 0.0
#         else:
#             self.bdy_vel_req_msg.twist.linear.x = 5
#             
# self.bdy_vel_req_msg.header.stamp = rospy.Time.now()
#         self.pub_bdy_vel_req.publish(self.bdy_vel_req_msg)
#         rospy.loginfo("Tau: %s", self.bdy_vel_req_msg.twist)
        return
    
    """
    Build a trajectory list
        
    List of Waypoints:
    Each waypoint will be specified as a list: [x,y,z,roll,pitch,yaw]
    """
    def buildTrajList(self):
        
        self.traj = {}
        self.traj['list'] = []
        
        #=======================================================================
        # Circular trajectory
        #=======================================================================
        x_robot_pos = self.nav_sts_data.position.north
        yRobotPos = self.nav_sts_data.position.east
        zRobotPos = self.nav_sts_data.position.depth
        self.traj['radius'] = 3.5
        self.traj['depth'] = 3.0
        
        stepTheta = 5.0
        for thetai in np.arange(0.0,360+stepTheta,stepTheta):
            xi = self.traj['radius']*cos(radians(thetai))
            yi = self.traj['radius']*sin(radians(thetai))
            self.traj['list'].append({'x' : xi, 'y' : yi, 'z' : self.traj['depth'], 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : 0.0})

        for i in range(len(self.traj['list'])-1):
            self.traj['list'][i]['yaw'] = atan2(self.traj['list'][i+1]['y']-self.traj['list'][i]['y'],self.traj['list'][i+1]['x']-self.traj['list'][i]['x'])
        self.traj['list'][i+1]['yaw'] = self.traj['list'][i]['yaw']
        
        self.traj['count'] = 0
        
        self.traj_marker = Marker()
        self.traj_marker.header.frame_id = "/world"
        self.traj_marker.header.stamp = rospy.Time.now()
        self.traj_marker.ns = 'real_points'
        self.traj_marker.id = 0
        self.traj_marker.type = Marker.SPHERE_LIST
        self.traj_marker.action = Marker.ADD
        self.traj_marker.pose.position.x = 0.0
        self.traj_marker.pose.position.y = 0.0
        self.traj_marker.pose.position.z = 0.0
        self.traj_marker.pose.orientation.x = 0.0
        self.traj_marker.pose.orientation.y = 0.0
        self.traj_marker.pose.orientation.z = 0.0
        self.traj_marker.pose.orientation.w = 1.0
        self.traj_marker.scale.x = 0.3
        self.traj_marker.scale.y = 0.3
        self.traj_marker.scale.z = 0.3
        self.traj_marker.color.a = 0.5
        self.traj_marker.color.r = 1.0
        self.traj_marker.color.g = 0.1
        self.traj_marker.color.b = 0.0
        
        for WP in self.traj['list']:
            self.traj_marker.points.append(Point(WP['x'], WP['y'], WP['z']))
            
        WP = self.traj['list'][0]
        self.virtTargPos = np.array([WP['x'],WP['y'],WP['z']])# is the distance from E to P
        self.virtTargYaw = WP['yaw']
        
        self.wp_toler_pos = 0.15
        pathFollContObj.moveTo(self.traj['radius'], 0.0, self.traj['depth'], pi/2)
        self.wp_toler_pos = 0.5
        return
    
    """
    Callback to receive navSts message
    """
    def navStsSubCallback(self,nav_sts_msg):
            
        self.nav_sts_data = nav_sts_msg
        if self.nav_sts_available == False:
            self.nav_sts_available = True
        
        return
    
    """
    wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    moveTo: move vehicle to waypoint using COLA2 API (Enric)
    wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
    """
    def moveTo(self, x, y, z, yaw=0.0):
        rospy.loginfo("    moveTo(x=%f, y=%f, z=%f, yaw=%f)", x, y, z, yaw)
        
        # Create a goal to send to the action server
        goal = WorldWaypointReqGoal()
        goal.goal.requester = rospy.get_name() + '_virtTargetControl'
        goal.goal.id = self.wp_act_lib_goal_id
        self.wp_act_lib_goal_id = self.wp_act_lib_goal_id + 1
        goal.goal.priority = 30
        
        goal.mode = ''
        goal.timeout = 180 # 3 minutes        
        
        goal.altitude_mode = False

        # Goal pose
        goal.position.north = x
        goal.position.east = y
        goal.position.depth = z

        goal.orientation.roll = 0
        goal.orientation.pitch = 0
        goal.orientation.yaw = yaw

        # Set movement type (X-Y-Z-Yaw)
        goal.disable_axis.x = False
        goal.disable_axis.y = False
        goal.disable_axis.z = False
        goal.disable_axis.roll = True
        goal.disable_axis.pitch = True
        goal.disable_axis.yaw = False

        # Set error tolerances
        goal.position_tolerance.x = self.wp_toler_pos
        goal.position_tolerance.y = self.wp_toler_pos
        goal.position_tolerance.z = self.wp_toler_pos
        goal.orientation_tolerance.roll = self.wp_toler_ori
        goal.orientation_tolerance.pitch = self.wp_toler_ori
        goal.orientation_tolerance.yaw = self.wp_toler_ori

        self.wp_act_lib_action_client.send_goal(goal)
        self.wp_act_lib_action_client.wait_for_result()
        result = self.wp_act_lib_action_client.get_result()
        
    """
    wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
    shutdownHook: gets called on shutdown and cancels all ongoing pilot goals (Enric)
    wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
    """    
    def shutdownHook(self):
        rospy.loginfo("+++ Canceling goals before shutdown +++")
        self.wp_act_lib_action_client.cancel_all_goals()
    
if __name__ == '__main__':
    try:
        rospy.init_node('virtual_target_navigator')

        #=======================================================================
        # Init: create navigator, go to initial position and create wps list
        #=======================================================================
        pathFollContObj= PathFollowerController()
        pathFollContObj.buildTrajList()

        r = rospy.Rate(20) 
        while not rospy.is_shutdown():
            pathFollContObj.updateVirtualTarget()
            pathFollContObj.pubVelReq()
            r.sleep()
            
    except rospy.ROSInterruptException:
        pass