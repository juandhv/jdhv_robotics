#!/usr/bin/env python
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import tf


if __name__ == '__main__':
    try:
        rospy.init_node('publish_mesh', log_level=rospy.INFO) #log_level=rospy.DEBUG
        rospy.loginfo("%s: publishing", rospy.get_name())
        
        topic = 'visualization_marker_array'
        publisher = rospy.Publisher(topic, Marker, queue_size = 2)
        
        markerArray = MarkerArray()
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
        
           # ... here I get the data I want to plot into a vector called trans
        
            marker = Marker()
            marker.header.frame_id = "/world"
           
            marker.ns = "my_namespace"
            marker.id = 0
            marker.type = marker.MESH_RESOURCE
            marker.action = marker.ADD
            marker.pose.position.x = -2.0
            marker.pose.position.y = 0.0
            marker.pose.position.z = 0
            quat = tf.transformations.quaternion_from_euler(-1.57, 0.0, 1.57)
            print quat
            marker.pose.orientation.x = quat[0]
            marker.pose.orientation.y = quat[1]
            marker.pose.orientation.z = quat[2]
            marker.pose.orientation.w = quat[3]
            marker.scale.x = 28
            marker.scale.y = 28
            marker.scale.z = 28
            marker.color.a = 0.4 # Don't forget to set the alpha!
            marker.color.r = 1.0;
            marker.color.g = 1.0;
            marker.color.b = 1.0;
            #only if using a MESH_RESOURCE marker type:
            #marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae"
            marker.mesh_resource = "package://jdhv_scenes/scenes/3D_ModelsMaps/Canyon.obj"
            marker.mesh_use_embedded_materials = True
            publisher.publish( marker )
            
            rospy.loginfo("%s: publishing", rospy.get_name())
            r.sleep()
        
        #rospy.spin()
            
    except rospy.ROSInterruptException:
        pass