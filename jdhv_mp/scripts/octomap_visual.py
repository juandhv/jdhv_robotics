#!/usr/bin/env python
"""
Created on Jun 17, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
"""

import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from std_msgs.msg import String

from sensor_msgs.msg import PointCloud2, PointField, LaserScan
import tf

from math import radians

"""
Start octomap visualization by publishing an occupied point
"""
def startOctomapVisual():
    rospy.loginfo("+++ Activating octomap visualization +++")
        
    pub_octomap = rospy.Publisher('revolving_profiler_scan', LaserScan)
        
    tf_broadcaster = tf.TransformBroadcaster()
    msg = LaserScan()
    # timestamp in the header is the acquisition time of 
    # the first ray in the scan.
    #
    # in frame frame_id, angles are measured around 
    # the positive Z axis (counterclockwise, if Z is up)
    # with zero angle being forward along the x axis
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = 'seaking_profiler'
                                                 
    msg.angle_min = radians(-2.0)
    msg.angle_max = radians(2.0)
    msg.angle_increment = radians(1.0)
     
    msg.time_increment = 1.0/14  
    msg.scan_time = 0.2
                         
    msg.range_min = 0       # minimum range value [m]
    msg.range_max = 0.5     # maximum range value [m]                
                         
    msg.ranges = [0.1]
    msg.intensities = []
        
    rospy.sleep(12.0)
        
    msg.header.stamp = rospy.Time.now()
    tf_broadcaster.sendTransform((0, 0, 0),                   
                                 tf.transformations.quaternion_from_euler(0, 0, 0),
                                 msg.header.stamp,
                                 'seaking_profiler',
                                 'girona500')
    pub_octomap.publish(msg)
    return pub_octomap

if __name__ == '__main__':
    try:
        rospy.init_node('octomap_visual')
        startOctomapVisual()
    except rospy.ROSInterruptException:
        pass