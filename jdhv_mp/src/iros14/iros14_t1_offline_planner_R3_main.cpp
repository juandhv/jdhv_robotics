/*
 * iros14_t1_offline_planner_R3_main.cpp
 *
 *  Created on: January 14, 2014
 *      Author: juandhv
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 *  Based on icra14_t1_offline_planner_R3_main.cpp
 */

#include "iros14_state_validity_checker.h"
#include "iros14_offline_planner_R3.h"

int main(int argc, char **argv)
{
	std::string planner_name;

	ros::init(argc, argv, "offline_planner_ompl_R3");
	ROS_INFO_STREAM("Starting offline_planner_ompl_R3 Node");

	//TestMap1
	std::vector<double> planning_bounds(6);
	planning_bounds[0] = -50.0;
	planning_bounds[1] = 50.0;
	planning_bounds[2] = -50.0;
	planning_bounds[3] = 50.0;
	planning_bounds[4] = 0.0;
	planning_bounds[5] = 12.0;

	std::vector<double> start_pos(3);
	start_pos[0] = -45.0;
	start_pos[1] = 0.0;
	start_pos[2] = 3.0;

	std::vector<double> goal_pos(3);
	goal_pos[0] = -22.0;
	goal_pos[1] = 20.0;
	goal_pos[2] = 3.0;

//	//TestMap2
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 20.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = 5.0;
//	start_pos[1] = -5.0;
//	start_pos[2] = 28.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 0.0;
//	goal_pos[1] = 38.0;
//	goal_pos[2] = 28.0;

//	//TestMap3
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 20.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -35.0;
//	start_pos[1] = -35.0;
//	start_pos[2] = 28.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 4.0;
//	goal_pos[1] = 4.0;
//	goal_pos[2] = 22.0;

//	//TestMap4
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 20.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -35.0;
//	start_pos[1] = -35.0;
//	start_pos[2] = 28.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 4.0;
//	goal_pos[1] = 4.0;
//	goal_pos[2] = 22.0;

//	//TestMap5
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 18.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -10.0;
//	start_pos[1] = -35.0;
//	start_pos[2] = 25.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 0.0;
//	goal_pos[1] = 35.0;
//	goal_pos[2] = 22.0;

	// Constructor
	//(RRT, RRTstar, RRTConnect, BallTreeRRTstar, LazyRRT, pRRT, TRRT), (EST), (BKPIECE1, KPIECE1, LBKPIECE1), (PDST), (PRM, LazyPRM, PRMstar, SPARS, SPARStwo), (SBL, pSBL)
	OmOfflinePlanner offPlanner(planning_bounds,start_pos,goal_pos);

	return 0;
}
