/*
 * iros14_offline_planner_R3.h
 *
 *  Created on: January 14, 2014
 *      Author: juandhv
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 *  Based on icra14_offline_planner_R3.h
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/BallTreeRRTstar.h>
#include <ompl/geometric/planners/rrt/LazyRRT.h>
#include <ompl/geometric/planners/rrt/pRRT.h>
#include <ompl/geometric/planners/rrt/TRRT.h>

#include <ompl/geometric/planners/est/EST.h>

#include <ompl/geometric/planners/kpiece/BKPIECE1.h>
#include <ompl/geometric/planners/kpiece/KPIECE1.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>

#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/geometric/planners/prm/LazyPRM.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/planners/prm/SPARS.h>
#include <ompl/geometric/planners/prm/SPARStwo.h>

#include <ompl/geometric/planners/sbl/SBL.h>
#include <ompl/geometric/planners/sbl/pSBL.h>

#include <ompl/geometric/planners/pdst/PDST.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;

//!  OmOfflinePlanner class.
/*!
 * Octomap Offline Planner.
 * Plan and visualize a trajectory given an initial and a final configuration. The map is assume as known and is represented
 * by an octomap.
*/
class OmOfflinePlanner {

public:
	//! Constructor.
	OmOfflinePlanner(std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);
	//! Path Planning using OMPL.
	/*!
	 * Path Planning using OMPL and OmStateValidityChecker (over octomaps).
	 * Based on the example of OMPL
	 */
	void planTheHardWay(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);

	void planWithSimpleSetup(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);


	//! Draw the path.
	/*!
	 * Publish the path as markers to be visualized in rviz
	 */
	void publishPathMarkers();

	//! Select the given planner.
	ob::PlannerPtr selectPlanner(std::string planner_name,ob::SpaceInformationPtr si);
private:
	//ROS
	ros::NodeHandle node_hand_;
	ros::Publisher path_marker_pub_;

	//OMPL
	ob::PathPtr path_result_;
	boost::shared_ptr<og::PathGeometric> geometric_path_result_;
	og::SimpleSetupPtr simple_setup_;

	std::string planner_name_;
	bool simple_setup_flag_;
	double exploration_altitue_;
	bool laser_octomap_flag_;
};
