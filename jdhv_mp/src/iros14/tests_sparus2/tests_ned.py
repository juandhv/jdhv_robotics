#!/usr/bin/env python
"""
Created on Jan 27, 2013

@author: eduard, juandhv

Purpose:  
"""

import NED as NED
import numpy as np
from numpy import linalg as LA
import math


def dotproduct(v1, v2):
  return sum((a*b) for a, b in zip(v1, v2))

def length(v):
  return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))


ned_origin_lat = 41.777491
ned_origin_lon = 3.038056

p1_lat = 41.777378
p1_lon = 3.037550

p2_lat = 41.777615
p2_lon = 3.037466

p3_lat = 41.777746
p3_lon = 3.038036

p4_lat = 41.777481
p4_lon = 3.038117

start_lat = 41.777483
start_lon = 3.037970

goal1_lat = 41.777641
goal1_lon = 3.037655

goal2_lat = 41.777690
goal2_lon = 3.037865

third_block_lat = 41.777367
third_block_lon = 3.037283

# Usar NED.degreeMinute2Degree() para convertir a solamente grados

ned = NED.NED(ned_origin_lat, ned_origin_lon, 0.0)
pose_block = ned.geodetic2ned([third_block_lat, third_block_lon, 0.0])
pose_p1 = ned.geodetic2ned([p1_lat, p1_lon, 0.0])
pose_p2 = ned.geodetic2ned([p2_lat, p2_lon, 0.0])
pose_p3 = ned.geodetic2ned([p3_lat, p3_lon, 0.0])
pose_p4 = ned.geodetic2ned([p4_lat, p4_lon, 0.0])
pose_start = ned.geodetic2ned([start_lat, start_lon, 0.0])
pose_goal1 = ned.geodetic2ned([goal1_lat, goal1_lon, 0.0])
pose_goal2 = ned.geodetic2ned([goal2_lat, goal2_lon, 0.0])

print "p1_north:", pose_p1[0]
print "p1_east:", pose_p1[1]

print "p2_north:", pose_p2[0]
print "p2_east:", pose_p2[1]

print "p3_north:", pose_p3[0]
print "p3_east:", pose_p3[1]

print "p4_north:", pose_p4[0]
print "p4_east:", pose_p4[1]

print "start_north:", pose_start[0]
print "start_east:", pose_start[1]

print "goal1_north:", pose_goal1[0]
print "goal1_east:", pose_goal1[1]

print "goal2_north:", pose_goal2[0]
print "goal2_east:", pose_goal2[1]

third_block_ned = np.array([pose_block[0],pose_block[1]])
third_block_xyz = np.array([63.5, 0.0])
tetha = angle(third_block_ned,third_block_xyz)
rot_matrix = np.zeros((2,2))
rot_matrix[0,0] = math.cos(tetha)
rot_matrix[0,1] = -math.sin(tetha)
rot_matrix[1,0] = math.sin(tetha)
rot_matrix[1,1] = math.cos(tetha)
# print rot_matrix*third_block_xyz



