#!/usr/bin/env python
"""
Created on Jan 27, 2013

@author: eduard, juandhv

Purpose:  
"""

import NED as NED
import numpy as np
from numpy import linalg as LA
import math
import pygmaps
import os

def dotproduct(v1, v2):
  return sum((a*b) for a, b in zip(v1, v2))

def length(v):
  return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))
  
def calculate_rot_mat(third_block_ned, third_block_xyz):
    tetha = angle(third_block_ned,third_block_xyz)
    rot_matrix = np.zeros((2,2))
    rot_matrix[0,0] = math.cos(-tetha)
    rot_matrix[0,1] = -math.sin(-tetha)
    rot_matrix[1,0] = math.sin(-tetha)
    rot_matrix[1,1] = math.cos(-tetha)
    return rot_matrix

def convert(file_name):
    #41 46.651', 3 2.281'
    ned_origin_lat = 41.777517
    ned_origin_lon = 3.038017
    
    #41 46.644', 3 2.249'
    third_block_lat = 41.7774
    third_block_lon = 3.037483
    
    # Usar NED.degreeMinute2Degree() para convertir a solamente grados
    
    ned = NED.NED(ned_origin_lat, ned_origin_lon, 0.0)
    pose_block = ned.geodetic2ned([third_block_lat, third_block_lon, 0.0])
    
    print "north:", pose_block[0]
    print "east:", pose_block[1]
    
    third_block_ned = np.array([pose_block[0],pose_block[1]])
    third_block_xyz = np.array([63.5, 0.0])
    
    rot_matrix = calculate_rot_mat(third_block_ned, third_block_xyz)
    
    #------------
    #41 46.651', 3 2.281'
    ned_origin_lat = 41.7790532262#41.777517
    ned_origin_lon = 3.0318505828#3.038017
    
    #41 46.644', 3 2.249'
    third_block_lat = 41.7774
    third_block_lon = 3.037483
    
    # Usar NED.degreeMinute2Degree() para convertir a solamente grados
    
    ned = NED.NED(ned_origin_lat, ned_origin_lon, 0.0)
    pose_block = ned.geodetic2ned([third_block_lat, third_block_lon, 0.0])
    
    print "north:", pose_block[0]
    print "east:", pose_block[1]
    
    third_block_ned = np.array([pose_block[0],pose_block[1]])
    third_block_xyz = np.array([63.5, 0.0])
    
    rot_matrix = calculate_rot_mat(third_block_ned, third_block_xyz)
    
    print rot_matrix.dot(third_block_xyz)
    
    ##----
#     file_name = "mission4_LOS"
    mission_xyz_file = open("MissionsXYZ/"+file_name+".yaml",'r')
    mission_ned_file = open("MissionsNED/"+file_name+"_NED.yaml",'w+')
    path = []
    
    content = mission_xyz_file.readlines()
    for line in content:
        if line[0:17] == "trajectory/north:":
            north_coord_lst = line[19:-2].split(',') 
        elif line[0:16] == "trajectory/east:":
            east_coord_lst = line[18:-2].split(',')
            if len(north_coord_lst) != len(east_coord_lst):
                print "ERROR, dimensions north and east are different"
            else:
                coordinates_NE = []
                for coord_i in range(len(north_coord_lst)):
                    coord_XY = np.array([float(north_coord_lst[coord_i]), float(east_coord_lst[coord_i])])
                    coord_NE = rot_matrix.dot(coord_XY)
                    coordinates_NE.append(coord_NE)
                    
                    lat_long = ned.ned2geodetic([coord_NE[0], coord_NE[1], 0.0])
                    if coord_i==1:
                        mymap = pygmaps.maps(lat_long[0],lat_long[1], 17)
                    point=(lat_long[0],lat_long[1])
                    path.append(point)
                
                temp_string = ""
                temp_string = temp_string+"trajectory/north: ["
                for i in range(len(coordinates_NE)):
                    if i == len(coordinates_NE)-1:
                        temp_string = temp_string + str(coordinates_NE[i][0])+"]"
                    else:
                        temp_string = temp_string + str(coordinates_NE[i][0])+", "
                temp_string = temp_string+"\n"
                mission_ned_file.write(temp_string)
                
                temp_string = ""
                temp_string = temp_string+"trajectory/east: ["
                for i in range(len(coordinates_NE)):
                    if i == len(coordinates_NE)-1:
                        temp_string = temp_string + str(coordinates_NE[i][1])+"]"
                    else:
                        temp_string = temp_string + str(coordinates_NE[i][1])+", "
                temp_string = temp_string+"\n"
                mission_ned_file.write(temp_string)
        else:
            mission_ned_file.write(line)
    
#     mymap.addpath(path,"#FF0000")#red: gps data
#     mymap.draw("./Maps/"+file_name+"_map.html")
    
    mission_xyz_file.close()
    mission_ned_file.close()


if __name__ == '__main__':

    files = os.listdir(os.getcwd()+'/MissionsXYZ/')
    
    for file in files:
        # do something
        file_name = file.split('.')[0]
        convert(file_name)