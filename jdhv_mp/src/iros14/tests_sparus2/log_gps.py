'''
Created on May 21, 2012

@author: juandhv
'''

# import pygmaps
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pygmaps

if __name__ == '__main__':
    ##Files
    file_name="gps_for_google_maps"
    gps_log_file=open("Logs/gps_for_google_maps1.txt",'r')
#     nav_log_file=open("Logs/cola2_navigation_fastrax_it_500_gps.txt",'r')
#     KML_file=open("Maps/"+file_name+"_.kml",'w')
    
    #GPS data
    content = gps_log_file.readlines()
    gps_path=[]
    gps_data={}
    i=1
    
#     headerKML="<kml xmlns=\"http://earth.google.com/kml/2.0\"><Placemark><description>\""+file_name+"\"</description><LineString><extrude>1</extrude><tessellate>1</tessellate><altitudeMode>relativeToGround</altitudeMode><coordinates>\n"
#     footerKML="</coordinates></LineString></Placemark></kml>"
#     KML_file.write(headerKML)
    for line in content:
        lineLst=line.split(',')
        gps_data['latDegDec']=float(lineLst[0])
        gps_data['lonDegDec']=float(lineLst[1])
        
        if i==1:
            mymap = pygmaps.maps(gps_data['latDegDec'],gps_data['lonDegDec'], 17)
        i=i+1
        point=(gps_data['latDegDec'],gps_data['lonDegDec'])
        gps_path.append(point)
        
#         KML_file.write(str(gps_data['latDegDec'])+", "+str(gps_data['lonDegDec'])+"\n")
#     KML_file.write(footerKML)
    
#     #NAV data
#     content = nav_log_file.readlines()
#     nav_path=[]
#     nav_data={}
#     i=1
# 
#     for line in content:
#         lineLst=line.split(',')
#         nav_data['latDegDec']=float(lineLst[0])
#         nav_data['lonDegDec']=float(lineLst[1])
#         
#         if i==1:
#             mymap = pygmaps.maps(nav_data['latDegDec'],nav_data['lonDegDec'], 17)
#         i=i+1
#         point=(nav_data['latDegDec'],nav_data['lonDegDec'])
#         nav_path.append(point)
#         
#         KML_file.write(str(nav_data['latDegDec'])+", "+str(nav_data['lonDegDec'])+"\n")
#     KML_file.write(footerKML)

    mymap.addpath(gps_path,"#FF0000")#red: gps data
#     mymap.addpath(nav_path,"#0000FF")#blue: nav data
    mymap.draw("./Maps/"+file_name+"Map.html")
    gps_log_file.close()
    
#     plt.show()
    
    pass