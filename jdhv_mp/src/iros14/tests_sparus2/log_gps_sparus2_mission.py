'''
Created on May 21, 2012

@author: juandhv
'''

# import pygmaps
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pygmaps
import os

import NED as NED

def extract_log_information(file_name):
    
    #NED
    ned_origin_lat = 41.7790532262#41.777517
    ned_origin_lon = 3.0318505828#3.038017
    ned = NED.NED(ned_origin_lat, ned_origin_lon, 0.0)
    
    ##Files
#     file_name="mission4_LOS"
    gps_log_file=open("Logs/"+file_name+"_gps.txt",'r')
    mission_file=open("MissionsNED/"+file_name+"_NED.yaml",'r')
    
    #GPS data
    content = gps_log_file.readlines()
    gps_path=[]
    gps_path_x = []
    gps_path_y = []
    
    gps_data={}
    i=1
    
    for line in content:
        lineLst=line.split(',')
        gps_data['latDegDec']=float(lineLst[0])
        gps_data['lonDegDec']=float(lineLst[1])
        
        gps_path_x.append(gps_data['latDegDec'])
        gps_path_y.append(gps_data['lonDegDec'])
        
        if i==1:
            mymap = pygmaps.maps(gps_data['latDegDec'],gps_data['lonDegDec'], 17)
        i=i+1
        point=(gps_data['latDegDec'],gps_data['lonDegDec'])
        gps_path.append(point)
    
    #Mission data
    mission_path_x = []
    mission_path_y = []
    
    content = mission_file.readlines()
    for line in content:
        if line[0:17] == "trajectory/north:":
            north_coord_lst = line[19:-2].split(',') 
        elif line[0:16] == "trajectory/east:":
            east_coord_lst = line[18:-2].split(',')
            
            if len(north_coord_lst) != len(east_coord_lst):
                print "ERROR, dimensions north and east are different"
            else:
                for coord_i in range(len(north_coord_lst)):
                    coord_NED = [float(north_coord_lst[coord_i]), float(east_coord_lst[coord_i]), 0.0]
                    coord_GEO = ned.ned2geodetic(coord_NED)
                    mymap.addpoint(coord_GEO[0], coord_GEO[1], "#0000FF")
                    
                    mission_path_x.append(coord_GEO[0])
                    mission_path_y.append(coord_GEO[1])
#                     mymap.addradpoint(coord_GEO[0], coord_GEO[1], 0.2, "#0000FF")
            
    
    

    mymap.addpath(gps_path,"#FF0000")#red: gps data
    mymap.draw("./Maps/"+file_name+"_gps_map.html")
    
    gps_log_file.close()
    mission_file.close()
    
    #---
    for i in range(len(gps_path)-1):
        gps_path_x.append(gps_path[i][0]) 
        gps_path_y.append(gps_path[i][1])
        
    plt.subplot(1, 1, 1)
    plt.plot(gps_path_y, gps_path_x, 'ro')
    plt.plot(mission_path_y, mission_path_x, 'bo', markersize=12)
    print plt.axis()
    
#     plt.axis([41.778950000000002, 41.779250000000005, 3.0316000000000001, 3.032])
#     plt.axis([3.0314000000000001, 3.0318499999999999, 41.7789, 41.779200000000003])

    plt.title('Mission points and gps data', fontsize=30)
    plt.xlabel('Longitude',fontsize=30)
    plt.ylabel('Latitude',fontsize=30)

    plt.show()

#     raw_input()
    
if __name__ == '__main__':

    files = os.listdir(os.getcwd()+'/MissionsXYZ/')
#     files = ['mission1_1.yaml', 'mission1_LOS.yaml']
#     files = ['mission3_1.yaml', 'mission3_LOS.yaml']

    files = ['mission4_1.yaml', 'mission4_LOS.yaml']
    
    for file in files:
        # do something
        file_name = file.split('.')[0]
        extract_log_information(file_name)
        
    
    pass