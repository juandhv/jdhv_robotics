/*
 * iros14_offline_planner_R3.cpp
 *
 *  Created on: January 14, 2014
 *      Author: juandhv
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 *  Based on icra14_offline_planner_R3.cpp
 */

#include "iros14_state_validity_checker.h"
#include "iros14_offline_planner_R3.h"

OmOfflinePlanner::OmOfflinePlanner(std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos):
	node_hand_()
{
	node_hand_.getParam("planner_name", planner_name_);
	node_hand_.getParam("simple_setup_flag", simple_setup_flag_);
	node_hand_.getParam("exploration_altitute", exploration_altitue_);
	node_hand_.getParam("laser_octomap_flag", laser_octomap_flag_);

	path_marker_pub_ = node_hand_.advertise<visualization_msgs::Marker>("/jdhv_robotics/planner_result", 10);

	if(simple_setup_flag_)
		planWithSimpleSetup(planner_name_,planning_bounds,start_pos,goal_pos);
	else
		planTheHardWay(planner_name_,planning_bounds,start_pos,goal_pos);

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		OmOfflinePlanner::publishPathMarkers();
		loop_rate.sleep();
	}
}

void OmOfflinePlanner::planTheHardWay(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));

	//=======================================================================
	// Set the bounds for the R^3 part of SE(3)
	//=======================================================================
	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds[0]);
	bounds.setHigh(0,planning_bounds[1]);
	bounds.setLow(1,planning_bounds[2]);
	bounds.setHigh(1,planning_bounds[3]);
	bounds.setLow(2,planning_bounds[4]);
	bounds.setHigh(2,planning_bounds[5]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Construct an instance of  space information from this state space
	//=======================================================================
	ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	boost::shared_ptr<ob::StateValidityChecker> om_stat_val_check;
	om_stat_val_check.reset(new OmStateValidityChecker(si,exploration_altitue_,laser_octomap_flag_));
	si->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	const ob::RealVectorStateSpace::StateType *pos;
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];

	//=======================================================================
	// Create a problem instance
	//=======================================================================
	ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

	//=======================================================================
	// Setting optimization objective
	//=======================================================================
//	ob::OptimizationObjective *opt;
//	boost::shared_ptr<ob::OptimizationObjective> newOptimizationObjective;
////	opt = new ob::PathLengthOptimizationObjective(si, std::numeric_limits<double>::epsilon());
//	opt = new ob::StateCostOptimizationObjective(si, std::numeric_limits<double>::epsilon());
////	opt = new ob::BoundedAdditiveOptimizationObjective(si, 40.0);
//	newOptimizationObjective.reset(opt);
//
//	pdef->setOptimizationObjective(newOptimizationObjective);
//
//	ROS_INFO("*** OK specified: ***");

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	pdef->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);
//	if(planner_name.compare("TRRT")==0)
//	{
//		boost::shared_ptr<og::TRRT> trrt;
//		trrt = boost::dynamic_pointer_cast<og::TRRT>(planner);
//		trrt->setFrontierThreshold(20.0);
//		trrt->setRange(1.0);
//	}

	//=======================================================================
	// Set the problem we are trying to solve for the planner
	//=======================================================================
	planner->setProblemDefinition(pdef);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	planner->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	planner->printProperties(std::cout);// print planner properties
	si->printSettings(std::cout);// print the settings for this space
	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = planner->solve(1.0);

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		path_result_ = pdef->getSolutionPath();
		geometric_path_result_ = boost::dynamic_pointer_cast<og::PathGeometric>(path_result_);

		ROS_INFO("Path has been found");

		// print the path to screen
		//path_result_->print(std::cout);
	}
	else
		ROS_INFO("Path has not been found");
}

void OmOfflinePlanner::planWithSimpleSetup(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));

	//=======================================================================
	// Set the bounds for the R^3 part of SE(3)
	//=======================================================================
	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds[0]);
	bounds.setHigh(0,planning_bounds[1]);
	bounds.setLow(1,planning_bounds[2]);
	bounds.setHigh(1,planning_bounds[3]);
	bounds.setLow(2,planning_bounds[4]);
	bounds.setHigh(2,planning_bounds[5]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);
//	if(planner_name.compare("TRRT")==0)
//	{
//		boost::shared_ptr<og::TRRT> trrt;
//		trrt = boost::dynamic_pointer_cast<og::TRRT>(planner);
//		trrt->setFrontierThreshold(20.0);
//	}

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	boost::shared_ptr<ob::StateValidityChecker> om_stat_val_check;
	om_stat_val_check.reset(new OmStateValidityChecker(si,exploration_altitue_,laser_octomap_flag_));
	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	const ob::RealVectorStateSpace::StateType *pos;
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
//	planner->printProperties(std::cout);// print planner properties
//	si->printSettings(std::cout);// print the settings for this space
//	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve( 2.0 );

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
//		simple_setup_->simplifySolution();
		simple_setup_->getSolutionPath().interpolate();
		geometric_path_result_.reset(new og::PathGeometric(simple_setup_->getSolutionPath()));

		ROS_INFO("Path has been found with simple_setup");


		// %Calculating costs%
		std::vector< ob::State * > states = geometric_path_result_->getStates();
		ob::RealVectorStateSpace dist_calc(3);
		double total_dist = 0.0;
		double total_cost = 0.0;

		//Calculates the total path cost
		ofstream costs_file;
		std::string costs_file_name;
		costs_file_name = "/home/juandhv/" + planner_name + ".txt";
		costs_file.open(costs_file_name.c_str());

		for (uint32_t i = 0; i < geometric_path_result_->getStateCount(); ++i)
		{
			total_cost = total_cost + om_stat_val_check->cost(states[i]);

			if(i>0)
			{
				total_dist = total_dist + dist_calc.distance(states[i-1],states[i]);
			}
			costs_file << total_dist << "," << total_cost << std::endl;
		}
		costs_file.close();
		// %Calculating costs%

	}
	else
		ROS_INFO("Path has not been found");
}

ob::PlannerPtr OmOfflinePlanner::selectPlanner(std::string planner_name,ob::SpaceInformationPtr si)
{
	// create a planner for the defined space
	if(planner_name.compare("RRT")==0)
		return ob::PlannerPtr(new og::RRT(si));
	else if(planner_name.compare("RRTstar")==0)
		return ob::PlannerPtr(new og::RRTstar(si));
	else if(planner_name.compare("RRTConnect")==0)
		return ob::PlannerPtr(new og::RRTConnect(si));
	else if(planner_name.compare("BallTreeRRTstar")==0)
		return ob::PlannerPtr(new og::BallTreeRRTstar(si));
	else if(planner_name.compare("LazyRRT")==0)
		return ob::PlannerPtr(new og::LazyRRT(si));
	else if(planner_name.compare("pRRT")==0)
		return ob::PlannerPtr(new og::pRRT(si));
	else if(planner_name.compare("TRRT")==0)
		return ob::PlannerPtr(new og::TRRT(si));
	else if(planner_name.compare("EST")==0)
		return ob::PlannerPtr(new og::EST(si));
	else if(planner_name.compare("BKPIECE1")==0)
		return ob::PlannerPtr(new og::BKPIECE1(si));
	else if(planner_name.compare("KPIECE1")==0)
		return ob::PlannerPtr(new og::KPIECE1(si));
	else if(planner_name.compare("LBKPIECE1")==0)
		return ob::PlannerPtr(new og::LBKPIECE1(si));
	else if(planner_name.compare("PDST")==0)
		return ob::PlannerPtr(new og::PDST(si));
	else if(planner_name.compare("PRM")==0)
		return ob::PlannerPtr(new og::PRM(si));
	else if(planner_name.compare("LazyPRM")==0)
		return ob::PlannerPtr(new og::LazyPRM(si));
	else if(planner_name.compare("PRMstar")==0)
		return ob::PlannerPtr(new og::PRMstar(si));
	else if(planner_name.compare("SPARS")==0)
		return ob::PlannerPtr(new og::SPARS(si));
	else if(planner_name.compare("SPARStwo")==0)
		return ob::PlannerPtr(new og::SPARStwo(si));
	else if(planner_name.compare("SBL")==0)
		return ob::PlannerPtr(new og::SBL(si));
	else if(planner_name.compare("pSBL")==0)
		return ob::PlannerPtr(new og::pSBL(si));
	else
		return ob::PlannerPtr(new og::RRT(si));
}

void OmOfflinePlanner::publishPathMarkers()
{
	const ob::RealVectorStateSpace::StateType *pos;

	// %Tag(MARKER_INIT)%
	visualization_msgs::Marker points, line_strip;
	points.header.frame_id = line_strip.header.frame_id =  "/world";
	points.header.stamp = line_strip.header.stamp = ros::Time::now();
	points.ns = line_strip.ns = "points_and_lines";
	points.action = line_strip.action = visualization_msgs::Marker::ADD;
	points.pose.orientation.w = line_strip.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	points.id = 0;
	line_strip.id = 1;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	points.type = visualization_msgs::Marker::POINTS;
	line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	points.scale.x = 0.2;
	points.scale.y = 0.2;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	line_strip.scale.x = 0.3;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	points.color.g = 1.0f;
	points.color.a = 1.0;

	// Line strip is blue
	line_strip.color.b = 1.0;
	line_strip.color.a = 1.0;

	// %Tag(HELIX)%
	// Create the vertices for the points and lines
	std::vector< ob::State * > states = geometric_path_result_->getStates();

	for (uint32_t i = 0; i < geometric_path_result_->getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		pos = states[i]->as<ob::RealVectorStateSpace::StateType>();

		geometry_msgs::Point p;
		p.x = pos->values[0];
		p.y = pos->values[1];
		p.z = pos->values[2];

		points.points.push_back(p);
		line_strip.points.push_back(p);
	}
	// %EndTag(HELIX)%

	path_marker_pub_.publish(points);
	path_marker_pub_.publish(line_strip);
}
