/*
 * state_validity_checker_morph_SE2_girona14.h
 *
 *  Created on: Mar 13, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (position state) is collision-free.
 *  The workspace is represented by an octomap.
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>
#include <string>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/BallTreeRRTstar.h>
#include <ompl/geometric/planners/rrt/LazyRRT.h>
#include <ompl/geometric/planners/rrt/pRRT.h>
#include <ompl/geometric/planners/rrt/TRRT.h>

#include <ompl/geometric/planners/est/EST.h>

#include <ompl/geometric/planners/kpiece/BKPIECE1.h>
#include <ompl/geometric/planners/kpiece/KPIECE1.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>

#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/geometric/planners/prm/LazyPRM.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/planners/prm/SPARS.h>
#include <ompl/geometric/planners/prm/SPARStwo.h>

#include <ompl/geometric/planners/sbl/SBL.h>
#include <ompl/geometric/planners/sbl/pSBL.h>

#include <ompl/geometric/planners/pdst/PDST.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//Eigen
#include <Eigen/Dense>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;


//!  OmStateValidityChecker class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap.
*/
class OmStateValidityChecker : public ob::StateValidityChecker {

public:
	//! OmStateValidityChecker constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	OmStateValidityChecker(const ob::SpaceInformationPtr &si, const double exploration_altitue, const double planning_depth, const bool laser_octomap_flag);
	//! OmStateValidityChecker destructor.
	/*!
	 * Destroy the octomap.
	 */
	~OmStateValidityChecker();
	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision)
	 */
	virtual bool isValid(const ob::State *state) const;

	virtual int isCollision(Eigen::Vector3f pos_query) const;

	virtual double cost(const ob::State *state) const;

	double octree_res_;
private:
	//ROS
	ros::NodeHandle node_hand_;

	// ROS tf
	tf::Pose last_pose_;

	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;

	//Girona500 info
	double length_g500_;

	double exploration_altitue_;
	double planning_depth_;
};
