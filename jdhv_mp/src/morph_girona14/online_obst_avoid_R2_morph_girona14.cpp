/*
 * online_obst_avoid_R2_morph_girona14.cpp
 *
 *  Created on: Apr 9, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *
 *  Obstacle avoidance for CVi vehicles in MORPH project.
 *  Octomaps as navigation maps.
 */

#include "state_validity_checker_R2_morph_girona14.hpp"
#include "online_obst_avoid_R2_morph_girona14.hpp"
#include "debugging_flags.hpp"

OnlineObstAvoidR2::OnlineObstAvoidR2(std::string planner_name):
	node_hand_(),
	current_line_angle_(0.0),
	line_angle_(0.0),
	dist_obst_cv1_(0.0),
	dist_obst_cv2_(0.0),
	ID_(0)
{
	pose_path_.header.frame_id = "/world";

	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	//=======================================================================
	// Get paramaters
	//=======================================================================
	node_hand_.getParam("planner_name", planner_name_);
	node_hand_.getParam("simple_setup_flag", simple_setup_flag_);
	node_hand_.getParam("exploration_altitute", exploration_altitue_);
	node_hand_.getParam("laser_octomap_flag", laser_octomap_flag_);
	node_hand_.getParam("rosbags_flag", rosbags_flag_);
	node_hand_.getParam("x_dist_rov", x_dist_rov_);
	node_hand_.getParam("y_dist_rov", y_dist_rov_);
	node_hand_.getParam("z_dist_rov", z_dist_rov_);
	node_hand_.getParam("mission_flag_available", mission_flag_available_);

	//=======================================================================
	// Get planner information
	//=======================================================================
	planning_bounds_ = std::vector<double> (4);

	pos_obst_cv1_ = Eigen::Vector3f(3);
	pos_obst_cv2_ = Eigen::Vector3f(3);
	pos_obst_cv1_[0] = pos_obst_cv1_[1] = pos_obst_cv1_[2] = 0.0;
	pos_obst_cv2_[0] = pos_obst_cv2_[1] = pos_obst_cv2_[2] = 0.0;

	//=======================================================================
	// Subscribers
	//=======================================================================
	//Navigation data (feedback)
	nav_sts_sub_ = node_hand_.subscribe("/cola2_navigation/nav_sts", 1, &OnlineObstAvoidR2::navStsSubCallback, this);
	nav_sts_available_ = false;

	// /mission_handler/Formation (permits to know the moment when the mission starts)
	mission_handler_sub_ = node_hand_.subscribe("/mission_handler/Formation", 1, &OnlineObstAvoidR2::missionHandlerFormationSubCallback, this);

	// /follow_section/feedback (this is to get the line_angle)
	//follow_sect_sub_ = node_hand_.subscribe("/follow_section/feedback", 1, &OnlineObstAvoidR2::followSectionSubCallback, this);
	follow_sect_sub_ = node_hand_.subscribe("/GCV/rof/yaw", 1, &OnlineObstAvoidR2::gcvRofYawSubCallback, this);

	//Mission Flag (feedback)
	mission_flag_sub_ = node_hand_.subscribe("/jdhv_robotics/mission_flag", 1, &OnlineObstAvoidR2::missionFlagSubCallback, this);

	//Driver range
	driver_range_sub_ =  node_hand_.subscribe("/driver/range", 1, &OnlineObstAvoidR2::driverRangeSubCallback, this);

	//Driver range
	ID_sub_ = node_hand_.subscribe("/ID", 1, &OnlineObstAvoidR2::IdCallback, this);

	//=======================================================================
	// Publishers
	//=======================================================================
	path_pose_pub_ = node_hand_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/pose_est_cvs", 10);

	cv1_dy_pub_ = node_hand_.advertise<std_msgs::Float64>("/CV1/rof/dy", 10);
	cv2_dy_pub_ = node_hand_.advertise<std_msgs::Float64>("/CV2/rof/dy", 10);

	//=======================================================================
	// Wait for nav sts
	//=======================================================================
	ROS_INFO("%s: waiting for navigation data", ros::this_node::getName().c_str());
	ros::Rate loop_rate(10);
	while (!nav_sts_available_)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}
	ROS_INFO("%s: navigation data available", ros::this_node::getName().c_str());

	//=======================================================================
	// Wait for mission flag
	//=======================================================================
	if(!mission_flag_available_)
		ROS_INFO("%s: waiting for mission flag (new)", ros::this_node::getName().c_str());

	if(!rosbags_flag_)
	{
		while (!mission_flag_available_)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}
	}
	ROS_INFO("%s: mission flag received", ros::this_node::getName().c_str());

	//use the service to get formation role
	ros::ServiceClient client_role = node_hand_.serviceClient<handler::GetRole>("/mission/getrole");
	handler::GetRole srv_role;

	if (client_role.call(srv_role))
	{
		ROS_INFO("%s: Service /mission/getrole OK", ros::this_node::getName().c_str());
		roles_ = srv_role.response.role;
	}
	else
	{
		ROS_ERROR("%s: failed to call service /mission/getrole", ros::this_node::getName().c_str());
	}

	//use the service to get formation data
	ros::ServiceClient client_formation = node_hand_.serviceClient<handler::GetFormation>("/mission/getformation");
	handler::GetFormation srv_formation;

	if (client_formation.call(srv_formation))
	{
		ROS_INFO("%s: Service /mission/getformation OK", ros::this_node::getName().c_str());
	}
	else
	{
		ROS_ERROR("%s: Failed to call service /mission/getformation", ros::this_node::getName().c_str());
	}

	for(int i = 0; i != roles_.size(); i++) {
		if(roles_[i].compare("lsv")==0)
		{
			formation_LSV_.index_mission_handler = i;
			formation_LSV_.ID = srv_formation.response.formation.ID[i];
			formation_LSV_.x = srv_formation.response.formation.x[i];
			formation_LSV_.y = srv_formation.response.formation.y[i];
			formation_LSV_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("gcv")==0)
		{
			formation_GCV_.index_mission_handler = i;
			formation_GCV_.ID = srv_formation.response.formation.ID[i];
			formation_GCV_.x = srv_formation.response.formation.x[i];
			formation_GCV_.y = srv_formation.response.formation.y[i];
			formation_GCV_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("cv1")==0)
		{
			formation_CV1_.index_mission_handler = i;
			formation_CV1_.ID = srv_formation.response.formation.ID[i];
			formation_CV1_.x = srv_formation.response.formation.x[i];
			formation_CV1_.y = srv_formation.response.formation.y[i];
			formation_CV1_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("cv2")==0)
		{
			formation_CV2_.index_mission_handler = i;
			formation_CV2_.ID = srv_formation.response.formation.ID[i];
			formation_CV2_.x = srv_formation.response.formation.x[i];
			formation_CV2_.y = srv_formation.response.formation.y[i];
			formation_CV2_.z = srv_formation.response.formation.z[i];
		}
	}

//	//	for(int i = 0; i != roles_.size(); i++) {
//	//		if(roles_[i].compare("lsv")==0)
//	//		{
//				formation_LSV_.index_mission_handler = 1;//i;
//				formation_LSV_.ID = 1;//srv_formation.response.formation.ID[i];
//				formation_LSV_.x = 5.0;//srv_formation.response.formation.x[i];
//				formation_LSV_.y = 0.0;//srv_formation.response.formation.y[i];
//				formation_LSV_.z = -5.0;//srv_formation.response.formation.z[i];
//	//		}
//	//		else if(roles_[i].compare("gcv")==0)
//	//		{
//				formation_GCV_.index_mission_handler = 3;//i;
//				formation_GCV_.ID = 3;//srv_formation.response.formation.ID[i];
//				formation_GCV_.x = -15.0;//srv_formation.response.formation.x[i];
//				formation_GCV_.y = 0.0;//srv_formation.response.formation.y[i];
//				formation_GCV_.z = -5.0;//srv_formation.response.formation.z[i];
//	//		}
//	//		else if(roles_[i].compare("cv1")==0)
//	//		{
//				formation_CV1_.index_mission_handler = 5;//i;
//				formation_CV1_.ID = 5;//srv_formation.response.formation.ID[i];
//				formation_CV1_.x = -10.0;//srv_formation.response.formation.x[i];
//				formation_CV1_.y = 5.0;//srv_formation.response.formation.y[i];
//				formation_CV1_.z = -10.0;//srv_formation.response.formation.z[i];
//	//		}
//	//		else if(roles_[i].compare("cv2")==0)
//	//		{
//				formation_CV2_.index_mission_handler = 4;//i;
//				formation_CV2_.ID = 4;//srv_formation.response.formation.ID[i];
//				formation_CV2_.x = -10.0;//srv_formation.response.formation.x[i];
//				formation_CV2_.y = -5.0;//srv_formation.response.formation.y[i];
//				formation_CV2_.z = -10.0;//srv_formation.response.formation.z[i];
//	//		}
//	//	}

	//=======================================================================
	// Obtain the first valid state
	//=======================================================================
	ros::spinOnce();
	last_nav_sts_data_pos_ =  Eigen::Vector3f(nav_sts_data_pos_);

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	ros::Duration(5.0).sleep();
	plann_timer_ = node_hand_.createTimer(ros::Duration(1.0), &OnlineObstAvoidR2::timerCallback, this);
}

void OnlineObstAvoidR2::timerCallback(const ros::TimerEvent &e)
{
	geometry_msgs::Pose pose;
	tf::Quaternion orien_quat;
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	//=======================================================================
	// Plan incrementally and online
	//=======================================================================
//#ifdef DEBUG_LEVEL1
//	ROS_INFO("*** Timer callback (%s) ***",auv_name_.c_str());
//#endif
	current_nav_sts_data_pos_ =  Eigen::Vector3f(nav_sts_data_pos_);
	current_line_angle_ = line_angle_;

//#ifdef DEBUG_LEVEL1
//	std::cout << "*** position :" << current_nav_sts_data_pos_ << std::endl;
//	std::cout << "*** line_angle :" << current_line_angle_ << std::endl;
//#endif

	//=======================================================================
	// Get updated map
	//=======================================================================

	if(laser_octomap_flag_)
		serv_name = "/laser_octomap/get_binary";
	else
		serv_name = "octomap_binary";
	octree_ = NULL;

	ROS_DEBUG("%s: requesting the octomap", ros::this_node::getName().c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_)
			ROS_DEBUG("%s: octomap received (%zu nodes, %f m res)", ros::this_node::getName().c_str(), octree_->size(), octree_->getResolution());
		else
			ROS_DEBUG("%s: error reading OcTree from stream", ros::this_node::getName().c_str());
	}

	//----- CV1

	orien_quat.setRPY(0., 0., current_line_angle_-M_PI/2.);

	pose.position.x = formation_CV1_.y*cos(current_line_angle_+M_PI/2.) + current_nav_sts_data_pos_[0];
	pose.position.y = formation_CV1_.y*sin(current_line_angle_+M_PI/2.) + current_nav_sts_data_pos_[1];
	if(formation_CV1_.z<0)
		pose.position.z = -formation_CV1_.z;
	else
		pose.position.z = (last_altitude_ + 2.0 - formation_CV1_.z);

	pose.orientation.x = orien_quat.getX();
	pose.orientation.y = orien_quat.getY();
	pose.orientation.z = orien_quat.getZ();
	pose.orientation.w = orien_quat.getW();

	std::vector<double> state_cv1(3);
	state_cv1[0] = pose.position.x;
	state_cv1[1] = pose.position.y;
	state_cv1[2] = pose.position.z;

	if(isCollision(state_cv1, 2.0)){
		ROS_WARN("%s: collision detected CV1", ros::this_node::getName().c_str());
		cv1_dy_.data = 3.0;
		pos_obst_cv1_[0] = state_cv1[0];
		pos_obst_cv1_[1] = state_cv1[1];
		pos_obst_cv1_[2] = state_cv1[2];
	}
	else if(pos_obst_cv1_.norm()!=0 && dist_obst_cv1_<dist_lsv_to_cv1_)
	{
		ROS_WARN("%s: avoiding collision CV1", ros::this_node::getName().c_str());
		cv1_dy_.data = 3.0;
	}
	else if(pos_obst_cv1_.norm()!=0 && dist_obst_cv1_>dist_lsv_to_cv1_)
	{
		cv1_dy_.data = 0.0;
		pos_obst_cv1_[0] = 0.0;
		pos_obst_cv1_[1] = 0.0;
		pos_obst_cv1_[2] = 0.0;
	}
	else{
		cv1_dy_.data = 0.0;
		ROS_DEBUG("%s: collision NOT detected CV1", ros::this_node::getName().c_str());
	}

	pose_path_.poses.push_back(pose);
	cv1_dy_pub_.publish(cv1_dy_);

	//----- CV2

	orien_quat.setRPY(0., 0., current_line_angle_+M_PI/2.);

	pose.position.x = -formation_CV2_.y*cos(current_line_angle_-M_PI/2.) + current_nav_sts_data_pos_[0];
	pose.position.y = -formation_CV2_.y*sin(current_line_angle_-M_PI/2.) + current_nav_sts_data_pos_[1];
	if(formation_CV2_.z<0)
		pose.position.z = -formation_CV2_.z;
	else
		pose.position.z = (last_altitude_ + 2.0 - formation_CV2_.z);

	pose.orientation.x = orien_quat.getX();
	pose.orientation.y = orien_quat.getY();
	pose.orientation.z = orien_quat.getZ();
	pose.orientation.w = orien_quat.getW();

	std::vector<double> state_cv2(3);
	state_cv2[0] = pose.position.x;
	state_cv2[1] = pose.position.y;
	state_cv2[2] = pose.position.z;

	if(isCollision(state_cv2, 2.0)){
		ROS_WARN("%s: collision detected CV2", ros::this_node::getName().c_str());
		cv2_dy_.data = -3.0;
		pos_obst_cv2_[0] = state_cv2[0];
		pos_obst_cv2_[1] = state_cv2[1];
		pos_obst_cv2_[2] = state_cv2[2];
	}
	else if(pos_obst_cv2_.norm()!=0 && dist_obst_cv2_<dist_lsv_to_cv2_)
	{
		ROS_WARN("%s: avoiding collision CV2", ros::this_node::getName().c_str());
		cv2_dy_.data = -3.0;
	}
	else if(pos_obst_cv2_.norm()!=0 && dist_obst_cv2_>dist_lsv_to_cv2_)
	{
		cv2_dy_.data = 0.0;
		pos_obst_cv2_[0] = 0.0;
		pos_obst_cv2_[1] = 0.0;
		pos_obst_cv2_[2] = 0.0;
	}
	else{
		cv2_dy_.data = 0.0;
		ROS_DEBUG("%s: collision NOT detected CV2", ros::this_node::getName().c_str());
	}

	pose_path_.poses.push_back(pose);
	cv2_dy_pub_.publish(cv2_dy_);

	//--

	pose_path_.header.stamp = ros::Time::now();

	path_pose_pub_.publish(pose_path_);

	//Clean memory deleting octomap
	delete octree_;
}

bool OnlineObstAvoidR2::isCollision(const std::vector<double> state, double cube_edge_length)
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;

//	std::cout << "*** pos_query: [" << state[0] << ", " << state[1] << ", " << planning_depth_ << "]" << std::endl;

	for(double xi = state[0]-(cube_edge_length/2.0);xi <= state[0]+(cube_edge_length/2.0);xi=xi+octree_res_)
		for(double yi = state[1]-(cube_edge_length/2.0);yi <= state[1]+(cube_edge_length/2.0);yi=yi+octree_res_)
			for(double zi = state[2]-(cube_edge_length/2.0);zi <= state[2]+(cube_edge_length/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result != NULL){
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						break;
					}
				}
			}
	return collision;
}

void OnlineObstAvoidR2::navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg)
{
	nav_sts_data_pos_ = Eigen::Vector3f(nav_sts_msg.position.north, nav_sts_msg.position.east, nav_sts_msg.position.depth);
	nav_sts_data_ori_ = Eigen::Vector3f(nav_sts_msg.orientation.roll, nav_sts_msg.orientation.pitch, nav_sts_msg.orientation.yaw);

	if(!nav_sts_available_)
		nav_sts_available_ = true;

	if(pos_obst_cv1_.norm()!=0)
		dist_obst_cv1_ = (nav_sts_data_pos_ - pos_obst_cv1_).norm();
	else
		dist_obst_cv1_ = 0.0;

	if(pos_obst_cv2_.norm()!=0)
		dist_obst_cv2_ = (nav_sts_data_pos_ - pos_obst_cv2_).norm();
	else
		dist_obst_cv2_ = 0.0;

	last_altitude_ = nav_sts_msg.altitude;

	ROS_DEBUG("%s: distance to obstacle in CV1 %f", ros::this_node::getName().c_str(), dist_obst_cv1_);
	ROS_DEBUG("%s: distance to obstacle in CV2 %f", ros::this_node::getName().c_str(), dist_obst_cv2_);
}

void OnlineObstAvoidR2::missionHandlerFormationSubCallback(const morph_msgs::Formation &mission_handler_formation_msg)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

void OnlineObstAvoidR2::followSectionSubCallback(const morph_msgs::follow_sectionActionFeedback &foll_sect_feedback_msg)
{
	line_angle_ = foll_sect_feedback_msg.feedback.Line_Angle*(M_PI/180.0);
}

void OnlineObstAvoidR2::gcvRofYawSubCallback(const std_msgs::Float64 &gcv_rof_yaw_msg)
{
	line_angle_ = gcv_rof_yaw_msg.data*(M_PI/180.0);
	//std::cout << "line_angle_: " << line_angle_ << std::endl;
}

void OnlineObstAvoidR2::missionFlagSubCallback(const std_msgs::Bool &mission_flag)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

void OnlineObstAvoidR2::driverRangeSubCallback(const acomms::AddressDurationPair &driver_range_msg)
{
	const double v_sound = 1516.0;

	if(driver_range_msg.local_address == ID_)
	{
		ROS_DEBUG("\n%s: Modem %d with range %d us or %.2f meters", ros::this_node::getName().c_str(), driver_range_msg.foreign_address, driver_range_msg.time_delay, (driver_range_msg.time_delay/2000000.0)*v_sound);
		if(driver_range_msg.foreign_address == formation_CV1_.ID)
		{
			dist_lsv_to_cv1_ = (driver_range_msg.time_delay/2000000.0)*v_sound;
			ROS_DEBUG("\n%s: distante to CV1 is %.2f meters", ros::this_node::getName().c_str(), dist_lsv_to_cv1_);
		}
		else if(driver_range_msg.foreign_address == formation_CV2_.ID)
		{
			dist_lsv_to_cv2_ = (driver_range_msg.time_delay/2000000.0)*v_sound;
			ROS_DEBUG("\n%s: distante to CV2 is %.2f meters", ros::this_node::getName().c_str(), dist_lsv_to_cv2_);
		}
	}
}

void OnlineObstAvoidR2::IdCallback(const std_msgs::Int8 &id_msg)
{
	ID_ = id_msg.data;
}

void OnlineObstAvoidR2::publishPathMarkers()
{
	const ob::RealVectorStateSpace::StateType *pos;
	geometry_msgs::Point pBound;

	// %Tag(HELIX)%
	// Create the vertices for the points and lines
	std::vector< ob::State * > states = geometric_path_result_->getStates();

	for (uint32_t i = 0; i < geometric_path_result_->getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		pos = states[i]->as<ob::RealVectorStateSpace::StateType>();

		geometry_msgs::Point p;
		p.x = pos->values[0];
		p.y = pos->values[1];
		p.z = pos->values[2];

		if(increm_path_.size()==0 || i!=0)
		{
			std::vector<double> pathPoint(3);
			pathPoint[0] = pos->values[0];
			pathPoint[1] = pos->values[1];
			pathPoint[2] = z_dist_rov_;
			increm_path_.push_back(pathPoint);
		}
	}
	std::cout << "*** Incremental Path length: " << increm_path_.size() << " ***" << std::endl;

	// %EndTag(HELIX)%

	// %Tag(MARKER_INIT)%
	visualization_msgs::Marker points, line_strip, bound_line_strip;
	points.header.frame_id = line_strip.header.frame_id = bound_line_strip.header.frame_id= "/world";
	points.header.stamp = line_strip.header.stamp = bound_line_strip.header.stamp = ros::Time::now();
	points.ns = line_strip.ns = bound_line_strip.ns = "points_and_lines";
	points.action = line_strip.action = bound_line_strip.action = visualization_msgs::Marker::ADD;
	points.pose.orientation.w = line_strip.pose.orientation.w = bound_line_strip.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	points.id = 0;
	line_strip.id = 1;
	bound_line_strip.id = 2;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	points.type = visualization_msgs::Marker::POINTS;
	line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	bound_line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	points.scale.x = 0.2;
	points.scale.y = 0.2;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	line_strip.scale.x = 0.3;
	bound_line_strip.scale.x = 0.05;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	points.color.g = 1.0f;
	points.color.a = 1.0;

	// Line strip is red
	line_strip.color.r = 1.0;
	line_strip.color.a = 1.0;

	bound_line_strip.color.r = 1.0;
	bound_line_strip.color.a = 1.0;
	// %EndTag(COLOR)%

	// %Tag(HELIX)%

	std::list< std:: vector<double> >::const_iterator lst_iterator;
	std::vector<double>::const_iterator vct_iterator;
	for (lst_iterator = increm_path_.begin(); lst_iterator != increm_path_.end(); ++lst_iterator)
	{

		geometry_msgs::Point p;
		vct_iterator = lst_iterator->begin();
		p.x = *vct_iterator;
		++vct_iterator;
		p.y = *vct_iterator;
		++vct_iterator;
		p.z = *vct_iterator;

		points.points.push_back(p);
		line_strip.points.push_back(p);

	}
	// %EndTag(HELIX)%

			pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[2];
	pBound.z = z_dist_rov_;
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[1];
	pBound.y = planning_bounds_[2];
	pBound.z = z_dist_rov_;
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[1];
	pBound.y = planning_bounds_[3];
	pBound.z = z_dist_rov_;
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[3];
	pBound.z = z_dist_rov_;
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[2];
	pBound.z = z_dist_rov_;
	bound_line_strip.points.push_back(pBound);

	path_marker_pub_.publish(points);
	path_marker_pub_.publish(line_strip);
	path_marker_pub_.publish(bound_line_strip);

}

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}
