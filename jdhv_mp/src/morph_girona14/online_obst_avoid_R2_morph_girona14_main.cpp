/*
 * online_obst_avoid_R2_morph_girona14_main.cpp
 *
 *  Created on: Apr 9, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Obstacle avoidance for CVi vehicles in MORPH project.
 *  Octomaps as navigation maps.
 */

#include "state_validity_checker_R2_morph_girona14.hpp"
#include "online_obst_avoid_R2_morph_girona14.hpp"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "online_obst_avoid_R2_morph_girona14");
	ROS_INFO_STREAM("Starting online_obst_avoid_R2_morph_girona14 Node");

	// Constructor
	//(RRT, RRTstar, RRTConnect, BallTreeRRTstar, LazyRRT, pRRT, TRRT), (EST), (BKPIECE1, KPIECE1, LBKPIECE1), (PDST), (PRM, LazyPRM, PRMstar, SPARS, SPARStwo), (SBL, pSBL)
	OnlineObstAvoidR2 onObstAvoid("RRTstar_auv1");

	// Spin
	ros::spin();

	// Exit main function without errors
	return 0;
}
