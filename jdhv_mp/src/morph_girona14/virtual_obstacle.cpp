/*
 * virtual_obstacle.cpp
 *
 *  Created on: Jun 30, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *
 *  Purpose: Create virtual obstacles.
 *  Based on laser_octomap.cpp (Guillem)
 */

#include <string>

// Boost
#include <boost/shared_ptr.hpp>

// ROS
#include <ros/ros.h>

// ROS LaserScan tools
#include <laser_geometry/laser_geometry.h>

// ROS messages
#include <geometry_msgs/Pose.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/PointCloud.h>
#include <std_msgs/ColorRGBA.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/Float64.h>

// ROS services
#include <std_srvs/Empty.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

// Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>
typedef octomap_msgs::GetOctomap OctomapSrv;
#include <octomap_ros/conversions.h>

#include <signal.h>

//MORPH
#include <morph_msgs/follow_sectionActionFeedback.h>
#include <morph_msgs/Formation.h>
#include <handler/GetFormation.h>
#include <handler/GetRole.h>

#include "online_obst_avoid_R2_morph_girona14.hpp"

//Standard namespace
using namespace std;

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}

//typedef struct{
//	int index_mission_handler;
//	double x;
//	double y;
//	double z;
//}formation_info;

/// CLASS DEFINITION ===========================================================
class VirtualObstacle
{
public:
	// Constructor and destructor
	VirtualObstacle();

	// Callbacks
	void mbCallback(const sensor_msgs::LaserScanConstPtr& scan);
	void odomCallback(const nav_msgs::OdometryConstPtr& odom_msg);
	void navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg);
	void timerCallback(const ros::TimerEvent& e);
	void missionHandlerSubCallback(const morph_msgs::Formation &mission_handler_formation_msg);

	// Services
	bool octomapBinarySrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
	bool octomapFullSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
	bool getOctomapBinarySrv(OctomapSrv::Request  &req, OctomapSrv::GetOctomap::Response &res);

	// Octree visualization
	void publishMap();
	std_msgs::ColorRGBA heightMapColor(double h);

	//
	void checkObstZones(const nav_msgs::OdometryConstPtr &odom_msg);

private:
	// Other functions
	void filter_single_outliers(sensor_msgs::LaserScan &scan, std::vector<bool> &rngflags);

	// ROS
	ros::NodeHandle node_hand_;
	ros::Publisher mb_scan_pub_output_;
	ros::Subscriber odometry_sub_, nav_sts_sub_, mb_scan_sub_input_, mission_handler_sub_;

	// ROS tf
	tf::Pose last_pose_;
	tf::StampedTransform robot_to_mb_;
	tf::TransformListener tf_listener_;

	// Names
	std::string fixed_frame_, robot_frame_, multibeam_frame_;

	// LaserScan => (x,y,z)
	laser_geometry::LaserProjection mb_projector_;

	// ROS Messages
	sensor_msgs::PointCloud cloud_;

	// Flags
	bool mission_flag_available_;
	bool initialized_;
	bool nav_sts_available_;
	bool create_virt_obst_;
	bool obst_zone_left_, obst_zone_right_;
	double last_pose_z_, last_altitude_;

	//follow_section feeback
	std::vector<string> roles_;
	formation_info formation_LSV_, formation_GCV_, formation_CV1_, formation_CV2_;
};

/// Constructor and destructor =================================================
VirtualObstacle::VirtualObstacle():
    		node_hand_(),
    		fixed_frame_("/world"),
    		robot_frame_("/girona500"),
    		multibeam_frame_("/multibeam"),
    		initialized_(false),
    		obst_zone_left_(false),
            obst_zone_right_(false),
    		create_virt_obst_(false),
    		mission_flag_available_(true)
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	// ROS parameters
	node_hand_.param("virtual_obstacle/fixed_frame", fixed_frame_, fixed_frame_);
	node_hand_.param("virtual_obstacle/robot_frame", robot_frame_, robot_frame_);
	node_hand_.param("virtual_obstacle/multibeam_frame", multibeam_frame_, multibeam_frame_);
	node_hand_.param("virtual_obstacle/create_virt_obst", create_virt_obst_, create_virt_obst_);
	node_hand_.param("virtual_obstacle/mission_flag_available", mission_flag_available_, mission_flag_available_);

	// Transforms TF and catch the static transform from vehicle to multibeam sensor
	tf_listener_.setExtrapolationLimit(ros::Duration(0.2));
	int count(0);
	ros::Time t;
	std::string err = "cannot find transform from robot_frame to multibeam_frame";
	tf_listener_.getLatestCommonTime(robot_frame_, multibeam_frame_, t, &err);

	while (!initialized_ && ros::ok())
	{
		try
		{
			tf_listener_.lookupTransform(robot_frame_, multibeam_frame_, t, robot_to_mb_);
			initialized_ = true;
		}
		catch (std::exception e)
		{
			tf_listener_.waitForTransform(robot_frame_, multibeam_frame_, ros::Time::now(), ros::Duration(1.0));
			tf_listener_.getLatestCommonTime(robot_frame_, multibeam_frame_, t, &err);
			count++;
			ROS_WARN("Cannot find transform from %s to %s", robot_frame_.c_str(), multibeam_frame_.c_str());
		}
		if (count > 10)
		{
			ROS_ERROR("No transform. Aborting...");
			exit(-1);
		}
	}
	ROS_WARN("Transform from %s to %s OK", robot_frame_.c_str(), multibeam_frame_.c_str());

	//=======================================================================
	// Subscribers
	//=======================================================================

	// /mission_handler/Formation (permits to know the moment when the mission starts)
	mission_handler_sub_ = node_hand_.subscribe("/mission_handler/Formation", 2, &VirtualObstacle::missionHandlerSubCallback, this);
	ros::Rate loop_rate(10);

	//Navigation data (feedback)
	nav_sts_sub_ = node_hand_.subscribe("/cola2_navigation/nav_sts", 1, &VirtualObstacle::navStsSubCallback, this);
	odometry_sub_ = node_hand_.subscribe("/pose_ekf_slam/odometry", 1, &VirtualObstacle::odomCallback, this);
	nav_sts_available_ = false;
	while (!nav_sts_available_)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}

	//=======================================================================
	// Wait for mission flag
	//=======================================================================
	if(!mission_flag_available_)
		ROS_INFO("%s: waiting for mission flag", ros::this_node::getName().c_str());

	while (!mission_flag_available_)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}
	ROS_INFO("%s: mission flag received", ros::this_node::getName().c_str());

	//use the service to get formation role
	ros::ServiceClient client_role = node_hand_.serviceClient<handler::GetRole>("/mission/getrole");
	handler::GetRole srv_role;

	if (client_role.call(srv_role))
	{
		ROS_INFO("%s: Service /mission/getrole OK", ros::this_node::getName().c_str());
		roles_ = srv_role.response.role;
	}
	else
	{
		ROS_ERROR("%s: failed to call service /mission/getrole", ros::this_node::getName().c_str());
	}

	//use the service to get formation data
	ros::ServiceClient client_formation = node_hand_.serviceClient<handler::GetFormation>("/mission/getformation");
	handler::GetFormation srv_formation;

	if (client_formation.call(srv_formation))
	{
		ROS_INFO("%s: Service /mission/getformation OK", ros::this_node::getName().c_str());
	}
	else
	{
		ROS_ERROR("%s: Failed to call service /mission/getformation", ros::this_node::getName().c_str());
	}

	for(int i = 0; i != roles_.size(); i++) {
		if(roles_[i].compare("lsv")==0)
		{
			formation_LSV_.index_mission_handler = i;
			formation_LSV_.ID = srv_formation.response.formation.ID[i];
			formation_LSV_.x = srv_formation.response.formation.x[i];
			formation_LSV_.y = srv_formation.response.formation.y[i];
			formation_LSV_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("gcv")==0)
		{
			formation_GCV_.index_mission_handler = i;
			formation_GCV_.ID = srv_formation.response.formation.ID[i];
			formation_GCV_.x = srv_formation.response.formation.x[i];
			formation_GCV_.y = srv_formation.response.formation.y[i];
			formation_GCV_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("cv1")==0)
		{
			formation_CV1_.index_mission_handler = i;
			formation_CV1_.ID = srv_formation.response.formation.ID[i];
			formation_CV1_.x = srv_formation.response.formation.x[i];
			formation_CV1_.y = srv_formation.response.formation.y[i];
			formation_CV1_.z = srv_formation.response.formation.z[i];
		}
		else if(roles_[i].compare("cv2")==0)
		{
			formation_CV2_.index_mission_handler = i;
			formation_CV2_.ID = srv_formation.response.formation.ID[i];
			formation_CV2_.x = srv_formation.response.formation.x[i];
			formation_CV2_.y = srv_formation.response.formation.y[i];
			formation_CV2_.z = srv_formation.response.formation.z[i];
		}
	}

////	for(int i = 0; i != roles_.size(); i++) {
////		if(roles_[i].compare("lsv")==0)
////		{
//			formation_LSV_.index_mission_handler = 1;//i;
//			formation_LSV_.ID = 1;//srv_formation.response.formation.ID[i];
//			formation_LSV_.x = 5.0;//srv_formation.response.formation.x[i];
//			formation_LSV_.y = 0.0;//srv_formation.response.formation.y[i];
//			formation_LSV_.z = -5.0;//srv_formation.response.formation.z[i];
////		}
////		else if(roles_[i].compare("gcv")==0)
////		{
//			formation_GCV_.index_mission_handler = 3;//i;
//			formation_GCV_.ID = 3;//srv_formation.response.formation.ID[i];
//			formation_GCV_.x = -15.0;//srv_formation.response.formation.x[i];
//			formation_GCV_.y = 0.0;//srv_formation.response.formation.y[i];
//			formation_GCV_.z = -5.0;//srv_formation.response.formation.z[i];
////		}
////		else if(roles_[i].compare("cv1")==0)
////		{
//			formation_CV1_.index_mission_handler = 5;//i;
//			formation_CV1_.ID = 5;//srv_formation.response.formation.ID[i];
//			formation_CV1_.x = -10.0;//srv_formation.response.formation.x[i];
//			formation_CV1_.y = 5.0;//srv_formation.response.formation.y[i];
//			formation_CV1_.z = -10.0;//srv_formation.response.formation.z[i];
////		}
////		else if(roles_[i].compare("cv2")==0)
////		{
//			formation_CV2_.index_mission_handler = 4;//i;
//			formation_CV2_.ID = 4;//srv_formation.response.formation.ID[i];
//			formation_CV2_.x = -10.0;//srv_formation.response.formation.x[i];
//			formation_CV2_.y = -5.0;//srv_formation.response.formation.y[i];
//			formation_CV2_.z = -10.0;//srv_formation.response.formation.z[i];
////		}
////	}

	//multibeam
	mb_scan_sub_input_ = node_hand_.subscribe("/multibeam_scan_input", 1, &VirtualObstacle::mbCallback, this);

	//=======================================================================
	// Publishers
	//=======================================================================
	mb_scan_pub_output_ = node_hand_.advertise<sensor_msgs::LaserScan> ("/multibeam_scan_output", 2, true);
}

/// Callbacks ==================================================================
void VirtualObstacle::mbCallback(const sensor_msgs::LaserScanConstPtr &scan)
{
	double obst_size = 4.0;
	double angle_min_obst, angle_max_obst;
	double angle_i;

	if(!obst_zone_left_ && !obst_zone_right_)
	{
		mb_scan_pub_output_.publish(scan);
	}
	else
	{
        if(obst_zone_left_)
        {
            sensor_msgs::LaserScan mbscan = *scan;
            if(formation_CV1_.z < 0.0)
            {
				angle_max_obst = atan((formation_CV1_.y+obst_size/2.)/(-formation_CV1_.z-last_pose_z_));
				angle_min_obst = atan((formation_CV1_.y-obst_size/2.)/(-formation_CV1_.z-last_pose_z_));
            }
            else
            {
            	angle_max_obst = atan((formation_CV1_.y+obst_size/2.)/(last_altitude_-formation_CV1_.z));
            	angle_min_obst = atan((formation_CV1_.y-obst_size/2.)/(last_altitude_-formation_CV1_.z));
            }

//            std::cout << "angle_min_obst:" << angle_min_obst << std::endl;
//            std::cout << "angle_max_obst:" << angle_max_obst << std::endl;
//            std::cout << "formation_CV1_.y:" << formation_CV1_.y << std::endl;
//            std::cout << "formation_CV1_.z:" << formation_CV1_.z << std::endl;

			angle_i = mbscan.angle_min;
			// Flag readings as maxrange
			for (int i=0; i<mbscan.ranges.size(); i++)
			{
				if(angle_i >= angle_min_obst && angle_i <= angle_max_obst)
				{
					if(formation_CV1_.z < 0.0)
						mbscan.ranges[i] = (-formation_CV1_.z-last_pose_z_)/cos(angle_i);
					else
						mbscan.ranges[i] = (last_altitude_-formation_CV1_.z)/cos(angle_i);
				}
				angle_i += mbscan.angle_increment;
			}
			mb_scan_pub_output_.publish(mbscan);
		}

		if(obst_zone_right_)
		{
			sensor_msgs::LaserScan mbscan = *scan;
			if(formation_CV2_.z < 0.0)
			{
				angle_max_obst = atan((formation_CV2_.y+obst_size/2.)/(-formation_CV2_.z-last_pose_z_));
				angle_min_obst = atan((formation_CV2_.y-obst_size/2.)/(-formation_CV2_.z-last_pose_z_));
			}
			else
			{
				angle_max_obst = atan((formation_CV2_.y+obst_size/2.)/(last_altitude_-formation_CV2_.z));
				angle_min_obst = atan((formation_CV2_.y-obst_size/2.)/(last_altitude_-formation_CV2_.z));
			}

			//            std::cout << "angle_min_obst:" << angle_min_obst << std::endl;
			//            std::cout << "angle_max_obst:" << angle_max_obst << std::endl;
			//            std::cout << "formation_CV2_.y:" << formation_CV2_.y << std::endl;
			//            std::cout << "formation_CV2_.z:" << formation_CV2_.z << std::endl;

			angle_i = mbscan.angle_min;
			// Flag readings as maxrange
			for (int i=0; i<mbscan.ranges.size(); i++)
			{
				if(angle_i >= angle_min_obst && angle_i <= angle_max_obst)
				{
					if(formation_CV2_.z < 0.0)
						mbscan.ranges[i] = (-formation_CV2_.z-last_pose_z_)/cos(angle_i);
					else
						mbscan.ranges[i] = (last_altitude_-formation_CV2_.z)/cos(angle_i);
				}
				angle_i += mbscan.angle_increment;
			}
			mb_scan_pub_output_.publish(mbscan);
		}
	}
}

void VirtualObstacle::odomCallback(const nav_msgs::OdometryConstPtr &odom_msg)
{
	if(!nav_sts_available_)
		nav_sts_available_ = true;
	tf::poseMsgToTF(odom_msg->pose.pose, last_pose_);

	last_pose_z_ = odom_msg->pose.pose.position.z;

	if(create_virt_obst_)
	{
        checkObstZones(odom_msg);
		if(obst_zone_left_ || obst_zone_right_)
		{
			ROS_WARN("%s: obstacle zone", ros::this_node::getName().c_str());
		}
		else
        {
            //ROS_DEBUG("%s: no obstacle zone", ros::this_node::getName().c_str());
        }
	}
}

void VirtualObstacle::navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg)
{
	last_altitude_ = nav_sts_msg.altitude;
}

void VirtualObstacle::missionHandlerSubCallback(const morph_msgs::Formation &mission_handler_formation_msg)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

void VirtualObstacle::checkObstZones(const nav_msgs::OdometryConstPtr &odom_msg)
{
	if(sqrt(pow(-132.0-odom_msg->pose.pose.position.x,2.0)+pow(7.0-odom_msg->pose.pose.position.y,2.0))<4.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	{
		obst_zone_left_ = true;
		obst_zone_right_ = false;
	}
	else if(sqrt(pow(-135.0-odom_msg->pose.pose.position.x,2.0)+pow(45.0-odom_msg->pose.pose.position.y,2.0))<4.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	{
		obst_zone_left_ = true;
		obst_zone_right_ = false;
	}
	else
	{
		obst_zone_left_ = false;
		obst_zone_right_ = false;
	}

	//  if(sqrt(pow(-115.0-odom_msg->pose.pose.position.x,2.0)+pow(36.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//  {
	//      obst_zone_left_ = true;
	//  }
	//  else if(sqrt(pow(-125.0-odom_msg->pose.pose.position.x,2.0)+pow(42.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//  {
	//      obst_zone_right_ = true;
	//  }
	//  else if(sqrt(pow(-144.0-odom_msg->pose.pose.position.x,2.0)+pow(64.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//  {
	//      obst_zone_right_ = true;
	//  }
	//  else if(sqrt(pow(-94.0-odom_msg->pose.pose.position.x,2.0)+pow(71.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//  {
	//      obst_zone_left_ = true;
	//  }
	//  else if(sqrt(pow(-82.0-odom_msg->pose.pose.position.x,2.0)+pow(64.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//  {
	//      obst_zone_right_ = true;
	//  }
	//  else
	//    {
	//      obst_zone_left_ = false;
	//        obst_zone_right_ = false;
	//    }

	/*  if(sqrt(pow(-138.0-odom_msg->pose.pose.position.x,2.0)+pow(28.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
    {
        obst_zone_left_ = true;
        obst_zone_right_ = false;
    }
    else if(sqrt(pow(-118.0-odom_msg->pose.pose.position.x,2.0)+pow(4.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
    {
        obst_zone_left_ = false;
        obst_zone_right_ = true;
    }
    else if(sqrt(pow(-102.0-odom_msg->pose.pose.position.x,2.0)+pow(-5.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
    {
        obst_zone_left_ = true;
        obst_zone_right_ = false;
    }
    else if(sqrt(pow(-94.0-odom_msg->pose.pose.position.x,2.0)+pow(32.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
    {
        obst_zone_left_ = false;
        obst_zone_right_ = true;
    }
//  else if(sqrt(pow(-135.0-odom_msg->pose.pose.position.x,2.0)+pow(37.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
//  {
//      obst_zone_left_ = true;
//      obst_zone_right_ = false;
//  }
//  else if(sqrt(pow(-111.0-odom_msg->pose.pose.position.x,2.0)+pow(7.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
//  {
//      obst_zone_left_ = true;
//      obst_zone_right_ = false;
//  }
//  else if(sqrt(pow(-95.0-odom_msg->pose.pose.position.x,2.0)+pow(-2.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
//  {
//      obst_zone_left_ = false;
//      obst_zone_right_ = true;
//  }
    else
    {
        obst_zone_left_ = false;
        obst_zone_right_ = false;
    }*/

	//  if(sqrt(pow(35.0-odom_msg->pose.pose.position.x,2.0)+pow(-24.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > -3.0-2.0 && odom_msg->pose.pose.position.x < -3.0+2.0 && odom_msg->pose.pose.position.y > -22.0-2.0 && odom_msg->pose.pose.position.y < -22.0+2.0)
	//      obst_zone_right_ = true;
	//  else if(sqrt(pow(2.0-odom_msg->pose.pose.position.x,2.0)+pow(-21.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > 36.0-2.0 && odom_msg->pose.pose.position.x < 36.0+2.0 && odom_msg->pose.pose.position.y > -23.0-2.0 && odom_msg->pose.pose.position.y < -23.0+2.0)
	//      obst_zone_right_ = true;
	//    else if(sqrt(pow(6.0-odom_msg->pose.pose.position.x,2.0)+pow(-22.0-odom_msg->pose.pose.position.y,2.0))<2.0)//(odom_msg->pose.pose.position.x > 36.0-2.0 && odom_msg->pose.pose.position.x < 36.0+2.0 && odom_msg->pose.pose.position.y > -23.0-2.0 && odom_msg->pose.pose.position.y < -23.0+2.0)
	//      obst_zone_left_ = true;
	//  else
	//    {
	//      obst_zone_left_ = false;
	//        obst_zone_right_ = false;
	//    }
}

/// MAIN NODE FUNCTION =========================================================
int main(int argc, char** argv){

	// Init ROS node
	ros::init(argc, argv, "virtual_obstacle");
	ros::NodeHandle private_nh("~");

	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	   ros::console::notifyLoggerLevelsChanged();
	}

	// Constructor
	VirtualObstacle mapper;

	// Spin
	ros::spin();

	// Exit main function without errors
	return 0;
}
