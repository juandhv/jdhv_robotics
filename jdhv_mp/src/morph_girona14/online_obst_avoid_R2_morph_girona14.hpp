/*
 * online_obst_avoid_R2_morph_girona14.h
 *
 *  Created on: Apr 9, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Obstacle avoidance for CVi vehicles in MORPH project.
 *  Octomaps as navigation maps.
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseArray.h>
// ROS messages
#include <auv_msgs/NavSts.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>
#include <string>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include "morph_girona14_planners_headers.hpp"

#include <signal.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//MORPH
#include <morph_msgs/follow_sectionActionFeedback.h>
#include <morph_msgs/Formation.h>
#include <handler/GetFormation.h>
#include <handler/GetRole.h>
#include <acomms/AddressDurationPair.h>
#include <math.h>

//Eigen
#include <Eigen/Dense>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;

typedef struct{
	int index_mission_handler;
	int ID;
	double x;
	double y;
	double z;
}formation_info;

//!  OmOnlinePlanner class.
/*!
 * Octomap Online Planner.
 * Plan and visualize a trajectory given an initial and a final configuration. The map is assume as known and is represented
 * by an octomap.
*/
class OnlineObstAvoidR2 {

public:
	//! Constructor.
	OnlineObstAvoidR2(std::string planner_name);
	
	//! Draw the path.
	/*!
	 * Publish the path as markers to be visualized in rviz
	 */
	void publishPathMarkers();

	//! Nav Sts
	void navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg);

	//! Follow section feedback
	void followSectionSubCallback(const morph_msgs::follow_sectionActionFeedback &foll_sect_feedback_msg);

	void gcvRofYawSubCallback(const std_msgs::Float64 &gcv_rof_yaw_msg);

	//! mission_handler_formation
	void missionHandlerFormationSubCallback(const morph_msgs::Formation &mission_handler_formation_msg);

	void missionFlagSubCallback(const std_msgs::Bool &mission_flag);

	void driverRangeSubCallback(const acomms::AddressDurationPair &driver_range_msg);

	void IdCallback(const std_msgs::Int8 &id_msg);

	void timerCallback(const ros::TimerEvent &e);

	bool isCollision(const std::vector<double> state, double cube_edge_length);

private:

	//ROS
	ros::NodeHandle node_hand_;

	ros::Publisher path_marker_pub_, path_pose_pub_, cv1_dy_pub_, cv2_dy_pub_;
	ros::Subscriber nav_sts_sub_, follow_sect_sub_, mission_flag_sub_, mission_handler_sub_, driver_range_sub_, ID_sub_;

	ros::Timer plann_timer_;

	//OMPL
	ob::PathPtr path_result_;
	boost::shared_ptr<og::PathGeometric> geometric_path_result_;
	og::SimpleSetupPtr simple_setup_;

	//Online planner
	Eigen::Vector3f last_nav_sts_data_pos_, current_nav_sts_data_pos_, start_pos_, goal_pos_;
	Eigen::Vector3f dir_motion_, orth_dir_motion_, pos_obst_cv1_, pos_obst_cv2_;
	std::vector<double> planning_bounds_;
	std::list< std::vector<double> > increm_path_;
	std::string planner_name_, auv_name_;
	bool simple_setup_flag_;
	double exploration_altitue_, x_dist_rov_, y_dist_rov_, z_dist_rov_, dist_obst_cv1_, dist_obst_cv2_;
	bool laser_octomap_flag_;
	bool rosbags_flag_;

	//Navigation information
	Eigen::Vector3f nav_sts_data_pos_;
	Eigen::Vector3f nav_sts_data_ori_;

	//Flags
	bool nav_sts_available_;
	bool mission_flag_available_;

	//follow_section feeback
	double line_angle_, current_line_angle_, dist_lsv_to_cv1_, dist_lsv_to_cv2_, last_altitude_, last_pose_z_;
	std_msgs::Float64 cv1_dy_, cv2_dy_;
	geometry_msgs::PoseArray pose_path_;
	std::vector<string> roles_;
	formation_info formation_LSV_, formation_GCV_, formation_CV1_, formation_CV2_;
	int ID_;

	//Octomap
	double octree_res_;
	AbstractOcTree* tree_;
	OcTree* octree_;
};

void stopNode(int sig);
