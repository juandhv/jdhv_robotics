/*
 * state_validity_checker_morph_SE2_girona14.cpp
 *
 *  Created on: Mar 13, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (position state) is collision-free.
 *  The workspace is represented by an octomap.
 */

#include "state_validity_checker_R2_morph_girona14.hpp"

OmStateValidityChecker::OmStateValidityChecker(const ob::SpaceInformationPtr &si, const double exploration_altitue, const double planning_depth, const bool laser_octomap_flag) :
	ob::StateValidityChecker(si)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	exploration_altitue_ = exploration_altitue;
	planning_depth_ = planning_depth;

	if(laser_octomap_flag)
		serv_name = "/laser_octomap/get_binary";
	else
		serv_name = "octomap_binary";
	octree_ = NULL;
	length_g500_ = 2.0;

	ROS_INFO("Requesting the map from %s...", node_hand_.resolveName(serv_name).c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_){
			ROS_INFO("Octomap received (%zu nodes, %f m res)", octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmStateValidityChecker::isValid(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;
	const ob::SE2StateSpace::StateType *state_se2;

	// extract the component of the state and cast it to what we expect
	state_se2 = state->as<ob::SE2StateSpace::StateType>();

//	std::cout << "*** pos_query: [" << state_se2->getX() << ", " << state_se2->getY() << ", " << planning_depth_ << "]" << std::endl;

	for(double xi = state_se2->getX()-(1.0*length_g500_);xi <= state_se2->getX()+(1.0*length_g500_);xi=xi+octree_res_)
		for(double yi = state_se2->getY()-(1.0*length_g500_);yi <= state_se2->getY()+(1.0*length_g500_);yi=yi+octree_res_)
			for(double zi = planning_depth_-(length_g500_/2.0);zi <= planning_depth_+(length_g500_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = true;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						break;
					}
				}
			}
	return !collision;
}

int OmStateValidityChecker::isCollision(Eigen::Vector3f pos_query) const
{
	OcTreeNode* result;
	point3d query;
	int collision(0);
	double node_occupancy;
	double x_max, y_max, z_max;
	double x_min, y_min, z_min;

	octree_->getMetricMax(x_max, y_max, z_max);
	octree_->getMetricMin(x_min, y_min, z_min);

	std::cout << "*** Min values: " << x_min << ", " << y_min << ", " <<  z_min << " ***" << std::endl;
	std::cout << "*** Max values: " << x_max << ", " << y_max << ", " <<  z_max << " ***" << std::endl;
	std::cout << "*** pos_query: " << pos_query << std::endl;

	for(double xi = pos_query[0]-(1.0*length_g500_);xi <= pos_query[0]+(1.0*length_g500_);xi=xi+octree_res_)
		for(double yi = pos_query[1]-(1.0*length_g500_);yi <= pos_query[1]+(1.0*length_g500_);yi=yi+octree_res_)
			for(double zi = planning_depth_-(length_g500_/2.0);zi <= planning_depth_+(length_g500_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(xi>x_max || yi>y_max || zi>z_max || xi<x_min || yi<y_min || zi<z_min){
					collision = -1;
					break;
				}
				else if(result == NULL){
					collision = 1;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = 1;
						break;
					}
				}
			}

	return collision;
}

double OmStateValidityChecker::cost(const ob::State *state) const
{
	point3d query;
	OcTreeNode* result;
	bool collision;
	double cost, bottom_z, z0, node_occupancy;

	// Extract the component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = state->as<ob::RealVectorStateSpace::StateType>();

	// Find the bottom
	z0 = pos->values[2];
	collision = false;
	while(!collision)
	{
		for(double xi = pos->values[0]-(length_g500_/2.0);xi <= pos->values[0]+(length_g500_/2.0);xi=xi+octree_res_)
			for(double yi = pos->values[1]-(length_g500_/2.0);yi <= pos->values[1]+(length_g500_/2.0);yi=yi+octree_res_)
				for(double zi = z0-(length_g500_/2.0);zi <= z0+(length_g500_/2.0);zi=zi+octree_res_){
					query.x() = xi;
					query.y() = yi;
					query.z() = zi;
					result = octree_->search (query);

					if(result == NULL){
						bottom_z = zi - octree_res_;
						collision = true;
						break;
					}
					else{
						node_occupancy = result->getOccupancy();
						if (node_occupancy > 0.4)
						{
							bottom_z = zi - octree_res_;
							collision = true;
							break;
						}
					}
				}
			z0 = z0 + octree_res_;
		}
	//

	if(pos->values[2] > (bottom_z - exploration_altitue_))
		cost = 10.0;
	else
		cost = ((bottom_z - pos->values[2]) / bottom_z) * 10.0;

//	ROS_INFO("Checking cost:");
//	std::cout << "Z:" << pos->values[2] << std::endl;
//	std::cout << "bottom_z:" << bottom_z << std::endl;
//	std::cout << "cost:" << cost << std::endl;

	return cost;
}

OmStateValidityChecker::~OmStateValidityChecker()
{
    delete octree_;
}
