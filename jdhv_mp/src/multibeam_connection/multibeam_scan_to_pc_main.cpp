/*
 * multibeam_scan_to_pc_main.cpp
 *
 * Convert Multibeam information to Point Cloud in order to be used by the octomap_server.
 * Based on laserscan_to_pc_converter.cpp (Enric)
 *
 *  Created on: Aug 26, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 */

#include "multibeam_scan_to_pc.hpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "multibeam_scan_to_pc", ros::init_options::AnonymousName);
    ROS_INFO_STREAM("Starting multibeam_scan_to_pc Node");

    Mb2PcConverter converter;

    ros::spin();

    return 0;
}
