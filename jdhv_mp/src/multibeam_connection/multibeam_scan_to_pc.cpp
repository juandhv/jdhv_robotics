/*
 * multibeam_scan_to_pc.cpp
 *
 * Convert Multibeam information to Point Cloud in order to be used by the octomap_server.
 * Based on laserscan_to_pc_converter.cpp (Enric)
 *
 *  Created on: Aug 26, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 */

#include "multibeam_scan_to_pc.hpp"

Mb2PcConverter::Mb2PcConverter():
	mission_flag_available_(false)
{
	// Override SIGINT handler
	signal(SIGINT, stopNode);

	//mission_flag_available_ = false;
	node_hand_.param("multibeam_scan_to_pc/mission_flag", mission_flag_available_, mission_flag_available_);

	//=======================================================================
	// Subscribers
	//=======================================================================
    if(!mission_flag_available_)
    {
    	//Mission Flag (feedback)
    	mission_flag_sub_ = node_hand_.subscribe("/jdhv_robotics/mission_flag", 1, &Mb2PcConverter::missionFlagSubCallback, this);
    	// Waiting for mission flag
    	ros::Rate loop_rate(10);
    	while (!mission_flag_available_)
    	{
    		ros::spinOnce();
    		loop_rate.sleep();
    	}
    	//ros::Duration(10).sleep();
    }

    mb_scan_sub_ = node_hand_.subscribe<sensor_msgs::LaserScan>("/multibeam_scan", 1, &Mb2PcConverter::scanCallback, this);

    //=======================================================================
    // Publishers
    //=======================================================================
    point_cloud_pub_ = node_hand_.advertise<sensor_msgs::PointCloud2>("/multibeam_cloud", 100, false);

#ifdef DEBUG_MSG
    ROS_INFO("Subscribed to %s", mb_scan_sub_.getTopic().c_str());
    ROS_INFO("Publishing to %s", point_cloud_pub_.getTopic().c_str());
#endif
}

void Mb2PcConverter::missionFlagSubCallback(const std_msgs::Bool &mission_flag)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

void Mb2PcConverter::scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    try {
		mb_projector_.projectLaser(*scan, cloud_); // transform in same frame
        point_cloud_pub_.publish(cloud_);
#ifdef DEBUG_MSG
        ROS_INFO("Got laser scan with %ld rays, publishing PC with %ld points", scan->ranges.size(), cloud_.data.size());
        ROS_INFO_STREAM("Ranges: ");

        std::cout << "(ranges size: " << scan->ranges.size() << ")" << std::endl;
        //std::copy( scan->ranges.begin(), scan->ranges.end(), std::ostream_iterator<double>(std::cout,", ") );
        for (int i=0; i<scan->ranges.size(); ++i) {
            std::cout << "i=" << i << ", r=" << scan->ranges[i] << "; ";
        }
        std::cout << std::endl;
#endif
    }
    catch (std::exception& e) {
        std::cout << "Could not convert scan to PC: " << e.what() << std::endl;
    }
}

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}
