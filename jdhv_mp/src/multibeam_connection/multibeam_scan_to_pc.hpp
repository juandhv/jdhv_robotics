/*
 * multibeam_scan_to_pc_main.h
 *
 * Convert Multibeam information to Point Cloud in order to be used by the octomap_server.
 * Based on laserscan_to_pc_converter.cpp (Enric)
 *
 *  Created on: Aug 26, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 */

//Standard includes
#include <iostream>
#include <iterator>

#include <signal.h>

//ROS
#include <ros/ros.h>

// ROS messages
#include <geometry_msgs/Pose.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <visualization_msgs/MarkerArray.h>

#include <std_msgs/Bool.h>

// ROS LaserScan tools
#include <laser_geometry/laser_geometry.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//#define DEBUG_MSG

class Mb2PcConverter {
     public:
        Mb2PcConverter();
        void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
        void missionFlagSubCallback(const std_msgs::Bool &mission_flag);
     private:
        ros::NodeHandle node_hand_;
        laser_geometry::LaserProjection mb_projector_;

        ros::Subscriber mb_scan_sub_, mission_flag_sub_;
        ros::Publisher point_cloud_pub_;

        sensor_msgs::PointCloud2 cloud_;

        bool mission_flag_available_;
};

void stopNode(int sig);
