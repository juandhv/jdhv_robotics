"""
Created on Nov 11, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Test of a basic implementation of Rapidly-exploring Random Trees (RRT). 
A point mobile robot is assumed to be moving in 2D workspace.
"""

from Maps import Map2D
from mission import Mission

from RRT import RRT
from RRTConnect import RRTConnect
from RRTConnectGreedy import RRTConnectGreedy
from RRTGreedy import RRTGreedy
from RRG import RRG
from RRTStar import RRTStar
from Anytime_RRT_RRTStar import Anytime_RRT_RRTStar

from point_model_2d import PointModel2D

import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':

    #===========================================================================
    # Load planner and mission parameters
    #===========================================================================
    test_id = "t1_auv" #"t2_car", "t1_auv", t4_car, t2_auv
    mission = Mission(test_id)
    mission.load_parameters()
    #===========================================================================
    # Workspace definition
    #===========================================================================
    ws = Map2D("XY", mission.get_ws_bounds())
#     ws.constructFromData(num_obst=6, min_radius=150.0, max_radius=200.0, max_map_dim=max_map_dim)
    ws.constructFromFiles(test_id)
    ws.visualizeMap()
    #===========================================================================
    # Differential model of a point in a 2-dimensional (2D) workspace
    #===========================================================================
    sys_model_2d = PointModel2D(step_size = mission.get_step_size())
    #===========================================================================
    # Creation of the object RRT
    #===========================================================================
    if mission.planner_type_ == "RRT":
        planner = RRT(ws,
                      num_attempts = mission.get_num_attempts(),
                      epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                      step_size = mission.get_step_size(),
                      diff_model = sys_model_2d)
    if mission.planner_type_ == "RRTGreedy":
        planner = RRTGreedy(ws,
                            num_attempts = mission.get_num_attempts(),
                            epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                            step_size = mission.get_step_size(),
                            diff_model = sys_model_2d,
                            goal_prob = mission.get_goal_prob())
    elif mission.planner_type_ == "RRTConnect":
        planner = RRTConnect(ws,
                             num_attempts = mission.get_num_attempts(),
                             epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                             step_size = mission.get_step_size(),
                             diff_model = sys_model_2d)
    elif mission.planner_type_ == "RRTConnectGreedy":
        planner = RRTConnectGreedy(ws,
                                   num_attempts = mission.get_num_attempts(),
                                   epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                                   step_size = mission.get_step_size(),
                                   diff_model = sys_model_2d)
    elif mission.planner_type_ == "RRG":
        planner = RRG(ws,
                      num_attempts = mission.get_num_attempts(),
                      epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                      step_size = mission.get_step_size(),
                      diff_model = sys_model_2d,
                      goal_prob = mission.get_goal_prob())
    elif mission.planner_type_ == "RRTStar":
        planner = RRTStar(ws,
                          num_attempts = mission.get_num_attempts(),
                          max_comp_time = mission.get_max_comp_time(),
                          epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                          step_size = mission.get_step_size(),
                          diff_model = sys_model_2d,
                          goal_prob = mission.get_goal_prob())
    elif mission.planner_type_ == "Anytime_RRT_RRTStar":
        planner = Anytime_RRT_RRTStar(ws,
                                      num_attempts = mission.get_num_attempts(),
                                      max_comp_time = mission.get_max_comp_time(),
                                      epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                                      step_size = mission.get_step_size(),
                                      diff_model = sys_model_2d,
                                      goal_prob = mission.get_goal_prob())
    #===========================================================================
    # Build the RRT and attempt to solve a query
    #===========================================================================
    q_init_vec = np.array(mission.get_init_conf_2D())
    q_goal_vec = np.array(mission.get_goal_conf_2D())
    
# #     #q_init_vec = ws.generateRandomConfCar()
#     q_init_vec[0] = 75.
#     q_init_vec[1] = 950.
# #     #q_init_vec[2] = -np.pi / 2.
# #     q_goal_vec = ws.generateRandomConfCar()
#     q_goal_vec[0] = 850.
#     q_goal_vec[1] = 150.
# #     #q_goal_vec[2] = -np.pi / 2.

    planner.solveQuery(q_init_vec, q_goal_vec)
    
    plt.ioff()
    plt.show()
    
    pass