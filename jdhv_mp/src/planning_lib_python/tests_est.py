"""
Created on Nov 11, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Test of a basic implementation of Expansive-Spaces Trees (EST). 
A point mobile robot is assumed to be moving in 2D workspace.
"""

from Maps import Map2D
from EST import EST
import numpy as np

import matplotlib.pyplot as plt

if __name__ == '__main__':
    
    #===========================================================================
    # General parameters
    #===========================================================================
    num_attempts = 200 #number of attempts
    max_map_dim = 1000.0 #as the workspace is assumed to be a square, this is the length of each side
    neighborhood_dist = 200 #number of closest neighbors
    #===========================================================================
    # Workspace definition
    #===========================================================================
    ws = Map2D()
#    ws.constructFromData(num_obst=4, min_radius=150.0, max_radius=200.0, max_map_dim=max_map_dim)
    ws.constructFromFiles(max_map_dim)
    ws.visualizeMap()
    #===========================================================================
    # Creation of the object EST
    #===========================================================================
    est = EST(ws, max_map_dim, num_attempts, neighborhood_dist)
    #===========================================================================
    # Building the EST and attempting to solve a query
    #===========================================================================
    q_init_vec = ws.generateRandomConf()
    q_goal_vec = ws.generateRandomConf()

    est.solveQuery(q_init_vec, q_goal_vec)
    
    plt.ioff()
    plt.show()
    
    pass