"""
Created on Aug 13, 2013

Purpose:
Graph theory related classes and functions.
Used in representing a configuration roadmaps for sampling-based algorithms.

Classes:
Graph - simple undirected graph

Based on Andres Jaramillo-Botero's graph class (Caltech 2006)
"""

import numpy as np

######### Class definitions #########

class Graph(object):
        """Graph is a simple graph without any multiple (parallel) edges
        or self-loops.  Attempting to add either will not change
        the graph and will not report an error.
        Derived from Aric Hagberg's Networkx lib     
        """
        def __init__(self):
            """Initialize Graph. 
            """
            self.adj_ = {}  # empty adjacency hash
            self.confs_ = {}
            self.vis_edges_handles_ = {}
            self.name_ = "Graph"
            
        def __str__(self):
            return self.name_
 
        def __iter__(self):
            """Return an iterator over the nodes in G.
 
            This is the iterator for the underlying adjacency dict.
            (Allows the expression 'for n in G')
            """
            return self.adj_.iterkeys()
 
        def __contains__(self,n):
            """Return True if n is a node in graph.
 
            Allows the expression 'n in G'.
 
            Testing whether an unhashable object, such as a list, is in the
            dict datastructure (self.adj_) will raise a TypeError.
            Rather than propagate this to the calling method, just
            return False.
 
            """
            try:
                return self.adj_.__contains__(n)
            except TypeError:
                return False
         
        def __len__(self):
            """Return the number of nodes in graph."""
            return len(self.adj_)
 
        def __getitem__(self, n):
            """Return the configuration (vector) information of node n.
 
            This provides graph G the natural property that G[n]
            """
            try:
                return self.confs_[n]
            except (KeyError, TypeError):
                print "node %s not in graph"%n
                raise
        
        def getNeighborsIter(self,n):
            """Return an iterator over all neighbors of node n.  """
            try:
                return self.adj_[n].iterkeys()
            except KeyError:
                raise "node %s not in graph"%n
        
        def getConfQ(self,n):
            """Return configuration of node n.  """
            return self.confs_[n]
     
        def getNeighbors(self, n):
            """Return a list of nodes connected to node n.  """
            # return lists now, was dictionary for with_labels=True
            return list(self.getNeighborsIter(n))
 
        def addNode(self, n, q_vec):
            """
            Add a single node n to the graph.
            The node n can be any hashable object except None. But most cases in this
            context will be an integer that identifies the node.
            
            q is the configuration added to the RoadMap
            When a parent node is provided, the graph is considered a tree
 
            Example:
 
            >>> G=Graph()
            >>> G.addNode(1)
            >>> G.addNode('Hello')
 
            """
            if n not in self.adj_:
                self.adj_[n] = {}
                self.confs_[n] = q_vec 
                
        def removeNode(self, n):
            del self.adj_[n]
            del self.confs_[n]
 
        def addEdge(self, u, v=None, cost=None, input_control=None, vis_edges_handle = None):
            """Add a single edge (u,v) to the graph.
            >> G.add_edge(u,v)
            and
            >>> G.add_edge( (u,v) )
            are equivalent forms of adding a single edge between nodes u and v.
            The nodes u and v will be automatically added if not already in
            the graph.  They must be a hashable (except None) Python object.
     
            The following examples all add the edge (1,2) to graph G.
 
            >>> G=Graph()
            >>> G.add_edge( 1, 2 )          # explicit two node form
            >>> G.add_edge( (1,2) )         # single edge as tuple of two nodes
 
            """
            if v is None:
                (u,v)=u  # no v given, assume u is an edge tuple
            # add nodes
            if u not in self.adj_:
                self.adj_[u]={}
            if v not in self.adj_:
                self.adj_[v]={}
            # don't create self loops, fail silently, nodes are still added
            if u==v:
                return
            self.adj_[u][v]=cost
            self.adj_[v][u]=cost
            
            if u not in self.vis_edges_handles_:
                self.vis_edges_handles_[u] = {}
            if vis_edges_handle is not None:
                self.vis_edges_handles_[u][v] = vis_edges_handle

        def removeEdge(self, u, v):
            del self.adj_[u][v]
 
        def hasNode(self,n):
            """Return True if graph has node n.
 
            (duplicates self.__contains__)
            "n in G" is a more readable version of "G.has_node(n)"?
            """
            try:
                return self.adj_.__contains__(n)
            except TypeError:
                return False
 
        def hasEdge(self, u, v=None):
            """Return True if graph contains the edge u-v. """
            if  v is None:
                (u,v)=u    # split tuple in first position
            return self.adj_.has_key(u) and self.adj_[u].has_key(v)
        
        def getEdgeCost(self, u, v):
            """Return the cost of a edge between two nodes. """
            if self.hasEdge(u, v):
                return self.adj_[u][v]
            else:
                return None
 
        def getOrder(self):
            """Return the order of a graph = number of nodes."""
            if len(self.adj_)!=0:
                return max(self.adj_.keys())+1
            else:
                return len(self.adj_)
        
def aStar(graph, q_start, q_goal):
    """
    A* algorithm (based on pseudocode given in Wikipedia)
    
    q_start: start configuration
    q_goal: goal configuration
    """
    DebugOutput = 0
    closedset = {}  # the empty set - set of nodes already evaluated.
    openset = {}
    openset[q_start] = None  # set containing the initial node. The set of tentative nodes to be evaluated.
    came_from = {}  # the empty map // The map of navigated nodes.

    g_score = {}
    g_score[q_start] = 0  # Cost from start along best known path.
    h_score = {}
    h_score[q_start] = estimateHeuristicCost(graph, q_start, q_goal)
    # Estimated total cost from start to goal through y.
    f_score = {}
    f_score[q_start] = h_score[q_start] 
 
    while len(openset.keys()) > 0:  # while openset is not empty
        x = openset.keys()[0]
        for xi in openset.keys():
            if f_score[xi] < f_score[x]:
                x = xi
        del openset[x]
        
        if x == q_goal:
            if DebugOutput == 1:
                print "+++ complete Python implementation +++"
            return reconstructPath(graph, came_from, q_goal)
        
        closedset[x] = None  # add x to closedset
        if DebugOutput == 1:
            print "exploring neighbors of node:", x
    
        for y in graph.getNeighbors(x):  # foreach y in neighbor_nodes(x)
            if DebugOutput == 1:
                print y
            if y in closedset.keys():  # if y in closedset
                continue
            tentative_g_score = g_score[x] + graph.getEdgeCost(x, y)

            if y not in openset.keys():  # if y not in openset
                openset[y] = None  # add y to openset
                tentative_is_better = True
            elif tentative_g_score < g_score[y]:
                tentative_is_better = True
            else:
                tentative_is_better = False

            if tentative_is_better == True:
                came_from[y] = x
                g_score[y] = tentative_g_score
                h_score[y] = estimateHeuristicCost(graph, y, q_goal)
                f_score[y] = g_score[y] + h_score[y]
                
    return []

def estimateHeuristicCost(graph, qi, qj):
    """
    Estimate heuristic cost from one configuration to another.
    
    qi: first configuration
    qj: second configuration
    """
    qi_vec = graph.getConfQ(qi)
    qj_vec = graph.getConfQ(qj)
    return np.linalg.norm(qi_vec - qj_vec)
    
def reconstructPath(graph, came_from, current_node):
    """
    Reconstruct the resulting path in a A* algorithm
    """
    next_node = current_node
    path = []
        
    while True:
        if next_node in came_from.keys():
            path.append(next_node)
            next_node = came_from[next_node]
        else:
            path.append(next_node)
            break
    return path
