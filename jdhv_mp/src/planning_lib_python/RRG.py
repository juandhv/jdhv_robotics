"""
Created on Dec 5, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
Purpose: Basic implementation of Rapidly-exploring Random Graph (RRG). 
C-space --> R2.
"""

from tree import Tree
from graph import Graph
from RRT import RRT
import operator
import graph

import numpy as np

TRAPPED = 0
REACHED = 1
ADVANCED = 2

EPSILON_FOUND_DISTANCE = 30.0

class RRG(RRT):
    """
    Implementation of Rapidly-exploring Random Graph, basic RRG.
    """
    def __init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model, goal_prob):
        """
        Constructor
        
        ws: 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size: step size for each expansion
        """
        RRT.__init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model)
        self.goal_prob_ = goal_prob
        
        return
    
    def build(self, q_init_vec, q_goal_vec):
        """
        Build a RRT from a initial configuration
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Graph()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            q_rand_vec = self.ws_.generateRandomConf(self.goal_prob_, q_goal_vec)
            if self.extend('init', q_rand_vec) != TRAPPED:
                if self.ws_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec) < EPSILON_FOUND_DISTANCE:
                    self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_goal_vec)
                    self.q_init_tree_.addEdge(self.q_init_tree_.getOrder() - 1, self.q_init_tree_.getOrder() - 2, EPSILON_FOUND_DISTANCE)
                    result_path = graph.aStar(self.q_init_tree_, 0, self.q_init_tree_.getOrder() - 1)
                    result_path.reverse()
                    break
            attempt_i += 1
            print "i:", attempt_i
        return result_path
    
    def extend(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        q_near_vec = self.q_init_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.diff_model_.findInput(q_near_vec, q_vec)
        q_new_vec = self.diff_model_.calcNewConf(q_near_vec, u_q_near_to_q_rand)

        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_near_vec, q_new_vec):
            q_new = self.q_init_tree_.getOrder()
            
            self.q_init_tree_.addNode(q_new, q_new_vec)
            self.q_init_tree_.addEdge(q_near, q_new, self.step_size_)#edge from a node i to a node j with a cost c
            
            self.ws_.visualizeEdge(q_near_vec, q_new_vec,'-r')

            closest_neighbors_qi = self.findClosestNeighbors(q_new)
            for closest_qi in closest_neighbors_qi:
                closest_qi_vec = self.q_init_tree_.getConfQ(closest_qi[0])
                if q_near != closest_qi[0] and not self.q_init_tree_.hasEdge(q_new, closest_qi[0]) and self.ws_.checkPathLocalPlanner(q_new_vec, closest_qi_vec):
                    self.q_init_tree_.addEdge(q_new, closest_qi[0], closest_qi[1])#edge from a node i to a node j with a cost c

                    self.ws_.visualizeEdge(q_new_vec, closest_qi_vec,'-y')

            if self.ws_.dist(q_new_vec, q_vec) < EPSILON_FOUND_DISTANCE:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
#     def findClosestNeighbors(self, qi, num_close_neigh = None):
#         """
#         Find the k closest configurations to qi
#         
#         qi: current configuration
#         """
#         
#         if num_close_neigh == None:
#             num_close_neigh = self.num_close_neigh_
#         else:
#             num_close_neigh = self.q_init_tree_.getOrder()
# 
#         qi_vec = self.q_init_tree_.getConfQ(qi)
#         neighbors = {}
#         
#         #=======================================================================
#         # Calculate the distance to all of the neighbors
#         #=======================================================================
#         for qj in self.q_init_tree_:
#             if qj!=qi and self.q_init_tree_.hasEdge(qi, qj):
#                 neighbors[qj] = self.q_init_tree_.getEdgeCost(qi, qj)
#             elif qj!=qi:
#                 qj_vec = self.q_init_tree_.getConfQ(qj)
#                 dist = np.linalg.norm(qi_vec - qj_vec)
#                 neighbors[qj] = dist
# 
#         #=======================================================================
#         # Find the k closest neighbors 
#         #=======================================================================
#         closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
#         
#         return closest_neighbors[0:self.num_close_neigh_]
    
    def findClosestNeighbors(self, qi, ball_radius = None):
        """
        Find the k closest configurations to qi
         
        qi: current configuration
        """
         
        if ball_radius == None:
            ball_radius = 1.5 * self.step_size_
 
        qi_vec = self.q_init_tree_.getConfQ(qi)
         
        closest_neighbors = []
         
        #=======================================================================
        # Calculate the distance to all of the neighbors
        #=======================================================================
        for qj in self.q_init_tree_:
            if qj!=qi:
                if self.q_init_tree_.hasEdge(qi, qj):
                    cost_i_to_j = self.q_init_tree_.getEdgeCost(qi, qj)
                else:
                    qj_vec = self.q_init_tree_.getConfQ(qj)
                    cost_i_to_j = np.linalg.norm(qi_vec - qj_vec)
                if cost_i_to_j <= ball_radius:
                    closest_neighbors.append((qj, cost_i_to_j))
 
#         #=======================================================================
#         # Find the k closest neighbors 
#         #=======================================================================
#         closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
         
        return closest_neighbors