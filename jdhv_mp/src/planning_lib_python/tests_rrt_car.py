"""
Created on Nov 11, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Test of a basic implementation of Rapidly-exploring Random Trees (RRT). 
A point mobile robot is assumed to be moving in 2D workspace.
"""

from Mission import Mission

from Maps import Map2D
from RRTCar import RRTCar
from RRTStarCar import RRTStarCar
import numpy as np

import matplotlib.pyplot as plt 

if __name__ == '__main__':
    
    #===========================================================================
    # Load planner and mission parameters
    #===========================================================================
    test_id = "t4_car"
    mission = Mission(test_id)
    mission.load_parameters()
    #===========================================================================
    # General parameters
    #===========================================================================
    num_attempts = 20000 #number of attempts
    max_map_dim = 1000.0 #as the workspace is assumed to be a square, this is the length of each side
    step_size_time = 10.0 #seconds to extend
    #===========================================================================
    # Workspace definition
    #===========================================================================
    ws = Map2D("CLR", mission.get_ws_bounds())
#     ws.constructFromData(num_obst=6, min_radius=150.0, max_radius=200.0, max_map_dim=max_map_dim)
    ws.constructFromFiles(test_id)
    ws.visualizeMap()
    #===========================================================================
    # Creation of the object RRT
    #===========================================================================
    planner = RRTCar(ws, max_map_dim, num_attempts, step_size_time, goal_prob = 0.05)
#     planner = RRTStarCar(ws, max_map_dim, num_attempts, max_time_comp = 120.0, step_size_time = 10.0, goal_prob = 0.05, num_close_neigh = 4)
    
    #===========================================================================
    # Building the RRT and attempting to solve a query
    #===========================================================================
    q_init_vec = ws.generateRandomConfCar()
    q_init_vec[0] = 75.
    q_init_vec[1] = 650.
    q_init_vec[2] = -np.pi / 2.
    q_goal_vec = ws.generateRandomConfCar()
    q_goal_vec[0] = 900.
    q_goal_vec[1] = 150.
    q_goal_vec[2] = -np.pi / 2.

    planner.solveQuery(q_init_vec, q_goal_vec)
    
    plt.ioff()
    plt.show()
    
    pass