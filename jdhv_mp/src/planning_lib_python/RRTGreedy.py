"""
Created on Nov 20, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Rapidly-exploring Random Trees (RRT).
This implementation is a greedy one, where random configurations are directed to 
the goal with a probability p. 
C-space --> R2.
"""

from RRT import RRT

TRAPPED = 0
REACHED = 1
ADVANCED = 2

class RRTGreedy(RRT):
    """
    Implementation of Rapidly-exploring Random Trees, basic RRTGreedy.
    """
    
    def __init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model, goal_prob):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size: step size for each expansion
        goal_prob: probability to define next node to be the goal
        """
        RRT.__init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model)
        self.goal_prob_ = goal_prob