'''
Created on Aug 13, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of 2D workspace.
Random obstacles may be defined.
'''

# ROS imports
import rospy

import numpy as np
import math
import random

from matplotlib.path import Path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.nxutils as nx

CAR_LENGTH = 100.
CAR_WIDTH = 40

AUV_LENGTH = 16.
AUV_WIDTH = 5

class PointR2(object):
    """
    classdocs
    """

    def __init__(self, type_ws = "XY", dim = [], planning_depth = 0.0):
        """
        Constructor
        Creates a 2D workspace map including a specified number of obstacles
        """
        self.type_ws_ = type_ws
        self.dim_ = dim
        self.planning_depth_ = planning_depth
        self.state_val_checker_ = None
        return
    
    def setCspace(self, state_val_checker):
        self.state_val_checker_ = state_val_checker
        return
    
    def cleanCspace(self):
        del self.state_val_checker_
        return
    
    def constructFromData(self, num_obst = 5, min_radius = 20.0, max_radius = 80.0, max_map_dim = 500.0):
        self.num_obst_ = num_obst
        self.min_radius_ = min_radius
        self.max_radius_ = max_radius
        self.max_map_dim_ = max_map_dim
        
        codes_file = open('map_data/codes_file.txt', 'w')
        vertices_file = open('map_data/vertices_file.txt', 'w')
        
        plt.ion()
        self.fig_ = plt.figure()
        self.ax_ = self.fig_.add_subplot(111)
        
        self.lst_verts_ = []
        
        #Creating the specified number of obstacles
        for obst_count in range(self.num_obst_):
            
            #Obstacles with a radius greater than the given minimum value
            radius = 0.0
            while radius < self.min_radius_:
                radius = random.random() * self.max_radius_

            #Obstacles with at least 3 vertices
            num_vertices = 0
            while num_vertices < 3:
                num_vertices = int(random.random()*8)
            
            #Center of the obstacle
            x_origin = 0.0
            while x_origin < radius:
                x_origin = random.random() * (self.max_map_dim_-radius)
            
            y_origin = 0.0
            while y_origin < radius:
                y_origin = random.random() * (self.max_map_dim_-radius)
                
            #List of vertices
            
            overlapping = True
            while overlapping:
                overlapping = False
                
                #Random angles that corresponding to each vertex
                angles = []
                while len(angles) < num_vertices:
                    angles.append(random.random() * 2*np.pi)
                angles.sort()
                
                vertices = []
                codes = []
                for angle in angles:
                    if len(vertices) == 0:
                        codes.append(Path.MOVETO)
                    else:
                        codes.append(Path.LINETO)
                    vert = [radius*np.cos(angle) + x_origin, radius*np.sin(angle) + y_origin]
                    if self.checkCollision(np.array(vert)):
                        overlapping = True
                    vertices.append(vert)
                vertices.append(vertices[0])
                codes.append(Path.LINETO)
            
            self.lst_verts_.append({})
            self.lst_verts_[obst_count]['codes'] = codes
            self.lst_verts_[obst_count]['verts'] = np.array(vertices)
            
            for item in codes:
                codes_file.write("%s\n" % item)
            codes_file.write("*****\n")
            
            for item in vertices:
                for vertix in item:
                    vertices_file.write("%s " % vertix)
                vertices_file.write("\n" % item)
            vertices_file.write("*****\n")
        
        codes_file.close()
        vertices_file.close() 

        return
    
    def constructFromFiles(self, test_name="ti", max_map_dim = 1000):
        self.max_map_dim_ = max_map_dim
        
        plt.ion()
        self.fig_ = plt.figure()
        self.ax_ = self.fig_.add_subplot(111)
        
        self.lst_verts_ = []
        
        self.num_obst_ = 0
        codes = []
        vertices = []

        with open("map_data/codes_file_" + test_name + ".txt", 'r') as f:
            for line in f:
                items = line.split()
                if items[0] == '*****':
                    self.lst_verts_.append({})
                    self.lst_verts_[self.num_obst_]['codes'] = codes
                    self.num_obst_ = self.num_obst_ + 1
                    codes = []
                else:
                    codes.append(int(items[0]))
        
        self.num_obst_ = 0
        with open("map_data/vertices_file_" + test_name + ".txt", 'r') as f:
            for line in f:
                items = line.split()
                if items[0] == '*****':
                    self.lst_verts_[self.num_obst_]['verts'] = np.array(vertices)
                    self.num_obst_ = self.num_obst_ + 1
                    vertices =[]
                else:
                    vertices.append([float(items[0]),float(items[1])])
                    
        return
    
    def visualizeMap(self):
        """
        Visualize the roadmap
        """
        plt.ion()
        
        for vertex in self.lst_verts_:
            path = Path(vertex['verts'], vertex['codes'])
            patch = patches.PathPatch(path, facecolor='orange', lw=2)
            self.ax_.add_patch(patch)
            plt.draw()
    
        self.ax_.axis(self.dim_)
        return
    
    def visualizeMapAUV(self):
        """
        Visualize the roadmap
        """
        plt.ion()
        
        for vertex in self.lst_verts_:
            path = Path(vertex['verts'], vertex['codes'])
            patch = patches.PathPatch(path, facecolor='orange', lw=2)
            self.ax_.add_patch(patch)
            plt.draw()
    
        self.ax_.axis(self.dim_)
        return
    
    def visualizeSample(self, sample, traj_pub, color_code = 'r'):
        """
        Visualize the sample in the roadmap
        
        General method
        """
        if self.type_ws_ == "XY":
            self.visualizeSampleXY(sample, traj_pub, color_code)
        elif self.type_ws_ == "CLR":
            self.visualizeSampleCar(sample, color_code)
        elif self.type_ws_ == "AUV":
            self.visualizeSampleAUV(sample, color_code)
    
    def visualizeSampleXY(self, sample, traj_pub, color_code = 'r'):
        """
        Visualize the sample in the roadmap
        
        For XY workspace
        """
        
        traj_points = Marker()
        traj_points.header.frame_id = "/world"
        traj_points.header.stamp = rospy.Time.now()
        traj_points.ns = 'online_planner_points'
        traj_points.id = 0
        traj_points.type = Marker.SPHERE_LIST
        traj_points.action = Marker.ADD
        traj_points.pose.position.x = 0.0
        traj_points.pose.position.y = 0.0
        traj_points.pose.position.z = 0.0
        traj_points.pose.orientation.x = 0.0
        traj_points.pose.orientation.y = 0.0
        traj_points.pose.orientation.z = 0.0
        traj_points.pose.orientation.w = 1.0
        traj_points.scale.x = 0.2
        traj_points.scale.y = 0.2
        traj_points.scale.z = 0.2
        traj_points.color.a = 1.0
        
        traj_points.color.r = 0.0
        traj_points.color.g = 0.0
        traj_points.color.b = 0.0
        
        if color_code == 'r':
            traj_points.color.r = 1.0
        elif color_code == 'g':
            traj_points.color.g = 1.0
        elif color_code == 'b':
            traj_points.color.b = 1.0
        else:
            traj_points.color.b = 1.0
        
        traj_points.points.append(Point(sample[0], sample[1], 0.0))
                    
        return
    
    def visualizeSampleCar(self, sample, color_code = 'r'):
        """
        Visualize the sample in the roadmap
        
        For a Car-like System
        """
        plt.ion()
        
        self.ax_.plot(sample[0],sample[1],color_code+'o')
        
        v1 = np.array([0., CAR_WIDTH/2.])
        v2 = np.array([CAR_LENGTH, CAR_WIDTH/2.])
        v3 = np.array([CAR_LENGTH, -CAR_WIDTH/2.])
        v4 = np.array([0., -CAR_WIDTH/2.])
        
        v1_ = np.array([v1[0]*np.cos(sample[2]) - v1[1]*np.sin(sample[2]), v1[0]*np.sin(sample[2]) + v1[1]*np.cos(sample[2])])
        v2_ = np.array([v2[0]*np.cos(sample[2]) - v2[1]*np.sin(sample[2]), v2[0]*np.sin(sample[2]) + v2[1]*np.cos(sample[2])])
        v3_ = np.array([v3[0]*np.cos(sample[2]) - v3[1]*np.sin(sample[2]), v3[0]*np.sin(sample[2]) + v3[1]*np.cos(sample[2])])
        v4_ = np.array([v4[0]*np.cos(sample[2]) - v4[1]*np.sin(sample[2]), v4[0]*np.sin(sample[2]) + v4[1]*np.cos(sample[2])])
        
        verts = [
            (sample[0] + v1_[0], sample[1] + v1_[1]), # left, bottom
            (sample[0] + v2_[0], sample[1] + v2_[1]), # left, top
            (sample[0] + v3_[0], sample[1] + v3_[1]), # right, top
            (sample[0] + v4_[0], sample[1] + v4_[1]), # right, bottom
            (0., 0.), # ignored
            ]
        
        codes = [Path.MOVETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.CLOSEPOLY,
                 ]
        
        path = Path(verts, codes)
        
        
        if color_code == 'g':
            patch = patches.PathPatch(path, facecolor='green', lw=2)
        elif color_code == 'b':
            patch = patches.PathPatch(path, facecolor='blue', lw=2)
        self.ax_.add_patch(patch)
        
        self.ax_.axis([0., 1000., 0., 1000.])
        plt.draw()
        return
    
    def visualizeSampleAUV(self, sample, color_code = 'r'):
        """
        Visualize the sample in the roadmap
        
        For a Car-like System
        """
        plt.ion()
        
        self.ax_.plot(sample[0],sample[1],color_code+'.')
        
        v1 = np.array([0., AUV_WIDTH/2.])
        v2 = np.array([AUV_LENGTH, AUV_WIDTH/2.])
        v3 = np.array([AUV_LENGTH, -AUV_WIDTH/2.])
        v4 = np.array([0., -AUV_WIDTH/2.])
        
        v1_ = np.array([v1[0]*np.cos(sample[2]) - v1[1]*np.sin(sample[2]), v1[0]*np.sin(sample[2]) + v1[1]*np.cos(sample[2])])
        v2_ = np.array([v2[0]*np.cos(sample[2]) - v2[1]*np.sin(sample[2]), v2[0]*np.sin(sample[2]) + v2[1]*np.cos(sample[2])])
        v3_ = np.array([v3[0]*np.cos(sample[2]) - v3[1]*np.sin(sample[2]), v3[0]*np.sin(sample[2]) + v3[1]*np.cos(sample[2])])
        v4_ = np.array([v4[0]*np.cos(sample[2]) - v4[1]*np.sin(sample[2]), v4[0]*np.sin(sample[2]) + v4[1]*np.cos(sample[2])])
        
        verts = [
            (sample[0] + v1_[0], sample[1] + v1_[1]), # left, bottom
            (sample[0] + v2_[0], sample[1] + v2_[1]), # left, top
            (sample[0] + v3_[0], sample[1] + v3_[1]), # right, top
            (sample[0] + v4_[0], sample[1] + v4_[1]), # right, bottom
            (0., 0.), # ignored
            ]
        
        codes = [Path.MOVETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.LINETO,
                 Path.CLOSEPOLY,
                 ]
        
        path = Path(verts, codes)
        
        
        if color_code == 'g':
            patch = patches.PathPatch(path, facecolor='green', lw=2)
        elif color_code == 'b':
            patch = patches.PathPatch(path, facecolor='blue', lw=2)
        elif color_code == 'r':
            patch = patches.PathPatch(path, facecolor='red', lw=2)
        elif color_code == 'm':
            patch = patches.PathPatch(path, facecolor='magenta', lw=2)
        elif color_code == 'c':
            patch = patches.PathPatch(path, facecolor='cyan', lw=2)
        self.ax_.add_patch(patch)
        
        self.ax_.axis(self.dim_)
        plt.draw()
        return
    
    def visualizeEdge(self, q1_vec, q2_vec, color_code  = 'b--', line_width = 1.0):
        """
        Visualize the edge in the roadmap
        """
        if self.type_ws_ == "XY":
            return self.visualizeEdgeXY(q1_vec, q2_vec, color_code, line_width)
        elif self.type_ws_ == "CLR":
            return self.visualizeEdgeCar(q1_vec, q2_vec, color_code)
        elif self.type_ws_ == "AUV":
            return self.visualizeEdgeAUV(q1_vec, q2_vec, color_code)
    
    def visualizeEdgeXY(self, q1, q2, color_code  = 'b--', line_width = 1.0):
        """
        Visualize the edge in the roadmap
        """
        plt.ion()
        
        line_handle, = self.ax_.plot([q1[0],q2[0]],[q1[1],q2[1]],color_code, linewidth = line_width)
    
        plt.draw()
        return line_handle
    
    def visualizeEdgeCar(self, q1, q2, color_code  = 'b--'):
        """
        Visualize the edge in the roadmap
        """
        plt.ion()
        
        self.ax_.plot(q1, q2, color_code)
    
        plt.draw()
        return
    
    def visualizeEdgeAUV(self, q1, q2, color_code  = 'b--'):
        """
        Visualize the edge in the roadmap
        """
        plt.ion()
        
        self.ax_.plot(q1, q2, color_code)
    
        plt.draw()
        return
    
    def checkPathLocalPlanner(self, q1_vec, q2_vec):
        """
        Check if it is possible to connect q1 and q2 with a straight line, using 
        a local planner
        
        q1_vec: start configuration
        q2_vec: goal configuration
        """
        
        #=======================================================================
        # Linear equation
        #=======================================================================
        m = (q2_vec[1] - q1_vec[1]) / (q2_vec[0] - q1_vec[0])
        if q1_vec[0] < q2_vec[0]:
            minor_x = q1_vec[0]
            major_x = q2_vec[0]
            
            inc_vec = np.array([q2_vec[0] - q1_vec[0],q2_vec[1] - q1_vec[1]])
        else:
            minor_x = q2_vec[0]
            major_x = q1_vec[0]
            
            inc_vec = np.array([q1_vec[0] - q2_vec[0],q1_vec[1] - q2_vec[1]])
        
        octomap_res = self.state_val_checker_.getOctomapRes();
        inc_vec = octomap_res * (inc_vec / np.linalg.norm(inc_vec))
        
        #=======================================================================
        # local planner
        #======================================================================= 
        xi = minor_x 
        while xi < major_x:
            yi = m*xi - m*q1_vec[0] + q1_vec[1]
            if self.checkCollisionXY(np.array([xi,yi])):
                return False
            xi = xi + inc_vec[0]
             
        return True
    
    def generateRandomConf(self, goal_prob = None, goal_vec = None):
        """
        Generate a collision-free random configuration
        """
        if self.type_ws_ == "XY":
            return self.generateRandomConfXY(goal_prob, goal_vec)
        elif self.type_ws_ == "CLR":
            return self.generateRandomConfCar(goal_prob, goal_vec)
        elif self.type_ws_ == "AUV":
            return self.generateRandomConfAUV(goal_prob, goal_vec)
    
    def generateRandomConfXY(self, goal_prob = None, goal_vec = None):
        """
        Generate a collision-free random configuration
        """
        if goal_prob is not None:
            random_num = random.random()
            if random_num < goal_prob:
                #print "goal"
                return np.array(goal_vec)

        while True:
            q_vec = (np.random.rand(1, 2))[0,:]#(np.random.rand(1, 2) * self.max_map_dim_)[0,:]
            q_vec[0] = np.random.rand()*(self.dim_[1]-self.dim_[0]) + self.dim_[0]
            q_vec[1] = np.random.rand()*(self.dim_[3]-self.dim_[2]) + self.dim_[2]
            if not self.checkCollision(q_vec):
                break
        return q_vec
    
    def generateRandomConfXYStar(self, goal_prob = None, goal_vec = None):
        """
        Generate a collision-free random configuration
        """
        if goal_prob is not None:
            random_num = random.random()
            if random_num < goal_prob:
                #print "goal"
                q_vec = []
                q_vec.append(50.0 - (np.random.rand() * 100.0))
                q_vec.append(50.0 - (np.random.rand() * 100.0))
                q_vec[0] = goal_vec[0] + q_vec[0]
                q_vec[1] = goal_vec[1] + q_vec[1]
                 
                return np.array(q_vec)

        while True:
            q_vec = (np.random.rand(1, 2))[0,:]#(np.random.rand(1, 2) * self.max_map_dim_)[0,:]
            q_vec[0] = np.random.rand()*(self.dim_[1]-self.dim_[0]) + self.dim_[0]
            q_vec[1] = np.random.rand()*(self.dim_[3]-self.dim_[2]) + self.dim_[2]
            if not self.checkCollision(q_vec):
                break
        return q_vec
    
    def generateRandomConfCar(self, goal_prob = None, goal_vec = None):
        """
        Generate a collision-free random configuration for a Car-like system
        """
        if goal_prob is not None:
            random_num = random.random()
            if random_num < goal_prob:
                #print "goal"
                return np.array(goal_vec)
             
        while True:
            q_vec = (np.random.rand(1, 3) * self.max_map_dim_)[0,:]
            q_vec[2] = 2.*np.pi*np.random.rand()#np.random.uniform(0,2*np.pi)
            if not self.checkCollision(q_vec):
                return q_vec
                break
        return q_vec
    
    def generateRandomConfAUV(self, goal_prob = None, goal_vec = None):
        """
        Generate a collision-free random configuration for an AUV
        """
        if goal_prob is not None:
            random_num = random.random()
            if random_num < goal_prob:
                #print "goal"
                return np.array(goal_vec)
             
        while True:
            q_vec = (np.random.rand(1, 3) * self.max_map_dim_)[0,:]
            q_vec[0] = np.random.rand()*(self.dim_[1]-self.dim_[0]) + self.dim_[0]
            q_vec[1] = np.random.rand()*(self.dim_[3]-self.dim_[2]) + self.dim_[2]
            q_vec[2] = 2.*np.pi*np.random.rand()#np.random.uniform(0,2*np.pi)
            if not self.checkCollision(q_vec):
                return q_vec
                break
        return q_vec
    
#     def generateRandomConfCar(self, PG = 0.5, goal_vec = None, d = 10000):
#         """
#         Generate a collision-free random configuration for a Car-like system
#         """
#         if goal_vec == None:
#             print "not goal specified"
#              
#             while True:
#                 q_vec = (np.random.rand(1, 3) * self.max_map_dim_)[0,:]
#                 q_vec[2] = np.random.uniform(0., 2.*np.pi)
#                 if not self.checkCollisionCar(q_vec):
#                     return q_vec
#                     break
#         else:
#             x = random.random()
#             if x < PG:
#                 print "returning goal"
#                 return goal_vec
#             elif x< 0.25:
#                 print "random and biased to goal"
#                 while True:
#                     q_vec = (np.random.rand(1, 3) * self.max_map_dim_)[0,:]
#                     q_vec[2] = np.random.uniform(0., 2.*np.pi)
#                     if not self.checkCollisionCar(q_vec) and self.distCar(q_vec, goal_vec) < 2.*d:
#                         return q_vec
#                         break
#             else:
#                 print "just random"
#                 while True:
#                     q_vec = (np.random.rand(1, 3) * self.max_map_dim_)[0,:]
#                     q_vec[2] = np.random.uniform(0,2*np.pi)
#                     if not self.checkCollisionCar(q_vec):
#                         return q_vec
#                         break
#         return goal_vec

    def checkCollision(self, sample):
        """
        Check if configuration is collision-free
        
        sample: configuration
        """
        if self.type_ws_ == "XY":
            return self.checkCollisionXY(sample)
        elif self.type_ws_ == "CLR":
            return self.checkCollisionCar(sample)
        elif self.type_ws_ == "AUV":
            return self.checkCollisionAUV(sample)
    
    def checkCollisionXY(self, sample):
        """
        Check if configuration is collision-free
        
        sample: configuration
        """
        if sample[0] < self.dim_[0] or sample[1] < self.dim_[2]:
            return True
        
        if sample[0] > self.dim_[1] or sample[1] > self.dim_[3]:
            return True
        
        return self.state_val_checker_.isCollCube(sample[0], sample[1], self.planning_depth_, 3.0)
    
    def isValidPoint(self, sample):
        """
        Check if point is collision-free
        
        sample: configuration
        """
        return self.state_val_checker_.isValidPoint(sample[0], sample[1], self.planning_depth_)
        
    
    def checkCollisionCar(self, sample):
        """
        Check if configuration is collision-free
        
        sample: configuration
        """

        v1 = np.array([0., CAR_WIDTH/2.])
        v2 = np.array([CAR_LENGTH, CAR_WIDTH/2.])
        v3 = np.array([CAR_LENGTH, -CAR_WIDTH/2.])
        v4 = np.array([0., -CAR_WIDTH/2.])
        
        v1_ = np.array([v1[0]*np.cos(sample[2]) - v1[1]*np.sin(sample[2]), v1[0]*np.sin(sample[2]) + v1[1]*np.cos(sample[2])])
        v2_ = np.array([v2[0]*np.cos(sample[2]) - v2[1]*np.sin(sample[2]), v2[0]*np.sin(sample[2]) + v2[1]*np.cos(sample[2])])
        v3_ = np.array([v3[0]*np.cos(sample[2]) - v3[1]*np.sin(sample[2]), v3[0]*np.sin(sample[2]) + v3[1]*np.cos(sample[2])])
        v4_ = np.array([v4[0]*np.cos(sample[2]) - v4[1]*np.sin(sample[2]), v4[0]*np.sin(sample[2]) + v4[1]*np.cos(sample[2])])
        
        if sample[0] < 0.0 or \
           sample[1] < 0.0 or \
           sample[0] + v1_[0] < 0.0 or \
           sample[0] + v2_[0] < 0.0 or \
           sample[0] + v3_[0] < 0.0 or \
           sample[0] + v4_[0] < 0.0 or \
           sample[1] + v1_[1] < 0.0 or \
           sample[1] + v2_[1] < 0.0 or \
           sample[1] + v3_[1] < 0.0 or \
           sample[1] + v4_[1] < 0.0:
            return True
        
        if sample[0] > self.max_map_dim_ or \
           sample[1] > self.max_map_dim_ or \
           sample[0] + v1_[0] > self.max_map_dim_ or \
           sample[0] + v2_[0] > self.max_map_dim_ or \
           sample[0] + v3_[0] > self.max_map_dim_ or \
           sample[0] + v4_[0] > self.max_map_dim_ or \
           sample[1] + v1_[1] > self.max_map_dim_ or \
           sample[1] + v2_[1] > self.max_map_dim_ or \
           sample[1] + v3_[1] > self.max_map_dim_ or \
           sample[1] + v4_[1] > self.max_map_dim_ :
            return True
        
        list_points = []
        list_points.append(sample[0:2])
        list_points.append(np.array([sample[0] + v1_[0], sample[1] + v1_[1]]))
        list_points.append(np.array([sample[0] + v2_[0], sample[1] + v2_[1]]))
        list_points.append(np.array([sample[0] + v3_[0], sample[1] + v3_[1]]))
        list_points.append(np.array([sample[0] + v4_[0], sample[1] + v4_[1]])) 
        
        for obs_verts in self.lst_verts_:
            if nx.points_inside_poly(list_points, obs_verts['verts']).any():
                return True 
        return False
    
    def checkCollisionAUV(self, sample):
        """
        Check if configuration is collision-free
        
        sample: configuration
        """

        v1 = np.array([0., 2.5*AUV_WIDTH])
        v2 = np.array([1.5*AUV_LENGTH, 2.5*AUV_WIDTH])
        v3 = np.array([1.5*AUV_LENGTH, -2.5*AUV_WIDTH])
        v4 = np.array([0., -2.5*AUV_WIDTH])
        
        v1_ = np.array([v1[0]*np.cos(sample[2]) - v1[1]*np.sin(sample[2]), v1[0]*np.sin(sample[2]) + v1[1]*np.cos(sample[2])])
        v2_ = np.array([v2[0]*np.cos(sample[2]) - v2[1]*np.sin(sample[2]), v2[0]*np.sin(sample[2]) + v2[1]*np.cos(sample[2])])
        v3_ = np.array([v3[0]*np.cos(sample[2]) - v3[1]*np.sin(sample[2]), v3[0]*np.sin(sample[2]) + v3[1]*np.cos(sample[2])])
        v4_ = np.array([v4[0]*np.cos(sample[2]) - v4[1]*np.sin(sample[2]), v4[0]*np.sin(sample[2]) + v4[1]*np.cos(sample[2])])
        
        if sample[0] < self.dim_[0] or \
           sample[1] < self.dim_[2] or \
           sample[0] + v1_[0] < self.dim_[0] or \
           sample[0] + v2_[0] < self.dim_[0] or \
           sample[0] + v3_[0] < self.dim_[0] or \
           sample[0] + v4_[0] < self.dim_[0] or \
           sample[1] + v1_[1] < self.dim_[2] or \
           sample[1] + v2_[1] < self.dim_[2] or \
           sample[1] + v3_[1] < self.dim_[2] or \
           sample[1] + v4_[1] < self.dim_[2]:
            return True
        
        if sample[0] > self.dim_[1] or \
           sample[1] > self.dim_[3] or \
           sample[0] + v1_[0] > self.dim_[1] or \
           sample[0] + v2_[0] > self.dim_[1] or \
           sample[0] + v3_[0] > self.dim_[1] or \
           sample[0] + v4_[0] > self.dim_[1] or \
           sample[1] + v1_[1] > self.dim_[3] or \
           sample[1] + v2_[1] > self.dim_[3] or \
           sample[1] + v3_[1] > self.dim_[3] or \
           sample[1] + v4_[1] > self.dim_[3]:
            return True
        
        list_points = []
        list_points.append(sample[0:2])
        list_points.append(np.array([sample[0] + v1_[0], sample[1] + v1_[1]]))
        list_points.append(np.array([sample[0] + v2_[0], sample[1] + v2_[1]]))
        list_points.append(np.array([sample[0] + v3_[0], sample[1] + v3_[1]]))
        list_points.append(np.array([sample[0] + v4_[0], sample[1] + v4_[1]])) 
        
        for obs_verts in self.lst_verts_:
            if nx.points_inside_poly(list_points, obs_verts['verts']).any():
                return True 
        return False 
    
    def dist(self, q1_vec, q2_vec):
        """
        Calculate the distance between two configurations.
        The available Configuration spaces are:
        X-Y (R2)
        X-Y-Yaw (R2-S1)
        
        q1_vec: first configuration vector
        q2_vec: second configuration vector 
        """
        if self.type_ws_ == "XY":
            return self.distXY(q1_vec, q2_vec)
        elif self.type_ws_ == "CLR":
            return self.distCar(q1_vec, q2_vec)
        elif self.type_ws_ == "AUV":
            return self.distAUV(q1_vec, q2_vec)
    
    def distXY(self, q1_vec, q2_vec):
        """
        Calculate the distance between two configurations in a
        X-Y (R2) C-space
        
        q1_vec: first configuration vector
        q2_vec: second configuration vector 
        """
        return np.linalg.norm(q1_vec - q2_vec)
    
    def distCar(self, q1_vec, q2_vec):
        """
        Calculate the distance between two configurations in a
        X-Y-Yaw (R2-S1) C-space
        
        q1_vec: first configuration vector
        q2_vec: second configuration vector 
        """
        A = CAR_LENGTH * CAR_LENGTH
        
        dcita = q1_vec[2] - q2_vec[2];
        if dcita > np.pi:
            dcita -= 2*np.pi
        elif dcita < -np.pi:
            dcita += 2*np.pi;
        
        d = (q1_vec[0] - q2_vec[0])*(q1_vec[0] - q2_vec[0]) + (q1_vec[1] - q2_vec[1])*(q1_vec[1] - q2_vec[1]) + A*dcita*dcita;
        d = math.pow(d, 0.5);
        
        return d
    
    def distAUV(self, q1_vec, q2_vec):
        """
        Calculate the distance between two configurations in a
        X-Y-Yaw (R2-S1) C-space
        
        q1_vec: first configuration vector
        q2_vec: second configuration vector 
        """
        A = AUV_LENGTH * AUV_LENGTH
        
        dcita = q1_vec[2] - q2_vec[2];
        if dcita > np.pi:
            dcita -= 2*np.pi
        elif dcita < -np.pi:
            dcita += 2*np.pi;
        
        d = (q1_vec[0] - q2_vec[0])*(q1_vec[0] - q2_vec[0]) + (q1_vec[1] - q2_vec[1])*(q1_vec[1] - q2_vec[1]) + A*dcita*dcita;
        d = math.pow(d, 0.5);
        
        return d
    
    def cleanEdge(self, line):
        """
        Clean edge visualization in the roadmap
        """
        self.ax_.lines.remove(line)
        plt.draw()
        return