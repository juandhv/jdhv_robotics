"""
Created on Jun 10, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Test of a basic implementation of Probabilistic RoadMap (PRM). 
A point mobile robot is assumed to be moving in 2D workspace.
"""

from Maps import Map2D
from PRM import PRM
from mission import Mission

import matplotlib.pyplot as plt

import numpy as np

if __name__ == '__main__':
    
    #===========================================================================
    # General parameters
    #===========================================================================
    num_nodes = 100 #number of samples
    max_map_dim = 1000.0 #as the workspace is assumed to be a square, this is the length of each side
    num_close_neigh = 6 #number of closest neighbors
    #===========================================================================
    # Load planner and mission parameters
    #===========================================================================
    test_id = "t1_auv"
    mission = Mission(test_id)
    mission.load_parameters()
    #===========================================================================
    # Workspace definition
    #===========================================================================
    #ws = Map2D()
#    ws.constructFromData(num_obst=4, min_radius=150.0, max_radius=200.0, max_map_dim=max_map_dim)
    #ws.constructFromFiles(max_map_dim, "t1")
    ws = Map2D("XY", mission.get_ws_bounds())
    ws.constructFromFiles(test_id)
    ws.visualizeMap()
    #===========================================================================
    # Creation of the object roadmap and learning phase
    #===========================================================================
    prm = PRM(ws,max_map_dim)
    prm.buildRoadMap(num_nodes,num_close_neigh)
    #===========================================================================
    # Query phase
    #===========================================================================
    q_init_vec = np.array(mission.get_init_conf_2D())
    q_goal_vec = np.array(mission.get_goal_conf_2D())

    query_result = prm.solveQuery(q_init_vec, q_goal_vec)

    plt.ioff()
    plt.show()
    
    pass