"""
Created on Feb 10, 2014

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Load planner parameters.
"""

class Mission(object):
    """
    Mision and planner parameters
    """

    def __init__(self, test_id = ""):
        """
        Constructor
        """
        self.file_path_ = "map_data/planner_params_" + test_id + ".txt"
        self.planner_type_ = "RRTStar"
        self.ws_bounds_ = [-5.0, 40.0, -5.0, 40.0]
        self.num_attempts_ = 20000
        self.step_size_ = 4.
        self.step_size_time_ = 5.
        self.max_comp_time_ = 900.0
        self.goal_prob_ = 0.1
        self.epsilon_found_dist_ = 2.
        self.q_init_conf_2D_ = [2., 0.] #[20., 20.]
        self.q_goal_conf_2D_ = [20., 30.] #[260., -160.]
        self.planning_depth_ = 0.30
        
        return
    
    def load_parameters(self):
        """
        Load mission and planner parameters from file
        """
        with open(self.file_path_) as fp:
            for line in fp:
                if line[0:12] == "planner_type":
                    self.planner_type_ = line[14:-1] 
                elif line[0:16] == "workspace_bounds": # X-Y workspace bounds
                    self.ws_bounds_ = map(float, line[19:-2].split(','))
                elif line[0:12] == "num_attempts": #number of attempts of expansion of the tree
                    self.num_attempts_ = int(line[14:-1])
                elif line[0:18] == "epsilon_found_dist": #epsilon distance to goal
                    self.epsilon_found_dist_ = float(line[20:-1])
                elif line[0:14] == "step_size_time":
                    self.step_size_time_ = float(line[16:-1])
                    print self.step_size_time_
                elif line[0:9] == "step_size": #distance to extend the tree
                    self.step_size_ = float(line[11:-1])
                elif line[0:13] == "max_comp_time": #distance to be extended
                    self.max_comp_time_ = float(line[15:-1])
                elif line[0:9] == "goal_prob": #goal probability
                    self.goal_prob_ = float(line[11:-1])
                elif line[0:13] == "q_init_vec_2D": #initial conf
                    self.q_init_conf_2D_ = map(float, line[16:-2].split(','))
                elif line[0:13] == "q_goal_vec_2D": #goal conf
                    self.q_goal_conf_2D_ = map(float, line[16:-2].split(','))
                elif line[0:13] == "q_init_vec_3D": #initial conf
                    self.q_init_conf_3D_ = map(float, line[16:-2].split(','))
                elif line[0:13] == "q_goal_vec_3D": #goal conf
                    self.q_goal_conf_3D_ = map(float, line[16:-2].split(','))
    
    def get_ws_bounds(self):
        """
        get the workspace bounds
        """
        return self.ws_bounds_
    
    def get_num_attempts(self):
        """
        get the number attempts to expand the tree
        """
        return self.num_attempts_
    
    def get_step_size(self):
        """
        get the size of the step in each expansion
        """
        return self.step_size_
    
    def get_step_size_time(self):
        """
        get the size of the step in each expansion
        """
        return self.step_size_time_
    
    def get_max_comp_time(self):
        """
        get the computation time
        """
        return self.max_comp_time_
    
    def get_goal_prob(self):
        """
        get the probability to bias the tree towards the goal state 
        """
        return self.goal_prob_
    
    def get_epsilon_goal_dist(self):
        """
        epsilon distance to goal 
        """
        return self.epsilon_found_dist_
    
    def get_init_conf_2D(self):
        """
        get initial configuration (2D)
        """
        return self.q_init_conf_2D_
    
    def get_goal_conf_2D(self):
        """
        get goal configuration (2D)
        """
        return self.q_goal_conf_2D_
    
    def get_init_conf_3D(self):
        """
        get initial configuration (3D)
        """
        return self.q_init_conf_3D_
    
    def get_goal_conf_3D(self):
        """
        get goal configuration (3D)
        """
        return self.q_goal_conf_3D_
    
    def get_planning_depth(self):
        return self.planning_depth_