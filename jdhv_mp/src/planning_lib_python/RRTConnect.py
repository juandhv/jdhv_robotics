"""
Created on Nov 14, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Rapidly-exploring Random Trees (RRT).
This version assumes 2 trees grown from the initial and the goal configurations (RRTConnect) 
C-space --> R2.
"""

from tree import Tree
from RRT import RRT

TRAPPED = 0
REACHED = 1
ADVANCED = 2

EPSILON_FOUND_DISTANCE = 20.0

class RRTConnect(RRT):
    """
    Implementation of RRTConnect.
    This version assumes 2 trees grown from the initial and the goal configurations (RRTConnect)
    """
    
    def build(self, q_init_vec, q_goal_vec):
        """
        Build an RRT both, initial and goal, trees
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        swap = 1
        #=======================================================================
        # Adding initial and goals configurations to its respective RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        
        self.q_goal_tree_ = Tree()
        self.q_goal_tree_.addNode(self.q_goal_tree_.getOrder(), q_goal_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            q_rand_vec = self.ws_.generateRandomConf()
            if swap == 1: 
                if self.extend('init', q_rand_vec) != TRAPPED:
                    if self.extend('goal', self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder() - 1)) == REACHED:
                        result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1, self.q_goal_tree_.getOrder() - 1)
                        break
                swap = 2
            elif swap == 2:
                if self.extend('goal', q_rand_vec) != TRAPPED:
                    if self.extend('init', self.q_goal_tree_.getConfQ(self.q_goal_tree_.getOrder() - 1)) == REACHED:
                        result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1, self.q_goal_tree_.getOrder() - 1)
                        break
                swap = 1
            attempt_i += 1
            print "i:", attempt_i
        return result_path
    
    def reconstructPath(self, q_leaf_init, q_leaf_goal):
        """
        Reconstruct the path
        
        q: leaf node
        """
        path = []
        path.append((q_leaf_init, 0))
        while True:
            q_leaf_init = self.q_init_tree_.getParent(q_leaf_init)
            if q_leaf_init == -1:
                break
            else:
                path.append((q_leaf_init, 0))
        path.reverse()
        
        path.append((q_leaf_goal, 1))
        while True:
            q_leaf_goal = self.q_goal_tree_.getParent(q_leaf_goal)
            if q_leaf_goal == -1:
                break
            else:
                path.append((q_leaf_goal, 1))
        
        return path
    
    def visualizeQueryResult(self,query_result):
        """
        Visualize the query result
        
        query_result: result of the query represented by a lists of configurations
        """
        for i in range(len(query_result)):
            qi_info = query_result[i]
            if qi_info[1] == 0:
                qi_vec = self.q_init_tree_.getConfQ(qi_info[0])
            else:
                qi_vec = self.q_goal_tree_.getConfQ(qi_info[0])
            self.ws_.visualizeSample(qi_vec, 'g.')
            
            if i > 0:
                qi_1_info = query_result[i-1]
                if qi_1_info[1] == 0:
                    qi_1_vec = self.q_init_tree_.getConfQ(qi_1_info[0])
                else:
                    qi_1_vec = self.q_goal_tree_.getConfQ(qi_1_info[0])
                    
                self.ws_.visualizeEdge(qi_1_vec, qi_vec, 'g')
        return