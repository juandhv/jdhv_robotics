"""
Created on Jan 15, 2014

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Test of a basic implementation of Rapidly-exploring Random Trees (RRT). 
A point mobile robot is assumed to be moving in 2D workspace.
"""

from Mission import Mission

from Maps import Map2D
from RRTAuv import RRTAuv
from RRTAuvKin import RRTAuvKin
from RRTStarAuv import RRTStarAuv
import numpy as np

import matplotlib.pyplot as plt 

if __name__ == '__main__':
    
    #===========================================================================
    # Load planner and mission parameters
    #===========================================================================
    test_id = "t2_auv"
    mission = Mission(test_id)
    mission.load_parameters()
    #===========================================================================
    # Workspace definition
    #===========================================================================
    ws = Map2D("AUV", mission.get_ws_bounds()) 
#     ws.constructFromData(num_obst=6, min_radius=150.0, max_radius=200.0, max_map_dim=max_map_dim)
    ws.constructFromFiles(test_id)
    ws.visualizeMapAUV()
    #===========================================================================
    # Creation of the object RRT
    #===========================================================================
    planner = RRTAuvKin(ws,
                        num_attempts = mission.get_num_attempts(),
                        max_time_comp = mission.get_max_comp_time(),
                        step_size_time = mission.get_step_size_time(),
                        goal_prob = mission.get_goal_prob())

#     planner = RRTAuv(ws,
#                         num_attempts = mission.get_num_attempts(),
#                         max_time_comp = mission.get_max_comp_time(),
#                         step_size_time = mission.get_step_size_time(),
#                         goal_prob = mission.get_goal_prob())
    #===========================================================================
    # Building the RRT and attempting to solve a query
    #===========================================================================
    q_init_vec = np.array(mission.get_init_conf_3D())
    q_goal_vec = np.array(mission.get_goal_conf_3D())
    
#     raw_input()
    result_path = planner.solveQuery(q_init_vec, q_goal_vec)
    
    plt.ioff()
    plt.show()
    
    pass