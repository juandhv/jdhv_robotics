"""
Created on Nov 14, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Expansive-Spaces Trees (EST). 
C-space --> R2.
"""

from graph import Graph
import numpy as np
import random

class EST(object):
    """
    Implementation of Expansive-Spaces Trees, basic EST.
    """
        
    def __init__(self,ws, max_map_dim, num_attempts, neighborhood_dist):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        neighborhood_dist: neighborhood maximum dist
        """
        self.ws_ = ws
        self.max_map_dim_ = max_map_dim
        self.num_attempts_ = num_attempts
        self.neighborhood_dist_ = neighborhood_dist
        self.distr_t_init = {}
        self.distr_t_goal = {}
        
        return
    
    def solveQuery(self, q_init_vec, q_goal_vec):
        """
        Attempt to solve a query using a EST
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        
        self.ws_.visualizeSample(q_init_vec,'g.')
        self.ws_.visualizeSample(q_goal_vec,'g.')
        
        self.buildEST(q_init_vec, q_goal_vec)
        
        return result_path
    
    def buildEST(self, q_init_vec, q_goal_vec):
        """
        Build an Expansive-Space Tree from a initial configuration
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        #=======================================================================
        # Adding initial and goal configurations to its respective trees
        #=======================================================================
        self.q_init_tree_ = Graph()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        self.distr_t_init[self.q_init_tree_.getOrder() - 1] = 1
        
        self.q_goal_tree_ = Graph()
        self.q_goal_tree_.addNode(self.q_goal_tree_.getOrder(), q_goal_vec)
        
        self.updateProb('init')
        self.updateProb('goal')
        #=======================================================================
        # Attempting num_attempts to extend the EST
        #=======================================================================
        for i in range(self.num_attempts_):
#             qs_new = []
#             qs_new.append(random.randint(0,self.q_init_tree_.getOrder()-1))
#             qs_new.append(random.randint(0,self.q_goal_tree_.getOrder()-1))
            qs_new = self.random_distr()
            print "qs_new",qs_new
            self.extendEST(qs_new)
            
#             raw_input("attempt complete")
        return

    def extendEST(self,qs_new):
        """
        Attempt to extend the EST
        
        q: selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Extending q_init_tree
        #=======================================================================
        q_new_vec = self.ws_.generateRandomConf()
        q_new_vec = q_new_vec / np.linalg.norm(q_new_vec)#normalized
        q_new_vec_len = random.uniform(-1.0,1.0) * self.neighborhood_dist_
        q_new_vec = q_new_vec * q_new_vec_len;
        
        q_vec = self.q_init_tree_.getConfQ(qs_new[0])
        
        q_new_vec = q_new_vec + q_vec
        
        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_vec, q_new_vec):
            self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_new_vec)
            self.updateProb('init')
            
            self.ws_.visualizeSample(q_new_vec)
            self.ws_.visualizeEdge(q_vec, q_new_vec,'--r')
        
        #=======================================================================
        # Extending q_goal_tree
        #=======================================================================
        q_new_vec = self.ws_.generateRandomConf()
        q_new_vec = q_new_vec / np.linalg.norm(q_new_vec)#normalized
        q_new_vec_len = random.uniform(-1.0,1.0) * self.neighborhood_dist_
        q_new_vec = q_new_vec * q_new_vec_len;
        
        q_vec = self.q_goal_tree_.getConfQ(qs_new[1])
        
        q_new_vec = q_new_vec + q_vec
        
        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_vec, q_new_vec):
            self.q_goal_tree_.addNode(self.q_goal_tree_.getOrder(), q_new_vec)
            self.updateProb('goal')
            
            self.ws_.visualizeSample(q_new_vec,'.b')
            self.ws_.visualizeEdge(q_vec, q_new_vec,'--b')
        return
    
    def updateProb(self,tree):
        if tree == 'init':
            #=======================================================================
            # Updating probability for q_init_tree
            #=======================================================================
            self.distr_t_init[self.q_init_tree_.getOrder() - 1] = 1
            q_vec = self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder() - 1)
    
            for qi in range(self.q_init_tree_.getOrder() - 1):
                qi_vec = self.q_init_tree_.getConfQ(qi)
                if np.linalg.norm(q_vec - qi_vec) < 100:
                    self.distr_t_init[self.q_init_tree_.getOrder() - 1] += 1
                    self.distr_t_init[qi] += 1
            
            print "distr_t_init", self.distr_t_init
        elif tree == 'goal':
            #=======================================================================
            # Updating probability for q_goal_tree
            #=======================================================================
            self.distr_t_goal[self.q_goal_tree_.getOrder() - 1] = 1
            q_vec = self.q_goal_tree_.getConfQ(self.q_goal_tree_.getOrder() - 1)
    
            for qi in range(self.q_goal_tree_.getOrder() - 1):
                qi_vec = self.q_goal_tree_.getConfQ(qi)
                if np.linalg.norm(q_vec - qi_vec) < 100:
                    self.distr_t_goal[self.q_goal_tree_.getOrder() - 1] += 1
                    self.distr_t_goal[qi] += 1
        
            print "distr_t_goal", self.distr_t_goal
            
        return
    
    def random_distr(self):
        """
        Selecting randomly and with a probabilistic function the
        node (configuration) to be extended.
        """
        #=======================================================================
        # Finding next configuration for q_init_tree
        #=======================================================================
        r = random.uniform(0, 1)
        s = 0
        for i in xrange(len(self.distr_t_init)):
            item = i
            prob = 1.0 - float(self.distr_t_init[i])/float(max(self.distr_t_init.values()))#1.0 / self.distr_t_init[i]#(self.distr_t_init[i] / self.q_init_tree_.getOrder())
            s += prob
            if s >= r:
                break
        q_next_init_tree = item
        #=======================================================================
        # Finding next configuration for q_init_tree
        #=======================================================================
        r = random.uniform(0, 1)
        s = 0
        for i in xrange(len(self.distr_t_goal)):
            item = i
            prob = 1.0 / self.distr_t_goal[i] #(self.distr_t_goal[i] / self.q_goal_tree_.getOrder())
            s += prob
            if s >= r:
                break
        q_next_goal_tree = item
        return [q_next_init_tree, q_next_goal_tree]
    