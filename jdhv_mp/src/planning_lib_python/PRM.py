"""
Created on Jun 10, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Probabilistic RoadMap (PRM). 
C-space --> R2.
"""

import numpy as np
import graph
from graph import Graph
import operator

class PRM(object):
    """
    Implementation of Probabilistic RoadMap, basic PRM.
    """

    def __init__(self,ws,max_map_dim):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        """
        self.ws_ = ws
        self.max_map_dim_ = max_map_dim
        self.samples_ = None
        
        return
    
    def buildRoadMap(self,num_nodes,num_close_neigh):
        """
        Build the RoadMap
        
        num_nodes: number of samples
        num_close_neigh: number of closest neighbors to connect
        """
        self.num_nodes_ = num_nodes
        self.num_close_neigh_ = num_close_neigh
        self.road_map_ = Graph()
        
        #=======================================================================
        # Sampling collision-free configurations
        #=======================================================================
        while self.road_map_.getOrder() < num_nodes:
            while True:
                q_vec = self.ws_.generateRandomConf()
                self.road_map_.addNode(self.road_map_.getOrder(), q_vec)

                self.ws_.visualizeSample(q_vec, '.r')
                break
        
        #=======================================================================
        # Finding closest nodes and connecting them, if possible
        #=======================================================================
        for qi in self.road_map_:
            qi_vec = self.road_map_.getConfQ(qi)
            closest_neighbors_qi = self.findClosestNeighbors(qi)
            for closest_qi in closest_neighbors_qi:
                closest_qi_vec = self.road_map_.getConfQ(closest_qi[0])
                if not self.road_map_.hasEdge(qi, closest_qi[0]) and self.ws_.checkPathLocalPlanner(qi_vec, closest_qi_vec):
                    self.road_map_.addEdge(qi, closest_qi[0], closest_qi[1])#edge from a node i to a node j with a cost c

                    self.ws_.visualizeEdge(qi_vec, closest_qi_vec)

        print "Learning phase complete (RoadMap built)"
        return
    
    def solveQuery(self, q_init_vec, q_goal_vec):
        """
        Attempt to solve a query using the roadmap built in the learning phase
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        
        #=======================================================================
        # Connect initial position to the roadmap
        #=======================================================================
        q_init = self.road_map_.getOrder()
        self.road_map_.addNode(q_init, q_init_vec)
        self.ws_.visualizeSample(q_init_vec,'go')
        closest_neighbors_q_init = self.findClosestNeighbors(q_init)
        connection_flag = False
        
        for closest_qi in closest_neighbors_q_init:
            closest_qi_vec = self.road_map_.getConfQ(closest_qi[0])
            if not self.road_map_.hasEdge(q_init, closest_qi[0]) and self.ws_.checkPathLocalPlanner(q_init_vec, closest_qi_vec):
                self.road_map_.addEdge(q_init, closest_qi[0], closest_qi[1])#edge from a node i to a node j with a cost c
                connection_flag = True
                
                self.ws_.visualizeEdge(self.road_map_.getConfQ(q_init),self.road_map_.getConfQ(closest_qi[0]), 'g--')
                break

        if not connection_flag:
            return result_path
        #=======================================================================
        # Connect goal position to the roadmap
        #=======================================================================
        q_goal = self.road_map_.getOrder()
        self.road_map_.addNode(q_goal, q_goal_vec)
        self.ws_.visualizeSample(q_goal_vec,'g.')
        closest_neighbors_q_goal = self.findClosestNeighbors(q_goal)
        connection_flag = False
        
        for closest_qi in closest_neighbors_q_goal:
            closest_qi_vec = self.road_map_.getConfQ(closest_qi[0])
            if not self.road_map_.hasEdge(q_goal, closest_qi[0]) and self.ws_.checkPathLocalPlanner(q_goal_vec, closest_qi_vec):
                self.road_map_.addEdge(q_goal, closest_qi[0], closest_qi[1])#edge from a node i to a node j with a cost c
                connection_flag = True
                
                self.ws_.visualizeEdge(self.road_map_.getConfQ(q_goal),self.road_map_.getConfQ(closest_qi[0]), 'g--')
                break

        if not connection_flag:
            return result_path
        #=======================================================================
        # Find a path, if possible
        #=======================================================================
        result_path = graph.aStar(self.road_map_, q_init, q_goal)
        result_path.reverse()

        if len(result_path) > 0:
            print "Query phase complete (Solution found)"
            self.visualizeQueryResult(result_path)
        else:
            print "Query phase complete (Solution not found)"
        
        return result_path
    
    def findClosestNeighbors(self, qi, num_close_neigh = None):
        """
        Find the k closest configurations to qi
        
        qi: current configuration
        """
        
        if num_close_neigh == None:
            num_close_neigh = self.num_close_neigh_
        else:
            num_close_neigh = self.road_map_.getOrder()

        qi_vec = self.road_map_.getConfQ(qi)
        neighbors = {}
        
        #=======================================================================
        # Calculate the distance to all of the neighbors
        #=======================================================================
        for qj in self.road_map_:
            if qj!=qi and self.road_map_.hasEdge(qi, qj):
                neighbors[qj] = self.road_map_.getEdgeCost(qi, qj)
            elif qj!=qi:
                qj_vec = self.road_map_.getConfQ(qj)
                dist = np.linalg.norm(qi_vec - qj_vec)
                neighbors[qj] = dist

        #=======================================================================
        # Find the k closest neighbors 
        #=======================================================================
        closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
        
        return closest_neighbors[0:self.num_close_neigh_]
    
    def visualizeQueryResult(self,query_result):
        """
        Visualize the query result
        
        query_result: result of the query represented by a lists of configurations
        """
        for i in range(len(query_result)):
            self.ws_.visualizeSample(self.road_map_.getConfQ(query_result[i]),'og')
            if i > 0:
                self.ws_.visualizeEdge(self.road_map_.getConfQ(query_result[i-1]),self.road_map_.getConfQ(query_result[i]), 'g', line_width=4.0)
        return