"""
Created on Dec 5, 2013

Purpose:
Tree theory related classes and functions.
Used in representing paths on tree-based algorithms.

Classes:
Tree

Based on Andres Jaramillo-Botero's graph class (Caltech 2006)
"""

from graph import Graph
import numpy as np

######### Class definitions #########

class Tree(Graph):
        """Tree represents each node with a parent.     
        """
        def __init__(self):
            """Initialize Tree. 
            """
            Graph.__init__(self)
            
            #===================================================================
            # Need to model a tree
            #===================================================================
            #self.adj_ = {}
            self.parents_ = {}
            self.accum_cost_ = {}
            self.input_control_ = {}
            self.name_ = "Tree"
            
        def __getitem__(self,n):
            """Return the accumulated cost of node n.
 
            This provides graph G the natural property that G[n]
            """
            try:
                return self.accum_cost_[n]
            except (KeyError, TypeError):
                print "node %s not in graph"%n
                raise
            
        def __setitem__(self, key, value):
            try:
                self.accum_cost_[key] = value
            except (KeyError, TypeError):
                print "node %s not in graph"%key
                raise
        
        def getParent(self, n):
            """Return parent of node n.  """
            return self.parents_[n]
        
        def getInputControl(self, u, v):
            """Return the input control of node n.  """
            return self.input_control_[u][v]
 
        def addNode(self, n, q_vec):
            """
            Add a single node n to the graph.
            The node n can be any hashable object except None. But most cases in this
            context will be an integer that identifies the node.
            
            q is the configuration added to the RoadMap
            """
            Graph.addNode(self, n, q_vec)
            
            if n == 0:
                self.accum_cost_[n] = 0
                self.parents_[n] = -1
                
        def removeNode(self, n):
            Graph.removeNode(self, n)
            if n in self.parents_:
                del self.parents_[n]
            
        def removeEdge(self, u, v):
            Graph.removeEdge(self, u, v)
            if v in self.parents_:
                del self.parents_[v]
            
            return self.vis_edges_handles_[u][v]            
                   
        def addEdge(self, u, v=None, cost=None, input_control=None, vis_edges_handle = None):
            """Add a single edge (u,v) to the graph.
            >> G.add_edge(u,v)
            and
            >>> G.add_edge( (u,v) )
            are equivalent forms of adding a single edge between nodes u and v.
            The nodes u and v will be automatically added if not already in
            the graph.  They must be a hashable (except None) Python object.
     
            The following examples all add the edge (1,2) to graph G.
 
            >>> G=Graph()
            >>> G.add_edge( 1, 2 )          # explicit two node form
            >>> G.add_edge( (1,2) )         # single edge as tuple of two nodes
 
            """
            if v is None:
                (u,v)=u  # no v given, assume u is an edge tuple
            # add nodes
            if u not in self.adj_:
                self.adj_[u]={}
            if v not in self.adj_:
                self.adj_[v]={}
            # don't create self loops, fail silently, nodes are still added
            if u==v:
                return
            self.adj_[u][v]=cost
            
            if input_control != None:
                if u not in self.input_control_:
                    self.input_control_[u] = {}
                self.input_control_[u][v] = input_control
            
            if u not in self.vis_edges_handles_:
                self.vis_edges_handles_[u] = {}
            if vis_edges_handle is not None:
                self.vis_edges_handles_[u][v] = vis_edges_handle
            else:
                self.vis_edges_handles_[u][v] = None
            
            self.accum_cost_[v] = self.accum_cost_[u] + cost
            self.parents_[v] = u
            
            dfs(self, v)
            
def dfs(tree, source):
    """
    Traverse the graph G with depth-first-search from source.
    Return list of nodes connected to source in DFS order.
    
    The objective is to update the cost using a DFS
    """
    nlist=[] # list of nodes in a DFS order
    seen={} # nodes seen
    queue=[source]  # use as LIFO queue
    seen[source]=True
    while queue:
        v=queue.pop()
        nlist.append(v)
        for w in tree.getNeighbors(v):
            if w not in seen:
                seen[w]=True
                
                if w != source:
                    tree[w] = tree[tree.getParent(w)] + tree.getEdgeCost(tree.getParent(w), w)
                    
                queue.append(w)
    return nlist