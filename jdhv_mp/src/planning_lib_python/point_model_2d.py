"""
Created on May 8, 2014

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
"""

import numpy as np

class PointModel2D(object):
    """
    Differential model of a point moving in a 2-dimensional (2D) workspace.
    """

    def __init__(self, step_size):
        """
        Constructor
        """
        self.step_size_ = step_size
        return
    
    def calcNewConf(self, q_init, input_control):
        """
        Calculate new (next) configuration given an input
        
        q_init: initial configuration where input is applied
        input_control: input to applied
        """
        q_new = q_init + input_control*self.step_size_
        
        return q_new
    
    def findInput(self, q1_from, q2_to):
        """
        Calculate or estimate the necessary input to move from q1 to q2.
        
        q1_vec: initial configuration
        q2_vec: goal configuration
        """
        q1_to_q2 = q2_to - q1_from
        q1_to_q2 = q1_to_q2 / np.linalg.norm(q1_to_q2)
        
        return q1_to_q2
        