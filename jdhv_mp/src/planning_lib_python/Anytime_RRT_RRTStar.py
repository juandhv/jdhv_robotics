"""
Created on Dec 11, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
Purpose: Basic implementation of Rapidly-exploring Random Tree * (RRT*). 
C-space --> R2.
"""

from tree import Tree
from RRT import RRT
import threading

import numpy as np

TRAPPED = 0
REACHED = 1
ADVANCED = 2

EPSILON_FOUND_DISTANCE = 20.0

class Anytime_RRT_RRTStar(RRT):
    """
    Implementation of Rapidly-exploring Random Tree*, basic RRT*.
    """

    def __init__(self, ws, num_attempts, max_comp_time, epsilon_goal_dist, step_size, diff_model, goal_prob):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size: step size for each expansion
        num_close_neigh: number of closest neighbors for second extension
        """
        RRT.__init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model)
        self.goal_prob_ = goal_prob
        self.stop_flag_ = False
        self.sol_found_ = False
        self.max_comp_time_ = max_comp_time
        
        self.close_to_goal_nodes_ = []
        
        return
    
    def stopMessage(self):
        self.stop_flag_ = True
    
    def build(self, q_init_vec, q_goal_vec):
        """
        Build a RRT from a initial configuration
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        t = threading.Timer(self.max_comp_time_, self.stopMessage)
        t.start() # after 30 seconds, "hello, world" will be printed
        
        result_path = []
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            if self.sol_found_:
                q_rand_vec = self.ws_.generateRandomConfXYStar(self.goal_prob_, q_goal_vec)
            else:
                q_rand_vec = self.ws_.generateRandomConfXY(self.goal_prob_, q_goal_vec)

            if self.extend('init', q_rand_vec) != TRAPPED:
                if self.ws_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec) < EPSILON_FOUND_DISTANCE:
#                     self.close_to_goal_nodes_.append(self.q_init_tree_.getOrder()-1)
                    print "solution found"
                    self.sol_found_ = True
            attempt_i += 1
            if self.stop_flag_:
                print "attempts:", attempt_i
                break
        if self.sol_found_:
            q_goal = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_goal, q_goal_vec)
            closest_neighbors_qi = self.findClosestNeighbors(q_goal, EPSILON_FOUND_DISTANCE)
            q_min_cost = closest_neighbors_qi[0][0]
            min_cost = self.q_init_tree_[closest_neighbors_qi[0][0]]
            for closest_qi in closest_neighbors_qi:
                if self.q_init_tree_[closest_qi[0]] < min_cost:
                    q_min_cost = closest_qi[0]
                    min_cost = self.q_init_tree_[closest_qi[0]]

            self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_goal_vec)
            self.q_init_tree_.addEdge(q_min_cost, self.q_init_tree_.getOrder() - 1, EPSILON_FOUND_DISTANCE)
            result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1)
            
        return result_path
    
    def extend(self, tree_id, q_vec):
        if self.sol_found_:
            return self.extendRRTStar(tree_id, q_vec)
        else:
            return self.extendRRT(tree_id, q_vec)
            
    def extendRRT(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        if tree_id == 'init':
            q_near_vec = self.q_init_tree_.getConfQ(q_near)
        elif tree_id == 'goal':
            q_near_vec = self.q_goal_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.diff_model_.findInput(q_near_vec, q_vec)
        q_new_vec = self.diff_model_.calcNewConf(q_near_vec, u_q_near_to_q_rand)

        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_near_vec, q_new_vec):
            if tree_id == 'init':
                q_new = self.q_init_tree_.getOrder()
                self.q_init_tree_.addNode(q_new, q_new_vec)
                self.q_init_tree_.addEdge(q_near, q_new, self.step_size_, None, self.ws_.visualizeEdge(q_near_vec, q_new_vec,'-r'))
            elif tree_id == 'goal':
                q_new = self.q_goal_tree_.getOrder()
                self.q_goal_tree_.addNode(q_new, q_new_vec)
                self.q_goal_tree_.addEdge(q_near, q_new, self.step_size_)
                self.ws_.visualizeEdge(q_near_vec, q_new_vec,'-b')

            if self.ws_.dist(q_new_vec, q_vec) < EPSILON_FOUND_DISTANCE:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
    def extendRRTStar(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        q_near_vec = self.q_init_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.diff_model_.findInput(q_near_vec, q_vec)
        q_new_vec = self.diff_model_.calcNewConf(q_near_vec, u_q_near_to_q_rand)

        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_near_vec, q_new_vec):
            q_new = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_new, q_new_vec)

            #===================================================================
            # Select a near configuration that generate a minimum-cost path to q_init             
            #===================================================================
            q_min = q_near
            cost_min = self.q_init_tree_[q_min] + self.ws_.dist(self.q_init_tree_.getConfQ(q_min), q_new_vec)

            closest_neighbors_qi = self.findClosestNeighbors(q_new)
            for closest_qi in closest_neighbors_qi:
                closest_qi_vec = self.q_init_tree_.getConfQ(closest_qi[0])
                if not self.q_init_tree_.hasEdge(q_new, closest_qi[0]) and self.ws_.checkPathLocalPlanner(closest_qi_vec, q_new_vec):
                    cost = self.q_init_tree_[closest_qi[0]] + self.ws_.dist(self.q_init_tree_.getConfQ(closest_qi[0]), q_new_vec)
                    if cost < cost_min:
                        q_min = closest_qi[0]
                        cost_min = cost
            self.q_init_tree_.addEdge(q_min, q_new, self.ws_.dist(self.q_init_tree_.getConfQ(q_min), q_new_vec), None, self.ws_.visualizeEdge(self.q_init_tree_.getConfQ(q_min), q_new_vec,'-r'))#edge from a node i to a node j with a cost c
            
            #===================================================================
            # Reconnect near neighbors is a shorter path can be done with q_new
            #===================================================================
            for closest_qi in closest_neighbors_qi:
                if closest_qi != q_min:
                    closest_qi_vec = self.q_init_tree_.getConfQ(closest_qi[0])
                    poss_new_cost = self.q_init_tree_[q_new] + self.ws_.dist(self.q_init_tree_.getConfQ(q_new), closest_qi_vec)
                    
                    if self.ws_.checkPathLocalPlanner(q_new_vec, closest_qi_vec) and self.q_init_tree_[closest_qi[0]] > poss_new_cost:
                        q_parent = self.q_init_tree_.getParent(closest_qi[0])
                        vis_edge_handle = self.q_init_tree_.removeEdge(q_parent, closest_qi[0])
                        self.ws_.cleanEdge(vis_edge_handle)

                        self.q_init_tree_.addEdge(q_new, closest_qi[0], self.ws_.dist(q_new_vec, closest_qi_vec), None, self.ws_.visualizeEdge(q_new_vec, closest_qi_vec,'-r'))#edge from a node i to a node j with a cost c

            if self.ws_.dist(q_new_vec, q_vec) < EPSILON_FOUND_DISTANCE:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
    def findClosestNeighbor(self, tree_id, qi_vec):
        """
        Find the closest configurations to qi
        
        qi: current configuration
        """
        
        dist = 100000

        for qj in self.q_init_tree_:
#             if qj not in self.close_to_goal_nodes_:
            qj_vec = self.q_init_tree_.getConfQ(qj)
            disti = self.ws_.dist(qi_vec, qj_vec)
            if disti < dist:
                dist = disti
                q_clos = qj

        return q_clos
    
    def findClosestNeighbors(self, qi, ball_radius = None):
        """
        Find the k closest configurations to qi
         
        qi: current configuration
        """
         
        if ball_radius == None:
            ball_radius = 1.5 * self.step_size_
 
        qi_vec = self.q_init_tree_.getConfQ(qi)
         
        closest_neighbors = []
         
        #=======================================================================
        # Calculate the distance to all of the neighbors
        #=======================================================================
        for qj in self.q_init_tree_:
            if qj!=qi:
                if self.q_init_tree_.hasEdge(qi, qj):
                    cost_i_to_j = self.q_init_tree_.getEdgeCost(qi, qj)
                else:
                    qj_vec = self.q_init_tree_.getConfQ(qj)
                    cost_i_to_j = np.linalg.norm(qi_vec - qj_vec)
                if cost_i_to_j <= ball_radius:
                    closest_neighbors.append((qj, cost_i_to_j))
 
#         #=======================================================================
#         # Find the k closest neighbors 
#         #=======================================================================
#         closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
         
        return closest_neighbors