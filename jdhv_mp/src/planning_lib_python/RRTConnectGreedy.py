"""
Created on Dec 3, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Rapidly-exploring Random Trees (RRT).
This version assumes 2 trees grown from the initial and the goal configurations (RRTConnect) 
C-space --> R2.
"""

from tree import Tree
from RRTConnect import RRTConnect

TRAPPED = 0
REACHED = 1
ADVANCED = 2

EPSILON_FOUND_DISTANCE = 20.0

class RRTConnectGreedy(RRTConnect):
    """
    Implementation of RRTConnect.
    This version assumes 2 trees grown from the initial and the goal configurations (RRTConnect)
    """
    
    def build(self, q_init_vec, q_goal_vec):
        """
        Build an RRT both, initial and goal, trees
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        swap = 1
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        
        self.q_goal_tree_ = Tree()
        self.q_goal_tree_.addNode(self.q_goal_tree_.getOrder(), q_goal_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            q_rand_vec = self.ws_.generateRandomConf()
            if swap == 1: 
                if self.extend('init', q_rand_vec) != TRAPPED:
                    if self.connect('goal', self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder() - 1)) == REACHED:
                        result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1, self.q_goal_tree_.getOrder() - 1)
                        break
                swap = 2
            elif swap == 2:
                if self.extend('goal', q_rand_vec) != TRAPPED:
                    if self.connect('init', self.q_goal_tree_.getConfQ(self.q_goal_tree_.getOrder() - 1)) == REACHED:
                        result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1, self.q_goal_tree_.getOrder() - 1)
                        break
                swap = 1
            attempt_i += 1
            print "i:", attempt_i
        return result_path
    
    def connect(self, tree_id, q_vec):
        """
        Attempt to connect the RRTs
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        
        while True:
            result = self.extend(tree_id, q_vec)
            if result != ADVANCED:
                break
        return result