"""
Created on Nov 14, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of Rapidly-exploring Random Trees (RRT). 
C-space --> R2.
"""

from tree import Tree

TRAPPED = 0
REACHED = 1
ADVANCED = 2

class RRT(object):
    """
    Implementation of Rapidly-exploring Random Trees, basic RRT.
    """

    def __init__(self, ws, num_attempts, epsilon_goal_dist, step_size, diff_model):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size: step size for each expansion
        """
        self.ws_ = ws
        self.num_attempts_ = num_attempts
        self.step_size_ = step_size
        self.goal_prob_ = 0.0
        self.epsilon_goal_dist_ = epsilon_goal_dist
        self.diff_model_ = diff_model
        
        return
    
    def solveQuery(self, q_init_vec, q_goal_vec):
        """
        Attempt to solve a query using a EST
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        
        self.ws_.visualizeSample(q_init_vec,'gs')
        self.ws_.visualizeSample(q_goal_vec,'bs')
        
        result_path = self.build(q_init_vec, q_goal_vec)
        
        if len(result_path) > 0:
            print "Query phase complete (Solution found)"
            self.visualizeQueryResult(result_path)
        else:
            print "Query phase complete (Solution not found)"
        
        return result_path
    
    def build(self, q_init_vec, q_goal_vec):
        """
        Build a RRT from a initial configuration
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        result_path = []
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            q_rand_vec = self.ws_.generateRandomConf(self.goal_prob_, q_goal_vec)
            if self.extend('init', q_rand_vec) != TRAPPED:
                if self.ws_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec) < self.epsilon_goal_dist_:
                    self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_goal_vec)
                    self.q_init_tree_.addEdge(self.q_init_tree_.getOrder() - 2, self.q_init_tree_.getOrder() - 1, self.epsilon_goal_dist_)
                    result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1)
                    break
            attempt_i += 1
            print "i:", attempt_i
        return result_path

    def extend(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        if tree_id == 'init':
            q_near_vec = self.q_init_tree_.getConfQ(q_near)
        elif tree_id == 'goal':
            q_near_vec = self.q_goal_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.diff_model_.findInput(q_near_vec, q_vec)
        q_new_vec = self.diff_model_.calcNewConf(q_near_vec, u_q_near_to_q_rand)

        if not self.ws_.checkCollision(q_new_vec) and self.ws_.checkPathLocalPlanner(q_near_vec, q_new_vec):
            if tree_id == 'init':
                q_new = self.q_init_tree_.getOrder()
                self.q_init_tree_.addNode(q_new, q_new_vec)
                self.q_init_tree_.addEdge(q_near, q_new, self.step_size_)
                self.ws_.visualizeEdge(q_near_vec, q_new_vec,'-b')
            elif tree_id == 'goal':
                q_new = self.q_goal_tree_.getOrder()
                self.q_goal_tree_.addNode(q_new, q_new_vec)
                self.q_goal_tree_.addEdge(q_near, q_new, self.step_size_)
                self.ws_.visualizeEdge(q_near_vec, q_new_vec,'-b')

            if self.ws_.dist(q_new_vec, q_vec) < self.epsilon_goal_dist_:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
    def findClosestNeighbor(self, tree_id, qi_vec):
        """
        Find the closest configurations to qi
        
        qi: current configuration
        """
        dist = 100000

        if tree_id == 'init':
            for qj in self.q_init_tree_:
                qj_vec = self.q_init_tree_.getConfQ(qj)
                disti = self.ws_.dist(qi_vec, qj_vec)
                if disti < dist:
                    dist = disti
                    q_clos = qj
        elif tree_id == 'goal':
            for qj in self.q_goal_tree_:
                qj_vec = self.q_goal_tree_.getConfQ(qj)
                disti = self.ws_.dist(qi_vec, qj_vec)
                if disti < dist:
                    dist = disti
                    q_clos = qj
        return q_clos
    
    def reconstructPath(self, q):
        """
        Reconstruct the path
        
        q: leaf node
        """
        path = []
        path.append(q)
        while True:
            q = self.q_init_tree_.getParent(q)
            if q == -1:
                break
            else:
                path.append(q)
        path.reverse()
        return path
    
    def visualizeQueryResult(self,query_result):
        """
        Visualize the query result
        
        query_result: result of the query represented by a lists of configurations
        """
        for i in range(len(query_result)):
            self.ws_.visualizeSample(self.q_init_tree_.getConfQ(query_result[i]), 'g.')
            if i > 0:
                self.ws_.visualizeEdge(self.q_init_tree_.getConfQ(query_result[i-1]),self.q_init_tree_.getConfQ(query_result[i]), 'g', line_width=4.0)
        return