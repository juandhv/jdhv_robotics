"""
Created on Dec 18, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of a Optimal Rapidly-exploring Random Trees (RRT*).
This version assumes 2 trees grown from the initial and the goal configurations 
C-space --> R2*S1.
"""

from tree import Tree
from RRT import RRT
import threading

import numpy as np
import operator

TRAPPED = 0
REACHED = 1
ADVANCED = 2

EPSILON_FOUND_DISTANCE = 25.0

DELTA_T = 0.1
CAR_LENGTH = 100.
MAX_VEL = 20.
MAX_FI = np.pi / 4.

class RRTStarCar(RRT):
    """
    Implementation of Rapidly-exploring Random Trees, basic RRT.
    """

    def __init__(self, ws, max_map_dim, num_attempts, max_time_comp, step_size_time, goal_prob, num_close_neigh):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size_time: step size in time (s) for each expansion
        goal_prob: probability to define next node to be the goal
        """
        self.ws_ = ws
        self.max_map_dim_ = max_map_dim
        self.num_attempts_ = num_attempts
        self.step_size_time_ = step_size_time
        self.goal_prob_ = goal_prob
        self.stop_flag_ = False
        self.sol_found_ = False
        self.max_time_comp_ = max_time_comp

        return
    
    def solveQuery(self, q_init_vec, q_goal_vec):
        """
        Attempt to solve a query using a EST
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        
        self.ws_.visualizeSample(q_init_vec,'g')
        self.ws_.visualizeSample(q_goal_vec,'b')
        
        self.q_init_vec_ = np.copy(q_init_vec)
        self.q_goal_vec_ = np.copy(q_goal_vec)
        
        result_path = self.build(q_init_vec, q_goal_vec)
        
        if len(result_path) > 0:
            print "Query phase complete (Solution found)"
            self.visualizeQueryResult(result_path)
        else:
            print "Query phase complete (Solution not found)"
        
        return result_path
    
    def stopMessage(self):
        """
        Stop flag activated by a timer
        """
        self.stop_flag_ = True

    def build(self, q_init_vec, q_goal_vec):
        """
        Build a RRT from a initial configuration
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        t = threading.Timer(self.max_time_comp_, self.stopMessage)
        t.start() # after 30 seconds, "hello, world" will be printed
        
        result_path = []
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        while attempt_i < self.num_attempts_:
            q_rand_vec = self.ws_.generateRandomConf(self.goal_prob_, q_goal_vec)
            if self.extend('init', q_rand_vec) != TRAPPED:
                dist_to_goal = self.ws_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec)#self.ws_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec)
                if dist_to_goal < EPSILON_FOUND_DISTANCE:
                    print "solution found"
                    self.sol_found_ = True
            attempt_i += 1
            if self.stop_flag_:
                print "attempts:", attempt_i
                break
        if self.sol_found_:
            q_goal = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_goal, q_goal_vec)
            closest_neighbors_qi = self.findClosestNeighbors(q_goal, EPSILON_FOUND_DISTANCE)
            print "possible solutions", closest_neighbors_qi
            q_min_cost = closest_neighbors_qi[0][0]
            min_cost = self.q_init_tree_[closest_neighbors_qi[0][0]]
            for closest_qi in closest_neighbors_qi:
                if self.q_init_tree_[closest_qi[0]] < min_cost:
                    q_min_cost = closest_qi[0]
                    min_cost = self.q_init_tree_[closest_qi[0]]
            
            self.q_init_tree_.addEdge(q_min_cost, self.q_init_tree_.getOrder() - 1, EPSILON_FOUND_DISTANCE)
            result_path = self.reconstructPath(self.q_init_tree_.getOrder() - 1)
            
        return result_path

    def extend(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        q_near_vec = self.q_init_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.findInput(q_near_vec, q_vec)
        q_new_vec, collision_flag, close_to_goal = self.calcNewConf(q_init = q_near_vec,\
                                                                    input_control = u_q_near_to_q_rand, \
                                                                    delta_t_integ = DELTA_T, \
                                                                    colli_check_flag = True,\
                                                                    draw_flag = True)

        if not collision_flag:
            q_new = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_new, q_new_vec)
            self.q_init_tree_.addEdge(q_near, q_new, self.step_size_time_)

            if self.ws_.dist(q_new_vec, q_vec) < EPSILON_FOUND_DISTANCE:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
    def findClosestNeighbor(self, tree_id, qi_vec):
        """
        Find the closest configurations to qi
        
        qi: current configuration
        """
        dist = 100000

        if tree_id == 'init':
            for qj in self.q_init_tree_:
                qj_vec = self.q_init_tree_.getConfQ(qj)
                disti = self.ws_.dist(qi_vec, qj_vec) #self.ws_.distCar(qi_vec, qj_vec)
                if disti < dist:
                    dist = disti
                    q_clos = qj
        elif tree_id == 'goal':
            for qj in self.q_goal_tree_:
                qj_vec = self.q_goal_tree_.getConfQ(qj)
                disti = self.ws_.dist(qi_vec, qj_vec) #self.ws_.distCar(qi_vec, qj_vec)
                if disti < dist:
                    dist = disti
                    q_clos = qj
        return q_clos
    
    def findClosestNeighbors(self, qi, ball_radius = None):
        """
        Find the k closest configurations to qi
         
        qi: current configuration
        """
         
        if ball_radius == None:
            ball_radius = 1.5 * self.step_size_
 
        qi_vec = self.q_init_tree_.getConfQ(qi)
         
        closest_neighbors = []
         
        #=======================================================================
        # Calculate the distance to all of the neighbors
        #=======================================================================
        for qj in self.q_init_tree_:
            if qj!=qi:
                if self.q_init_tree_.hasEdge(qi, qj):
                    cost_i_to_j = self.q_init_tree_.getEdgeCost(qi, qj)
                else:
                    qj_vec = self.q_init_tree_.getConfQ(qj)
                    cost_i_to_j = np.linalg.norm(qi_vec - qj_vec)
                if cost_i_to_j <= ball_radius:
                    closest_neighbors.append((qj, cost_i_to_j))
 
#         #=======================================================================
#         # Find the k closest neighbors 
#         #=======================================================================
#         closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
         
        return closest_neighbors
    
    def calcNewConf(self, q_init, input_control, delta_t_integ = DELTA_T, colli_check_flag = False, draw_flag = False):
        """
        Calculate new (next) configuration given an input
        
        q_init: initial configuration where input is applied
        input_control: input to applied
        """
        t = delta_t_integ
        
        q_i_1 = np.array(q_init)
        q_i = np.zeros(3)
        
        q_x_plot = [q_i_1[0]]
        q_y_plot = [q_i_1[1]]
        
        check_coll_count = 1
        while t <= self.step_size_time_:
            q_i[0] = q_i_1[0] + delta_t_integ*input_control['v']*np.cos(q_i_1[2])
            q_i[1] = q_i_1[1] + delta_t_integ*input_control['v']*np.sin(q_i_1[2])
            q_i[2] = q_i_1[2] + delta_t_integ*(input_control['v']/CAR_LENGTH)*np.tan(input_control['fi'])
            
            if q_i[2] > np.pi:
                q_i[2] = -np.pi + (q_i[2] - np.pi)
            elif q_i[2] < -np.pi:
                q_i[2] = np.pi - (-np.pi - q_i[2])
                
            if q_i[2] > 2*np.pi:
                print q_i[2]
                raw_input("possible error2")
            
            if draw_flag:
                q_x_plot.append(q_i[0])
                q_y_plot.append(q_i[1])
#             else:
#                 q_x_plot.append(q_i[0])
#                 q_y_plot.append(q_i[1])

            if colli_check_flag:
                if check_coll_count%5 == 0 and self.ws_.checkCollision(q_i):
                    return np.array(q_i), True, False
            
            if self.ws_.dist(q_i, self.q_goal_vec_) < EPSILON_FOUND_DISTANCE:
                self.ws_.visualizeEdge(q_x_plot, q_y_plot,'-r')
                return np.array(q_i), False, True
            
            q_i_1[0] = q_i[0]
            q_i_1[1] = q_i[1]
            q_i_1[2] = q_i[2]
            
            t += delta_t_integ
            check_coll_count += 1
        
        if draw_flag:
            self.ws_.visualizeEdge(q_x_plot, q_y_plot,'-r')
#         else:
#             self.ws_.visualizeEdgeCar(q_x_plot, q_y_plot,'-b')

        q_new_vec = np.array(q_i)
        
        return q_new_vec, False, False
    
    def findInput(self, q1_vec_from, q2_vec_to):
        """
        Calculate or estimate the necessary input to move from q1 to q2.
          
        q1_vec: initial configuration
        q2_vec: goal configuration
        """
        
#         v = MAX_VEL*np.random.rand() #MAX_VEL/2. + (MAX_VEL/2.)*np.random.rand()
#         linear_v = MAX_VEL*.4 + (MAX_VEL*.6)*np.random.rand()
        
        input_control = {}
        input_control['v'] = MAX_VEL#linear_v
        index = -1
        dists = {}
        fis = []
        
        for fi_angle in np.arange(-MAX_FI, MAX_FI+0.1, MAX_FI/2.):
            input_control['fi'] = fi_angle
            q_new_vec, collision_flag, close_to_goal = self.calcNewConf(q1_vec_from, \
                                                         input_control = input_control, \
                                                         delta_t_integ = DELTA_T*10, \
                                                         colli_check_flag = False, \
                                                         draw_flag = False)
            
            if close_to_goal:
                return input_control
            
            index += 1
            
            if collision_flag:
                dists[index] = 100000
            else:
                dists[index] = self.ws_.dist(q_new_vec, q2_vec_to)
            fis.append(fi_angle)
        dists_index = sorted(dists.iteritems(), key=operator.itemgetter(1))
        
        if dists_index[0][1] > dists_index[1][1]:
            fi_min = fis[dists_index[1][0]]
        else:
            fi_min = fis[dists_index[0][0]]
            
        input_control['fi'] = (MAX_FI/2.)*np.random.rand() + fi_min
        
#         print "q1_vec_from:", q1_vec_from
#         print "dists_index:", dists_index 
#         print "fi_min:", fi_min
#         print "collision_flag:", collision_flag
            
        
  
        return input_control

    def reconstructPath(self, q):
        """
        Reconstruct the path
        
        q: leaf node
        """
        path = []
        path.append(q)
        while True:
            q = self.q_init_tree_.getParent(q)
            if q == -1:
                break
            else:
                path.append(q)
        path.reverse()
        return path
    
    def visualizeQueryResult(self,query_result):
        """
        Visualize the query result
        
        query_result: result of the query represented by a lists of configurations
        """
        for i in range(len(query_result) - 1):
            if i > 0:
                self.ws_.visualizeSample(self.q_init_tree_.getConfQ(query_result[i]),'g')
        return