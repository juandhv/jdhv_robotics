#!/usr/bin/env python
#
# Creation:
#   July 2013
#
# Author:
#   Guillem Vallicrosa
#
# Description:
#   This code is intended as a temporary replacement of communication node for 
#   simulations and first tests on vehicle formations.

# ROS basic
import roslib
roslib.load_manifest('fake_comm_morph')
import rospy

from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry

# UDP connection
import select
import socket
import struct

# Toggle debug
DEBUG = True
MODEM_PERIOD = 3.0

#=======================================================================
class FakeComm(object):
    '''
    Class to handle sending Pose_in_Section messages to all other vehicles, and
    construct Poses_in_Section messages from what it received from the others.
    '''
    
    #===================================================================
    def __init__(self):
        '''
        Loads the parameters needed and starts the publishers and subscribers.
        '''
        # Parameters
        broadcast = rospy.get_param("~broadcast_ip", "192.168.1.255")
        self.address = (broadcast, 9604)
        self.ID_ = 0#None
        rospy.loginfo('fake_comms: broadcasting to %s', broadcast)
        
        # Socket to send
        self.sock_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_send.bind(('', 0))
        self.sock_send.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        
        # Socket to read
        self.sock_recv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock_recv.settimeout(0.05)
        self.sock_recv.bind(('', 9604))
        
        # Publisher
        self.pub = rospy.Publisher('pose_other_vehicles', Odometry)
        self.secPoses = []
        self.odom_msg_ = Odometry()
        self.odom_msg_.header.frame_id = 'world'
        
        # Subscriber
        self.sub = rospy.Subscriber('/pose', Pose, self.poseSecCallback, queue_size = 10)
        #Navigation data (feedback)
        self.nav_sts_sub_ = rospy.Subscriber("/pose_ekf_slam/odometry", Odometry, self.navStsSubCallback, queue_size = 10)
        self.nav_sts_available_ = False
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        print "Waiting for nav sts..."
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.nav_sts_available_:
            r.sleep()
        print "nav sts Ok"
        
        # Time
        self.time = rospy.Time.now()

    def navStsSubCallback(self,nav_sts_msg):
        """
        Callback to receive navSts message
        """
        self.nav_sts_data_ = nav_sts_msg
        if self.nav_sts_available_ == False:
            self.nav_sts_available_ = True
        
        return
    
    #===================================================================
    def iterate(self):
        '''
        Receives information from all vehicles and publishes accordingly.
        '''
        # Need to know own ID
        if self.ID_ is None:
            return

        ## RECEIVE
        # Check availability
        rlist, dummyw, dummyx = select.select([self.sock_recv], [], [], 0.01)
        
        # Something to read
        while rlist:
            
            # Receive and deserialize message
            fmt = "< I 7f"
            data = self.sock_recv.recv(struct.calcsize(fmt))
            msg_ID, msg = self.deserialize(data)
            
            # Check if its myself
            if msg_ID == self.ID_:
                # Recheck availability and start again
                rlist, dummyw, dummyx = select.select([self.sock_recv], [], [], 0.01)
                continue
            
#             # Search and add msg
#             found = False
#             for i in range(len(self.secPoses)):
#                 if msg_ID == self.secPoses[i].ID:
#                     if DEBUG: print '(update)',
#                     self.secPoses[i] = msg
#                     found = True
#             if not found:
#                 if DEBUG: print '(new)',
#                 self.secPoses.append(msg)
                
            # Debug info
#             if DEBUG:
#                 print 'ID: %d (%s) %d' % (msg_ID, ('new', 'update')[found], \
#                                            msg.header.stamp.to_sec()) 
                
            # Publish all Poses (not all times)
#             if msg_ID == self.secPoses[0].ID:
#                 self.msg.header.stamp = rospy.Time.now()
#                 self.msg.pose = self.secPoses
            self.odom_msg_ = self.nav_sts_data_
            self.odom_msg_.header.stamp = rospy.Time.now()
            self.odom_msg_.pose.pose = msg
            self.pub.publish(self.odom_msg_)
                
            # Recheck availability
            rlist, dummyw, dummyx = select.select([self.sock_recv], [], [], 0.01)
    
    #===================================================================
    def poseSecCallback(self, msg):
        '''
        Reads Pose_in_Section message and sends it through UDP.
        '''
        # Do not do it too fast
        if (rospy.Time.now() - self.time).to_sec() <= 0.5:
            return
            
        # Update time
#         self.time = msg.header.stamp
#         
#         if self.ID is None:
#             rospy.loginfo('ID initialized to: %s', msg.ID)
#         
        # Send data
#         self.ID = msg.ID
        data = self.serialize(msg)
        count = 0
        while count < len(data):
            count += self.sock_send.sendto(data, self.address)
    
    #===================================================================
    def serialize(self, msg):
        '''
        Serializes a Pose_in_Section message.
        '''
        #fmt = "< I 2i 20s B 2f b"
        fmt = "< I 7f"
#         data = struct.pack(fmt, msg.header.seq, msg.header.stamp.secs, \
#                msg.header.stamp.nsecs, msg.header.frame_id, msg.ID, msg.Gamma, \
#                msg.Beta, msg.Flag)
        data = struct.pack(fmt, self.ID_, msg.position.x, msg.position.y, msg.position.z, \
               self.nav_sts_data_.pose.pose.orientation.x, self.nav_sts_data_.pose.pose.orientation.y, self.nav_sts_data_.pose.pose.orientation.z, self.nav_sts_data_.pose.pose.orientation.w)
               
        return data
    
    #===================================================================
    def deserialize(self, data):
        '''
        Deserializes a Pose_in_Section message.
        '''
        fmt = "< I 7f"
        data = struct.unpack(fmt, data)
        msg_ID = data[0]
        msg = Pose()
        msg.position.x         = data[1]
        msg.position.y         = data[2]
        msg.position.z         = data[3]
        msg.orientation.x      = data[4]
        msg.orientation.y      = data[5]
        msg.orientation.z      = data[6]
        msg.orientation.w      = data[7]
        
        return msg_ID, msg

#=======================================================================
if __name__ == '__main__':
    
    # Initialize ROS node
    rospy.init_node('aggregator_client')
    rate = rospy.Rate(4.0)
    
    # Start node
    comm = FakeComm()
    while not rospy.is_shutdown():
        comm.iterate()
        rate.sleep()
    
    # Closing
    comm.sock_send.close()
    comm.sock_recv.close()
