/*
 * icra14_state_validity_checker.cpp
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

#include "state_validity_checker_R3_comarob14.hpp"

OmStateValidityChecker::OmStateValidityChecker(const ob::SpaceInformationPtr &si, const double exploration_altitue, const bool laser_octomap_flag) :
	ob::StateValidityChecker(si)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	exploration_altitue_ = exploration_altitue;
	if(laser_octomap_flag)
		serv_name = "/laser_octomap/get_binary";
	else
		serv_name = "octomap_binary";
	octree_ = NULL;
	length_g500_ = 1.5;

	ROS_INFO("Requesting the map from %s...", node_hand_.resolveName(serv_name).c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_){
			ROS_INFO("Octomap received (%zu nodes, %f m res)", octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmStateValidityChecker::isValid(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;

	// extract the component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = state->as<ob::RealVectorStateSpace::StateType>();

	for(double xi = pos->values[0]-(length_g500_/2.0);xi <= pos->values[0]+(length_g500_/2.0);xi=xi+octree_res_)
		for(double yi = pos->values[1]-(length_g500_/2.0);yi <= pos->values[1]+(length_g500_/2.0);yi=yi+octree_res_)
			for(double zi = pos->values[2]-(length_g500_/2.0);zi <= pos->values[2]+(length_g500_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = true;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						break;
					}
				}
			}
	return !collision;
}

bool OmStateValidityChecker::isCollision(std::vector<double> pos_query) const
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;

	for(double xi = pos_query[0]-(length_g500_/2.0);xi <= pos_query[0]+(length_g500_/2.0);xi=xi+octree_res_)
		for(double yi = pos_query[1]-(length_g500_/2.0);yi <= pos_query[1]+(length_g500_/2.0);yi=yi+octree_res_)
			for(double zi = pos_query[2]-(length_g500_/2.0);zi <= pos_query[2]+(length_g500_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = true;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						break;
					}
				}
			}

	return collision;
}

OmStateValidityChecker::~OmStateValidityChecker()
{
    delete octree_;
}


VisibilityObjective::VisibilityObjective(const ob::SpaceInformationPtr& si, const double exploration_altitue, const bool laser_octomap_flag) :
        		ob::StateCostIntegralObjective(si, true)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	exploration_altitue_ = exploration_altitue;
	std::cout << "Checking cost:" << std:: endl;
	if(laser_octomap_flag)
		serv_name = "/laser_octomap/get_binary";
	else
		serv_name = "octomap_binary";
	octree_ = NULL;
	length_g500_ = 1.5;

	ROS_INFO("Requesting the map from %s...", node_hand_.resolveName(serv_name).c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_){
			ROS_INFO("Octomap received (%zu nodes, %f m res)", octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

ob::Cost VisibilityObjective::stateCost(const ob::State* state) const
{
	point3d query;
	OcTreeNode* result;
	bool collision;
	double cost, bottom_z, z0, node_occupancy;

	// Extract the component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = state->as<ob::RealVectorStateSpace::StateType>();

	// Find the bottom
	z0 = pos->values[2];
	collision = false;
	while(!collision)
	{
		for(double xi = pos->values[0]-(length_g500_/2.0);xi <= pos->values[0]+(length_g500_/2.0);xi=xi+octree_res_)
			for(double yi = pos->values[1]-(length_g500_/2.0);yi <= pos->values[1]+(length_g500_/2.0);yi=yi+octree_res_)
				for(double zi = z0-(length_g500_/2.0);zi <= z0+(length_g500_/2.0);zi=zi+octree_res_){
					query.x() = xi;
					query.y() = yi;
					query.z() = zi;
					result = octree_->search (query);

					if(result == NULL){
						bottom_z = zi - octree_res_;
						collision = true;
						break;
					}
					else{
						node_occupancy = result->getOccupancy();
						if (node_occupancy > 0.4)
						{
							bottom_z = zi - octree_res_;
							collision = true;
							break;
						}
					}
				}
		z0 = z0 + octree_res_;
	}
	//

	if(pos->values[2] > (bottom_z - exploration_altitue_))
		cost = 10.0;
	else
		cost = ((bottom_z - pos->values[2]) / bottom_z) * 10.0;

	//	std::cout << "Z:" << pos->values[2] << std::endl;
	//	std::cout << "bottom_z:" << bottom_z << std::endl;
	//	std::cout << "cost:" << cost << std::endl;

	return ob::Cost(cost);
}

/** Return an optimization objective which attempts to steer the robot
    away from obstacles. */
ob::OptimizationObjectivePtr getVisibilityObjective(const ob::SpaceInformationPtr& si, const double exploration_altitue, const bool laser_octomap_flag)
{
    return ob::OptimizationObjectivePtr(new VisibilityObjective(si, exploration_altitue, laser_octomap_flag));
}
