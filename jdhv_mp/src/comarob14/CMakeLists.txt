set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_executable(online_map_plan_R3_comarob14
cpp_files/online_map_plan_R3_comarob14_main.cpp
cpp_files/online_map_plan_R3_comarob14.cpp
cpp_files/state_validity_checker_R3_comarob14.cpp
)
target_link_libraries(icra14_t3_online_map_plan_R3 ${catkin_LIBRARIES} ${OMPL_LIBRARIES})
#target_link_libraries(online_map_plan_R3_comarob14 ${catkin_LIBRARIES} ompl)
