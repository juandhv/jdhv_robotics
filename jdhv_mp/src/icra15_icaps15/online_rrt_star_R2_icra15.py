#!/usr/bin/env python
"""
Created on Jan 15, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Basic implementation of an Optimal Rapidly-exploring Random Trees (RRT*).
This version assumes 2 trees grown from the initial and the goal configurations 
C-space --> R2*S1.
"""

# ROS imports
import roslib; roslib.load_manifest('jdhv_mp')
import rospy

from visualization_msgs.msg import Marker

from std_msgs.msg import Bool
from auv_msgs.msg import NavSts, WorldWaypointReq, BodyVelocityReq, GoalDescriptor
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point

import numpy as np
import operator
from threading import Thread
from collections import deque

from planning_lib_python.tree import Tree
from planning_lib_python.c_spaces import PointR2
from planning_lib_python.mission import Mission
from planning_lib_python.point_model_2d import PointModel2D

import octomap_swig.state_validity_checker_octomap as wrappers_swig

TRAPPED = 0
REACHED = 1
ADVANCED = 2
 
class OnlineRRTStar():
    """
    Implementation of Rapidly-exploring Random Tree*, basic RRT*.
    """

    def __init__(self, c_space, num_attempts, max_comp_time, epsilon_goal_dist, step_size, diff_model, planning_depth, goal_prob):
        """
        Constructor
        
        ws: 2D workspace
        max_map_dim: Length of each side of the squared 2D workspace
        num_attempts: number of attempts to expand the tree
        step_size: step size for each expansion
        """
        
        #=======================================================================
        # Planner parameters
        #=======================================================================
        self.c_space_ = c_space
        self.num_attempts_ = num_attempts
        self.step_size_ = step_size
        self.epsilon_goal_dist_ = epsilon_goal_dist
        self.diff_model_ = diff_model

        self.planning_depth_ = planning_depth
        self.goal_prob_ = goal_prob
        self.stop_flag_ = False
        self.sol_found_ = False
        self.max_comp_time_ = max_comp_time
        
        self.rrt_build_flag_ = False

        self.state_val_checker_ = None 
        
        self.close_to_goal_nodes_ = []
        
        self.result_path_ = []
        
        #=======================================================================
        # Subscribers
        #=======================================================================
        #Navigation data (feedback)
        self.nav_sts_sub_ = rospy.Subscriber("/pose_ekf_slam/odometry", Odometry, self.navStsSubCallback, queue_size = 1)
        self.nav_sts_available_ = False
        
        #flag for indicating that more waypoints are needed
        self.wp_need_flag_sub_ = rospy.Subscriber("/jdhv_robotics/need_waypoints", Bool, self.wpNeedSubCallback, queue_size = 1)
        self.wp_need_flag_ = True
        
        #=======================================================================
        # Publishers
        #=======================================================================
        self.online_traj_point_pub_ = rospy.Publisher("/jdhv_robotics/online_traj_point", Point)
        self.online_traj_pub_ = rospy.Publisher("/jdhv_robotics/online_traj", Marker)
        self.cancel_wps_pub_ = rospy.Publisher("/jdhv_robotics/cancel_wps", Bool)
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        rospy.logwarn("%s: waiting for navigation data", rospy.get_name())
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.nav_sts_available_:
            r.sleep()
        rospy.logwarn("%s: navigation data received", rospy.get_name())
        
        #=======================================================================
        # Register handler to be called when rospy process begins shutdown.
        #=======================================================================
        rospy.on_shutdown(self.shutdownHook)
        
        return
    
    def __del__(self):
        """
        Destructor
        """
        return
    
    def shutdownHook(self):
        """
        Shutdown routine
        """
        self.stop_flag_ = True
        rospy.loginfo("%s: shutting down node", rospy.get_name())
        rospy.signal_shutdown("manual shutdown")
        return
    
    def navStsSubCallback(self, nav_sts_msg):
        """
        Callback to receive navSts message
        """
        self.nav_sts_data_ = nav_sts_msg
        if self.nav_sts_available_ == False:
            self.nav_sts_available_ = True
        return
    
    def wpNeedSubCallback(self, wp_need_flag_msg):
        """
        Callback to received messages that indicates if waypoints are needed
        """
        self.wp_need_flag_ = wp_need_flag_msg.data
        if not self.wp_need_flag_:
            #rospy.logdebug("%s received that NO more waypoints required", rospy.get_name())
            pass
        else:
            #rospy.logdebug("%s received that MORE waypoints required", rospy.get_name())
            pass
        return
    
    def setupPlannerQuery(self, q_init_vec, q_goal_vec):
        """
        Setup initial and goal configuration for solving a query
        with the RRT
        
        q_init_vec: initial configuration
        q_goal_vec: goal configuration
        """
        self.q_init_vec_ = q_init_vec
        self.q_goal_vec_ = q_goal_vec
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        self.q_init_tree_ = Tree()
        self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), q_init_vec)
        return

    def build(self, event=None):
        """
        Build a RRT from a initial configuration        
        """
        
        if self.rrt_build_flag_:
            rospy.loginfo("%s: return from build, waiting for rrt_build_flag_",rospy.get_name())
            return
        
        #=======================================================================
        # Get an update octomap
        #=======================================================================
        self.rrt_build_flag_ = True

        online_planner.updateOctomap()

        q_goal_vec = self.q_goal_vec_
        #=======================================================================
        # Making k attempts to extend the tree
        #=======================================================================
        attempt_i = 0
        
        while attempt_i < 100: #self.num_attempts_:
            if self.sol_found_:
                q_rand_vec = self.c_space_.generateRandomConfXYStar(self.goal_prob_, q_goal_vec)
            else:
                q_rand_vec = self.c_space_.generateRandomConfXY(self.goal_prob_, q_goal_vec)
  
            if self.extend('init', q_rand_vec) != TRAPPED:
                dist_to_goal = self.c_space_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec)#self.c_space_.dist(self.q_init_tree_.getConfQ(self.q_init_tree_.getOrder()-1), q_goal_vec)
                if dist_to_goal < self.epsilon_goal_dist_:
                    #self.close_to_goal_nodes_.append(self.q_init_tree_.getOrder()-1)
#                     rospy.loginfo("%s: a solution has been found", rospy.get_name())
                    self.sol_found_ = True
            attempt_i += 1
            if self.stop_flag_:
                print "attempts:", attempt_i
                break
        
        if self.sol_found_:
            q_goal = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_goal, q_goal_vec)
            closest_neighbors_qi = self.findClosestNeighbors(q_goal, self.epsilon_goal_dist_)
    
            q_min_cost = closest_neighbors_qi[0][0]
            min_cost = self.q_init_tree_[closest_neighbors_qi[0][0]]
            for closest_qi in closest_neighbors_qi:
                if self.q_init_tree_[closest_qi[0]] < min_cost:
                    q_min_cost = closest_qi[0]
                    min_cost = self.q_init_tree_[closest_qi[0]]
              
            self.q_init_tree_.addEdge(q_min_cost, self.q_init_tree_.getOrder() - 1, self.epsilon_goal_dist_)
            self.result_path_ = self.reconstructPath(self.q_init_tree_.getOrder() - 1)
            
            self.q_init_tree_.removeEdge(q_min_cost, self.q_init_tree_.getOrder() - 1)
            self.q_init_tree_.removeNode(self.q_init_tree_.getOrder() - 1)
            del self.result_path_[-1]
            
#             print "self.result_path_[1]:"
#             print self.q_init_tree_.getConfQ(self.result_path_[1])
#             print self.c_space_.isValidPoint(self.q_init_tree_.getConfQ(self.result_path_[1]))

            poss_new_root = self.result_path_[1]
            poss_new_root_vec = self.q_init_tree_.getConfQ(poss_new_root)
            if self.wp_need_flag_ and self.c_space_.isValidPoint(poss_new_root_vec):
                rospy.loginfo("%s: path found, next point is valid, sending to the controller", rospy.get_name())
                
                self.online_traj_point_pub_.publish(Point(poss_new_root_vec[0], poss_new_root_vec[1], self.planning_depth_))
                self.pruneTree(poss_new_root)
            elif not self.c_space_.isValidPoint(poss_new_root_vec):
                rospy.loginfo("%s: path found but point is not valid yet",rospy.get_name())
                if self.wp_need_flag_ and not self.c_space_.checkCollisionXY(poss_new_root_vec):
                    rospy.loginfo("%s: path found but point is not valid yet, but sending to the controller",rospy.get_name())
                    self.online_traj_point_pub_.publish(Point(poss_new_root_vec[0], poss_new_root_vec[1], self.planning_depth_))
                    self.pruneTree(poss_new_root)
            elif not self.wp_need_flag_:
                rospy.loginfo("%s: no waypoints needed now",rospy.get_name())
    
        self.visualizeRRT()    
        self.rrt_build_flag_ = False
#         self.sol_found_ = False #in case update octomap disconnect a previous solution
        return
    
    def extend(self, tree_id, q_vec):
        """
        Attempt to extend the RRT
        
        tree_id: is the tree to be extended (init or goal).
        q_vec: random configuration selected configuration from which the tree will be extended
        """
        #=======================================================================
        # Find closest neighbor and try to move from it to q_rand
        #=======================================================================
        q_near = self.findClosestNeighbor(tree_id, q_vec)
        q_near_vec = self.q_init_tree_.getConfQ(q_near)
        
        u_q_near_to_q_rand = self.diff_model_.findInput(q_near_vec, q_vec)
        q_new_vec = self.calcNewConf(q_near_vec, u_q_near_to_q_rand)

        if not self.c_space_.checkCollision(q_new_vec) and self.c_space_.checkPathLocalPlanner(q_near_vec, q_new_vec):
            q_new = self.q_init_tree_.getOrder()
            self.q_init_tree_.addNode(q_new, q_new_vec)

            #===================================================================
            # Select a near configuration that generate a minimum-cost path to q_init             
            #===================================================================
            q_min = q_near
            cost_min = self.q_init_tree_[q_min] + self.c_space_.dist(self.q_init_tree_.getConfQ(q_min), q_new_vec)

            closest_neighbors_qi = self.findClosestNeighbors(q_new)
            for closest_qi in closest_neighbors_qi:
                closest_qi_vec = self.q_init_tree_.getConfQ(closest_qi[0])
                if not self.q_init_tree_.hasEdge(q_new, closest_qi[0]) and self.c_space_.checkPathLocalPlanner(closest_qi_vec, q_new_vec):
                    cost = self.q_init_tree_[closest_qi[0]] + self.c_space_.dist(self.q_init_tree_.getConfQ(closest_qi[0]), q_new_vec)
                    if cost < cost_min:
                        q_min = closest_qi[0]
                        cost_min = cost
            self.q_init_tree_.addEdge(q_min, q_new, self.c_space_.dist(self.q_init_tree_.getConfQ(q_min), q_new_vec))#edge from a node i to a node j with a cost c
            
            #===================================================================
            # Reconnect near neighbors is a shorter path can be done with q_new
            #===================================================================
            for closest_qi in closest_neighbors_qi:
                if closest_qi != q_min:
                    closest_qi_vec = self.q_init_tree_.getConfQ(closest_qi[0])
                    poss_new_cost = self.q_init_tree_[q_new] + self.c_space_.dist(self.q_init_tree_.getConfQ(q_new), closest_qi_vec)
                    
                    if self.c_space_.checkPathLocalPlanner(q_new_vec, closest_qi_vec) and self.q_init_tree_[closest_qi[0]] > poss_new_cost:
                        q_parent = self.q_init_tree_.getParent(closest_qi[0])
                        vis_edge_handle = self.q_init_tree_.removeEdge(q_parent, closest_qi[0])
                        #self.c_space_.cleanEdge(vis_edge_handle)

                        self.q_init_tree_.addEdge(q_new, closest_qi[0], self.c_space_.dist(q_new_vec, closest_qi_vec))#edge from a node i to a node j with a cost c

            if self.c_space_.dist(q_new_vec, q_vec) < self.epsilon_goal_dist_:
                return REACHED
            else:
                return ADVANCED
        return TRAPPED
    
    def calcNewConf(self, q_init, input_control):
        """
        Calculate new (next) configuration given an input
        
        q_init: initial configuration where input is applied
        input_control: input to applied
        """
        q_new = q_init
        step = self.step_size_/10.0
        while step<self.step_size_:
            q_last = q_new
            q_new = q_last + input_control*(self.step_size_/10.0)
            step += self.step_size_/10.0
            if self.c_space_.dist(q_new, self.q_goal_vec_) < self.epsilon_goal_dist_:
                break
        
        return q_new
    
    def reconstructPath(self, q):
        """
        Reconstruct the path
        
        q: leaf node
        """
        path = []
        path.append(q)
        while True:
            q = self.q_init_tree_.getParent(q)
            if q == -1:
                break
            else:
                path.append(q)
        path.reverse()
        return path
    
    def updateOctomap(self, event = None):
        """
        Update the map and delete configurations in collision
        """
        
        self.c_space_.cleanCspace()
        self.c_space_.setCspace(wrappers_swig.OctomapStateValChecker(mission.get_planning_depth()))
        
        #DFS search to find configurations which can be on a collision according to the updated octomap 
        source = 0
        nlist=[] # list of nodes in a DFS order
        seen={} # nodes seen
        queue=[source]  # use as LIFO queue
        seen[source]=True
        while queue:
            v=queue.pop()
            nlist.append(v)
            
            q_vec = self.q_init_tree_.getConfQ(v)
            if self.q_init_tree_.getParent(v) != -1:
                q_parent_vec = self.q_init_tree_.getConfQ(self.q_init_tree_.getParent(v))
                collission_detected = not self.c_space_.checkPathLocalPlanner(q_parent_vec,q_vec)
            else:
                collission_detected = self.c_space_.checkCollisionXY(q_vec)
            
            if collission_detected and v == 0:
                rospy.logwarn("%s: imminent collision has been detected, goals need to be canceled",rospy.get_name())
                self.cancel_wps_pub_.publish(Bool(True))
                
                new_q_init_vec = np.array(mission.get_init_conf_2D())
                new_q_init_vec[0] = self.nav_sts_data_.pose.pose.position.x
                new_q_init_vec[1] = self.nav_sts_data_.pose.pose.position.y
                
                del self.q_init_tree_
                self.q_init_tree_ = Tree()
                self.q_init_tree_.addNode(self.q_init_tree_.getOrder(), new_q_init_vec)
                self.wp_need_flag_ = True
                self.sol_found_ = False
                rospy.logwarn("%s: New tree created and navigator has been informed that more wps are needed",rospy.get_name())
                return
                #self.avoidCollision(v)
            elif collission_detected:
#                 rospy.logdebug("%s: %d must be deleted",rospy.get_name(), v)
                self.avoidCollision(v)
#                 rospy.logdebug("%s: %d was deleted",rospy.get_name(), v)
                
                self.sol_found_ = False
                self.result_path_ = []            
            else:
                for w in self.q_init_tree_.getNeighbors(v):
                    if w not in seen:
                        seen[w]=True
                        queue.append(w)
        return
    
    def pruneTree(self, new_root):
        """
        Prune the tree
        """
        #=======================================================================
        # Adding initial configuration to the RRT
        #=======================================================================
        new_tree = Tree()
        new_tree.addNode(new_tree.getOrder(), self.q_init_tree_.getConfQ(new_root))
        equiv = {}#equivalences
        equiv[new_root] = new_tree.getOrder() - 1
        
        source_node = new_root
        #BFS search to find configurations which can be on a collision according to the updated octomap 
        nlist=[] # list of nodes in a DFS order
        seen={} # nodes seen
        queue=[source_node]  # use as LIFO queue
        
        seen[source_node]=True
        while queue:
            v=queue.pop()
            nlist.append(v)
            for w in self.q_init_tree_.getNeighbors(v):
                if w not in seen:
                    seen[w]=True

                    if w != source_node:
                        new_tree.addNode(new_tree.getOrder(), self.q_init_tree_.getConfQ(w))
                        equiv[w] = new_tree.getOrder() - 1
                        new_tree.addEdge(equiv[self.q_init_tree_.getParent(w)], equiv[w], self.q_init_tree_.getEdgeCost(self.q_init_tree_.getParent(w),w))

                    queue.append(w)
        
        del self.q_init_tree_
        self.q_init_tree_ = new_tree
        
        del self.result_path_[0]
        new_result_path = []
        for qi in self.result_path_:
            new_result_path.append(equiv[qi])
        
        self.result_path_ = new_result_path
        return
    
    def avoidCollision(self, source_node):
        #DFS search to find configurations which can be on a collision according to the updated octomap 
        nlist=[] # list of nodes in a DFS order
        seen={} # nodes seen
        queue=[source_node]  # use as LIFO queue
        seen[source_node]=True
        while queue:
            v=queue.pop()
            nlist.append(v)
            for w in self.q_init_tree_.getNeighbors(v):
                if w not in seen:
                    seen[w]=True

                    if w != source_node:
                        self.q_init_tree_.removeEdge(self.q_init_tree_.getParent(w),w)
                        
                    queue.append(w)
        
        self.q_init_tree_.removeEdge(self.q_init_tree_.getParent(source_node),source_node)
        for node in seen:
            self.q_init_tree_.removeNode(node)
        
        return
    
    def findClosestNeighbor(self, tree_id, qi_vec):
        """
        Find the closest configurations to qi
        
        qi: current configuration
        """
        
        dist = 100000

        for qj in self.q_init_tree_:
            #if qj not in self.close_to_goal_nodes_:
            qj_vec = self.q_init_tree_.getConfQ(qj)
            disti = self.c_space_.dist(qi_vec, qj_vec)
            if disti < dist:
                dist = disti
                q_clos = qj

        return q_clos
    
    def findClosestNeighbors(self, qi, ball_radius = None):
        """
        Find the k closest configurations to qi
         
        qi: current configuration
        """
         
        if ball_radius == None:
            ball_radius = 1.5 * self.step_size_
 
        qi_vec = self.q_init_tree_.getConfQ(qi)
         
        closest_neighbors = []
         
        #=======================================================================
        # Calculate the distance to all of the neighbors
        #=======================================================================
        for qj in self.q_init_tree_:
            if qj!=qi:
                if self.q_init_tree_.hasEdge(qi, qj):
                    cost_i_to_j = self.q_init_tree_.getEdgeCost(qi, qj)
                else:
                    qj_vec = self.q_init_tree_.getConfQ(qj)
                    cost_i_to_j = np.linalg.norm(qi_vec - qj_vec)
                if cost_i_to_j <= ball_radius:
                    closest_neighbors.append((qj, cost_i_to_j))
 
#         #=======================================================================
#         # Find the k closest neighbors 
#         #=======================================================================
#         closest_neighbors = sorted(neighbors.iteritems(), key=operator.itemgetter(1))
         
        return closest_neighbors
    
    def visualizeRRT(self, event=None):
        """
        Visualize RRT
        """
        
        q_init_goal = Marker()
        visual_rrt = Marker()
        result_path = Marker()
        
        result_path.header.frame_id = visual_rrt.header.frame_id = q_init_goal.header.frame_id = "/world"
        result_path.header.stamp = visual_rrt.header.stamp = q_init_goal.header.stamp = rospy.Time.now()
        q_init_goal.ns = 'online_planner_points'
        visual_rrt.ns = 'online_planner_rrt'
        result_path.ns = 'online_planner_result'
        q_init_goal.id = 0
        visual_rrt.id = 1
        result_path.id = 2
        result_path.type = q_init_goal.type = Marker.POINTS #Marker.SPHERE_LIST
        visual_rrt.type = Marker.LINE_LIST #Marker.SPHERE_LIST
        result_path.action = visual_rrt.action = q_init_goal.action = Marker.ADD
        result_path.pose.position.x = q_init_goal.pose.position.x = 0.0
        result_path.pose.position.y = q_init_goal.pose.position.y = 0.0
        result_path.pose.position.z = q_init_goal.pose.position.z = 0.0
        result_path.pose.orientation.x = q_init_goal.pose.orientation.x = 0.0
        result_path.pose.orientation.y = q_init_goal.pose.orientation.y = 0.0
        result_path.pose.orientation.z = q_init_goal.pose.orientation.z = 0.0
        result_path.pose.orientation.w = q_init_goal.pose.orientation.w = 1.0
        visual_rrt.scale.x = 0.05
        result_path.scale.x = q_init_goal.scale.x = 0.2
        result_path.scale.y = q_init_goal.scale.y = 0.2
        result_path.scale.z = q_init_goal.scale.z = 0.2
        result_path.color.a = visual_rrt.color.a = q_init_goal.color.a = 1.0
        result_path.color.r = q_init_goal.color.r = 0.0
        result_path.color.b = q_init_goal.color.b = 0.0
        result_path.color.g = visual_rrt.color.b = q_init_goal.color.g = 1.0
        
        q_init_goal.points.append(Point(self.q_init_vec_[0], self.q_init_vec_[1], self.planning_depth_))
        q_init_goal.points.append(Point(self.q_goal_vec_[0], self.q_goal_vec_[1], self.planning_depth_))

        #DFS
        source = 0
        nlist=[] # list of nodes in a DFS order
        seen={} # nodes seen
        queue=[source]  # use as LIFO queue
        seen[source]=True
        while queue:
            v=queue.pop()
            nlist.append(v)
            for w in self.q_init_tree_.getNeighbors(v):
                if w not in seen:
                    seen[w]=True
                      
                    if w != source:
                        q_parent_vec = self.q_init_tree_.getConfQ(self.q_init_tree_.getParent(w))
                        q_vec = self.q_init_tree_.getConfQ(w)
                        visual_rrt.points.append(Point(q_parent_vec[0], q_parent_vec[1], self.planning_depth_))
                        visual_rrt.points.append(Point(q_vec[0], q_vec[1], self.planning_depth_))
                        
                    queue.append(w)

        for q in self.result_path_:
            q_vec = self.q_init_tree_.getConfQ(q)
            result_path.points.append(Point(q_vec[0], q_vec[1], self.planning_depth_))

        self.online_traj_pub_.publish(q_init_goal)
        self.online_traj_pub_.publish(visual_rrt)
        self.online_traj_pub_.publish(result_path)

        return
    
    def stopMessage(self):
        """
        Stop flag activated by a timer
        """
        self.stop_flag_ = True
        return

if __name__ == '__main__':
    try:
        rospy.init_node('online_planner_icra15', log_level=rospy.INFO) #log_level=rospy.DEBUG
        rospy.loginfo("+++ Online planner: ICRA-2015 (Python) +++")
        
        #===========================================================================
        # Load planner and mission parameters
        #===========================================================================
#         test_id = "t1_auv"
        mission = Mission()
#         mission.load_parameters()

        #===========================================================================
        # Differential model of a point in a 2-dimensional (2D) workspace
        #===========================================================================
        sys_model_2d = PointModel2D(step_size = mission.get_step_size())
        
        #=======================================================================
        # Init: create planner
        #=======================================================================
        c_space = PointR2(type_ws = "XY", 
                          dim = mission.get_ws_bounds(),
                          planning_depth = mission.get_planning_depth())
        
        online_planner = OnlineRRTStar(c_space = c_space,
                                       num_attempts = mission.get_num_attempts(),
                                       max_comp_time = mission.get_max_comp_time(),
                                       epsilon_goal_dist = mission.get_epsilon_goal_dist(),
                                       step_size = mission.get_step_size(),
                                       diff_model = sys_model_2d,
                                       planning_depth = mission.get_planning_depth(),
                                       goal_prob = mission.get_goal_prob())
        
        #===========================================================================
        # Build the RRT and attempt to solve a query
        #===========================================================================
        q_init_vec = np.array(mission.get_init_conf_2D())
        q_goal_vec = np.array(mission.get_goal_conf_2D())
        
        #===========================================================================
        # Setup and start on-line planner
        #===========================================================================
        online_planner.setupPlannerQuery(q_init_vec, q_goal_vec)
        rospy.Timer(rospy.Duration(2), online_planner.build)

        #===========================================================================
        # Timer for visualization
        #===========================================================================
#         rospy.Timer(rospy.Duration(5), online_planner.visualizeRRT)
        
        rospy.spin()
            
    except rospy.ROSInterruptException:
        pass