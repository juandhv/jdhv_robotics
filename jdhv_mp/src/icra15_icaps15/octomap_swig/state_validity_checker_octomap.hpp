/* File: example.h */

#include <iostream>

#include <signal.h>

//ROS
#include <ros/ros.h>

#include <octomap/octomap.h>
#include <octomap/OcTree.h>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;

using namespace std;
using namespace octomap;

void stopNode(int sig);

class OctomapStateValChecker {
public:
	OctomapStateValChecker(double planning_depth);
	~OctomapStateValChecker();
	bool isValidPoint(double x, double y, double z);
	bool isCollCube(double x, double y, double z, double sideLength);
	double getOctomapRes(void);
private:
	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;
	double octree_res_;

	//Girona500 info
	double robot_length_;

	//planning parameters
	double planning_depth_;
};
