/* File: example.h */

#include "state_validity_checker_octomap.hpp"

void stopNode(int sig)
{
//	ros::shutdown();
	exit(0);
}

OctomapStateValChecker::OctomapStateValChecker(double planning_depth)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;
	int argc=0;

	planning_depth_ = planning_depth;

	//=======================================================================
	// Create node to be able to use ROS utilities
	//=======================================================================
	ros::init(argc, NULL, "octomap_client_swig");
	ros::NodeHandle node_hand_("~");

	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	serv_name = "/laser_octomap/get_binary";

	octree_ = NULL;
	robot_length_ = 1.5;

	ROS_INFO("Requesting the map from %s...", node_hand_.resolveName(serv_name).c_str());

	//node_hand_.ok() &&
	while(!ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_){
			ROS_INFO("Octomap received (%zu nodes, %f m res)", octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}

//	ros::shutdown();
}

OctomapStateValChecker::~OctomapStateValChecker()
{
	cout << "Delete Octree!!!" << endl;
	delete octree_;
}

bool OctomapStateValChecker::isValidPoint(double x, double y, double z)
{
	OcTreeNode* result;
	point3d query;
	double node_occupancy;

	query.x() = x;
	query.y() = y;
	query.z() = z;

	result = octree_->search (query);

	if(result == NULL){
		return false;
	}
	else{
		node_occupancy = result->getOccupancy();
		if (node_occupancy <= 0.4)
			return true;
	}
	return false;
}

bool OctomapStateValChecker::isCollCube(double x, double y, double z, double side_length)
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;

	for(double xi = x-(side_length/2.0);xi <= x+(side_length/2.0);xi=xi+octree_res_*1.5)
		for(double yi = y-(side_length/2.0);yi <= y+(side_length/2.0);yi=yi+octree_res_*1.5)
			for(double zi = z-(side_length/2.0);zi <= z+(side_length/2.0);zi=zi+octree_res_*1.5){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = false;//true;
					//break;
				}
				else{
					node_occupancy = result->getOccupancy();
//					cout << "not NULL" << endl;
//					cout << "node_occupancy: " << node_occupancy << endl;
					if (node_occupancy > 0.4)
					{
						collision = true;
						return collision;
						break;
					}
				}
			}

	return collision;
}

double OctomapStateValChecker::getOctomapRes(void)
{
	return octree_res_;
}
