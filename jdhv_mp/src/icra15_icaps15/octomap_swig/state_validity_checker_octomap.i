/* File : state_validity_checker_octomap.i */
%module state_validity_checker_octomap
%{
/* Put headers and other declarations here */
#include "state_validity_checker_octomap.hpp"
%}

class OctomapStateValChecker {
public:
	OctomapStateValChecker(double planning_depth);
	~OctomapStateValChecker();
	bool isCollCube(double x, double y, double z, double sideLength);
	bool isValidPoint(double x, double y, double z);
	double getOctomapRes(void);
private:
	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;
	double octree_res_;

	//Girona500 info
	double robot_length_;
};