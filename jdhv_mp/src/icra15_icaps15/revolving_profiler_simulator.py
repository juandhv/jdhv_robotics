#!/usr/bin/env python
"""
Created on May 21, 2014

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Create laser_scan messages from a profiler.

Modified version of revolving_profiler_simulator.py (Enric)
"""

#
# This ROS node publishes position commands to a revolving joint on G500
# and listens to its attached range sensor.
#

import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from math import *
from sensor_msgs.msg import LaserScan, Range, JointState
from auv_msgs.msg import NavSts
import cola2_lib
from cola2_lib import cola2_ros_lib
from cola2_lib import cola2_lib
import random
import tf
from threading import Thread

class RevolvingProfilerSimulator :

    ###############
    # Constructor #
    ###############
    def __init__(self):
        # Config
        self.config = cola2_ros_lib.Config()
        self.config.angle_inc = radians(1.0)
        self.config.angle_inc_rate = 30
        self.config.grabbing_rate = 5
        self.config.publish_every = 5
        self.config.mode = 'sector' # 'cont' or 'sector'
        self.config.llim = radians(-45) # llim=-180, rlim=0 for l'Amarrador simulation
        self.config.rlim = radians(45)
        self.config.max_range = 10.0
        self.config.frame_id = '/seaking_profiler'
        self.config.robot_frame_name = '/sparus2'
        self.config.noise_stdev = .1

        # App data
        self.scanRight = True
        self.angle = self.config.llim
        self.accumAngles = []
        self.accumRanges = []
        self.firstBeamTime = rospy.Time.now()
        
        # Received data 
        self.inputRange  = 0.0
        self.rangeAvailable = False
        self.jointState = JointState()
        self.jointStateAvailable = False
        
        self.tfBroadcaster = tf.TransformBroadcaster()  
  
        #=======================================================================
        # Subscribers 
        #=======================================================================
        #Navigation data (feedback)
        self.nav_sts_sub_ = rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.navStsSubCallback, queue_size = 1)
        self.nav_sts_available_ = False
        
        rospy.Subscriber('/sparus/profiler_sonar', Range, self.handleRange, queue_size = 1)
        rospy.Subscriber('/sparus/profiler_state', JointState, self.handleJointState, queue_size = 1)

        # Create publishers
        self.commandPub = rospy.Publisher('/sparus/profiler_state_command', JointState)
        self.scanPub  = rospy.Publisher('/revolving_profiler_scan', LaserScan)
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        print "Waiting for nav sts..."
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.nav_sts_available_:
            r.sleep()
            
    def navStsSubCallback(self,nav_sts_msg):
        """
        Callback to receive navSts message
        """

        self.nav_sts_data = nav_sts_msg
        if self.nav_sts_available_ == False:
            self.nav_sts_available_ = True
        
        return

    def handleRange(self, data):
        self.inputRange = data.range
        self.rangeAvailable = True
        
    def handleJointState(self, data):
        self.jointState = data
        self.jointStateAvailable = True

    def keepRevolving(self):
        r = rospy.Rate(self.config.angle_inc_rate)
        while not rospy.is_shutdown():
            if self.jointStateAvailable:
                print "Moving joint"
                msg = JointState()
                msg.header.stamp = msg.header.stamp = rospy.Time.now()
                msg.header.frame_id = 'seaking_profiler'
                msg.name = ''
                msg.position = [self.angle]
                msg.velocity = []
                msg.effort = []

                self.commandPub.publish(msg)
                
                # Update angle
                # TODO: what if sector goes through 0???
                if self.config.mode == 'sector' and\
                   (self.angle < self.config.llim or self.angle > self.config.rlim):
                    self.scanRight = not self.scanRight

                if self.scanRight:
                    ainc = self.config.angle_inc
                else:
                    ainc = -self.config.angle_inc
                self.angle = cola2_lib.wrapAngle(self.angle + ainc)

            else:
                print "No joint state available"
            
            r.sleep()
            
    def keepPublishingLaserScans(self):
        r = rospy.Rate(self.config.angle_inc_rate)
        lastScanRight = self.scanRight
        while not rospy.is_shutdown():
            print "Not enough beams yet, nb=%d, pubevery=%d"%(len(self.accumAngles), self.config.publish_every)
            if self.rangeAvailable and self.jointStateAvailable:
                
                # Grab new measurement
                if len(self.accumAngles) == 0:
                    self.firstBeamTime = rospy.Time.now()
                
                if self.inputRange < self.config.max_range:
                    self.accumAngles.append(self.jointState.position[0])
                    noise = random.gauss(0.0, self.config.noise_stdev)
                    rvalue = self.inputRange + noise
                    self.accumRanges.append(rvalue)
                    print "Grabbed new beam"
                else:
                    self.accumAngles.append(self.jointState.position[0])
                    self.accumRanges.append(self.config.max_range-0.05)
                    #print "Discarding beam > max_range"

                if not lastScanRight == self.scanRight:
                    # Drop beams on direction change
                    self.accumAngles = []
                    self.accumRanges = []
                    print "!!!"
                    print "!!! DROPPED BEAMS ON DIRECTION CHANGE"
                    print "!!!"
                lastScanRight = self.scanRight
                
                if len(self.accumRanges) >= self.config.publish_every:
                    msg = LaserScan()
                    # timestamp in the header is the acquisition time of 
                    # the first ray in the scan.
                    #
                    # in frame frame_id, angles are measured around 
                    # the positive Z axis (counterclockwise, if Z is up)
                    # with zero angle being forward along the x axis
                    msg.header.stamp = self.firstBeamTime
                    msg.header.frame_id = self.config.frame_id
                                             
                    msg.angle_min = min(self.accumAngles)        # start angle of the scan [rad]
                    msg.angle_max = max(self.accumAngles)       # end angle of the scan [rad]
                    
                    if self.scanRight:
                        msg.angle_increment = self.config.angle_inc # angular distance between measurements [rad]
                    else:
                        msg.angle_increment = -self.config.angle_inc

                    msg.time_increment = 1.0/self.config.angle_inc_rate  # time between measurements [seconds] - if your scanner
                                             # is moving, this will be used in interpolating position
                                             # of 3d points
                    msg.scan_time = msg.time_increment * (len(self.accumRanges)-1)        # time between scans [seconds]
                    
                    msg.range_min = min(self.accumRanges)       # minimum range value [m]
                    msg.range_max = self.config.max_range#max(self.accumRanges)     # maximum range value [m]                
                    
                    msg.ranges = self.accumRanges
                    msg.intensities = []
                    
                    self.accumAngles = []
                    self.accumRanges = []               
                    
                    print "Publishing LaserScan msg"
                    self.tfBroadcaster.sendTransform((0, 0, 0),                   
                             tf.transformations.quaternion_from_euler(0, 0, 0),
                             msg.header.stamp,
                             self.config.frame_id,
                             self.config.robot_frame_name) 

                    self.scanPub.publish(msg)                
               
            r.sleep()
            
    def keepPublishingTransforms(self):
        r = rospy.Rate(20)
        while not rospy.is_shutdown():
            self.tfBroadcaster.sendTransform((0, 0, 0),                   
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     rospy.Time.now(),
                     self.config.frame_id,
                     self.config.robot_frame_name)                
            r.sleep()            

if __name__ == '__main__':
    try:
        rospy.init_node('revolving_profiler_simulator', anonymous=True)
        profiler_simulator = RevolvingProfilerSimulator()

        # Starting two separate threads
        th1 = Thread(target = profiler_simulator.keepRevolving)
        th1.start()
        
        th2 = Thread(target = profiler_simulator.keepPublishingLaserScans)
        th2.start()        

        #th3 = Thread(target = profiler_simulator.keepPublishingTransforms)
        #th3.start()

        # Actively wait for shutdown request
        # th1.join()
        # th2.join()        
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            print "inside revolving_profiler_simulator"
            r.sleep()
        
    except rospy.ROSInterruptException: pass
