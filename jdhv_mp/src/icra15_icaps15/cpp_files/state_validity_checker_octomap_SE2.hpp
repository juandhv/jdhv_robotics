/*
 * state_validity_checker_octomap_SE2.hpp
 *
 *  Created on: July 27, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (position state) is collision-free.
 *  The workspace is represented by an octomap.
 */

#ifndef OMPL_CONTRIB_STATE_VALIDITY_CHECKER_OCTOMAP_SE2_
#define OMPL_CONTRIB_STATE_VALIDITY_CHECKER_OCTOMAP_SE2_

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>
#include <string>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//Eigen
#include <Eigen/Dense>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;


//!  OmStateValidityCheckerSE2 class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap.
*/
class OmStateValidityCheckerSE2 : public ob::StateValidityChecker {

public:
	//! OmStateValidityCheckerSE2 constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	OmStateValidityCheckerSE2(const ob::SpaceInformationPtr &si, const double planning_depth, const double side_length, const bool lazy_collision_eval);
	//! OmStateValidityCheckerSE2 destructor.
	/*!
	 * Destroy the octomap.
	 */
	~OmStateValidityCheckerSE2();
	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision)
	 */
	virtual bool isValid(const ob::State *state) const;

	virtual bool isValidPoint(const ob::State *state) const;

	virtual int isCollision(Eigen::Vector3f pos_query) const;

	double octree_res_;
private:
	//ROS
	ros::NodeHandle node_hand_;

	// ROS tf
	tf::Pose last_pose_;

	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;
	double octree_min_x_, octree_min_y_, octree_min_z_;
	double octree_max_x_, octree_max_y_, octree_max_z_;

	double side_length_, planning_depth_;
	bool lazy_collision_eval_;
};

#endif
