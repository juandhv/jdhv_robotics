/*
 * online_rrt_star_R2_icra15_main.cpp
 *
 *  Created on: June 25, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Online path planning (geometric, without differential constraints) using a modified RRT* (taken from OMPL).
 *  Octomaps are used to represent workspace and to validate configurations.
 */

#include "online_rrt_star_R2_icra15.hpp"

void shutdownNode(int sig);

//!  OnlinePlannerR2 class.
/*!
 * Online Planner.
 * Setup a modified RRT* for online computation of collision-free paths.
 * C-Space: R2
 * Workspace is represented with Octomaps
*/
class OnlinePlannerR2
{
    public:
		// Constructor
		OnlinePlannerR2();
		void timerCallback(const ros::TimerEvent &e);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;
		//OMPL, online planner
		og::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
};

OnlinePlannerR2::OnlinePlannerR2()
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, shutdownNode);

	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(2);
	goal_state_.resize(2);

	node_handler_.param("online_planner/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("online_planner/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("online_planner/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("online_planner/start_state", start_state_, start_state_);
	node_handler_.param("online_planner/goal_state", goal_state_, goal_state_);
	node_handler_.param("online_planner/timer_period", timer_period_, timer_period_);
	node_handler_.param("online_planner/solving_time", solving_time_, solving_time_);

	//=======================================================================
	// Instantiate the state space (R2)
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(2));

	// Set the bounds for the state space
	ob::RealVectorBounds bounds(2);

	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = ob::PlannerPtr(new og::OnlineRRTstar(si, planning_depth_));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	const ob::RealVectorStateSpace::StateType *pos;
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_state_[0];
	pos->values[1] = start_state_[1];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_state_[0];
	pos->values[1] = goal_state_[1];

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	// planner->printProperties(std::cout);// print planner properties
	// si->printSettings(std::cout);// print the settings for this space

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OnlinePlannerR2::timerCallback, this);

	ros::spin();
}

void OnlinePlannerR2::timerCallback(const ros::TimerEvent &e)
{
	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "online_planner_R2_icra15");

	ROS_INFO("%s: online planner without kinematic constraints (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OnlinePlannerR2 online_planner;

	return 0;
}

void shutdownNode(int sig)
{
	ROS_INFO("%s: shutting down node", ros::this_node::getName().c_str());
	ros::shutdown();
}


