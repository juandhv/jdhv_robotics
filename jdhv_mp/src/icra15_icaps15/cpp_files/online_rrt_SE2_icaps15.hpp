/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

/*
 * online_rrt_SE2_icra15.hpp
 *
 *  Modified on: July 27, 2014
 *       Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *     Based on: RRT (OMPL)
 *
 *  Purpose: Rapidly-exploring Random Trees (RRT) with kinematic constraints.
 *  C-space --> R2*S1.
 */

#ifndef OMPL_CONTRIB_ONLINE_RRT_ONLINERRT_
#define OMPL_CONTRIB_ONLINE_RRT_ONLINERRT_

#include <ompl/control/planners/PlannerIncludes.h>
#include <ompl/datastructures/NearestNeighbors.h>

#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/geometric/PathGeometric.h>

#include <limits>
#include <vector>
#include <utility>
#include <signal.h>

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//motion models of auv
#include "motion_model_auv.hpp"

#include "state_validity_checker_octomap_SE2.hpp"

namespace ompl
{

    namespace control
    {

        /**
           @anchor cRRT
           @par Short description
           OnlineRRT is a tree-based motion planner that uses the following
           idea: OnlineRRT samples a random state @b qr in the state space,
           then finds the state @b qc among the previously seen states
           that is closest to @b qr and expands from @b qc towards @b
           qr, until a state @b qm is reached. @b qm is then added to
           the exploration tree.
           This implementation is intended for systems with differential constraints.
           @par External documentation
           S.M. LaValle and J.J. Kuffner, Randomized kinodynamic planning, <em>Intl. J. of Robotics Research</em>, vol. 20, pp. 378–400, May 2001. DOI: [10.1177/02783640122067453](http://dx.doi.org/10.1177/02783640122067453)<br>
           [[PDF]](http://ijr.sagepub.com/content/20/5/378.full.pdf)
           [[more]](http://msl.cs.uiuc.edu/~lavalle/rrtpubs.html)
        */

        /** \brief Rapidly-exploring Random Tree */
        class OnlineRRT : public base::Planner
        {
        public:

            /** \brief Constructor */
            OnlineRRT(const SpaceInformationPtr &si, const double planning_depth);

            virtual ~OnlineRRT();

            /** \brief Continue solving for some amount of time. Return true if solution was found. */
            virtual base::PlannerStatus solve(const base::PlannerTerminationCondition &ptc);

            /** \brief Clear datastructures. Call this function if the
                input data to the planner has changed and you do not
                want to continue planning */
            virtual void clear();

            /** In the process of randomly selecting states in the state
                space to attempt to go towards, the algorithm may in fact
                choose the actual goal state, if it knows it, with some
                probability. This probability is a real number between 0.0
                and 1.0; its value should usually be around 0.05 and
                should not be too large. It is probably a good idea to use
                the default value. */
            void setGoalBias(double goalBias)
            {
                goalBias_ = goalBias;
            }

            /** \brief Get the goal bias the planner is using */
            double getGoalBias() const
            {
                return goalBias_;
            }

            /** \brief Return true if the intermediate states generated along motions are to be added to the tree itself */
            bool getIntermediateStates() const
            {
                return addIntermediateStates_;
            }

            /** \brief Specify whether the intermediate states generated along motions are to be added to the tree itself */
            void setIntermediateStates(bool addIntermediateStates)
            {
                addIntermediateStates_ = addIntermediateStates;
            }

            virtual void getPlannerData(base::PlannerData &data) const;

            /** \brief Set a different nearest neighbors datastructure */
            template<template<typename T> class NN>
            void setNearestNeighbors()
            {
                nn_.reset(new NN<Motion*>());
            }

            virtual void setup();

        protected:


            /** \brief Representation of a motion

                This only contains pointers to parent motions as we
                only need to go backwards in the tree. */
            class Motion
            {
            public:

                Motion() : state(NULL), control(NULL), steps(0), parent(NULL), acum_cost(0.0)
                {
                }

                /** \brief Constructor that allocates memory for the state and the control */
                Motion(const SpaceInformation *si) : state(si->allocState()), control(si->allocControl()), steps(0), parent(NULL), acum_cost(0.0)
                {
                }

                ~Motion()
                {
                }

                /** \brief The state contained by the motion */
                base::State       *state;

                /** \brief The control contained by the motion */
                Control           *control;

                /** \brief The number of steps the control is applied for */
                unsigned int       steps;

                /** \brief The parent motion in the exploration tree */
                Motion            *parent;

                /** \brief The set of motions descending from the current motion */
                std::vector<Motion*> children;

                /** \brief Accumulated cost*/
                double       acum_cost;
            };

            /** \brief Free the memory allocated by this planner */
            void freeMemory();

            /** \brief Compute distance between motions (actually distance between contained states) */
            double distanceFunction(const Motion *a, const Motion *b) const
            {
                return si_->distance(a->state, b->state);
            }

            /** \brief Removes the given motion from the parent's child list */
            void removeFromParent(Motion *m);

            ///////////////////////////////////////
            // Functions defined for planning online.

            /** \brief Update the map and delete configurations in collision. */
            void updateOctomap();

            /** \brief Check recursively for collisions in a branch rooted at node m (DFS). */
            void checkCollisionInBranch(Motion *m);

            /** \brief Delete a branch (subtree) recursively. */
            void discardBranch(Motion *m);

            /** \brief Prune the tree by defining m as the root and deleting the rest of the branches. */
            void pruneTree(Motion *m);

            ///////////////////////////////////////
            // ROS related functions

            /** \brief ROS callback to receive navigation information. */
            void odomCallback(const nav_msgs::OdometryConstPtr &odom_msg);

            /** \brief Callback to receive messages that indicate waypoints are needed. */
            void wpNeedSubCallback(const std_msgs::Bool &wp_need_flag_msg);

            /** \brief Visualize RRT by publishing messages for RViz. */
            void visualizeRRT(PathControl *path);


            /** \brief State sampler */
            base::StateSamplerPtr                          sampler_;

            /** \brief Control sampler */
            DirectedControlSamplerPtr                      controlSampler_;

            /** \brief The base::SpaceInformation cast as control::SpaceInformation, for convenience */
            const SpaceInformation                        *siC_;

            /** \brief A nearest-neighbors datastructure containing the tree of motions */
            std::shared_ptr< NearestNeighbors<Motion*> > nn_;

            /** \brief The fraction of time the goal is picked as the state to expand towards (if such a state is available) */
            double                                         goalBias_;

            /** \brief Flag indicating whether intermediate states are added to the built tree of motions */
            bool                                           addIntermediateStates_;

            /** \brief The random number generator */
            RNG                                            rng_;

            /** \brief The most recent goal motion.  Used for PlannerData computation */
            Motion                                         *lastGoalMotion_;

            /** \brief A list of states in the tree that satisfy the goal condition */
            std::vector<Motion*>                           goalMotions_;

            /** \brief Best cost found so far by algorithm */
            double                                     best_cost_;

            double										best_dif_;

            ///////////////////////////////////////
            // Attributes (variables and pointers) for planning online

            std::shared_ptr<OmStateValidityCheckerSE2> om_state_validity_checker_;

            ros::NodeHandle node_handler_;
            ros::Subscriber nav_sts_sub_, wp_need_flag_sub_;
            ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_;

            tf::Pose last_pose_;

            double planning_depth_, checking_side_length_, solving_time_, max_update_time_, extra_cost_accept_;
            bool nav_sts_available_, rrt_build_flag_, pending_cancel_wp_, lazy_collision_eval_, wp_need_flag_;
        };

    }
}

#endif
