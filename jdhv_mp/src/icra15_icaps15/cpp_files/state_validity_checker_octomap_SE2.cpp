/*
 * state_validity_checker_octomap_SE2.cpp
 *
 *  Created on: July 27, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (position state) is collision-free.
 *  The workspace is represented by an octomap.
 */

#include "state_validity_checker_octomap_SE2.hpp"

OmStateValidityCheckerSE2::OmStateValidityCheckerSE2(const ob::SpaceInformationPtr &si, const double planning_depth, const double side_length, const bool lazy_collision_eval) :
	ob::StateValidityChecker(si)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	planning_depth_ = planning_depth;
	lazy_collision_eval_ = lazy_collision_eval;

	serv_name = "/laser_octomap/get_binary";

	octree_ = NULL;
	side_length_ = side_length;

	ROS_DEBUG("%s: requesting the map to %s...", ros::this_node::getName().c_str(), node_hand_.resolveName(serv_name).c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		std::cout << std::endl;
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();

		octree_->getMetricMin(octree_min_x_, octree_min_y_, octree_min_z_);
		octree_->getMetricMax(octree_max_x_, octree_max_y_, octree_max_z_);

		if (octree_){
			ROS_DEBUG("%s: Octomap received (%zu nodes, %f m res)", ros::this_node::getName().c_str(), octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmStateValidityCheckerSE2::isValid(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;
	const ob::SE2StateSpace::StateType *state_se2;

	// extract the component of the state and cast it to what we expect
	state_se2 = state->as<ob::SE2StateSpace::StateType>();

	if(lazy_collision_eval_ && (state_se2->getX() < octree_min_x_ || state_se2->getY() < octree_min_y_ || state_se2->getX() > octree_max_x_ || state_se2->getY() > octree_max_y_))
		return true;

	for(double xi = state_se2->getX()-(side_length_/2.0);xi <= state_se2->getX()+(side_length_/2.0);xi=xi+octree_res_)
		for(double yi = state_se2->getY()-(side_length_/2.0);yi <= state_se2->getY()+(side_length_/2.0);yi=yi+octree_res_)
			for(double zi = planning_depth_-(side_length_/2.0);zi <= planning_depth_+(side_length_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = false;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						return !collision;
					}
				}
			}
	return !collision;
}

bool OmStateValidityCheckerSE2::isValidPoint(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	double node_occupancy;

	// extract the component of the state and cast it to what we expect
	const ob::SE2StateSpace::StateType *state_se2 = state->as<ob::SE2StateSpace::StateType>();

	query.x() = state_se2->getX();
	query.y() = state_se2->getY();
	query.z() = planning_depth_;

	result = octree_->search (query);

	if(result == NULL){
		return false;
	}
	else{
		node_occupancy = result->getOccupancy();
		if (node_occupancy <= 0.4)
			return true;
	}
	return false;
}

int OmStateValidityCheckerSE2::isCollision(Eigen::Vector3f pos_query) const
{
	OcTreeNode* result;
	point3d query;
	int collision(0);
	double node_occupancy;

	if(lazy_collision_eval_ && (pos_query[0] < octree_min_x_ || pos_query[1] < octree_min_y_ || pos_query[0] > octree_max_x_ || pos_query[1] > octree_max_y_))
		return -1;

	for(double xi = pos_query[0]-(side_length_/2.0);xi <= pos_query[0]+(side_length_/2.0);xi=xi+octree_res_)
		for(double yi = pos_query[1]-(side_length_/2.0);yi <= pos_query[1]+(side_length_/2.0);yi=yi+octree_res_)
			for(double zi = planning_depth_-(side_length_/2.0);zi <= planning_depth_+(side_length_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(xi>octree_max_x_ || yi>octree_max_y_ || zi>octree_max_z_ || xi<octree_min_x_ || yi<octree_min_y_ || zi<octree_min_z_){
					collision = -1;
					break;
				}
				else if(result == NULL){
					collision = 1;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = 1;
						break;
					}
				}
			}

	return collision;
}

OmStateValidityCheckerSE2::~OmStateValidityCheckerSE2()
{
    delete octree_;
}
