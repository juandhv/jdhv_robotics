/*
 * motion_model_auv.cpp
 *
 *  Created on: Mar 17, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Differential model of motion of an auv.
 *  Kinematic model.
 */

//motion models of auv
#include "motion_model_auv.hpp"


//! Constructor.
KinematicAuvModel::KinematicAuvModel(const ob::StateSpace *space) : space_(space), auv_length_(2.0)
{
}

/// implement the function describing the robot motion: qdot = f(q, u)
void KinematicAuvModel::operator()(const ob::State *state, const oc::Control *control, std::valarray<double> &dstate) const
{
	const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
	const double theta = state->as<ob::SE2StateSpace::StateType>()->getYaw();

	dstate.resize(3);
	dstate[0] = u[0] * cos(theta);
	dstate[1] = u[0] * sin(theta);
	dstate[2] = u[1];//u[0] * tan(u[1]) / carLength_;
}

/// implement y(n+1) = y(n) + d
void KinematicAuvModel::update(ob::State *state, const std::valarray<double> &dstate) const
{
	ob::SE2StateSpace::StateType &s = *state->as<ob::SE2StateSpace::StateType>();
	s.setX(s.getX() + dstate[0]);
	s.setY(s.getY() + dstate[1]);
	s.setYaw(s.getYaw() + dstate[2]);
	space_->enforceBounds(state);
}

//! Constructor for Euler integrator.
template<typename F>
EulerIntegrator<F>::EulerIntegrator(const ob::StateSpace *space, double timeStep) : space_(space), timeStep_(timeStep), ode_(space)
{
}

template<typename F>
void EulerIntegrator<F>::propagate(const ob::State *start, const oc::Control *control, const double duration, ob::State *result) const
{
	double t = timeStep_;
	std::valarray<double> dstate;
	space_->copyState(result, start);
	while (t < duration + std::numeric_limits<double>::epsilon())
	{
		ode_(result, control, dstate);
		ode_.update(result, timeStep_ * dstate);
		t += timeStep_;
	}
//	if (t + std::numeric_limits<double>::epsilon() > duration)
//	{
//		ode_(result, control, dstate);
//		ode_.update(result, (t - duration) * dstate);
//	}
}

template<typename F>
double EulerIntegrator<F>::getTimeStep(void) const
{
	return timeStep_;
}

template<typename F>
void EulerIntegrator<F>::setTimeStep(double timeStep)
{
	timeStep_ = timeStep;
}

/// @cond IGNORE

KinAUVControlSpace::KinAUVControlSpace(const ob::StateSpacePtr &stateSpace) : oc::RealVectorControlSpace(stateSpace, 2)
{
}

KinAUVStatePropagator::KinAUVStatePropagator(const oc::SpaceInformationPtr &si) : oc::StatePropagator(si),
		integrator_(si->getStateSpace().get(), 0.0)
{
}

void KinAUVStatePropagator::propagate(const ob::State *state, const oc::Control* control, const double duration, ob::State *result) const
{
	integrator_.propagate(state, control, duration, result);
}

void KinAUVStatePropagator::setIntegrationTimeStep(double timeStep)
{
	integrator_.setTimeStep(timeStep);
}

double KinAUVStatePropagator::getIntegrationTimeStep(void) const
{
	return integrator_.getTimeStep();
}

/// @endcond
