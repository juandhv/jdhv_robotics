/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

/*
 * mod_kin_dyn_rrt.cpp
 *
 *  Created on: April 16, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *
 *  Modified RRT for kinodynamic motion planning that permits to reuse last known solution
 *  and keep a record of total cost.
 *  (based on OMPL's RRT)
 */

#include <ompl/base/goals/GoalSampleableRegion.h>
#include <ompl/tools/config/SelfConfig.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <limits>

//Eigen
#include <Eigen/Dense>

#include "mod_kin_dyn_rrt.h"

ompl::control::ModKinDynRRT::ModKinDynRRT(const SpaceInformationPtr &si) : base::Planner(si, "ModKinDynRRT")
{
    specs_.approximateSolutions = true;
    siC_ = si.get();
    addIntermediateStates_ = false;
    lastGoalMotion_ = NULL;
    bestCost_ = base::Cost(std::numeric_limits<double>::quiet_NaN());

    goalBias_ = 0.05;

    Planner::declareParam<double>("goal_bias", this, &ModKinDynRRT::setGoalBias, &ModKinDynRRT::getGoalBias, "0.:.05:1.");
    Planner::declareParam<bool>("intermediate_states", this, &ModKinDynRRT::setIntermediateStates, &ModKinDynRRT::getIntermediateStates);
}

ompl::control::ModKinDynRRT::~ModKinDynRRT()
{
    freeMemory();
}

void ompl::control::ModKinDynRRT::setup()
{
    base::Planner::setup();
    if (!nn_)
        nn_.reset(tools::SelfConfig::getDefaultNearestNeighbors<Motion*>(this));
    nn_->setDistanceFunction(std::bind(&ModKinDynRRT::distanceFunction, this, std::placeholders::_1, std::placeholders::_2));

    // Setup optimization objective
    //
    // If no optimization objective was specified, then default to
    // optimizing path length as computed by the distance() function
    // in the state space.
    if (pdef_)
    {
    	if (pdef_->hasOptimizationObjective())
    		opt_ = pdef_->getOptimizationObjective();
    	else
    	{
    		OMPL_INFORM("%s: No optimization objective specified. Defaulting to optimizing path length for the allowed planning time.", getName().c_str());
    		opt_.reset(new base::PathLengthOptimizationObjective(si_));
    	}
    }
    else
    {
    	OMPL_INFORM("%s: problem definition is not set, deferring setup completion...", getName().c_str());
    	setup_ = false;
    }
}

void ompl::control::ModKinDynRRT::clear()
{
    Planner::clear();
    sampler_.reset();
    controlSampler_.reset();
    freeMemory();
    if (nn_)
        nn_->clear();
    lastGoalMotion_ = NULL;
    bestCost_ = base::Cost(std::numeric_limits<double>::quiet_NaN());
}

void ompl::control::ModKinDynRRT::freeMemory()
{
    if (nn_)
    {
        std::vector<Motion*> motions;
        nn_->list(motions);
        for (unsigned int i = 0 ; i < motions.size() ; ++i)
        {
            if (motions[i]->state)
                si_->freeState(motions[i]->state);
            if (motions[i]->control)
                siC_->freeControl(motions[i]->control);
            delete motions[i];
        }
    }
}

ompl::base::PlannerStatus ompl::control::ModKinDynRRT::solve(const base::PlannerTerminationCondition &ptc)
{
    checkValidity();
    base::Goal                   *goal = pdef_->getGoal().get();
    base::GoalSampleableRegion *goal_s = dynamic_cast<base::GoalSampleableRegion*>(goal);

    bool symCost = opt_->isSymmetric();

    while (const base::State *st = pis_.nextStart())
    {
        Motion *motion = new Motion(siC_);
        si_->copyState(motion->state, st);
        motion->cost = opt_->identityCost();
        siC_->nullControl(motion->control);
        nn_->add(motion);
    }

    if (nn_->size() == 0)
    {
        OMPL_ERROR("%s: There are no valid initial states!", getName().c_str());
        return base::PlannerStatus::INVALID_START;
    }

    if (!sampler_)
        sampler_ = si_->allocStateSampler();
    if (!controlSampler_)
        controlSampler_ = siC_->allocDirectedControlSampler();

    // \TODO Make this variable unnecessary, or at least have it
    // persist across solve runs
    base::Cost bestCost    = opt_->infiniteCost();

    bestCost_ = opt_->infiniteCost();

    OMPL_INFORM("%s: Starting planning with %u states already in datastructure", getName().c_str(), nn_->size());

    Motion *solution  = NULL;
    Motion *approxsol = NULL;
    double  approxdif = std::numeric_limits<double>::infinity();

    Motion      *rmotion = new Motion(siC_);
    base::State  *rstate = rmotion->state;
    Control       *rctrl = rmotion->control;
    base::State  *xstate = si_->allocState();

    Eigen::MatrixXf start_cov(4,4);
    Eigen::MatrixXf result_cov(4,4);

    while (ptc == false)
    {
        /* sample random state (with goal biasing) */
        if (goal_s && rng_.uniform01() < goalBias_ && goal_s->canSample())
            goal_s->sampleGoal(rstate);
        else
            sampler_->sampleUniform(rstate);

        /* find closest state in the tree */
        Motion *nmotion = nn_->nearest(rmotion);

//        std::cout << std::endl << "random:" <<  rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getX() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getY() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getYaw() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0]<< std::endl;

        /* sample a random control that attempts to go towards the random state, and also sample a control duration */
        unsigned int cd = controlSampler_->sampleTo(rctrl, nmotion->control, nmotion->state, rmotion->state);

//        std::cout << "start:" <<  nmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getX() << ", " << nmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getY() << ", " << nmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getYaw() << ", " << nmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0]<< std::endl;
//        std::cout << "result:" <<  rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getX() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getY() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->getYaw() << ", " << rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0]<< std::endl;
//        std::cout << "cd:" <<  cd << std::endl;
//
//        int cov_ind = 0;
//        for(int i=0; i<4 ; i++)
//        {
//        	for(int j=0; j<4 ; j++)
//        	{
//        		start_cov(i,j) = nmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind];
//        		cov_ind++;
//        	}
//        }
//
//        cov_ind = 0;
//        for(int i=0; i<4 ; i++)
//        {
//        	for(int j=0; j<4 ; j++)
//        	{
//        		result_cov(i,j) = rmotion->state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind];
//        		cov_ind++;
//        	}
//        }

        //std::cout << "duration: " << duration << std::endl;
//        std::cout << "start_cov: " << start_cov << std::endl;
//        std::cout << "result_cov: " << result_cov << std::endl<< std::endl;

        if (addIntermediateStates_)
        {
            // this code is contributed by Jennifer Barry
            std::vector<base::State *> pstates;
            cd = siC_->propagateWhileValid(nmotion->state, rctrl, cd, pstates, true);
            std::cout << "cd: " << cd << std::endl;

            if (cd >= siC_->getMinControlDuration())
            {
                Motion *lastmotion = nmotion;
                bool solved = false;
                size_t p = 0;
                for ( ; p < pstates.size(); ++p)
                {
                    /* create a motion */
                    Motion *motion = new Motion();
                    motion->state = pstates[p];
                    //we need multiple copies of rctrl
                    motion->control = siC_->allocControl();
                    siC_->copyControl(motion->control, rctrl);
                    motion->steps = 1;
                    motion->parent = lastmotion;
                    motion->incCost = opt_->motionCost(nmotion->state, motion->state);
                    motion->cost = opt_->combineCosts(nmotion->cost, motion->incCost);

                    lastmotion = motion;
                    nn_->add(motion);
                    double dist = 0.0;
                    solved = goal->isSatisfied(motion->state, &dist);
                    if (solved && opt_->isCostBetterThan(motion->cost, bestCost_))
                    {
                        approxdif = dist;
                        solution = motion;
                        bestCost_ = motion->cost;
                        std::cout << "approxdif = " << approxdif << std::endl;
                        std::cout << "cost = " << motion->cost << std::endl;
                        std::cout << "bestCost_ = " << bestCost_ << std::endl;
                        //break;
                    }
//                    else if (dist < approxdif)
//                    {
//                        approxdif = dist;
//                        approxsol = motion;
//                        std::cout << "approxdif = " << approxdif << std::endl;
//                        std::cout << "cost = " << motion->cost << std::endl;
//                    }
                }

                //free any states after we hit the goal
                while (++p < pstates.size())
                    si_->freeState(pstates[p]);
//                if (solved)
//                    break;
            }
            else
                for (size_t p = 0 ; p < pstates.size(); ++p)
                    si_->freeState(pstates[p]);
        }
        else
        {
            if (cd >= siC_->getMinControlDuration())
            {
                /* create a motion */
                Motion *motion = new Motion(siC_);
                si_->copyState(motion->state, rmotion->state);
                siC_->copyControl(motion->control, rctrl);
                motion->steps = cd;
                motion->parent = nmotion;
                motion->incCost = opt_->motionCost(nmotion->state, motion->state);
                motion->cost = opt_->combineCosts(nmotion->cost, motion->incCost);

                nn_->add(motion);
                double dist = 0.0;
                bool solv = goal->isSatisfied(motion->state, &dist);
                //std::cout << "dist = " << dist << std::endl;
                if (solv && opt_->isCostBetterThan(motion->cost, bestCost_))
                {
                    approxdif = dist;
                    solution = motion;
                    bestCost_ = motion->cost;
                    std::cout << "approxdif = " << approxdif << std::endl;
                    std::cout << "cost = " << motion->cost << std::endl;
                    std::cout << "bestCost_ = " << bestCost_ << std::endl;
                    //break;
                }
//                else if (dist < approxdif)
//                {
//                    approxdif = dist;
//                    approxsol = motion;
//                    std::cout << "approxdif = " << approxdif << std::endl;
//                    std::cout << "cost = " << motion->cost << std::endl;
//                }
            }
        }
    }

    bool solved = false;
    bool approximate = false;
    if (solution == NULL)
    {
        solution = approxsol;
        approximate = true;
    }

    if (solution != NULL)
    {
        lastGoalMotion_ = solution;

        /* construct the solution path */
        std::vector<Motion*> mpath;
        while (solution != NULL)
        {
            mpath.push_back(solution);
            solution = solution->parent;
        }

        /* set the solution path */
        PathControl *path = new PathControl(si_);
        for (int i = mpath.size() - 1 ; i >= 0 ; --i)
            if (mpath[i]->parent)
                path->append(mpath[i]->state, mpath[i]->control, mpath[i]->steps * siC_->getPropagationStepSize());
            else
                path->append(mpath[i]->state);
        solved = true;
        pdef_->addSolutionPath(base::PathPtr(path), approximate, approxdif, getName());
    }

    if (rmotion->state)
        si_->freeState(rmotion->state);
    if (rmotion->control)
        siC_->freeControl(rmotion->control);
    delete rmotion;
    si_->freeState(xstate);

    OMPL_INFORM("%s: Created %u states", getName().c_str(), nn_->size());

    return base::PlannerStatus(solved, approximate);
}

void ompl::control::ModKinDynRRT::getPlannerData(base::PlannerData &data) const
{
    Planner::getPlannerData(data);

    std::vector<Motion*> motions;
    if (nn_)
        nn_->list(motions);

    double delta = siC_->getPropagationStepSize();

    if (lastGoalMotion_)
        data.addGoalVertex(base::PlannerDataVertex(lastGoalMotion_->state));

    for (unsigned int i = 0 ; i < motions.size() ; ++i)
    {
        const Motion *m = motions[i];
        if (m->parent)
        {
            if (data.hasControls())
                data.addEdge(base::PlannerDataVertex(m->parent->state),
                             base::PlannerDataVertex(m->state),
                             control::PlannerDataEdgeControl(m->control, m->steps * delta));
            else
                data.addEdge(base::PlannerDataVertex(m->parent->state),
                             base::PlannerDataVertex(m->state));
        }
        else
            data.addStartVertex(base::PlannerDataVertex(m->state));
    }
}
