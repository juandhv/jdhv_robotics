/*
 * offline_planner_SE2_main.cpp
 *
 *  Created on: December 1, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Offline path planning (with differential constraints) using RRT.
 *  Octomaps are used to represent workspace and to validate configurations.
 */

#include <ompl/control/SpaceInformation.h>
#include <ompl/base/goals/GoalState.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/control/spaces/RealVectorControlSpace.h>
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/PlannerIncludes.h>
#include <ompl/control/SimpleSetup.h>
#include <ompl/config.h>
#include <iostream>

//ROS
#include <ros/ros.h>
#include <ros/package.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//motion models of auv
#include "motion_model_auv.hpp"
#include "state_validity_checker_octomap_fcl_SE2.hpp"

//ROS
#include <ros/ros.h>

namespace ob = ompl::base;
namespace oc = ompl::control;

//!  OfflinePlannerSE2 class.
/*!
 * Offline Planner.
 * Setup a modified RRT for online computation of collision-free paths.
 * C-Space: SE2
 * Workspace is represented with Octomaps
*/
class OfflinePlannerSE2
{
    public:
		//! Constructor
		OfflinePlannerSE2();
		void planWithSimpleSetup();
		void visualizeRRT(oc::PathControl path);
		void createMissionFile(oc::PathControl path);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		//OMPL, online planner
		oc::SimpleSetupPtr simple_setup_;
		double planning_depth_, solving_time_, min_control_duration_, max_control_duration_, checking_side_length_;
		bool lazy_collision_eval_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
//		boost::shared_ptr<og::PathGeometric> geometric_path_result_;
		ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_;
};

OfflinePlannerSE2::OfflinePlannerSE2()
{
	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(4);
	goal_state_.resize(4);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/offline_solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/min_control_duration", min_control_duration_, min_control_duration_);
	node_handler_.param("planning_framework/max_control_duration", max_control_duration_, max_control_duration_);
	node_handler_.param("planning_framework/checking_side_length", checking_side_length_, checking_side_length_);
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);

	planWithSimpleSetup();

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		loop_rate.sleep();
	}
}

void OfflinePlannerSE2::planWithSimpleSetup()
{
	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);

	//=======================================================================
	// Instantiate the state space (SE3)
	//=======================================================================
	ob::StateSpacePtr space(new ob::SE2StateSpace());

	//=======================================================================
	// Instantiate the control space
	//=======================================================================
    oc::ControlSpacePtr cspace(new KinAUVControlSpace(space));

    // set the bounds for the control space
    ob::RealVectorBounds cbounds(2);
    cbounds.setLow(0,0.9);
    cbounds.setHigh(0,1.0);
    cbounds.setLow(1,-0.3);
    cbounds.setHigh(1,0.3);

    cspace->as<KinAUVControlSpace>()->setBounds(cbounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = oc::SimpleSetupPtr( new oc::SimpleSetup(cspace) );
	oc::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	// Set minimum and maximum duration of control action
	si->setMinMaxControlDuration(min_control_duration_, max_control_duration_);

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = ob::PlannerPtr(new oc::RRT(si));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	OmFclStateValidityCheckerSE2 * om_stat_val_check = new OmFclStateValidityCheckerSE2(si, planning_depth_, lazy_collision_eval_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_);
	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// set the propagation routine for this space
	//=======================================================================
	simple_setup_->setStatePropagator(oc::StatePropagatorPtr(new KinAUVStatePropagator(simple_setup_->getSpaceInformation())));
	//simple_setup_->setStatePropagator(std::bind(&propagate, _1, _2, _3, _4));

	ob::RealVectorBounds bounds(2);
	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	space->as<ob::SE2StateSpace>()->setBounds(bounds);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<ob::SE2StateSpace> start(space);

	start->setX(double(start_state_[0]));
	start->setY(double(start_state_[1]));
	start->setYaw(double(start_state_[3]));

	// create a goal state
	ob::ScopedState<ob::SE2StateSpace> goal(space);

	goal->setX(double(goal_state_[0]));
	goal->setY(double(goal_state_[1]));
	goal->setYaw(double(goal_state_[3]));

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal, 0.05);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();
	static_cast<KinAUVStatePropagator*>(simple_setup_->getStatePropagator().get())->setIntegrationTimeStep(simple_setup_->getSpaceInformation()->getPropagationStepSize());

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space

	ob::PlannerStatus solved = simple_setup_->solve( solving_time_ );

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		//		simple_setup_->simplifySolution();
		oc::PathControl path = simple_setup_->getSolutionPath();
		visualizeRRT(path);
		createMissionFile(path);

		ROS_INFO("%s: path has been found with simple_setup", ros::this_node::getName().c_str());
	}
	else
		ROS_INFO("%s: path has not been found", ros::this_node::getName().c_str());
}

void OfflinePlannerSE2::visualizeRRT(oc::PathControl path)
{
	geometry_msgs::PoseArray pose_path;
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path;
	tf::Quaternion orien_quat;

//	path->interpolate();

//	ob::Goal *goal_state = simple_setup_->getGoal();
//	ob::State *start_state= simple_setup_->getStartState(0);
//
//	const base::SE2StateSpace::StateType *q_start = start_state->as<base::SE2StateSpace::StateType>();
//
	// %Tag(MARKER_INIT)%
	pose_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	pose_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;
	result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 1;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.4;
	result_path.scale.y = q_init_goal.scale.y = 0.4;
	result_path.scale.z = q_init_goal.scale.z = 0.4;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	result_path.color.g = q_init_goal.color.b = 1.0;
	result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.g = 1.0;
	visual_rrt.color.a = 1.0;

	//	std::cout << "Number of nodes of the RRT*: " << nn_->size() << std::endl;

//	std::vector<Motion*> motions;
	const ob::SE2StateSpace::StateType *state;
	geometry_msgs::Point p;

	std::vector< ob::State * > states = path.getStates();

	for (uint32_t i = 0; i < path.getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		state = states[i]->as<ob::SE2StateSpace::StateType>();

		p.x = state->getX();
		p.y = state->getY();
		p.z = planning_depth_;//pos->values[2];

		result_path.points.push_back(p);

		//--poses
		geometry_msgs::Pose pose;

		orien_quat.setRPY(0., 0., state->getYaw());

		pose.position.x = p.x;
		pose.position.y = p.y;
		pose.position.z = p.z;
		pose.orientation.x = orien_quat.getX();
		pose.orientation.y = orien_quat.getY();
		pose.orientation.z = orien_quat.getZ();
		pose.orientation.w = orien_quat.getW();
		pose_path.poses.push_back(pose);
	}

	online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	//online_traj_pub_.publish(result_path);
	online_traj_pose_pub_.publish(pose_path);
}

//! Mission file.
/*!
 * Create mission file from resulting path.
*/
void OfflinePlannerSE2::createMissionFile(oc::PathControl path)
{
	std::vector< ob::State * > states = path.getStates();
	const ob::SE2StateSpace::StateType *state_se2;

	std::string mission_north_coords = "trajectory/north: [";
	std::string mission_east_coords = "trajectory/east: [";
	std::string mission_depth_coords = "trajectory/z: [";
	std::string altitude_modes = "trajectory/altitude_mode: [";
	std::string wait_times = "trajectory/wait: [";

	for (uint32_t i = 0; i < path.getStateCount(); ++i)
	{
		std::ostringstream north_coord;
		std::ostringstream east_coord;
		std::ostringstream depth_coord;

		state_se2 = states[i]->as<ob::SE2StateSpace::StateType>();
		north_coord << std::showpoint << setprecision(8) << state_se2->getX();
		east_coord << std::showpoint << setprecision(8) << state_se2->getY();
		depth_coord << std::showpoint << setprecision(8) << planning_depth_;

		if(i==0)
		{
			mission_north_coords = mission_north_coords + north_coord.str() + ", ";
			mission_east_coords = mission_east_coords + east_coord.str() + ", ";
			mission_depth_coords = mission_depth_coords + "0.0, ";
			altitude_modes = altitude_modes + "False, ";
			wait_times = wait_times + "0.0, ";
		}

		if(i!=0)
		{
			mission_north_coords = mission_north_coords + ", ";
			mission_east_coords = mission_east_coords + ", ";
			mission_depth_coords = mission_depth_coords + ", ";
			altitude_modes = altitude_modes + ", ";
			wait_times = wait_times + ", ";
		}


		mission_north_coords = mission_north_coords + north_coord.str();
		mission_east_coords = mission_east_coords + east_coord.str();
		mission_depth_coords = mission_depth_coords + depth_coord.str();
		altitude_modes = altitude_modes + "False";
		wait_times = wait_times + "0.0";

		if(i==(path.getStateCount()-1))
		{
			mission_north_coords = mission_north_coords + ", " + north_coord.str() + "]\n";
			mission_east_coords = mission_east_coords + ", "+ east_coord.str() + "]\n";
			mission_depth_coords = mission_depth_coords + ", 0.0" + "]\n";
			altitude_modes = altitude_modes + ", False" + "]\n";
			wait_times = wait_times + ", 0.0" + "]\n";
		}
	}

	std::string mission_path = ros::package::getPath("jdhv_scenes");
	mission_path = mission_path + "/launch/config/missions/mission.yaml";

	ofstream myfile;
	myfile.open (mission_path.c_str());
	myfile << mission_north_coords;
	myfile << mission_east_coords;
	myfile << mission_depth_coords;
	myfile << altitude_modes;
	myfile << "trajectory/mode: 'los_cte'\n";
	myfile << "trajectory/time_out: 10\n";
	myfile << "trajectory/tolerance: [2.0, 2.0, 0.5, 0.5, 0.5, 0.3]\n";
	myfile << wait_times;
	myfile << "trajectory/force_initial_final_waypoints_at_surface: true\n";

	myfile.close();
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "offline_planner_SE2");

	ROS_INFO("%s: offline planner with kinematic constraints (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OfflinePlannerSE2 offline_planner;

	return 0;
}
