/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2008, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

/*
 * online_rrt_SE2_iros15.cpp
 *
 *  Modified on: December 1, 2014
 *       Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *     Based on: RRT (OMPL)
 *
 *  Purpose: Rapidly-exploring Random Trees (RRT) with kinematic constraints.
 *  C-space --> R2*S1.
 */

#include <ompl/base/goals/GoalSampleableRegion.h>
#include <ompl/tools/config/SelfConfig.h>
#include <limits>

#include "online_rrt_SE2.hpp"

ompl::control::OnlineRRT::OnlineRRT(const SpaceInformationPtr &si, const double planning_depth) :
	base::Planner(si, "OnlineRRT"),
	goalBias_(0.05),
    lastGoalMotion_(NULL),
    pending_cancel_wp_(false)
{
	ros::Duration(5).sleep();

    specs_.approximateSolutions = true;
    siC_ = si.get();
    addIntermediateStates_ = false;
    lastGoalMotion_ = NULL;

    goalBias_ = 0.05;

    Planner::declareParam<double>("goal_bias", this, &OnlineRRT::setGoalBias, &OnlineRRT::getGoalBias, "0.:.05:1.");
    Planner::declareParam<bool>("intermediate_states", this, &OnlineRRT::setIntermediateStates, &OnlineRRT::getIntermediateStates);

	//=======================================================================
	// Online planner and ROS parameters
	//=======================================================================
	rrt_build_flag_ = false;
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);
	node_handler_.param("planning_framework/solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/max_update_time", max_update_time_, max_update_time_);
	node_handler_.param("planning_framework/extra_cost_accept", extra_cost_accept_, extra_cost_accept_);
	planning_depth_ = planning_depth;

	//=======================================================================
	// Subscribers
	//=======================================================================
	//Navigation data (feedback)
	nav_sts_sub_ = node_handler_.subscribe("/pose_ekf_slam/odometry", 1, &ompl::control::OnlineRRT::odomCallback, this);
	nav_sts_available_ = false;

	//flag for indicating that more waypoints are needed
	wp_need_flag_sub_ = node_handler_.subscribe("/jdhv_robotics/need_waypoints", 1, &ompl::control::OnlineRRT::wpNeedSubCallback, this);
	wp_need_flag_ = false;
	pending_cancel_wp_ = false;

	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);

	//=======================================================================
	// Waiting for nav sts
	//=======================================================================
	ros::Rate loop_rate(10);
	if(!nav_sts_available_)
		ROS_WARN("%s: waiting for navigation data", ros::this_node::getName().c_str());
	while (!nav_sts_available_ && ros::ok())
	{
		ros::spinOnce();
		loop_rate.sleep();
	}
	ROS_WARN("%s: navigation data received", ros::this_node::getName().c_str());
}

ompl::control::OnlineRRT::~OnlineRRT()
{
    freeMemory();
}

void ompl::control::OnlineRRT::setup()
{
	base::Planner::setup();
	if (!nn_)
		nn_.reset(tools::SelfConfig::getDefaultNearestNeighbors<Motion*>(this));
	nn_->setDistanceFunction(std::bind(&OnlineRRT::distanceFunction, this, std::placeholders::_1, std::placeholders::_2));
}

void ompl::control::OnlineRRT::clear()
{
	Planner::clear();
	sampler_.reset();
	controlSampler_.reset();
	freeMemory();
	if (nn_)
		nn_->clear();
	lastGoalMotion_ = NULL;

	goalMotions_.clear();
}

void ompl::control::OnlineRRT::freeMemory()
{
    if (nn_)
    {
        std::vector<Motion*> motions;
        nn_->list(motions);
        for (unsigned int i = 0 ; i < motions.size() ; ++i)
        {
            if (motions[i]->state)
                si_->freeState(motions[i]->state);
            if (motions[i]->control)
                siC_->freeControl(motions[i]->control);
            delete motions[i];
        }
    }
}

ompl::base::PlannerStatus ompl::control::OnlineRRT::solve(const base::PlannerTerminationCondition &ptc)
{
    checkValidity();
    base::Goal                   *goal = pdef_->getGoal().get();
    base::GoalSampleableRegion *goal_s = dynamic_cast<base::GoalSampleableRegion*>(goal);

	if(rrt_build_flag_)
	{
		ROS_INFO("%s: return from build, waiting for rrt_build_flag_", ros::this_node::getName().c_str());
		bool addedSolution, approximate;
		return base::PlannerStatus(addedSolution, approximate);
	}

	rrt_build_flag_ = true;
	ptc.terminate();

	//=======================================================================
	// Obtain an updated octomap
	//=======================================================================
	time::point start = time::now();
	updateOctomap();
	double updateTime = time::seconds(time::now() - start);
	ROS_INFO("%s: updateOctomap took %f s", ros::this_node::getName().c_str(), updateTime);

	base::PlannerTerminationCondition ptc_new = base::timedPlannerTerminationCondition(solving_time_);
	start = time::now();

    while (const base::State *st = pis_.nextStart())
    {
        Motion *motion = new Motion(siC_);
        si_->copyState(motion->state, st);
        siC_->nullControl(motion->control);
        nn_->add(motion);
    }

    if (nn_->size() == 0)
    {
        OMPL_ERROR("%s: There are no valid initial states!", getName().c_str());
        return base::PlannerStatus::INVALID_START;
    }

    if (!sampler_)
        sampler_ = si_->allocStateSampler();
    if (!controlSampler_)
        controlSampler_ = siC_->allocDirectedControlSampler();

    Motion *solution  = NULL;
    Motion *approxsol = NULL;
    double  approxdif = std::numeric_limits<double>::infinity();

    solution       = lastGoalMotion_;

    // \TODO Make this variable unnecessary, or at least have it
    // persist across solve runs
    double best_cost = std::numeric_limits<double>::infinity();
    best_cost_ = std::numeric_limits<double>::max();

    if (solution)
    {
    	best_cost = solution->acum_cost;
    	best_cost_ = best_cost;
    	goal->isSatisfied(solution->state, &approxdif);;
    	OMPL_INFORM("%s: Starting planning with existing solution of dist %.5f and cost %.5f", getName().c_str(), approxdif, best_cost_);
    	ROS_INFO("%s: Previous solution with dist %.5f and cost %.5f", ros::this_node::getName().c_str(), approxdif, best_cost_);
    }
    else
    	ROS_INFO("%s: No previous solution", ros::this_node::getName().c_str());
    OMPL_INFORM("%s: Starting planning with %u states already in datastructure", getName().c_str(), nn_->size());

    Motion      *rmotion = new Motion(siC_);
    base::State  *rstate = rmotion->state;
    Control       *rctrl = rmotion->control;
    base::State  *xstate = si_->allocState();

    while (ptc_new == false && updateTime <= max_update_time_)
    {
        /* sample random state (with goal biasing) */
        if (goal_s && rng_.uniform01() < goalBias_ && goal_s->canSample())
            goal_s->sampleGoal(rstate);
        else
            sampler_->sampleUniform(rstate);

        /* find closest state in the tree */
        Motion *nmotion = nn_->nearest(rmotion);

        /* sample a random control that attempts to go towards the random state, and also sample a control duration */
        unsigned int cd = controlSampler_->sampleTo(rctrl, nmotion->control, nmotion->state, rmotion->state);
        bool checkForSolution = false;

        if (addIntermediateStates_)
        {
            // this code is contributed by Jennifer Barry
            std::vector<base::State *> pstates;
            cd = siC_->propagateWhileValid(nmotion->state, rctrl, cd, pstates, true);

            if (cd >= siC_->getMinControlDuration())
            {
                Motion *lastmotion = nmotion;
                bool solved = false;
                size_t p = 0;
                for ( ; p < pstates.size(); ++p)
                {
                    /* create a motion */
                    Motion *motion = new Motion();
                    motion->state = pstates[p];
                    //we need multiple copies of rctrl
                    motion->control = siC_->allocControl();
                    siC_->copyControl(motion->control, rctrl);
                    motion->steps = 1;
                    motion->parent = lastmotion;
                    motion->acum_cost = motion->parent->acum_cost + motion->steps*rctrl->as<oc::RealVectorControlSpace::ControlType>()->values[0];

                    lastmotion = motion;
                    nn_->add(motion);
                    motion->parent->children.push_back(motion);

                    double dist = 0.0;
                    solved = goal->isSatisfied(motion->state, &dist);
                    if (solved)
                    {
                        approxdif = dist;
                        solution = motion;
                        break;
                    }
                    if (dist < approxdif)// && motion->acum_cost < best_cost_)
                    {
                        approxdif = dist;
                        approxsol = motion;
                        best_cost = motion->acum_cost;
                    }
                }

                //free any states after we hit the goal
                while (++p < pstates.size())
                    si_->freeState(pstates[p]);
                if (solved)
                    break;
            }
            else
                for (size_t p = 0 ; p < pstates.size(); ++p)
                    si_->freeState(pstates[p]);
        }
        else
        {
            if (cd >= siC_->getMinControlDuration())
            {
                /* create a motion */
                Motion *motion = new Motion(siC_);
                si_->copyState(motion->state, rmotion->state);
                siC_->copyControl(motion->control, rctrl);
                motion->steps = cd;
                motion->parent = nmotion;
                motion->acum_cost = motion->parent->acum_cost + motion->steps*rctrl->as<oc::RealVectorControlSpace::ControlType>()->values[0];

                nn_->add(motion);
                motion->parent->children.push_back(motion);

                double dist = 0.0;
                bool solv = goal->isSatisfied(motion->state, &dist);
                if (solv)
                {
                    approxdif = dist;
                    solution = motion;
                    break;
                }
                if (dist < approxdif)// && motion->acum_cost < best_cost_)
                {
                    approxdif = dist;
                    approxsol = motion;
                    best_cost = motion->acum_cost;
                }
            }
        }
    }

    double planTime = time::seconds(time::now() - start);
    ROS_INFO("%s: Planning took %f s", ros::this_node::getName().c_str(), planTime);

    bool solved = false;
    bool approximate = false;

    if (solution == NULL) // first time or not previous solution
    {
    	ROS_INFO("%s: New first approx solution", ros::this_node::getName().c_str());
        solution = approxsol;
        approximate = true;
    }
    else if (best_cost_ != std::numeric_limits<double>::max() && approxsol != NULL)
    {
    	ROS_INFO("%s: New approx solution", ros::this_node::getName().c_str());
    	solution = approxsol;
    	approximate = true;
    }
    else if(approxsol == NULL)
    {
    	ROS_INFO("%s: No approx solution", ros::this_node::getName().c_str());
    }

	/* set the solution path */
	PathControl *path = new PathControl(si_);
	std::cout << "******** nn_->size(): " << nn_->size() << std::endl;

	if (nn_->size()<=1)
		ROS_ERROR("%s: tree is not expanding", ros::this_node::getName().c_str());
    if (solution != NULL)
    {
        //----
        if(best_cost_ == std::numeric_limits<double>::max())
        {
        	lastGoalMotion_ = solution;
        	best_cost_ = solution->acum_cost;
        	best_dif_ = approxdif;
        	goalMotions_.push_back(solution);

        	ROS_INFO("%s: First solution, dist = %.5f, cost = %.5f", ros::this_node::getName().c_str(), approxdif, solution->acum_cost);
        }
        else if(approxdif < best_dif_ && solution->acum_cost < (extra_cost_accept_/100)*lastGoalMotion_->acum_cost)
        {
        	lastGoalMotion_ = solution;
        	best_cost_ = solution->acum_cost;
        	best_dif_ = approxdif;
        	goalMotions_.push_back(solution);

        	ROS_INFO("%s: New solution, dist = %.5f, cost = %.5f", ros::this_node::getName().c_str(), approxdif, solution->acum_cost);
        }
        else
        	solution = lastGoalMotion_;

        /* construct the solution path */
        std::vector<Motion*> mpath;
        while (solution != NULL)
        {
            mpath.push_back(solution);
            solution = solution->parent;
        }

        for (int i = mpath.size() - 1 ; i >= 0 ; --i)
            if (mpath[i]->parent)
                path->append(mpath[i]->state, mpath[i]->control, mpath[i]->steps * siC_->getPropagationStepSize());
            else
                path->append(mpath[i]->state);
        solved = true;
        pdef_->addSolutionPath(base::PathPtr(path), approximate, approxdif, getName());

        geometry_msgs::Point waypoint_msg;
        const base::SE2StateSpace::StateType *waypoint_state;
        double pruneTime;

        if(wp_need_flag_ && om_state_validity_checker_->isValidPoint(path->getState(1)))
        {
        	ROS_INFO("%s: waypoint needed. Path found, next waypoint is valid, sending it", ros::this_node::getName().c_str());
        	wp_need_flag_ = false;

        	waypoint_state = path->getState(1)->as<base::SE2StateSpace::StateType>();
        	waypoint_msg.x = waypoint_state->getX();
        	waypoint_msg.y = waypoint_state->getY();
        	waypoint_msg.z = planning_depth_;

        	online_traj_point_pub_.publish(waypoint_msg);

        	start = time::now();
        	pruneTree(mpath[mpath.size() - 2]);
        	pruneTime = time::seconds(time::now() - start);
        	ROS_INFO("%s: pruneTree took %f s", ros::this_node::getName().c_str(), pruneTime);
        }
        else if(wp_need_flag_ && !om_state_validity_checker_->isValidPoint(path->getState(1)))
        {
        	if(wp_need_flag_ && om_state_validity_checker_->isValid(path->getState(1)))
        	{
        		ROS_INFO("%s: waypoint needed. Path found but waypoint is not valid yet, sending it anyway", ros::this_node::getName().c_str());
        		wp_need_flag_ = false;

        		waypoint_state = path->getState(1)->as<base::SE2StateSpace::StateType>();
        		waypoint_msg.x = waypoint_state->getX();
        		waypoint_msg.y = waypoint_state->getY();
        		waypoint_msg.z = planning_depth_;

        		online_traj_point_pub_.publish(waypoint_msg);

        		start = time::now();
        		pruneTree(mpath[mpath.size() - 2]);
        		pruneTime = time::seconds(time::now() - start);
        		ROS_INFO("%s: pruneTree took %f s", ros::this_node::getName().c_str(), pruneTime);
        	}
        }
        else if(pending_cancel_wp_)
        {
        	ROS_INFO("%s: cancel ongoing waypoints (was pending). Now sending new waypoint", ros::this_node::getName().c_str());
        	wp_need_flag_ = false;

        	std_msgs::Bool cancel_wp_msg;
        	cancel_wp_msg.data = true;
        	cancel_wps_pub_.publish(cancel_wp_msg);
        	pending_cancel_wp_ = false;

        	start = time::now();
        	pruneTree(mpath[mpath.size() - 2]);
        	pruneTime = time::seconds(time::now() - start);
        	ROS_INFO("%s: pruneTree took %f s", ros::this_node::getName().c_str(), pruneTime);

        	waypoint_state = path->getState(1)->as<base::SE2StateSpace::StateType>();
        	waypoint_msg.x = waypoint_state->getX();
        	waypoint_msg.y = waypoint_state->getY();
        	waypoint_msg.z = planning_depth_;
        	online_traj_point_pub_.publish(waypoint_msg);
        	ROS_INFO("%s: New waypoint [%f, %f, %f]", ros::this_node::getName().c_str(), waypoint_msg.x, waypoint_msg.y, waypoint_msg.z);
        }
        else if(!wp_need_flag_)
        {
        	ROS_INFO("%s: no waypoints needed now", ros::this_node::getName().c_str());
        }
    }

	visualizeRRT(path);

    if (rmotion->state)
        si_->freeState(rmotion->state);
    if (rmotion->control)
        siC_->freeControl(rmotion->control);
    delete rmotion;
    si_->freeState(xstate);

    OMPL_INFORM("%s: Created %u states", getName().c_str(), nn_->size());

	rrt_build_flag_ = false;

    return base::PlannerStatus(solved, approximate);
}

void ompl::control::OnlineRRT::getPlannerData(base::PlannerData &data) const
{
    Planner::getPlannerData(data);

    std::vector<Motion*> motions;
    if (nn_)
        nn_->list(motions);

    double delta = siC_->getPropagationStepSize();

    if (lastGoalMotion_)
        data.addGoalVertex(base::PlannerDataVertex(lastGoalMotion_->state));

    for (unsigned int i = 0 ; i < motions.size() ; ++i)
    {
        const Motion *m = motions[i];
        if (m->parent)
        {
            if (data.hasControls())
                data.addEdge(base::PlannerDataVertex(m->parent->state),
                             base::PlannerDataVertex(m->state),
                             control::PlannerDataEdgeControl(m->control, m->steps * delta));
            else
                data.addEdge(base::PlannerDataVertex(m->parent->state),
                             base::PlannerDataVertex(m->state));
        }
        else
            data.addStartVertex(base::PlannerDataVertex(m->state));
    }
}

void ompl::control::OnlineRRT::updateOctomap()
{
	//=======================================================================
	// Set state validity checking for this space (get an updated octomap)
	//=======================================================================
	std::vector<double> planning_bounds_x;
	std::vector<double> planning_bounds_y;
	planning_bounds_x.resize(2);
	planning_bounds_x[0] = -std::numeric_limits<double>::max();
	planning_bounds_x[1] = std::numeric_limits<double>::max();
	planning_bounds_y.resize(2);
	planning_bounds_y[0] = -std::numeric_limits<double>::max();
	planning_bounds_y[1] = std::numeric_limits<double>::max();
	om_state_validity_checker_.reset(new OmFclStateValidityCheckerSE2(si_, planning_depth_, lazy_collision_eval_, lazy_collision_eval_, planning_bounds_x, planning_bounds_y));
	si_->setStateValidityChecker(base::StateValidityCheckerPtr(om_state_validity_checker_));
//	siC_->setStateValidityChecker(base::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	//DFS to find configurations under collision in updated octomap
	//=======================================================================
	size_t root_node;
	std::vector<Motion*> motions;
	nn_->list(motions);
	if(motions.size()>0)
	{
		//Find the root of the tree
		for (size_t i = 0 ; i < motions.size() ; ++i)
		{
			if (motions[i]->parent == NULL)
			{
				root_node = i;
				break;
			}
		}

		//Get current vehicle's configuration to check motion from it to tree root
		Motion *current_motion = new Motion(siC_);
		base::State *current_state = current_motion->state;
		base::SE2StateSpace::StateType *pos_current_state = current_state->as<base::SE2StateSpace::StateType>();

		double useless_pitch, useless_roll, yaw;
		last_pose_.getBasis().getEulerYPR(yaw, useless_pitch, useless_roll);

		pos_current_state->setX(last_pose_.getOrigin().getX());
		pos_current_state->setY(last_pose_.getOrigin().getY());
		pos_current_state->setYaw(yaw);

		//Check tree for collisions
		if(!si_->checkMotion(current_state, motions[root_node]->state) || !si_->isValid(motions[root_node]->state))
		{
			ROS_WARN("%s: possible collision has been detected, goals need to be canceled",ros::this_node::getName().c_str());

			best_dif_ = std::numeric_limits<double>::max();
			best_cost_ = std::numeric_limits<double>::max();

			sampler_.reset();
			controlSampler_.reset();
			freeMemory();
			nn_->clear();
			lastGoalMotion_ = NULL;
			goalMotions_.clear();

			pdef_->clearStartStates();
			pdef_->addStartState(current_state);
			nn_->add(current_motion);

//			std_msgs::Bool cancel_wp_msg;
//			cancel_wp_msg.data = true;
//			cancel_wps_pub_.publish(cancel_wp_msg);
			wp_need_flag_ = false;
			pending_cancel_wp_ = true;
		}
		else
			checkCollisionInBranch(motions[root_node]);
	}
}

void ompl::control::OnlineRRT::removeFromParent(Motion *m)
{
    std::vector<Motion*>::iterator it = m->parent->children.begin ();
    while (it != m->parent->children.end ())
    {
        if (*it == m)
        {
            it = m->parent->children.erase(it);
            it = m->parent->children.end ();
        }
        else
            ++it;
    }
}

void ompl::control::OnlineRRT::checkCollisionInBranch(Motion *m)
{
    for (size_t i = 0; i < m->children.size(); ++i)
    	if(!si_->isValid(m->children[i]->state) || !si_->checkMotion(m->state, m->children[i]->state))
    		discardBranch(m->children[i]);
    	else
    		checkCollisionInBranch(m->children[i]);
}

void ompl::control::OnlineRRT::discardBranch(Motion *m)
{
	for(size_t i = 0; i < goalMotions_.size(); ++i)
		if(m == goalMotions_[i])// && goalMotions_.size()==1)
		{
			best_dif_ = std::numeric_limits<double>::max();
			best_cost_ = std::numeric_limits<double>::max();
			lastGoalMotion_ = NULL;
			goalMotions_.clear();
		}

	while (m->children.size() > 0)
		discardBranch(m->children[0]);

	removeFromParent(m);
	nn_->remove(m);
	si_->freeState(m->state);
	siC_->freeControl(m->control);
	delete m;
}

void ompl::control::OnlineRRT::pruneTree(Motion *m)
{
	Motion *root_node;
	root_node = m->parent;

	std::vector<Motion*>::iterator it = m->parent->children.begin ();
	while (it != m->parent->children.end ())
	{
		if (*it != m)
			discardBranch(*it);
		else
			++it;
	}

	m->parent = NULL;
	nn_->remove(root_node);
	si_->freeState(root_node->state);
	siC_->freeControl(root_node->control);
	delete root_node;

	pdef_->clearStartStates();
	pdef_->addStartState(m->state);
}

//ROS related functions
void ompl::control::OnlineRRT::odomCallback(const nav_msgs::OdometryConstPtr &odom_msg)
{
	if(!nav_sts_available_)
		nav_sts_available_ = true;
    tf::poseMsgToTF(odom_msg->pose.pose, last_pose_);
}

void ompl::control::OnlineRRT::wpNeedSubCallback(const std_msgs::Bool &wp_need_flag_msg)
{
	wp_need_flag_ = wp_need_flag_msg.data;
	if(!wp_need_flag_)
	{
		//ROS_DEBUG("%s received that NO more waypoints required", ros::this_node::getName().c_str())
		;
	}
	else
	{
		//ROS_DEBUG("%s received that MORE waypoints required", ros::this_node::getName().c_str())
		;
	}
}

void ompl::control::OnlineRRT::visualizeRRT(PathControl *path)
{
	geometry_msgs::PoseArray pose_path;
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path;
	tf::Quaternion orien_quat;

//	path->interpolate();

	base::Goal *goal_state = pdef_->getGoal().get();
	base::State *start_state= pdef_->getStartState(0);

	const base::SE2StateSpace::StateType *q_start = start_state->as<base::SE2StateSpace::StateType>();

	// %Tag(MARKER_INIT)%
	pose_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	pose_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;
	result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 1;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.4;
	result_path.scale.y = q_init_goal.scale.y = 0.4;
	result_path.scale.z = q_init_goal.scale.z = 0.4;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	result_path.color.g = q_init_goal.color.g = 1.0;
	result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.b = 1.0;
	visual_rrt.color.a = 1.0;

	//	std::cout << "Number of nodes of the RRT*: " << nn_->size() << std::endl;

//	std::vector<Motion*> motions;
	const base::SE2StateSpace::StateType *state;
	geometry_msgs::Point p;
//
//	p.x = q_start->values[0];
//	p.y = q_start->values[1];
//	p.z = planning_depth_;
//	q_init_goal.points.push_back(p);
//
//	nn_->list(motions);
//
//	for (unsigned int i = 1 ; i < motions.size() ; ++i)
//	{
//		if (motions[i]->state && motions[i]->parent != NULL)
//		{
//			pos = motions[i]->state->as<base::RealVectorStateSpace::StateType>();
//			p.x = pos->values[0];
//			p.y = pos->values[1];
//			p.z = planning_depth_;
//
//			visual_rrt.points.push_back(p);
//
//			pos = motions[i]->parent->state->as<base::RealVectorStateSpace::StateType>();
//			p.x = pos->values[0];
//			p.y = pos->values[1];
//			p.z = planning_depth_;
//			visual_rrt.points.push_back(p);
//		}
//	}
	std::vector< base::State * > states = path->getStates();

	for (uint32_t i = 0; i < path->getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		state = states[i]->as<base::SE2StateSpace::StateType>();

		p.x = state->getX();
		p.y = state->getY();
		p.z = planning_depth_;//pos->values[2];

		result_path.points.push_back(p);

		//--poses
		geometry_msgs::Pose pose;

		orien_quat.setRPY(0., 0., state->getYaw());

		pose.position.x = p.x;
		pose.position.y = p.y;
		pose.position.z = p.z;
		pose.orientation.x = orien_quat.getX();
		pose.orientation.y = orien_quat.getY();
		pose.orientation.z = orien_quat.getZ();
		pose.orientation.w = orien_quat.getW();
		pose_path.poses.push_back(pose);
	}

	online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	online_traj_pub_.publish(result_path);
	online_traj_pose_pub_.publish(pose_path);
}
