/*
 * online_rrt_SE2_iros15_main.cpp
 *
 *  Created on: December 1, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Online path planning (with differential constraints) using a modified RRT (taken from OMPL).
 *  Octomaps are used to represent workspace and to validate configurations.
 */

#include "online_rrt_SE2.hpp"

void shutdownNode(int sig);

//!  OnlinePlannerSE2 class.
/*!
 * Online Planner.
 * Setup a modified RRT for online computation of collision-free paths.
 * C-Space: SE2
 * Workspace is represented with Octomaps
*/
class OnlinePlannerSE2
{
    public:
		//! Constructor
		OnlinePlannerSE2();
		void timerCallback(const ros::TimerEvent &e);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;
		//OMPL, online planner
		oc::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_, min_control_duration_, max_control_duration_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
};

void propagate(const ob::State *start, const oc::Control *control, const double duration, ob::State *result)
{
    const ob::SE2StateSpace::StateType *se2state = start->as<ob::SE2StateSpace::StateType>();
    const double* pos = se2state->as<ob::RealVectorStateSpace::StateType>(0)->values;
    const double rot = se2state->as<ob::SO2StateSpace::StateType>(1)->value;
    const double* ctrl = control->as<oc::RealVectorControlSpace::ControlType>()->values;

    result->as<ob::SE2StateSpace::StateType>()->setXY(
        pos[0] + ctrl[0] * duration * cos(rot),
        pos[1] + ctrl[0] * duration * sin(rot));
    result->as<ob::SE2StateSpace::StateType>()->setYaw(
        rot    + ctrl[1] * duration);
}


OnlinePlannerSE2::OnlinePlannerSE2()
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, shutdownNode);

	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(2);
	goal_state_.resize(2);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/timer_period", timer_period_, timer_period_);
	node_handler_.param("planning_framework/solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/min_control_duration", min_control_duration_, min_control_duration_);
	node_handler_.param("planning_framework/max_control_duration", max_control_duration_, max_control_duration_);

	//=======================================================================
	// Instantiate the state space (SE2)
	//=======================================================================
	ob::StateSpacePtr space(new ob::SE2StateSpace());

	//=======================================================================
	// Instantiate the control space
	//=======================================================================
    oc::ControlSpacePtr cspace(new KinAUVControlSpace(space));

    // set the bounds for the control space
    ob::RealVectorBounds cbounds(2);
    cbounds.setLow(0,0.9);
    cbounds.setHigh(0,1.0);
    cbounds.setLow(1,-0.3);
    cbounds.setHigh(1,0.3);

    cspace->as<KinAUVControlSpace>()->setBounds(cbounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = oc::SimpleSetupPtr( new oc::SimpleSetup(cspace) );
	oc::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	// Set minimum and maximum duration of control action
	si->setMinMaxControlDuration(min_control_duration_, max_control_duration_);

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = ob::PlannerPtr(new oc::OnlineRRT(si, planning_depth_));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

//	//=======================================================================
//	// Set state validity checking for this space
//	//=======================================================================
//	OmStateValidityCheckerSE2 * om_stat_val_check = new OmStateValidityCheckerSE2(si, planning_depth_, checking_side_length_);
//	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// set the propagation routine for this space
	//=======================================================================
	simple_setup_->setStatePropagator(oc::StatePropagatorPtr(new KinAUVStatePropagator(simple_setup_->getSpaceInformation())));
	//simple_setup_->setStatePropagator(std::bind(&propagate, _1, _2, _3, _4));

	ob::RealVectorBounds bounds(2);
	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	space->as<ob::SE2StateSpace>()->setBounds(bounds);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<ob::SE2StateSpace> start(space);

	start->setX(double(start_state_[0]));
	start->setY(double(start_state_[1]));
	start->setYaw(double(start_state_[2]));

	// create a goal state
	ob::ScopedState<ob::SE2StateSpace> goal(space);

	goal->setX(double(goal_state_[0]));
	goal->setY(double(goal_state_[1]));
	goal->setYaw(double(goal_state_[2]));

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal, 0.05);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();
	static_cast<KinAUVStatePropagator*>(simple_setup_->getStatePropagator().get())->setIntegrationTimeStep(simple_setup_->getSpaceInformation()->getPropagationStepSize());

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OnlinePlannerSE2::timerCallback, this);

	ros::spin();
}

void OnlinePlannerSE2::timerCallback(const ros::TimerEvent &e)
{
	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "online_planner_SE2_icra15");

	ROS_INFO("%s: online planner with kinematic constraints (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
//	   ros::console::notifyLoggerLevelsChanged();
//	}

	OnlinePlannerSE2 online_planner;

	return 0;
}

void shutdownNode(int sig)
{
	ROS_INFO("%s: shutting down node", ros::this_node::getName().c_str());
	ros::shutdown();
}
