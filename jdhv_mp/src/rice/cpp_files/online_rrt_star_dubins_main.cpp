/*
 * online_rrt_star_R2_icra15_main.cpp
 *
 *  Created on: Feb 15, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Online path planning (geometric, without differential constraints) using a modified RRT* using the Dubins State Space.
 *  Octomaps are used to represent workspace and to validate configurations.
 */

#include "online_rrt_star_dubins.hpp"

void shutdownNode(int sig);

//!  OnlinePlannerR2 class.
/*!
 * Online Planner.
 * Setup a modified RRT* for online computation of collision-free paths.
 * C-Space: R2
 * Workspace is represented with Octomaps
*/
class OnlinePlannerR2
{
    public:
		// Constructor
		OnlinePlannerR2();
		void timerCallback(const ros::TimerEvent &e);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;
		//OMPL, online planner
		og::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
};

OnlinePlannerR2::OnlinePlannerR2()
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, shutdownNode);

	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(2);
	goal_state_.resize(2);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/timer_period", timer_period_, timer_period_);
	node_handler_.param("planning_framework/solving_time", solving_time_, solving_time_);

	//=======================================================================
	// Instantiate the state space (R2)
	//=======================================================================
	ob::StateSpacePtr space(new ob::DubinsStateSpace(3.0));
	//ob::StateSpacePtr space(new ob::ReedsSheppStateSpace());

	// Set the bounds for the state space
	ob::RealVectorBounds bounds(2);

	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	space->as<ob::SE2StateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = ob::PlannerPtr(new og::OnlineRRTstar(si, planning_depth_));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);

	start[0] = double(start_state_[0]);
	start[1] = double(start_state_[1]);
	start[2] = double(start_state_[2]);

	// create a goal state
	ob::ScopedState<> goal(space);

	goal[0] = double(goal_state_[0]);
	goal[1] = double(goal_state_[1]);
	goal[2] = double(goal_state_[2]);
	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
//	simple_setup_->print();
//	planner->printProperties(std::cout);// print planner properties
//	si->printSettings(std::cout);// print the settings for this space

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OnlinePlannerR2::timerCallback, this);

	ros::spin();
}

void OnlinePlannerR2::timerCallback(const ros::TimerEvent &e)
{
	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "online_planner_dubins");

	ROS_INFO("%s: online planner using Dubins state space (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OnlinePlannerR2 online_planner;

	return 0;
}

void shutdownNode(int sig)
{
	ROS_INFO("%s: shutting down node", ros::this_node::getName().c_str());
	ros::shutdown();
}


