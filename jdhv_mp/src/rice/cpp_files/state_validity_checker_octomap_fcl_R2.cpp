/*
 * state_validity_checker_octomap_fcl_R2.cpp
 *
 *  Created on: Mar 26, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (R2 state) is collision-free.
 *  The workspace is represented by an octomap and collision check is done with FCL.
 */

#include "state_validity_checker_octomap_fcl_R2.hpp"

OmFclStateValidityCheckerR2::OmFclStateValidityCheckerR2(const ob::SpaceInformationPtr &si, const double planning_depth, const bool lazy_collision_eval, std::vector<double> planning_bounds_x, std::vector<double> planning_bounds_y) :
	ob::StateValidityChecker(si)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	planning_depth_ = planning_depth;
	lazy_collision_eval_ = lazy_collision_eval;
	planning_bounds_x_ = planning_bounds_x;
	planning_bounds_y_ = planning_bounds_y;

	serv_name = "/laser_octomap/get_binary";
	octree_ = NULL;

	ROS_DEBUG("%s: requesting the map to %s...", ros::this_node::getName().c_str(), node_hand_.resolveName(serv_name).c_str());

	while((node_hand_.ok() && !ros::service::call(serv_name, req, resp)) || resp.map.data.size()==0)
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		abs_octree_ = octomap_msgs::msgToMap(resp.map);
		std::cout << std::endl;
		if (abs_octree_){
			octree_ = dynamic_cast<octomap::OcTree*>(abs_octree_);
			tree_ = new fcl::OcTree(std::shared_ptr<const octomap::OcTree>(octree_));
			tree_obj_ = new fcl::CollisionObject((std::shared_ptr<fcl::CollisionGeometry>(tree_)));

			vehicle_solid_.reset(new fcl::Box(1.5,1.5,1.0));
		}

		octree_->getMetricMin(octree_min_x_, octree_min_y_, octree_min_z_);
		octree_->getMetricMax(octree_max_x_, octree_max_y_, octree_max_z_);

		if (octree_){
			ROS_DEBUG("%s: Octomap received (%zu nodes, %f m res)", ros::this_node::getName().c_str(), octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmFclStateValidityCheckerR2::isValid(const ob::State *state) const
{
	const ob::RealVectorStateSpace::StateType *state_pos;

	//ompl::tools::Profiler::Begin("collision");

	// extract the component of the state and cast it to what we expect
	state_pos = state->as<ob::RealVectorStateSpace::StateType>();

	if(lazy_collision_eval_ && (state_pos->values[0] < octree_min_x_ || state_pos->values[1] < octree_min_y_ || state_pos->values[0] > octree_max_x_ || state_pos->values[1] > octree_max_y_))
	{
		//ompl::tools::Profiler::End("collision");
		return true;
	}

	if(state_pos->values[0] < planning_bounds_x_[0] || state_pos->values[1] < planning_bounds_y_[0] || state_pos->values[0] > planning_bounds_x_[1] || state_pos->values[1] > planning_bounds_y_[1])
	{
		//ompl::tools::Profiler::End("collision");
		return false;
	}

	//FCL
	fcl::Transform3f vehicle_tf;
	vehicle_tf.setIdentity();
	vehicle_tf.setTranslation(fcl::Vec3f(state_pos->values[0], state_pos->values[1], planning_depth_));

	fcl::CollisionObject vehicle_co(vehicle_solid_, vehicle_tf);
	fcl::CollisionRequest collision_request;
	fcl::CollisionResult collision_result;

	fcl::collide(tree_obj_, &vehicle_co, collision_request, collision_result);

	//std::cout << "Collision (FCL): " << collision_result.isCollision() << std::endl;

	if(collision_result.isCollision())
	{
		//ompl::tools::Profiler::End("collision");
		return	false;
	}
	else
	{
		//ompl::tools::Profiler::End("collision");
		return	true;
	}
}

double OmFclStateValidityCheckerR2::clearance(const ob::State *state) const
{
	const ob::RealVectorStateSpace::StateType *state_pos;
	double minDist = std::numeric_limits<double>::infinity ();

	//ompl::tools::Profiler::Begin("clearance");

	// extract the component of the state and cast it to what we expect
	state_pos = state->as<ob::RealVectorStateSpace::StateType>();

	fcl::Transform3f vehicle_tf;
	vehicle_tf.setIdentity();
	vehicle_tf.setTranslation(fcl::Vec3f(state_pos->values[0], state_pos->values[1], planning_depth_));

	fcl::CollisionObject vehicle_co(vehicle_solid_, vehicle_tf);
	fcl::DistanceRequest distanceRequest;
	fcl::DistanceResult distanceResult;

	fcl::distance(tree_obj_, &vehicle_co, distanceRequest, distanceResult);

	//std::cout << "Distance (FCL): " << distanceResult.min_distance << std::endl;

	if (distanceResult.min_distance < minDist)
		minDist = distanceResult.min_distance;

	//ompl::tools::Profiler::End("clearance");

	return minDist;
}

bool OmFclStateValidityCheckerR2::isValidPoint(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	double node_occupancy;

	// extract the component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = state->as<ob::RealVectorStateSpace::StateType>();

	query.x() = pos->values[0];
	query.y() = pos->values[1];
	query.z() = planning_depth_;

	result = octree_->search (query);

	if(result == NULL){
		return false;
	}
	else{
		node_occupancy = result->getOccupancy();
		if (node_occupancy <= 0.4)
			return true;
	}
	return false;
}

OmFclStateValidityCheckerR2::~OmFclStateValidityCheckerR2()
{
    delete octree_;
//    delete tree_;
//    delete tree_obj_;
}
