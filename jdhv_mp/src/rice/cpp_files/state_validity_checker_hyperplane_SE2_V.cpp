/*
 * state_validity_checker_hyperplane_SE2_V.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (SE2 state) is collision-free.
 *  The workspace is represented by convex obstacles (hyperplanes).
 */

#include "state_validity_checker_hyperplane_SE2_V.hpp"

#include <iostream>
#include <cmath>

HyperplaneStateValidityCheckerSE2V::HyperplaneStateValidityCheckerSE2V(const ob::SpaceInformationPtr &si, const double planning_depth, const bool lazy_collision_eval, const double accep_prob) :
	ob::StateValidityChecker(si)
{
	planning_depth_ = planning_depth;
	accep_prob_ = accep_prob;

	A1_.resize(6,3);
	A2_.resize(6,3);
	A3_.resize(6,3);
	B1_.resize(6,1);
	B2_.resize(6,1);
	B3_.resize(6,1);
	Eigen::Vector3d p1(0.0, 3.5, 0.0);
	Eigen::Vector3d q1(12.5, 3.5, 0.0);
	Eigen::Vector3d r1(12.5, 3.5, 7.0);

	Eigen::Vector3d p2(0.0, 3.5, 0.0);
	Eigen::Vector3d q2(0.0, 17.5, 0.0);
	Eigen::Vector3d r2(0.0, 17.5, 7.0);

	Eigen::Vector3d p3(12.5, 17.5, 0.0);
	Eigen::Vector3d q3(0.0, 17.5, 0.0);
	Eigen::Vector3d r3(12.5, 17.5, 7.0);

	Eigen::Vector3d p4(17.0, 3.5, 0.0);
	Eigen::Vector3d q4(17.0, 17.5, 0.0);
	Eigen::Vector3d r4(17.0, 17.5, 7.0);

	Eigen::Vector3d p5(29.5, 3.5, 0.0);
	Eigen::Vector3d q5(29.5, 17.5, 0.0);
	Eigen::Vector3d r5(29.5, 17.5, 7.0);

	Eigen::Vector3d p6(34.0, 3.5, 0.0);
	Eigen::Vector3d q6(34.0, 17.5, 0.0);
	Eigen::Vector3d r6(34.0, 17.5, 7.0);

	Eigen::Vector3d p7(46.5, 3.5, 0.0);
	Eigen::Vector3d q7(46.5, 17.5, 0.0);
	Eigen::Vector3d r7(46.5, 17.5, 7.0);

	//Obstacle1
	//H1
	Eigen::Vector3d v1(q1 - p1);
	Eigen::Vector3d v2(r1 - p1);
	Eigen::Vector3d n(v1.cross(v2));
	A1_(0,0) = n(0); A1_(0,1) = n(1); A1_(0,2) = n(2);
	B1_(0,0) = n(0)*p1(0)+n(1)*p1(1)+n(2)*p1(2);

	//H2
	v1 = p2 - q2;
	v2 = r2 - q2;
	n = v1.cross(v2);
	A1_(1,0) = n(0); A1_(1,1) = n(1); A1_(1,2) = n(2);
	B1_(1,0) = n(0)*p2(0)+n(1)*p2(1)+n(2)*p2(2);

	//H3
	v1 = q2 - p3;
	v2 = r2 - p3;
	n = v1.cross(v2);
	A1_(2,0) = n(0); A1_(2,1) = n(1); A1_(2,2) = n(2);
	B1_(2,0) = n(0)*p3(0)+n(1)*p3(1)+n(2)*p3(2);

	//H4
	v1 = q1 - p3;
	v2 = r1 - p3;
	n = v2.cross(v1);
	A1_(3,0) = n(0); A1_(3,1) = n(1); A1_(3,2) = n(2);
	B1_(3,0) = n(0)*p3(0)+n(1)*p3(1)+n(2)*p3(2);

	//H5
	v1 = r1 - r3;
	v2 = r2 - r3;
	n = v2.cross(v1);
	A1_(4,0) = n(0); A1_(4,1) = n(1); A1_(4,2) = n(2);
	B1_(4,0) = n(0)*r1(0)+n(1)*r1(1)+n(2)*r1(2);

	//H6
	v1 = q1 - p3;
	v2 = q2 - p3;
	n = v1.cross(v2);
	A1_(5,0) = n(0); A1_(5,1) = n(1); A1_(5,2) = n(2);
	B1_(5,0) = n(0)*q1(0)+n(1)*q1(1)+n(2)*q1(2);

	//Obstacle2
	//H7
	A2_ = A1_;
	B2_ = B1_;
	A3_ = A1_;
	B3_ = B1_;
	v1 = p4 - q4;
	v2 = r4 - q4;
	n = v1.cross(v2);
	A2_(1,0) = n(0); A2_(1,1) = n(1); A2_(1,2) = n(2);
	B2_(1,0) = n(0)*p4(0)+n(1)*p4(1)+n(2)*p4(2);

	//H8
	v1 = p5 - q5;
	v2 = r5 - q5;
	n = v2.cross(v1);
	A2_(3,0) = n(0); A2_(3,1) = n(1); A2_(3,2) = n(2);
	B2_(3,0) = n(0)*p5(0)+n(1)*p5(1)+n(2)*p5(2);

	//Obstacle3
	//H9
	v1 = p6 - q6;
	v2 = r6 - q6;
	n = v1.cross(v2);
	A3_(1,0) = n(0); A3_(1,1) = n(1); A3_(1,2) = n(2);
	B3_(1,0) = n(0)*p6(0)+n(1)*p6(1)+n(2)*p6(2);

	//H10
	v1 = p7 - q7;
	v2 = r7 - q7;
	n = v2.cross(v1);
	A3_(3,0) = n(0); A3_(3,1) = n(1); A3_(3,2) = n(2);
	B3_(3,0) = n(0)*p7(0)+n(1)*p7(1)+n(2)*p7(2);
}

bool HyperplaneStateValidityCheckerSE2V::isValid(const ob::State *state) const
{
	const ob::SE2StateSpace::StateType *state_se2;
	const ob::RealVectorStateSpace::StateType *state_linear_v;
	Eigen::MatrixXf Px(2,2);
	Eigen::MatrixXf a(2,1);
	Eigen::MatrixXf Pv_sq;
	double Pv;
	double b_;
	double erf_inv;

	//ompl::tools::Profiler::Begin("collision hyperplanes");

	// extract the component of the state and cast it to what we expect
	state_se2 = state->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
	state_linear_v = state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1);

	Px(0,0) = state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[0];
	Px(0,1) = state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[2];
	Px(1,0) = state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[8];
	Px(1,1) = state->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[10];

	if(state_linear_v->values[0] < 0.25 || state_linear_v->values[0] > 0.5)
	{
		//ompl::tools::Profiler::End("collision hyperplanes");
		return false;
	}

	bool valid = false;
	for(int i=0;i < 6;i++)
	{
		a(0,0) = A1_(i,0);
		a(1,0) = A1_(i,1);
		Pv_sq = a.transpose()*Px*a;
		Pv = sqrt(Pv_sq(0,0));

		if(accep_prob_ == 0.6)
			erf_inv = -0.179143;
		else if(accep_prob_ == 0.7)
			erf_inv = -0.370807;
		else if(accep_prob_ == 0.8)
			erf_inv = -0.595116;
		else if(accep_prob_ == 0.9)
			erf_inv = -0.906194;
		else if(accep_prob_ == 0.91)
			erf_inv = -0.948057;
		else if(accep_prob_ == 0.92)
			erf_inv = -0.993536;
		else if(accep_prob_ == 0.93)
			erf_inv = -1.043542;
		else if(accep_prob_ == 0.94)
			erf_inv = -1.099391;
		else if(accep_prob_ == 0.95)
			erf_inv = -1.163087;
		else if(accep_prob_ == 0.99)
			erf_inv = -1.644976;
		b_ = sqrt(2)*Pv*(erf_inv);

//		std::cout << "b_: " << b_ << std::endl;
//		std::cout << "b: " << B1_(i,0) << std::endl;

		if(state_se2->getX()*A1_(i,0)+state_se2->getY()*A1_(i,1)+planning_depth_*A1_(i,2) > (B1_(i,0) - b_))
		{
			valid = true;
			break;
		}
	}

	if(valid==false)
	{
		//ompl::tools::Profiler::End("collision hyperplanes");
		return valid;
	}

	valid = false;
	for(int i=0;i < 6;i++)
	{
		a(0,0) = A2_(i,0);
		a(1,0) = A2_(i,1);
		Pv_sq = a.transpose()*Px*a;
		Pv = sqrt(Pv_sq(0,0));

		if(accep_prob_ == 0.6)
			erf_inv = -0.179143;
		else if(accep_prob_ == 0.7)
			erf_inv = -0.370807;
		else if(accep_prob_ == 0.8)
			erf_inv = -0.595116;
		else if(accep_prob_ == 0.9)
			erf_inv = -0.906194;
		else if(accep_prob_ == 0.91)
			erf_inv = -0.948057;
		else if(accep_prob_ == 0.92)
			erf_inv = -0.993536;
		else if(accep_prob_ == 0.93)
			erf_inv = -1.043542;
		else if(accep_prob_ == 0.94)
			erf_inv = -1.099391;
		else if(accep_prob_ == 0.95)
			erf_inv = -1.163087;
		else if(accep_prob_ == 0.99)
			erf_inv = -1.644976;
		b_ = sqrt(2)*Pv*(erf_inv);

		if(state_se2->getX()*A2_(i,0)+state_se2->getY()*A2_(i,1)+planning_depth_*A2_(i,2) > (B2_(i,0) - b_))
		{
			valid = true;
			break;
		}
	}

	if(valid==false)
	{
		//ompl::tools::Profiler::End("collision hyperplanes");
		return valid;
	}

	valid = false;
	for(int i=0;i < 6;i++)
	{
		a(0,0) = A3_(i,0);
		a(1,0) = A3_(i,1);
		Pv_sq = a.transpose()*Px*a;
		Pv = sqrt(Pv_sq(0,0));

		if(accep_prob_ == 0.6)
			erf_inv = -0.179143;
		else if(accep_prob_ == 0.7)
			erf_inv = -0.370807;
		else if(accep_prob_ == 0.8)
			erf_inv = -0.595116;
		else if(accep_prob_ == 0.9)
			erf_inv = -0.906194;
		else if(accep_prob_ == 0.91)
			erf_inv = -0.948057;
		else if(accep_prob_ == 0.92)
			erf_inv = -0.993536;
		else if(accep_prob_ == 0.93)
			erf_inv = -1.043542;
		else if(accep_prob_ == 0.94)
			erf_inv = -1.099391;
		else if(accep_prob_ == 0.95)
			erf_inv = -1.163087;
		else if(accep_prob_ == 0.99)
			erf_inv = -1.644976;
		b_ = sqrt(2)*Pv*(erf_inv);

		if(state_se2->getX()*A3_(i,0)+state_se2->getY()*A3_(i,1)+planning_depth_*A3_(i,2) > (B3_(i,0) - b_))
		{
			//ompl::tools::Profiler::End("collision hyperplanes");
			valid = true;
			break;
		}
	}

	return valid;
}


HyperplaneStateValidityCheckerSE2V::~HyperplaneStateValidityCheckerSE2V()
{
//    delete octree_;
//    delete tree_;
//    delete tree_obj_;
}
