/*
 * motion_model_auv.cpp
 *
 *  Created on: December 1, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Differential models of AUVs.
 *  Kinematic and dynamic models.
 */

//motion models of auv
#include "motion_model_auv.hpp"


//! Constructor.
KinematicAuvModel::KinematicAuvModel(const ob::StateSpace *space) : space_(space), auv_length_(2.0)
{
}

/// implement the function describing the robot motion: qdot = f(q, u)
void KinematicAuvModel::operator()(const ob::State *state, const oc::Control *control, std::valarray<double> &dstate) const
{
	const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
	const double theta = state->as<ob::SE2StateSpace::StateType>()->getYaw();

	dstate.resize(3);
	dstate[0] = u[0] * cos(theta);
	dstate[1] = u[0] * sin(theta);
	dstate[2] = u[1];//u[0] * tan(u[1]) / carLength_;
}

/// implement y(n+1) = y(n) + d
void KinematicAuvModel::update(ob::State *state, const std::valarray<double> &dstate) const
{
	ob::SE2StateSpace::StateType &s = *state->as<ob::SE2StateSpace::StateType>();
	s.setX(s.getX() + dstate[0]);
	s.setY(s.getY() + dstate[1]);
	s.setYaw(s.getYaw() + dstate[2]);
	space_->enforceBounds(state);
}

//! Constructor for Euler integrator.
template<typename F>
EulerIntegrator<F>::EulerIntegrator(const ob::StateSpace *space, double timeStep) : space_(space), timeStep_(timeStep), ode_(space)
{
}

template<typename F>
void EulerIntegrator<F>::propagate(const ob::State *start, const oc::Control *control, const double duration, ob::State *result) const
{
	double t = timeStep_;
	std::valarray<double> dstate;
	space_->copyState(result, start);
	while (t < duration + std::numeric_limits<double>::epsilon())
	{
		ode_(result, control, dstate);
		ode_.update(result, timeStep_ * dstate);
		t += timeStep_;
	}
//	if (t + std::numeric_limits<double>::epsilon() > duration)
//	{
//		ode_(result, control, dstate);
//		ode_.update(result, (t - duration) * dstate);
//	}
}

template<typename F>
double EulerIntegrator<F>::getTimeStep(void) const
{
	return timeStep_;
}

template<typename F>
void EulerIntegrator<F>::setTimeStep(double timeStep)
{
	timeStep_ = timeStep;
}

/// @cond IGNORE

KinAUVControlSpace::KinAUVControlSpace(const ob::StateSpacePtr &stateSpace) : oc::RealVectorControlSpace(stateSpace, 2)
{
}

KinAUVStatePropagator::KinAUVStatePropagator(const oc::SpaceInformationPtr &si) : oc::StatePropagator(si),
		integrator_(si->getStateSpace().get(), 0.0)
{
}

void KinAUVStatePropagator::propagate(const ob::State *state, const oc::Control* control, const double duration, ob::State *result) const
{
	integrator_.propagate(state, control, duration, result);
}

void KinAUVStatePropagator::setIntegrationTimeStep(double timeStep)
{
	integrator_.setTimeStep(timeStep);
}

double KinAUVStatePropagator::getIntegrationTimeStep(void) const
{
	return integrator_.getTimeStep();
}

/// @endcond

//void ompl::app::DynamicCarPlanning::ode(const control::ODESolver::StateType& q, const control::Control *ctrl, control::ODESolver::StateType& qdot)
//   32 {
//   33     // Retrieving control inputs
//   34     const double *u = ctrl->as<control::RealVectorControlSpace::ControlType>()->values;
//   35
//   36     // zero out qdot
//   37     qdot.resize (q.size (), 0);
//   38
//   39     qdot[0] = q[3] * cos(q[2]);
//   40     qdot[1] = q[3] * sin(q[2]);
//   41     qdot[2] = q[3] * mass_ * lengthInv_ * tan(q[4]);
//   42
//   43     qdot[3] = u[0];
//   44     qdot[4] = u[1];
//   45 }

DynUnicycleControlSpace::DynUnicycleControlSpace(const ob::StateSpacePtr &stateSpace) : oc::RealVectorControlSpace(stateSpace, 2)
{
}

// Definition of the ODE for the Dynamic Unicycle car.
void DynUnicycleODE (const oc::ODESolver::StateType& q, const oc::Control* control, oc::ODESolver::StateType& qdot)
{
	const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;

	// Zero out qdot
	qdot.resize (q.size (), 0);

	qdot[0] = q[3] * cos(q[2]); // v*cos(theta)
	qdot[1] = q[3] * sin(q[2]); // v*sin(theta)

	qdot[2] = u[0]; // turning rate
	qdot[3] = u[1]; // linear acceleration
}

// This is a callback method invoked after numerical integration.
void DynUnicyclePostIntegration (const ob::State* /*state*/, const oc::Control* /*control*/, const double /*duration*/, ob::State *result)
{
	// Normalize orientation between 0 and 2*pi
	ob::SO2StateSpace SO2;

	// Normalize orientation value between 0 and 2*pi
	ob::SO2StateSpace::StateType* so2 = result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->as<ob::SO2StateSpace::StateType>(1);
	SO2.enforceBounds(so2);
}

void propagateDynUnicycle(const ob::State *start, const oc::Control *control, const double duration, ob::State *result)
{
    const ob::SE2StateSpace::StateType *se2state = start->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
    const double* lin_v_state = start->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values;

    const double* pos = se2state->as<ob::RealVectorStateSpace::StateType>(0)->values;
    const double rot = se2state->as<ob::SO2StateSpace::StateType>(1)->value;
    const double* ctrl = control->as<oc::RealVectorControlSpace::ControlType>()->values;

    result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setXY(
        pos[0] + lin_v_state[0] * duration * cos(rot),
        pos[1] + lin_v_state[0] * duration * sin(rot));
    result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setYaw(
        rot    + ctrl[0] * duration);
    result->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0] = lin_v_state[0] + ctrl[1] * duration;

    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    // Normalize orientation value between 0 and 2*pi
    ob::SO2StateSpace::StateType* so2 = result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->as<ob::SO2StateSpace::StateType>(1);
    SO2.enforceBounds(so2);

    //covariance
	Eigen::MatrixXf start_cov(4,4);
	Eigen::MatrixXf result_cov(4,4);
	Eigen::MatrixXf A(4,4);
	Eigen::MatrixXf Ad(4,4);
	Eigen::MatrixXf Pw(4,4);
	A << 0.0, 1.0, 0.0, 0.0,
		 0.0, 0.0, 0.0, 0.0,
		 0.0, 0.0, 0.0, 1.0,
		 0.0, 0.0, 0.0, 0.0;

	Ad = Eigen::MatrixXf::Identity(4, 4) + A*duration;
	Ad = Ad.inverse();
	Pw = 0.01*Eigen::MatrixXf::Identity(4, 4)*duration*duration;

    int cov_ind = 0;
    for(int i=0; i<4 ; i++)
    {
    	for(int j=0; j<4 ; j++)
		{
    		start_cov(i,j) = start->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind];
    		cov_ind++;
		}
    }

    result_cov = Ad*start_cov*Ad.transpose() + Pw;
//    std::cout << "duration: " << duration << std::endl;
//    std::cout << "start_cov: " << start_cov << std::endl;
//    std::cout << "result_cov: " << result_cov << std::endl;

    cov_ind = 0;
    for(int i=0; i<4 ; i++)
    {
    	for(int j=0; j<4 ; j++)
    	{
    		result->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind] = result_cov(i,j);
    		cov_ind++;
    	}
    }
}
