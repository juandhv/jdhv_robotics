/*
 * offline_planning_framework_main.cpp
 *
 *  Created on: March 5, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Framework for planning collision-free paths offline, i.e. the map is assumed to be known
 *  and there is no connection with vehicle's controllers. Iterative planning uses last
 *  known solution path in order to guarantee that new solution paths are always at
 *  least as good as the previous one.
 */

#include <ompl/control/SpaceInformation.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/objectives/MaximizeMinClearanceObjective.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/config.h>

#include <iostream>
#include <vector>

//ROS
#include <ros/ros.h>
#include <ros/package.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

#include "state_validity_checker_octomap_fcl_R2.hpp"
#include "state_validity_checker_octomap_fcl_SE2.hpp"
#include "new_state_sampler.h"
#include "state_cost_objective.hpp"

//ROS
#include <ros/ros.h>

//
#include <boost/bind.hpp>

namespace ob = ompl::base;
namespace oc = ompl::control;

//!  OfflinePlannFramework class.
/*!
 * Offline Planning Framework.
 * Setup a sampling-based planner for offline computation of collision-free paths.
 * C-Space: R2 or Dubins
 * Workspace is represented with Octomaps
*/
class OfflinePlannFramework
{
    public:
		//! Constructor
		OfflinePlannFramework();
		//! Planner setup
		void planWithSimpleSetup();
		//! Periodic callback to solve the query.
		void timerCallback(const ros::TimerEvent &e);
		//! Procedure to visualize the resulting path
		void visualizeRRT(og::PathGeometric geopath);
		//! Procedure to create a mission file using the resulting path
		void createMissionFile(og::PathGeometric geopath);
    private:
		//Profiling files
		std::fstream log_file_;

    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;
		ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_;

		//OMPL, online planner
		og::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_;
		bool lazy_collision_eval_, use_new_sampler_, motion_cost_interpolation_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
		std::string planner_name_, state_space_type_, optimization_objective_, scen_name_test_numb_, log_file_path_;
		std::vector<const ob::State *> path_states_;
};

//!  Constructor.
/*!
 * Load planner parameters from configuration file.
 * Publishers to visualize the resulting path.
*/
OfflinePlannFramework::OfflinePlannFramework()
{
	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(2);
	goal_state_.resize(2);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/timer_period", timer_period_, timer_period_);
	node_handler_.param("planning_framework/offline_solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);
	node_handler_.param("planning_framework/planner_name", planner_name_, planner_name_);
	node_handler_.param("planning_framework/use_new_sampler", use_new_sampler_, use_new_sampler_);
	node_handler_.param("planning_framework/state_space_type", state_space_type_, state_space_type_);
	node_handler_.param("planning_framework/optimization_objective", optimization_objective_, optimization_objective_);
	node_handler_.param("planning_framework/motion_cost_interpolation", motion_cost_interpolation_, motion_cost_interpolation_);
	node_handler_.param("planning_framework/scen_name_test_numb", scen_name_test_numb_, scen_name_test_numb_);

	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);
}

//!  Planner setup.
/*!
 * Setup a sampling-based planner using OMPL.
*/
void OfflinePlannFramework::planWithSimpleSetup()
{
	//=======================================================================
	// Instantiate the state space
	//=======================================================================
	ob::StateSpacePtr space;
	if(state_space_type_.compare("Dubins")==0)
		space = ob::StateSpacePtr(new ob::DubinsStateSpace(3.0));
	else//R2
		space = ob::StateSpacePtr(new ob::RealVectorStateSpace(2));

	// Set the bounds for the state space
	ob::RealVectorBounds bounds(2);

	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	if(state_space_type_.compare("Dubins")==0)
		space->as<ob::SE2StateSpace>()->setBounds(bounds);
	else
		space->as<ob::RealVectorStateSpace>()->setBounds(bounds);
	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner;
	if(planner_name_.compare("RRT")==0)
		planner = ob::PlannerPtr(new og::RRT(si));
	if(planner_name_.compare("PRMstar")==0)
		planner = ob::PlannerPtr(new og::PRMstar(si));
	else if(planner_name_.compare("RRTstar")==0)
	{
		planner = ob::PlannerPtr(new og::RRTstar(si));
		//planner->as<og::RRTstar>()->setGoalBias(0.0);
		//planner->as<og::RRTstar>()->setPrune(true);
	}
	else
		planner = ob::PlannerPtr(new og::RRT(si));
	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);

	start[0] = double(start_state_[0]);
	start[1] = double(start_state_[1]);

	if(state_space_type_.compare("Dubins")==0)
		start[2] = double(start_state_[2]);
	// create a goal state
	ob::ScopedState<> goal(space);

	goal[0] = double(goal_state_[0]);
	goal[1] = double(goal_state_[1]);

	if(state_space_type_.compare("Dubins")==0)
		goal[2] = double(goal_state_[2]);
	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Set optimization objective
	//=======================================================================
	if(optimization_objective_.compare("PathLength")==0)//path length Objective
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));
	else if(optimization_objective_.compare("PathLengthClearance")==0)//balanced objective path length and clearance
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective1(si));
	else if(optimization_objective_.compare("RiskZones")==0)//approximate clearance (integral of clearance)
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getRiskZonesObjective(si, motion_cost_interpolation_));
	else if(optimization_objective_.compare("DirVectorsRisk")==0)//approximate risk (integral of risk)
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getDirVectorsRiskObjective(si));
	else
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));

	//path clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getClearanceObjective(si));

	//path length and clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective1(si));

	//path length and clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective2(si));
	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space

	//=======================================================================
	// Cleaning profile file
	//=======================================================================
	std::string jdhv_mp_pkg_path = ros::package::getPath("jdhv_mp");
	log_file_path_ = jdhv_mp_pkg_path + "/src/rice/logs/" + "offline" + scen_name_test_numb_ + "_" + optimization_objective_ + ".txt";
	log_file_.open(log_file_path_.c_str(), ios::out | ios::trunc );
	log_file_.close();

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OfflinePlannFramework::timerCallback, this);

	ros::spin();
}

//!  Periodic callback to solve the query.
/*!
 * Solve the query.
*/
void OfflinePlannFramework::timerCallback(const ros::TimerEvent &e)
{
	//=======================================================================
	// Set a modified sampler
	//=======================================================================
	if(use_new_sampler_)
		simple_setup_->getSpaceInformation()->getStateSpace()->setStateSamplerAllocator(std::bind(newAllocStateSampler, std::placeholders::_1, path_states_));

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	ob::StateValidityCheckerPtr om_stat_val_check;
	if(state_space_type_.compare("Dubins")==0)
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerSE2(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_));
	else
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerR2(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_));
	//om_stat_val_check = ob::StateValidityCheckerPtr(new OmStateValidityCheckerR2(simple_setup_->getSpaceInformation(), planning_depth_, 1.5, lazy_collision_eval_));
	simple_setup_->setStateValidityChecker(om_stat_val_check);

	//=======================================================================
	// Profiling performance
	//=======================================================================
	log_file_.open(log_file_path_.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

	//ompl::tools::Profiler::Clear();
	//ompl::tools::Profiler::Start();

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);

	//=======================================================================
	// Getting data from profiler
	//=======================================================================
	//ompl::tools::Profiler::Stop();
	//ompl::tools::Profiler::Status(log_file_);
	//ompl::tools::Profiler::Status();

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		// simple_setup_->simplifySolution();
		og::PathGeometric path = simple_setup_->getSolutionPath();
		ROS_INFO("%s: path with cost %f has been found with simple_setup", ros::this_node::getName().c_str(), path.cost(simple_setup_->getProblemDefinition()->getOptimizationObjective()).value());
		path.interpolate(int(path.length()/2.0));
		visualizeRRT(path);
		createMissionFile(path);

		if(use_new_sampler_)
		{
			std::vector<ob::State*> path_states;
			path_states = path.getStates();

			ob::StateSpacePtr space = simple_setup_->getStateSpace();
			path_states_.clear();
			path_states_.reserve(path_states.size()-1);
			//for (std::vector<ob::State *>::const_iterator st = path_states.begin(); st != path_states.end(); ++st)
			for (size_t i = path_states.size()-1; i > 0; i--)
			{
				ob::State *s = space->allocState();
				space->copyState(s, path_states[i]);
				path_states_.push_back(s);
			}
		}
		//=======================================================================
		// Clear previous solution path
		//=======================================================================
		simple_setup_->clear();
	}
	else
		ROS_INFO("%s: path has not been found", ros::this_node::getName().c_str());

	log_file_.close();
}

//!  Resulting path visualization.
/*!
 * Visualize resulting path.
*/
void OfflinePlannFramework::visualizeRRT(og::PathGeometric geopath)
{
	// %Tag(MARKER_INIT)%
	geometry_msgs::PoseArray pose_path;
	tf::Quaternion orien_quat;
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path, visual_result_path;
	pose_path.header.frame_id = visual_result_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	pose_path.header.stamp = visual_result_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	visual_result_path.ns = "online_planner_result_path";
	visual_result_path.action = result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;

	visual_result_path.pose.orientation.w = result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 2;
	visual_result_path.id = 3;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visual_result_path.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.5;
	result_path.scale.y = q_init_goal.scale.y = 0.5;
	result_path.scale.z = q_init_goal.scale.z = 0.5;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	visual_result_path.scale.x = 0.20;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	visual_result_path.color.r = 1.0;
	result_path.color.b = q_init_goal.color.g = 1.0;
	visual_result_path.color.a = result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.b = 1.0;
	visual_rrt.color.a = 1.0;

	const ob::SE2StateSpace::StateType *state_se2;
	const ob::RealVectorStateSpace::StateType *state_r2;

	geometry_msgs::Point p;

	ob::PlannerData planner_data(simple_setup_->getSpaceInformation());
	simple_setup_->getPlannerData(planner_data);

	std::vector< unsigned int > edgeList;
	int num_parents;
	//ROS_INFO("%s: number of states in the tree: %d", ros::this_node::getName().c_str(), planner_data.numVertices());
	log_file_ << "Number of tree nodes: " << planner_data.numVertices() << "\n";

	for (unsigned int i = 1 ; i < planner_data.numVertices() ; ++i)
	{
		if (planner_data.getVertex(i).getState() && planner_data.getIncomingEdges(i,edgeList) > 0)
		{
			if(state_space_type_.compare("Dubins")==0)
			{
				state_se2 = planner_data.getVertex(i).getState()->as<ob::SE2StateSpace::StateType>();
				p.x = state_se2->getX();
				p.y = state_se2->getY();
			}
			else
			{
				state_r2 = planner_data.getVertex(i).getState()->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
			}
			p.z = planning_depth_;
			visual_rrt.points.push_back(p);

			if(state_space_type_.compare("Dubins")==0)
			{
				state_se2 = planner_data.getVertex(edgeList[0]).getState()->as<ob::SE2StateSpace::StateType>();
				p.x = state_se2->getX();
				p.y = state_se2->getY();
			}
			else
			{
				state_r2 = planner_data.getVertex(edgeList[0]).getState()->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
			}
			p.z = planning_depth_;
			visual_rrt.points.push_back(p);
		}
	}
	std::vector< ob::State * > states = geopath.getStates();

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		if(state_space_type_.compare("Dubins")==0)
		{
			state_se2 = states[i]->as<ob::SE2StateSpace::StateType>();
			p.x = state_se2->getX();
			p.y = state_se2->getY();

			//--poses
			geometry_msgs::Pose pose;

			orien_quat.setRPY(0., 0., state_se2->getYaw());

			pose.position.x = p.x;
			pose.position.y = p.y;
			pose.position.z = p.z;
			pose.orientation.x = orien_quat.getX();
			pose.orientation.y = orien_quat.getY();
			pose.orientation.z = orien_quat.getZ();
			pose.orientation.w = orien_quat.getW();
			pose_path.poses.push_back(pose);

			result_path.points.push_back(p);
		}
		else
		{
			state_r2 = states[i]->as<ob::RealVectorStateSpace::StateType>();
			p.x = state_r2->values[0];
			p.y = state_r2->values[1];
		}
		p.z = planning_depth_;
		result_path.points.push_back(p);

		if(i>0)
		{
			if(!state_space_type_.compare("Dubins")==0)
			{
				visual_result_path.points.push_back(p);

				state_r2 = states[i-1]->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
				p.z = planning_depth_;
				visual_result_path.points.push_back(p);
			}
		}
	}

	//online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	online_traj_pub_.publish(visual_result_path);
	online_traj_pose_pub_.publish(pose_path);
	online_traj_pub_.publish(result_path);
}

//!  Mission file.
/*!
 * Create mission file from resulting path.
*/
void OfflinePlannFramework::createMissionFile(og::PathGeometric geopath)
{
	std::vector< ob::State * > states = geopath.getStates();
	const ob::SE2StateSpace::StateType *state_se2;
	const ob::RealVectorStateSpace::StateType *state_r2;

	std::string mission_north_coords = "trajectory/north: [";
	std::string mission_east_coords = "trajectory/east: [";
	std::string mission_depth_coords = "trajectory/z: [";
	std::string altitude_modes = "trajectory/altitude_mode: [";
	std::string wait_times = "trajectory/wait: [";

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		std::ostringstream north_coord;
		std::ostringstream east_coord;
		std::ostringstream depth_coord;

		// extract the component of the state and cast it to what we expect
		if(state_space_type_.compare("Dubins")==0)
		{
			state_se2 = states[i]->as<ob::SE2StateSpace::StateType>();
			north_coord << state_se2->getX();
			east_coord << state_se2->getY();
		}
		else
		{
			state_r2 = states[i]->as<ob::RealVectorStateSpace::StateType>();
			north_coord << state_r2->values[0];
			east_coord << state_r2->values[1];
		}
		depth_coord << planning_depth_;

		if(i==0)
		{
			mission_north_coords = mission_north_coords + north_coord.str() + ", ";
			mission_east_coords = mission_east_coords + east_coord.str() + ", ";
			mission_depth_coords = mission_depth_coords + "0.0, ";
			altitude_modes = altitude_modes + "False, ";
			wait_times = wait_times + "0.0, ";
		}

		if(i!=0)
		{
			mission_north_coords = mission_north_coords + ", ";
			mission_east_coords = mission_east_coords + ", ";
			mission_depth_coords = mission_depth_coords + ", ";
			altitude_modes = altitude_modes + ", ";
			wait_times = wait_times + ", ";
		}


		mission_north_coords = mission_north_coords + north_coord.str();
		mission_east_coords = mission_east_coords + east_coord.str();
		mission_depth_coords = mission_depth_coords + depth_coord.str();
		altitude_modes = altitude_modes + "False";
		wait_times = wait_times + "0.0";

		if(i==(geopath.getStateCount()-1))
		{
			mission_north_coords = mission_north_coords + ", " + north_coord.str() + "]\n";
			mission_east_coords = mission_east_coords + ", "+ east_coord.str() + "]\n";
			mission_depth_coords = mission_depth_coords + ", 0.0" + "]\n";
			altitude_modes = altitude_modes + ", False" + "]\n";
			wait_times = wait_times + ", 0.0" + "]\n";
		}
	}

	std::string mission_path = ros::package::getPath("jdhv_scenes");
	mission_path = mission_path + "/launch/config/missions/mission.yaml";

	ofstream myfile;
	myfile.open (mission_path.c_str());
	myfile << mission_north_coords;
	myfile << mission_east_coords;
	myfile << mission_depth_coords;
	myfile << altitude_modes;
	myfile << "trajectory/mode: 'sparus_los'\n";
	myfile << "trajectory/time_out: 10\n";
	myfile << wait_times;

	myfile.close();
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "new_offline_approach");

	ROS_INFO("%s: offline planner (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OfflinePlannFramework offline_framework;

	offline_framework.planWithSimpleSetup();

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		loop_rate.sleep();
	}
	return 0;
}
