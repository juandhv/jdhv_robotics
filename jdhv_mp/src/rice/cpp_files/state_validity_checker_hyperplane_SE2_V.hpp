/*
 * state_validity_checker_hyperplane_SE2_V.hpp
 *
 *  Created on: Apr 22, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (SE2 state) is collision-free.
 *  The workspace is represented by convex obstacles (hyperplanes).
 */

#ifndef OMPL_CONTRIB_STATE_VALIDITY_CHECKER_HYPERPLANE_SE2_V_
#define OMPL_CONTRIB_STATE_VALIDITY_CHECKER_HYPERPLANE_SE2_V_

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>
#include <string>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/tools/debug/Profiler.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
//Eigen
#include <Eigen/Dense>

//FCL
#include <fcl/shape/geometric_shapes.h>
#include <fcl/shape/geometric_shapes_utility.h>
#include <fcl/narrowphase/narrowphase.h>
#include <fcl/octree.h>
#include <fcl/traversal/traversal_node_octree.h>
#include <fcl/broadphase/broadphase.h>
#include <fcl/shape/geometric_shape_to_BVH_model.h>
#include <fcl/math/transform.h>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <fcl/distance.h>

#include <iostream>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;

//!  HyperplaneStateValidityCheckerSE2V class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap using FCL.
*/
class HyperplaneStateValidityCheckerSE2V : public ob::StateValidityChecker {

public:
	//! HyperplaneStateValidityCheckerSE2V constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	HyperplaneStateValidityCheckerSE2V(const ob::SpaceInformationPtr &si, const double planning_depth, const bool lazy_collision_eval, const double accep_prob);

	//! HyperplaneStateValidityCheckerSE2V destructor.
	/*!
	 * Destroy the octomap.
	 */
	~HyperplaneStateValidityCheckerSE2V();

	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision) using FCL
	 */
	virtual bool isValid(const ob::State *state) const;

private:
	//ROS
	ros::NodeHandle node_hand_;

	//Eigen for matrix and vector
	Eigen::MatrixXf A1_, A2_, A3_;
	Eigen::MatrixXf B1_, B2_, B3_;

	double planning_depth_, accep_prob_;
	bool lazy_collision_eval_;
};

#endif
