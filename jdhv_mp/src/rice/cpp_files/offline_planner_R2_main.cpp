/*
 * offline_planner_R2_main.cpp
 *
 *  Created on: Feb 18, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Offline path planning (without differential constraints) using RRT*.
 *  Octomaps are used to represent workspace and to validate configurations.
 */

#include <ompl/control/SpaceInformation.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/objectives/MaximizeMinClearanceObjective.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/config.h>
#include <iostream>

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

#include "state_validity_checker_octomap_fcl_R2.hpp"

//ROS
#include <ros/ros.h>

namespace ob = ompl::base;
namespace oc = ompl::control;

ob::OptimizationObjectivePtr getPathLengthObjective(const ob::SpaceInformationPtr& si);
//ob::OptimizationObjectivePtr getThresholdPathLengthObj(const ob::SpaceInformationPtr& si);
//ob::OptimizationObjectivePtr getClearanceObjective(const ob::SpaceInformationPtr& si);
//ob::OptimizationObjectivePtr getBalancedObjective1(const ob::SpaceInformationPtr& si);
//ob::OptimizationObjectivePtr getBalancedObjective2(const ob::SpaceInformationPtr& si);
//ob::OptimizationObjectivePtr getPathLengthObjWithCostToGo(const ob::SpaceInformationPtr& si);

//!  OfflinePlannerR2 class.
/*!
 * Offline Planner.
 * Setup a modified RRT for online computation of collision-free paths.
 * C-Space: R2
 * Workspace is represented with Octomaps
*/
class OfflinePlannerR2
{
    public:
		//! Constructor
		OfflinePlannerR2();
		void planWithSimpleSetup();
		void visualizeRRT(og::PathGeometric geopath);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		//OMPL, online planner
		og::SimpleSetupPtr simple_setup_;
		double planning_depth_, solving_time_, checking_side_length_;
		bool lazy_collision_eval_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
//		boost::shared_ptr<og::PathGeometric> geometric_path_result_;
		ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_;
		std::string planner_name_;
};

OfflinePlannerR2::OfflinePlannerR2()
{
	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(2);
	goal_state_.resize(2);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/offline_solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/checking_side_length", checking_side_length_, checking_side_length_);
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);
	node_handler_.param("planning_framework/planner_name", planner_name_, planner_name_);

	planWithSimpleSetup();

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		loop_rate.sleep();
	}
}

void OfflinePlannerR2::planWithSimpleSetup()
{
	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);

	//=======================================================================
	// Instantiate the state space (R2)
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(2));

	// Set the bounds for the state space
	ob::RealVectorBounds bounds(2);

	bounds.setLow(0, planning_bounds_x_[0]);
	bounds.setHigh(0, planning_bounds_x_[1]);
	bounds.setLow(1, planning_bounds_y_[0]);
	bounds.setHigh(1, planning_bounds_y_[1]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner;
	if(planner_name_.compare("RRT")==0)
		planner = ob::PlannerPtr(new og::RRT(si));
	else if(planner_name_.compare("RRTstar")==0)
		planner = ob::PlannerPtr(new og::RRTstar(si));
	else
		planner = ob::PlannerPtr(new og::RRT(si));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	OmFclStateValidityCheckerR2 * om_stat_val_check = new OmFclStateValidityCheckerR2(si, planning_depth_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_);
	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);

	start[0] = double(start_state_[0]);
	start[1] = double(start_state_[1]);

	// create a goal state
	ob::ScopedState<> goal(space);

	goal[0] = double(goal_state_[0]);
	goal[1] = double(goal_state_[1]);

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Set optimization objective
	//=======================================================================
	// Since we want to find an optimal plan, we need to define what
	// is optimal with an OptimizationObjective structure. Un-comment
	// exactly one of the following 6 lines to see some examples of
	// optimization objectives.
	// pdef->setOptimizationObjective(getPathLengthObjective(si));
	// pdef->setOptimizationObjective(getThresholdPathLengthObj(si));
	// pdef->setOptimizationObjective(getClearanceObjective(si));
	// pdef->setOptimizationObjective(getBalancedObjective1(si));
	// pdef->setOptimizationObjective(getBalancedObjective2(si));
	// pdef->setOptimizationObjective(getPathLengthObjWithCostToGo(si));
	simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));
	//getClearanceObjective

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space

	ob::PlannerStatus solved = simple_setup_->solve( solving_time_ );

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		//		simple_setup_->simplifySolution();
		og::PathGeometric path = simple_setup_->getSolutionPath();
		visualizeRRT(path);

		ROS_INFO("%s: path has been found with simple_setup", ros::this_node::getName().c_str());
	}
	else
		ROS_INFO("%s: path has not been found", ros::this_node::getName().c_str());
}

void OfflinePlannerR2::visualizeRRT(og::PathGeometric geopath)
{
	// %Tag(MARKER_INIT)%
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path, visual_result_path;
	visual_result_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	visual_result_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	visual_result_path.ns = "online_planner_result_path";
	visual_result_path.action = result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;

	visual_result_path.pose.orientation.w = result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 2;
	visual_result_path.id = 3;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visual_result_path.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.5;
	result_path.scale.y = q_init_goal.scale.y = 0.5;
	result_path.scale.z = q_init_goal.scale.z = 0.5;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	visual_result_path.scale.x = 0.20;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	visual_result_path.color.g = 1.0;
	result_path.color.g = q_init_goal.color.g = 1.0;
	visual_result_path.color.a = result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.b = 1.0;
	visual_rrt.color.a = 1.0;

	ob::PlannerData planner_data(simple_setup_->getSpaceInformation());
	simple_setup_->getPlannerData(planner_data);
	std::vector< unsigned int > edgeList;
	int num_parents;
	const ob::RealVectorStateSpace::StateType *state_r2;

	const ob::RealVectorStateSpace::StateType *state;
	geometry_msgs::Point p;

	for (unsigned int i = 1 ; i < planner_data.numVertices() ; ++i)
	{
		if (planner_data.getVertex(i).getState() && planner_data.getIncomingEdges(i,edgeList) > 0)
		{
			state_r2 = planner_data.getVertex(i).getState()->as<ob::RealVectorStateSpace::StateType>();
			p.x = state_r2->values[0];
			p.y = state_r2->values[1];
			p.z = planning_depth_ + 0.1;
			visual_rrt.points.push_back(p);

			state_r2 = planner_data.getVertex(edgeList[0]).getState()->as<ob::RealVectorStateSpace::StateType>();
			p.x = state_r2->values[0];
			p.y = state_r2->values[1];
			p.z = planning_depth_ + 0.1;
			visual_rrt.points.push_back(p);
		}
	}
	std::vector< ob::State * > states = geopath.getStates();

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		state = states[i]->as<ob::RealVectorStateSpace::StateType>();

		p.x = state->values[0];
		p.y = state->values[1];
		p.z = planning_depth_;//pos->values[2];

		result_path.points.push_back(p);

		if(i>0)
		{
			visual_result_path.points.push_back(p);
			state = states[i-1]->as<ob::RealVectorStateSpace::StateType>();

			p.x = state->values[0];
			p.y = state->values[1];
			p.z = planning_depth_;
			visual_result_path.points.push_back(p);
		}
	}

	//online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	online_traj_pub_.publish(visual_result_path);
	//online_traj_pub_.publish(result_path);
}

ob::OptimizationObjectivePtr getPathLengthObjective(const ob::SpaceInformationPtr& si)
{
	return ob::OptimizationObjectivePtr(new ob::PathLengthOptimizationObjective(si));
}

//ob::OptimizationObjectivePtr getClearanceObjective(const ob::SpaceInformationPtr& si)
//{
//	return ob::OptimizationObjectivePtr(new ob::ClearanceObjective(si));
//}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "offline_planner_R2");

	ROS_INFO("%s: offline planner without kinematic constraints (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OfflinePlannerR2 offline_planner;

	return 0;
}
