/*
 * new_offline_uncertainty_approach_main.cpp
 *
 *  Created on: April 3, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  New approach for Offline path planning with uncertainty.
 */

#include <ompl/control/SpaceInformation.h>
#include <ompl/control/planners/PlannerIncludes.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/objectives/MaximizeMinClearanceObjective.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/config.h>

#include <iostream>
#include <vector>

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

#include "state_validity_checker_octomap_fcl_SE2_V.hpp"
#include "state_validity_checker_hyperplane_SE2_V.hpp"
#include "new_state_sampler.h"
#include "state_cost_objective.hpp"
#include "motion_model_auv.hpp"
#include "mod_kin_dyn_rrt.h"

//ROS
#include <ros/ros.h>

//
#include <boost/bind.hpp>

namespace ob = ompl::base;
namespace oc = ompl::control;

class MyArbitraryGoalState : public ob::GoalState
{
public:
	MyArbitraryGoalState(const ob::SpaceInformationPtr &si) : ob::GoalState(si)
    {
    }
    virtual double distanceGoal (const ob::State *st) const
    {
    	ob::SE2StateSpace SE2;
    	const ob::SE2StateSpace::StateType *st_se2;
    	const ob::SE2StateSpace::StateType *state_se2;

    	st_se2 = st->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
    	state_se2 = state_->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);

    	double dist = SE2.distance(st_se2, state_se2);

//    	std::cout << "state_se2: " << state_se2->getX() << ", " << state_se2->getY() << ", " << state_se2->getYaw() << std::endl;
//    	std::cout << "st_se2: " << st_se2->getX() << ", " << st_se2->getY() << ", " << st_se2->getYaw() << std::endl;
//    	std::cout << "dist: " << dist << std::endl;

    	return dist;
    }
};

ob::StateSpacePtr constructStateSpace(void)
{
	ob::StateSpacePtr state_space = ob::StateSpacePtr(new ob::CompoundStateSpace());
	//SE2 component (x, y, yaw)
	state_space->as<ob::CompoundStateSpace>()->addSubspace(ob::StateSpacePtr(new ob::SE2StateSpace()), 1.);
	//linear velocity, now included in the state vector
	state_space->as<ob::CompoundStateSpace>()->addSubspace(ob::StateSpacePtr(new ob::RealVectorStateSpace(1)), 0.0);
	//Covariance
	state_space->as<ob::CompoundStateSpace>()->addSubspace(ob::StateSpacePtr(new ob::RealVectorStateSpace(16)), 0.0);
	state_space->as<ob::CompoundStateSpace>()->lock();
	return state_space;
}

//!  OfflinePlannerUncertainty class.
/*!
 * Offline Planner.
 * Setup an RRT* for offline computation of collision-free paths.
 * C-Space: R2 or SE2
 * Workspace is represented with Octomaps
*/
class OfflinePlannerUncertainty
{
    public:
		//! Constructor
		OfflinePlannerUncertainty();
		void planWithSimpleSetup();
		void timerCallback(const ros::TimerEvent &e);
		void visualizeRRT(oc::PathControl geopath);
    private:
    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;

		//OMPL, online planner
		oc::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_, min_control_duration_, max_control_duration_, accep_prob_;
		bool lazy_collision_eval_, use_new_sampler_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, start_state_, goal_state_;
		ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_;
		std::string planner_name_, state_space_type_;
		std::vector<const ob::State *> path_states_;
		std::vector<const oc::Control *> path_controls_;
};

OfflinePlannerUncertainty::OfflinePlannerUncertainty()
{
	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	start_state_.resize(3);
	goal_state_.resize(3);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/timer_period", timer_period_, timer_period_);
	node_handler_.param("planning_framework/offline_solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);
	node_handler_.param("planning_framework/planner_name", planner_name_, planner_name_);
	node_handler_.param("planning_framework/use_new_sampler", use_new_sampler_, use_new_sampler_);
	node_handler_.param("planning_framework/state_space_type", state_space_type_, state_space_type_);
	node_handler_.param("planning_framework/min_control_duration", min_control_duration_, min_control_duration_);
	node_handler_.param("planning_framework/max_control_duration", max_control_duration_, max_control_duration_);
	node_handler_.param("planning_framework/accep_prob", accep_prob_, accep_prob_);

	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);
}

void OfflinePlannerUncertainty::planWithSimpleSetup()
{
	//=======================================================================
	// Instantiate the state space (SE2)
	//=======================================================================
	ob::StateSpacePtr space(constructStateSpace());

	// set the bounds for the R^2 part of SE(2)
	ob::RealVectorBounds bounds_se2(2);

	bounds_se2.setLow(0, planning_bounds_x_[0]);
	bounds_se2.setHigh(0, planning_bounds_x_[1]);
	bounds_se2.setLow(1, planning_bounds_y_[0]);
	bounds_se2.setHigh(1, planning_bounds_y_[1]);
	space->as<ob::CompoundStateSpace>()->as<ob::SE2StateSpace>(0)->setBounds(bounds_se2);

	// set the bounds for the linear velocity (part of the state space
	ob::RealVectorBounds bounds_linear_vel(1);

	bounds_linear_vel.setLow(0, 0.1);
	bounds_linear_vel.setHigh(0, 0.5);
	space->as<ob::CompoundStateSpace>()->as<ob::RealVectorStateSpace>(1)->setBounds(bounds_linear_vel);

	// set the bounds for the covariance
	ob::RealVectorBounds bounds_covariance(16);
	bounds_covariance.setLow(-0.1);
	bounds_covariance.setHigh(1.0);
	space->as<ob::CompoundStateSpace>()->as<ob::RealVectorStateSpace>(2)->setBounds(bounds_covariance);

	//=======================================================================
	// Instantiate the control space
	//=======================================================================
	oc::ControlSpacePtr cspace(new DynUnicycleControlSpace(space));

	// set the bounds for the control space
	ob::RealVectorBounds cbounds(2);
	//turning rate
	cbounds.setLow(0,-0.2);
	cbounds.setHigh(0,0.2);
	//linear acceleration
	cbounds.setLow(1,-0.05);
	cbounds.setHigh(1,0.05);

	cspace->as<DynUnicycleControlSpace>()->setBounds(cbounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = oc::SimpleSetupPtr( new oc::SimpleSetup(cspace) );
	oc::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	// Set minimum and maximum duration of control action
	//si->setMinMaxControlDuration(min_control_duration_, max_control_duration_);
	si->setMinMaxControlDuration(100, 5000);
	si->setPropagationStepSize(0.01);

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner;
	if(planner_name_.compare("RRT")==0)
		planner = ob::PlannerPtr(new oc::RRT(si));
	if(planner_name_.compare("PRMstar")==0)
		planner = ob::PlannerPtr(new oc::RRT(si));
	else if(planner_name_.compare("RRTstar")==0)
	{
		planner = ob::PlannerPtr(new oc::ModKinDynRRT(si));
		//planner = ob::PlannerPtr(new og::ModRRTstar(si));
		planner->as<oc::ModKinDynRRT>()->setGoalBias(0.0);
		//planner->as<og::RRTstar>()->setPrune(true);
	}
	else
		planner = ob::PlannerPtr(new oc::RRT(si));
	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	// create a start state
	ob::ScopedState<> start(space);

	start[0] = double(start_state_[0]);
	start[1] = double(start_state_[1]);
	start[2] = double(start_state_[2]);
	start[3] = 0.3;//linear velocity
	for(int i=0; i<16 ; i++)
		start[i+4] = 0.0;
	start[4] = 0.0;
	start[9] = 0.0;
	start[14] = 0.0;
	start[19] = 0.0;
	// create a goal state
	ob::ScopedState<> goal(space);

	goal[0] = double(goal_state_[0]);
	goal[1] = double(goal_state_[1]);
	goal[2] = double(goal_state_[2]);
	goal[3] = 0.3;
	for(int i=0; i<16 ; i++)
		goal[i+4] = 0.0;

	//	ob::State *state1 = space->allocState();
	//	ob::State *state2 = space->allocState();
	//
	//	state1->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setXY(3.0, 2.0);
	//	state1->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setYaw(0.2);
	//	state1->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0] = 0.0;
	//	for(int i=0; i<16 ; i++)
	//		state1->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[i] = 0.0;
	//
	//	state2->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setXY(2.0, 1.0);
	//	state2->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setYaw(0.3);
	//	state2->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0] = 0.0;
	//	for(int i=0; i<16 ; i++)
	//		state2->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[i] = 5000.0;
	//
	//	std::cout << "state1: " << state1 << std::endl;
	//	std::cout << "state2: " << state2 << std::endl;
	//	std::cout << "distance: " << si->distance(state1, state2) << std::endl;
	//	exit(0);

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal, 5.0);

	//MyArbitraryGoalState *gs = new MyArbitraryGoalState(si);
	//gs->setState(goal);
	//gs->setThreshold(5.0);
	//simple_setup_->setGoal(ob::GoalPtr(gs));

	//=======================================================================
	// set the propagation routine for this space
	//=======================================================================
	//simple_setup_->setStatePropagator(oc::StatePropagatorPtr(new KinAUVStatePropagator(simple_setup_->getSpaceInformation())));
	//oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (simple_setup_->getSpaceInformation(), &DynUnicycleODE));
	//simple_setup_->setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &DynUnicyclePostIntegration));
	simple_setup_->setStatePropagator(std::bind(&propagateDynUnicycle, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
//
//	//=======================================================================
//	// Set optimization objective
//	//=======================================================================
//	//path length Objective
//	simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));
//
//	//path clearance Objective
//	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getClearanceObjective(si));
//
//	//path length and clearance Objective
//	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective1(si));
//
//	//path length and clearance Objective
//	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective2(si));
	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OfflinePlannerUncertainty::timerCallback, this);

	ros::spin();
}

void OfflinePlannerUncertainty::timerCallback(const ros::TimerEvent &e)
{
	//=======================================================================
	// Set a modified sampler
	//=======================================================================
	if(use_new_sampler_)
	{
		simple_setup_->getSpaceInformation()->getControlSpace()->setControlSamplerAllocator(std::bind(newAllocControlSampler, std::placeholders::_1, path_controls_));
		simple_setup_->getSpaceInformation()->getStateSpace()->setStateSamplerAllocator(std::bind(newAllocStateSampler, std::placeholders::_1, path_states_));
		simple_setup_->getSpaceInformation()->setDirectedControlSamplerAllocator(newAllocDirectedControlSampler);
	}

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	ob::StateValidityCheckerPtr om_stat_val_check;
	//om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerSE2V(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_));
	om_stat_val_check = ob::StateValidityCheckerPtr(new HyperplaneStateValidityCheckerSE2V(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, accep_prob_));
	simple_setup_->setStateValidityChecker(om_stat_val_check);

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	//ompl::tools::::Profiler::Clear();
	//ompl::tools::::Profiler::Start();

	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		//		simple_setup_->simplifySolution();
		oc::PathControl path_control = simple_setup_->getSolutionPath();
		//path_control.print(std::cout);

		og::PathGeometric path = path_control.asGeometric();
		ROS_INFO("%s: path with length %f has been found with simple_setup", ros::this_node::getName().c_str(), path.length()/*, path.cost(simple_setup_->getProblemDefinition()->getOptimizationObjective()).value()*/);
		//path.interpolate(int(path.length()/2.0));
		visualizeRRT(path_control);

		if(use_new_sampler_)
		{
			std::vector<ob::State*> path_control_states;
			path_control_states = path_control.getStates();
			std::vector<oc::Control*> path_control_controls;
			path_control_controls = path_control.getControls();

			ob::StateSpacePtr space = simple_setup_->getStateSpace();
			oc::ControlSpacePtr control_space = simple_setup_->getControlSpace();

			path_states_.clear();
			path_states_.reserve(path_control_states.size()-1);

			path_controls_.clear();
			path_controls_.reserve(path_control_controls.size());

			for (int i = path_control_controls.size()-1; i >= 0; i--)
			{
				oc::Control *c = control_space->allocControl();
				control_space->copyControl(c, path_control_controls[i]);
				path_controls_.push_back(c);
			}
			for (size_t i = path_control_states.size()-1; i > 0; i--)
			{
				ob::State *s = space->allocState();
				space->copyState(s, path_control_states[i]);
				path_states_.push_back(s);

				std::cout << "cov " <<std::endl;
				int cov_ind = 0;
				for(int i=0; i<4 ; i++)
				{
					for(int j=0; j<4 ; j++)
					{
						std::cout << s->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind] << ", ";
						cov_ind++;
					}
					std::cout << std::endl;
				}
			}

			double total_duration=0;
			for(int i=0 ;i<path_control_controls.size(); i++)
			{
				std::cout << "path_control.getControlDuration(): " << path_control.getControlDuration(i) << std::endl;
				total_duration = total_duration + path_control.getControlDuration(i);
			}
			std::cout << "total_duration: " << total_duration << std::endl;
			std::cout << "path_control_states.size(): " << path_control_states.size() << std::endl;
			std::cout << "path_control_controls.size(): " << path_control_controls.size() << std::endl;
		}
		//=======================================================================
		// Clear previous solution path
		//=======================================================================
		simple_setup_->clear();

		exit(0);
	}
	else
	{
		ROS_INFO("%s: path has not been found", ros::this_node::getName().c_str());
		oc::PathControl path_control(simple_setup_->getSpaceInformation());// = simple_setup_->getSolutionPath();
		visualizeRRT(path_control);
	}

	//oc::PathControl path_control(simple_setup_->getSpaceInformation());// = simple_setup_->getSolutionPath();
	//path_control.print(std::cout);

//	og::PathGeometric path = path_control.asGeometric();
//	ROS_INFO("%s: path with length %f has been found with simple_setup", ros::this_node::getName().c_str(), path.length()/*, path.cost(simple_setup_->getProblemDefinition()->getOptimizationObjective()).value()*/);
	//path.interpolate(int(path.length()/2.0));
	//visualizeRRT(path_control);

	//ompl::tools::::Profiler::Stop();
	//ompl::tools::::Profiler::Status();
}

void OfflinePlannerUncertainty::visualizeRRT(oc::PathControl geopath)
{
	// %Tag(MARKER_INIT)%
	geometry_msgs::PoseArray pose_path;
	tf::Quaternion orien_quat;
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path, visual_result_path;
	pose_path.header.frame_id = visual_result_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	pose_path.header.stamp = visual_result_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	visual_result_path.ns = "online_planner_result_path";
	visual_result_path.action = result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;

	visual_result_path.pose.orientation.w = result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 2;
	visual_result_path.id = 3;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visual_result_path.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.5;
	result_path.scale.y = q_init_goal.scale.y = 0.5;
	result_path.scale.z = q_init_goal.scale.z = 0.5;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	visual_result_path.scale.x = 0.20;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	visual_result_path.color.r = 1.0;
	result_path.color.g = q_init_goal.color.g = 1.0;
	visual_result_path.color.a = result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.b = 1.0;
	visual_rrt.color.a = 1.0;

	const ob::SE2StateSpace::StateType *state_se2;
	const ob::RealVectorStateSpace::StateType *state_r2;

	geometry_msgs::Point p;

	ob::PlannerData planner_data(simple_setup_->getSpaceInformation());
	simple_setup_->getPlannerData(planner_data);

	std::vector< unsigned int > edgeList;
	int num_parents;
	ROS_INFO("%s: number of states in the tree: %d", ros::this_node::getName().c_str(), planner_data.numVertices());
	for (unsigned int i = 1 ; i < planner_data.numVertices() ; ++i)
	{
		if (planner_data.getVertex(i).getState() && planner_data.getIncomingEdges(i,edgeList) > 0)
		{
			state_se2 = planner_data.getVertex(i).getState()->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
			p.x = state_se2->getX();
			p.y = state_se2->getY();
			p.z = planning_depth_;
			visual_rrt.points.push_back(p);

			state_se2 = planner_data.getVertex(edgeList[0]).getState()->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
			p.x = state_se2->getX();
			p.y = state_se2->getY();
			p.z = planning_depth_;
			visual_rrt.points.push_back(p);
		}
	}
	std::vector< ob::State * > states = geopath.getStates();

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect

		state_se2 = states[i]->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
		p.x = state_se2->getX();
		p.y = state_se2->getY();

		//--poses
		geometry_msgs::Pose pose;

		orien_quat.setRPY(0., 0., state_se2->getYaw());

		pose.position.x = p.x;
		pose.position.y = p.y;
		pose.position.z = planning_depth_;
		pose.orientation.x = orien_quat.getX();
		pose.orientation.y = orien_quat.getY();
		pose.orientation.z = orien_quat.getZ();
		pose.orientation.w = orien_quat.getW();
		pose_path.poses.push_back(pose);

		p.z = planning_depth_;
		result_path.points.push_back(p);

		if(i>0)
		{
			if(!state_space_type_.compare("Dubins")==0)
			{
				visual_result_path.points.push_back(p);

				state_r2 = states[i-1]->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
				p.z = planning_depth_;
				visual_result_path.points.push_back(p);
			}
		}
	}

	//online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	online_traj_pub_.publish(visual_result_path);
	online_traj_pose_pub_.publish(pose_path);
	//online_traj_pub_.publish(result_path);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "new_offline_uncertainty_approach");

	ROS_INFO("%s: online planner (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OfflinePlannerUncertainty offline_planner_uncertainty;

	offline_planner_uncertainty.planWithSimpleSetup();

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		loop_rate.sleep();
	}

	return 0;
}
