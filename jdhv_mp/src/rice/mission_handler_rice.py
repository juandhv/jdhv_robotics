#!/usr/bin/env python
"""
Created on June 8, 2015

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Mission handler
"""

# Standard Python imports
from math import *
import numpy as np
import actionlib
from collections import deque

# Debug control variables
DEBUG_OCTOMAP_VIS = False

# ROS imports
import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String, Bool
from std_srvs.srv import Empty, EmptyRequest
from visualization_msgs.msg import Marker
import tf

# cola2 and related imports
from cola2_lib import cola2_ros_lib
from cola2_msgs.msg import WorldWaypointReqGoal, WorldWaypointReqAction, WorldSectionReqGoal, WorldSectionReqAction
from cola2_msgs.srv import Submerge, SubmergeRequest

#auv_msgs
from auv_msgs.msg import NavSts, WorldWaypointReq, BodyVelocityReq, GoalDescriptor
from nav_msgs.msg import Odometry

# Octomap visualization
if DEBUG_OCTOMAP_VIS:
    import OctomapVisual

STATE_ZERO = 0
STATE_INIT = 1
STATE_TRAJ_INIT_ORI = 2
STATE_TRAJ_POS = 3
STATE_TRAJ_FINAL_ORI = 4

virtFactorPos = 5
virtFactorOri = 1 #2

class MissionHandler(object):
    """
    Mission Handler
    """

    def __init__(self, mission_depth=2.0):
        """
        Constructor
        """
        #=======================================================================
        # Initial values
        #=======================================================================
        self.mission_path_  = deque([])
        self.mission_depth_ = mission_depth
        
        #=======================================================================
        # Default waypoint tolerance values
        #=======================================================================
        self.wp_toler_pos_ = 1.2
        self.wp_toler_ori_ = radians(8.0)
        
        #=======================================================================
        # Services
        #=======================================================================
        try:
            rospy.wait_for_service('/cola2_control/submerge')
            self.srv_submerge_ = rospy.ServiceProxy('/cola2_control/submerge', Submerge)
            
            rospy.wait_for_service('/cola2_control/disable_goto')
            self.srv_disable_goto_ = rospy.ServiceProxy('/cola2_control/disable_goto', Empty) 
        except:
            print "Some services could not be found"

        #=======================================================================
        # Action lib params
        #=======================================================================
        self.wp_act_lib_conf_ = cola2_ros_lib.Config()
        self.wp_act_lib_conf_.trajectory_step = 0.25
        self.wp_act_lib_goal_id_ = 36000
        
        rospy.loginfo("%s: creating action client for waypoints", rospy.get_name())
        self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('world_waypoint_req', WorldWaypointReqAction)
        #self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('absolute_movement', WorldWaypointReqAction)
        rospy.logwarn("%s: waiting for action server for waypoints", rospy.get_name())
        self.wp_act_lib_action_client_.wait_for_server()
        rospy.loginfo("%s: ready to use action client for waypoints", rospy.get_name())
        
        rospy.loginfo("%s: creating action client for sections", rospy.get_name())
        self.sect_act_lib_action_client_ = actionlib.SimpleActionClient('world_section_req', WorldSectionReqAction)
        rospy.logwarn("%s: waiting for action server for sections", rospy.get_name())
        self.sect_act_lib_action_client_.wait_for_server()
        rospy.loginfo("%s: ready to use action client for sections", rospy.get_name())
        
        #=======================================================================
        # Subscribers 
        #=======================================================================
        #Navigation data (feedback)
        self.nav_sts_sub_ = rospy.Subscriber("/pose_ekf_slam/odometry", Odometry, self.navStsSubCallback, queue_size = 1)
        self.nav_sts_available_ = False
        
        self.online_traj_point_sub_ = rospy.Subscriber("/jdhv_robotics/online_traj_point", Point, self.onlineTrajPointsCallback, queue_size = 1)
        self.online_traj_sect_sub_ = rospy.Subscriber("/jdhv_robotics/online_traj_sect", WorldSectionReqGoal, self.onlineTrajSectionsCallback, queue_size = 1)
        
        self.cancel_goal_sub_ = rospy.Subscriber("/jdhv_robotics/cancel_wps", Bool, self.cancelWaypointsCallback, queue_size = 1)
        
        self.start_plann_sub_ = rospy.Subscriber("/jdhv_robotics/start_planner", Bool, self.startPlannSubCallback, queue_size = 1)
        self.mission_start_ = True
        
        self.start2goal_query_solved_sub_ = rospy.Subscriber("/jdhv_robotics/start2goal_query_solved", Bool, self.start2goalSolvedSubCallback, queue_size = 1)
        self.start2goal_query_solved_ = False
        
        #=======================================================================
        # Publishers
        #=======================================================================
        self.wp_need_flag_pub_ = rospy.Publisher("/jdhv_robotics/need_waypoints", Bool, queue_size = 2)
        self.stop_plann_pub_ = rospy.Publisher("/jdhv_robotics/stop_planner", Bool, queue_size = 2)

        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        rospy.logwarn("%s: waiting for navigation data", rospy.get_name())
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.nav_sts_available_:
            r.sleep()
        rospy.logwarn("%s: navigation data received", rospy.get_name())
        
        #=======================================================================
        # Waiting for mission start
        #=======================================================================
        rospy.logwarn("%s: waiting for mission start", rospy.get_name())
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.mission_start_:
            r.sleep()
        rospy.logwarn("%s: mission start received", rospy.get_name())

        #=======================================================================
        # Register handler to be called when rospy process begins shutdown.
        #=======================================================================
        rospy.on_shutdown(self.shutdownHook)
        
        return
    
    def submerge(self):
        """
        mission_handler submerges the AUV
        """
        submerge_req = SubmergeRequest()
        submerge_req.altitude_mode = False
        submerge_req.z = self.mission_depth_+1.0
        ret = self.srv_submerge_(submerge_req)
        while not rospy.is_shutdown() and not ret.attempted:
            rospy.sleep(1.0)
            ret = self.srv_submerge_(submerge_req)
        
        while not rospy.is_shutdown() and self.nav_sts_data_.pose.pose.position.z < self.mission_depth_:
            rospy.loginfo("%s: submerging to %f, current: %f", rospy.get_name(), self.mission_depth_, self.nav_sts_data_.pose.pose.position.z)
            rospy.sleep(1.0)
        self.srv_disable_goto_(EmptyRequest())
        self.wp_need_flag_pub_.publish(Bool(True))
    
    def run(self):
        """
        Run mission received from online planner
        """
        self.prev_waypoint_ = None
        r = rospy.Rate(10)
        while not rospy.is_shutdown() and not self.start2goal_query_solved_:
            if len(self.mission_path_)>0:
                waypoint = self.mission_path_.popleft()
                self.moveTo(waypoint[0], waypoint[1], waypoint[2], waypoint[3], True)
                if len(self.mission_path_)==0:
                    self.wp_need_flag_pub_.publish(Bool(True))
                
            r.sleep()

        return
    
    def __del__(self):
        """
        Destructor
        """
        return
    
    
    def navStsSubCallback(self, nav_sts_msg):
        """
        Callback to receive navSts message
        """
        self.nav_sts_data_ = nav_sts_msg
        if self.nav_sts_available_ == False:
            self.nav_sts_available_ = True
        
        return
    
    def startPlannSubCallback(self, start_plann_msg):
        self.mission_start_ = True
        return
    
    def onlineTrajPointsCallback(self, point_msg):
        """
        Callback to receive points of planned path 
        """
        rospy.loginfo("%s:\n waypoint received [%f, %f, %f]", rospy.get_name(), point_msg.x, point_msg.y, point_msg.z)
        self.mission_path_.append([point_msg.x, point_msg.y, point_msg.z, 0.0])
        self.wp_need_flag_pub_.publish(Bool(False))
        
        return
    
    def cancelWaypointsCallback(self, cancel_msg):
        rospy.loginfo("%s: planner requests to cancel waypoints", rospy.get_name())
        self.wp_act_lib_action_client_.cancel_all_goals()
        self.sect_act_lib_action_client_.cancel_all_goals()
        self.mission_path_  = deque([])
        #self.wp_need_flag_pub_.publish(Bool(False))
        
        return
    
    def start2goalSolvedSubCallback(self, solved_msg):
        self.stop_plann_pub_.publish(Bool(True))
        self.start2goal_query_solved_ = True
        self.shutdownHook()
        
        return
    
    def moveTo(self, x, y, z, yaw=0.0, los=True):
        """
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
        moveTo: move vehicle to waypoint using COLA2 API (based on Enric's code)
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        """
        rospy.loginfo("%s:\n moveTo(x=%f, y=%f, z=%f, yaw=%f)", rospy.get_name(), x, y, z, yaw)
        
#         # Create a goal to send to the action server
#         goal = WorldWaypointReqGoal()
#         goal.goal.requester = rospy.get_name() + '_virtTargetControl'
#         goal.goal.id = self.wp_act_lib_goal_id_
#         self.wp_act_lib_goal_id_ = self.wp_act_lib_goal_id_ + 1
#         goal.goal.priority = 10
#           
#         goal.mode = ''
#         goal.timeout = 180 # 3 minutes        
#           
#         goal.altitude_mode = False
#   
#         # Goal pose
#         goal.position.north = x
#         goal.position.east = y
#         goal.position.depth = z
#   
#         goal.orientation.roll = 0
#         goal.orientation.pitch = 0
#         goal.orientation.yaw = yaw
#   
#         if los:
#             goal.mode = 'sparus_los'
#             # Set movement type (X-Z-Yaw) using los
#             goal.disable_axis.x = False
#             goal.disable_axis.y = True
#             goal.disable_axis.z = False
#             goal.disable_axis.roll = True
#             goal.disable_axis.pitch = True
#             goal.disable_axis.yaw = False
#         else:
#             # Only Z
#             goal.disable_axis.x = True
#             goal.disable_axis.y = True
#             goal.disable_axis.z = False
#             goal.disable_axis.roll = True
#             goal.disable_axis.pitch = True
#             goal.disable_axis.yaw = True
#   
#         # Set error tolerances
#         goal.position_tolerance.x = self.wp_toler_pos_
#         goal.position_tolerance.y = self.wp_toler_pos_
#         goal.position_tolerance.z = self.wp_toler_pos_
#         goal.orientation_tolerance.roll = self.wp_toler_ori_
#         goal.orientation_tolerance.pitch = self.wp_toler_ori_
#         goal.orientation_tolerance.yaw = self.wp_toler_ori_
#   
#         self.wp_act_lib_action_client_.send_goal(goal)
#         self.wp_act_lib_action_client_.wait_for_result()
#         result = self.wp_act_lib_action_client_.get_result()
        
        #-----
        section_msg = WorldSectionReqGoal()
          
        section_msg.priority = 10
        section_msg.controller_type = WorldSectionReqGoal.LOSCTE
        section_msg.disable_z = False
  
        section_msg.tolerance.x = 1.5
        section_msg.tolerance.y = 1.5
        section_msg.tolerance.z = 1.0
  
        section_msg.initial_surge = 0.3
        #section.use_initial_yaw = true;
        section_msg.final_surge = 0.3
        section_msg.use_final_yaw = True
        section_msg.final_position.z   = z
        #section.timeout = 10.0;
  
        if self.prev_waypoint_ is None:
            section_msg.initial_position.x = self.nav_sts_data_.pose.pose.position.x
            section_msg.initial_position.y = self.nav_sts_data_.pose.pose.position.y
            section_msg.initial_position.z = self.nav_sts_data_.pose.pose.position.z
        else:
            section_msg.initial_position.x = self.prev_waypoint_[0]
            section_msg.initial_position.y = self.prev_waypoint_[1]
            section_msg.initial_position.z = self.prev_waypoint_[2]
            
        section_msg.initial_yaw = yaw # TO-DO
        section_msg.final_position.x = x
        section_msg.final_position.y = y
        section_msg.final_yaw = yaw
  
        self.sect_act_lib_action_client_.send_goal(section_msg)
        self.sect_act_lib_action_client_.wait_for_result()
        result = self.sect_act_lib_action_client_.get_result()
        
    def followSection(self, section_msg):
        """
        Follow a section (new controller)    
        """
        rospy.loginfo("%s:\n followSection(section = [%f, %f, %f, %f] to [%f, %f, %f, %f])", rospy.get_name(), section_msg.initial_position.x, section_msg.initial_position.y, section_msg.initial_position.z, section_msg.initial_yaw, section_msg.final_position.x, section_msg.final_position.y, section_msg.final_position.z, section_msg.final_yaw)

        self.sect_act_lib_action_client_.send_goal(section_msg)
        self.sect_act_lib_action_client_.wait_for_result()
        result = self.sect_act_lib_action_client_.get_result()
        
    def shutdownHook(self):
        """
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        shutdownHook: gets called on shutdown and cancels all ongoing pilot goals (Enric)
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        """
        rospy.loginfo("%s: shutting down node", rospy.get_name())
        self.wp_act_lib_action_client_.cancel_all_goals()
#         rospy.signal_shutdown("manual shutdown")
    
if __name__ == '__main__':
    try:
        rospy.init_node('mission_handler15', log_level=rospy.INFO) #log_level=rospy.DEBUG
        rospy.loginfo("%s: Mission Handler RICE-2015", rospy.get_name())
        
        #=======================================================================
        # Init: create mission_handl
        #=======================================================================
        mission_handl = MissionHandler(1.1)
        mission_handl.submerge()
        mission_handl.run()
        
        #rospy.spin()
            
    except rospy.ROSInterruptException:
        pass