"""
Created on July 14, 2015

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Generate charts using Matplotlib
"""

#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
from pylab import figure, show
from IPython.external.qt_for_kernel import matplotlib
import os

def compareRiskFunctions():
    N = 2
    
    ind = np.arange(N)  # the x locations for the groups
    width = 0.2       # the width of the bars
    
    fig, ax = plt.subplots()
    path_length_means = (0.05, 0.17)
    rects1 = ax.bar(ind, path_length_means, width, color='r')
    
    risk_vectors_means = (0.52, 1.64)
    rects2 = ax.bar(ind+width, risk_vectors_means, width, color='b')
    
    risk_zones_means = (1.42, 4.56)
    rects3 = ax.bar(ind+2*width, risk_zones_means, width, color='g')
    
    path_length_clearance_means = (9.87, 32.4)
    rects4 = ax.bar(ind+3*width, path_length_clearance_means, width, color='y')
    
    # add some text for labels, title and axes ticks
    ax.set_ylabel('Computation time [s]')
    ax.set_title('Computation time by task and optimization metric')
    ax.set_xticks(ind+width)
    ax.set_xticklabels( ('Task1', 'Task2') )
    ax.grid()
    plt.ylim(0, 35)
    
    ax.legend( (rects1[0], rects2[0], rects3[0], rects4[0]), ('Path Length', 'Direction Vectors Risk', 'Risk Zones', 'Path Length + Clearance'), loc='upper left' )
    
    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.2f'%height,
                    ha='center', va='bottom')
    
    autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)
    autolabel(rects4)
    
    plt.show()
    return

def compareLazyRiskCheck(file1_name="onlineblocks_t2_RiskZones_LazyTrue_Def.txt", file2_name="onlineblocks_t2_RiskZones_LazyFalse_Def.txt"):
    file1_path = "../logs/" + file1_name
    file1 = open(file1_path,"r")
    t1 = []
    comp_times1 = []
    comp_times1_perc = []
    nums_nodes1 = []
    for line in file1:
        if "RiskZones"  in line:
            comp_time1_string = line[16:-1].split(' ')[1]
            comp_time1_string = comp_time1_string[0:-1]
            comp_time1 = float(comp_time1_string)
            comp_times1.append(comp_time1)
            
            comp_time1_perc_string = line[16:-1].split(' ')[2]
            comp_time1_perc_string = comp_time1_perc_string[1:-3]
            comp_time1_perc = float(comp_time1_perc_string)
            comp_times1_perc.append(comp_time1_perc)
        elif "*** Profiling statistics. Total counted time : "  in line:
            inc_time1_string = line[47:-1].split(' ')[1]
            inc_time1 = float(inc_time1_string)
            
            if len(t1) == 0:
                t1.append(1.0)
            else:
                t1.append(t1[-1]+inc_time1)
                
        elif "Number of tree nodes:"  in line:
            nums_nodes1.append(int(line[22:-1]))
                
    file2_path = "../logs/" + file2_name
    file2 = open(file2_path,"r")
    t2 = []
    comp_times2 = []
    comp_times2_perc = []
    nums_nodes2 = []
    for line in file2:
        if "RiskZones"  in line:
            comp_time2_string = line[16:-1].split(' ')[1]
            comp_time2_string = comp_time2_string[0:-1]
            comp_time2 = float(comp_time2_string)
            comp_times2.append(comp_time2)
            
            comp_time2_perc_string = line[16:-1].split(' ')[2]
            comp_time2_perc_string = comp_time2_perc_string[1:-3]
            comp_time2_perc = float(comp_time2_perc_string)
            comp_times2_perc.append(comp_time2_perc)
        elif "*** Profiling statistics. Total counted time : "  in line:
            inc_time2_string = line[47:-1].split(' ')[1]
            inc_time2 = float(inc_time2_string)
            
            if len(t2) == 0:
                t2.append(1.0)
            else:
                t2.append(t2[-1]+inc_time2)
                
        elif "Number of tree nodes:"  in line:
            nums_nodes2.append(int(line[22:-1]))
    
    matplotlib.rcParams.update({'font.size':16})
    fig = figure()
    ax = fig.add_subplot(111)
    ax.plot(t1, comp_times1, '.')
    ax.plot(t2, comp_times2, 'r*')
    ax.grid()
    ax.set_xlabel('Mission time [s]', fontsize=18)
    ax.set_ylabel('Computation time [s]', fontsize=18)
    #ax.set_title('Risk Zones Computation Time each Period of Time', fontsize=18)
    plt.xlim(0, 280)
    plt.ylim(0, 2)
    ax.legend( ('Opportunistic Risk Check', 'No Opportunistic Risk Check'), loc='lower right' )
    
    fig2 = figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(t1, comp_times1_perc, '.')
    ax2.plot(t2, comp_times2_perc, 'r*')
    ax2.grid()
    ax2.set_xlabel('Mission time [s]', fontsize=18)
    ax2.set_ylabel('Percentage of Computation time [%]', fontsize=18)
    #ax2.set_title('Risk Zones Computation Time Percentage each Period of Time', fontsize=18)
    plt.xlim(0, 280)
    plt.ylim(0, 100)
    ax2.legend( ('Opportunistic Risk Check', 'No Opportunistic Risk Check'), loc='lower right' )
    
    fig3 = figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(t1, nums_nodes1, '.')
    ax3.plot(t2, nums_nodes2, 'r*')
    ax3.grid()
    ax3.set_xlabel('Mission time [s]', fontsize=18)
    ax3.set_ylabel('Number of nodes (states)', fontsize=18)
    #ax3.set_title('Number of Tree Nodes each Period of Time', fontsize=18)
    plt.xlim(0, 280)
    plt.ylim(0, 500)
    ax3.legend( ('Opportunistic Risk Check', 'No Opportunistic Risk Check'), loc='upper right' )
    
    plt.show()
    return

if __name__ == '__main__':
    #compareRiskFunctions()
    #compareLazyRiskCheck()
    path_videos = "/home/juandhv/Dropbox/PhD/InfoPapers/ICRA16-Rice/Video-ICRA16/"
    lst_videos = os.listdir(path_videos)
    print lst_videos
    for video_name in lst_videos:
        if ".ogv" in video_name:
            #print video_name[:-4]
            command_str = "~/Desktop/ffmpeg-2.6.2/ffmpeg -i " + path_videos + video_name + " -codec:v mpeg4 -q:v 0 -strict -2 " + path_videos + video_name[:-4] + ".mp4"
            print "start processing"
            os.system(command_str)
            print command_str
            print "stop processing"

    pass