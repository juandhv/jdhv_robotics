"""
Created on July 14, 2015

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Purpose: Generate charts using Matplotlib
"""

#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    
    u0 = 0.3
    u1 = 0.2
    
    x1 = 0.0
    x2 = 0.0
    x3 = 0.0
    
    x1_vec = []
    x2_vec = []
    x3_vec = []
    
    for t in np.arange(0.0, 30.0, 0.1):
        xd1 = u0 * np.cos(x3)
        xd2 = u0 * np.sin(x3)
        xd3 = u1
        
        x1 = x1 + 0.1*xd1
        x2 = x2 + 0.1*xd2
        x3 = x3 + 0.1*xd3
        
        x1_vec.append(x1)
        x2_vec.append(x2)
        x3_vec.append(x3)
    
    fig = plt.figure()
    #ax1 = fig.add_axes([0.1, 0.1, 0.4, 0.7])
#     ax2 = fig.add_axes([0.55, 0.1, 0.4, 0.7])
    
    x = np.arange(0.0, 2.0, 0.02)
    y1 = np.sin(2*np.pi*x)
    y2 = np.exp(-x)
    l1 = plt.plot(x1_vec, x2_vec, 'rs-')
    
#     y3 = np.sin(4*np.pi*x)
#     y4 = np.exp(-2*x)
#     l3, l4 = ax2.plot(x, y3, 'yd-', x, y4, 'k^')
#     
#     fig.legend((l1, l2), ('Line 1', 'Line 2'), 'upper left')
#     fig.legend((l3, l4), ('Line 3', 'Line 4'), 'upper right')
    plt.show()
    
    print "x1_vec: ", x1_vec

    pass