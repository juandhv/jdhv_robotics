/*
 * icra14_offline_planner_SE3.h
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>

#include <signal.h>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include "icra14_planners_headers.hpp"

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;


//!  OmStateValidityChecker class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap.
*/
class OmStateValidityChecker : public ob::StateValidityChecker {

public:
	//! OmStateValidityChecker constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	OmStateValidityChecker(const ob::SpaceInformationPtr &si);
	//! OmStateValidityChecker destructor.
	/*!
	 * Destroy the octomap.
	 */
	~OmStateValidityChecker();
	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision)
	 */
	virtual bool isValid(const ob::State *state) const;

	virtual double cost(const ob::State *state) const;

private:
	//ROS
	ros::NodeHandle node_hand_;

	// ROS tf
	tf::Pose last_pose_;

	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;
	double octree_res_;

	//Girona500 info
	double length_g500_;
};

//!  OmOfflinePlanner class.
/*!
 * Octomap Offline Planner.
 * Plan and visualize a trajectory given an initial and a final configuration. The map is assume as known and is represented
 * by an octomap.
*/
class OmOfflinePlanner {

public:
	//! Constructor.
	OmOfflinePlanner(std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);
	//! Path Planning using OMPL.
	/*!
	 * Path Planning using OMPL and OmStateValidityChecker (over octomaps).
	 * Based on the example of OMPL
	 */
	void planTheHardWay(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);

	void planWithSimpleSetup(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos);


	//! Draw the path.
	/*!
	 * Publish the path as markers to be visualized in rviz
	 */
	void publishPathMarkers();

	//! Select the given planner.
	ob::PlannerPtr selectPlanner(std::string planner_name,ob::SpaceInformationPtr si);
private:
	//ROS
	ros::NodeHandle node_hand_;
	ros::Publisher path_marker_pub_;

	//OMPL
	ob::PathPtr path_result_;
	boost::shared_ptr<og::PathGeometric> geometric_path_result_;
	og::SimpleSetupPtr simple_setup_;

	std::string planner_name_;
	bool simple_setup_flag_;
};

void stopNode(int sig);
