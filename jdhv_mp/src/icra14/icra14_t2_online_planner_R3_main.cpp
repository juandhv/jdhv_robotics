/*
 * icra14_t2_online_planner_R3_main.cpp
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

#include "icra14_state_validity_checker.hpp"
#include "icra14_online_map_plan_R3.hpp"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "offline_planner_ompl_R3");
	ROS_INFO_STREAM("Starting offline_planner_ompl_R3 Node");

	// Constructor
	//(RRT, RRTstar, RRTConnect, BallTreeRRTstar, LazyRRT, pRRT, TRRT), (EST), (BKPIECE1, KPIECE1, LBKPIECE1), (PDST), (PRM, LazyPRM, PRMstar, SPARS, SPARStwo), (SBL, pSBL)
	OmOnlinePlanner onPlanner("RRTstar");

	// Spin
	ros::spin();

	// Exit main function without errors
	return 0;
}
