/*
 * icra14_online_map_plan_R3.cpp
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

#include "icra14_state_validity_checker.hpp"
#include "icra14_online_map_plan_R3.hpp"

OmOnlinePlanner::OmOnlinePlanner(std::string planner_name):
	node_hand_()
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	//=======================================================================
	// Get paramaters
	//=======================================================================
	node_hand_.getParam("planner_name", planner_name_);
	node_hand_.getParam("simple_setup_flag", simple_setup_flag_);
	node_hand_.getParam("exploration_altitute", exploration_altitue_);
	node_hand_.getParam("laser_octomap_flag", laser_octomap_flag_);
	node_hand_.getParam("rosbags_flag", rosbags_flag_);
	if(rosbags_flag_)
		ros::Duration(350.0).sleep();//Waiting for octomap_visualization
	else
		ros::Duration(20).sleep();//Waiting for octomap_visualization

	//=======================================================================
	// Getting planner information
	//=======================================================================
	planning_bounds_ = std::vector<double> (6);

	//=======================================================================
	// Subscribers
	//=======================================================================
	//Navigation data (feedback)
	nav_sts_sub_ = node_hand_.subscribe("/cola2_navigation/nav_sts", 1, &OmOnlinePlanner::navStsSubCallback, this);
	nav_sts_data_pos_ = std::vector<double> (3);
	nav_sts_data_ori_ = std::vector<double> (3);
	nav_sts_available_ = false;
	mission_flag_available_ = true;

	//Mission Flag (feedback)
	mission_flag_sub_ = node_hand_.subscribe("/jdhv_robotics/mission_flag", 1, &OmOnlinePlanner::missionFlagSubCallback, this);

	//=======================================================================
	// Publishers
	//=======================================================================
	path_marker_pub_ = node_hand_.advertise<visualization_msgs::Marker>("/jdhv_robotics/planner_result", 10);

	//=======================================================================
	// Waiting for nav sts
	//=======================================================================
	ros::Rate loop_rate(10);
	while (!nav_sts_available_)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}

	//=======================================================================
	// Waiting for mission flag
	//=======================================================================
	if(!rosbags_flag_)
	{
		while (!mission_flag_available_)
		{
			ros::spinOnce();
			loop_rate.sleep();
		}
	}

	last_nav_sts_data_pos_ =  std::vector<double>(nav_sts_data_pos_);
	ROS_INFO("*** First position obtained ***");

	//=======================================================================
	// Timer for planning
	//=======================================================================
	plannTimer_ = node_hand_.createTimer(ros::Duration(30.0), &OmOnlinePlanner::timerCallback, this);
}

void OmOnlinePlanner::timerCallback(const ros::TimerEvent &e)
{
	ROS_INFO("*** Planning timer invoking its callback ***");
	//=======================================================================
	// Planning online
	//=======================================================================
	current_nav_sts_data_pos_ =  std::vector<double>(nav_sts_data_pos_);
	
	if(simple_setup_flag_)
		planWithSimpleSetup(planner_name_,last_nav_sts_data_pos_,current_nav_sts_data_pos_);
	else
		planTheHardWay(planner_name_,last_nav_sts_data_pos_,current_nav_sts_data_pos_);
	
	last_nav_sts_data_pos_ =  std::vector<double>(current_nav_sts_data_pos_);

	publishPathMarkers();
}

void OmOnlinePlanner::navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg)
{
	nav_sts_data_pos_[0] = nav_sts_msg.position.north;
	nav_sts_data_pos_[1] = nav_sts_msg.position.east;
	nav_sts_data_pos_[2] = nav_sts_msg.position.depth;

	nav_sts_data_ori_[0] = nav_sts_msg.orientation.roll;
	nav_sts_data_ori_[1] = nav_sts_msg.orientation.pitch;
	nav_sts_data_ori_[2] = nav_sts_msg.orientation.yaw;

	if(!nav_sts_available_)
		nav_sts_available_ = true;
}

void OmOnlinePlanner::missionFlagSubCallback(const std_msgs::Bool &mission_flag)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

void OmOnlinePlanner::planTheHardWay(std::string planner_name,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	const ob::RealVectorStateSpace::StateType *pos;
	bool collision;

	ROS_INFO("*** Waiting 20 seconds in order to have explored the goal position ***");
	ros::Duration(20).sleep();//Waiting to have a stable version of the map

	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));

	//=======================================================================
	// Finding direction of motion
	//=======================================================================
	// set the bounds for the R^3 part of SE(3)
	if(start_pos[0] > goal_pos[0])
	{
		planning_bounds_[0] = goal_pos[0] - 5.0;
		planning_bounds_[1] = start_pos[0] + 5.0;
	}
	else
	{
		planning_bounds_[0] = start_pos[0] - 5.0;
		planning_bounds_[1] = goal_pos[0] + 5.0;
	}

	if(start_pos[1] > goal_pos[1])
	{
		planning_bounds_[2] = goal_pos[1] - 5.0;
		planning_bounds_[3] = start_pos[1] + 5.0;
	}
	else
	{
		planning_bounds_[2] = start_pos[1] - 5.0;
		planning_bounds_[3] = goal_pos[1] + 5.0;
	}

	if(start_pos[2] > goal_pos[2])
		planning_bounds_[4] = goal_pos[2];
	else
		planning_bounds_[4] = start_pos[2];
	planning_bounds_[5] = 60.0;

	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds_[0]);
	bounds.setHigh(0,planning_bounds_[1]);
	bounds.setLow(1,planning_bounds_[2]);
	bounds.setHigh(1,planning_bounds_[3]);
	bounds.setLow(2,planning_bounds_[4]);
	bounds.setHigh(2,planning_bounds_[5]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Construct an instance of  space information from this state space
	//=======================================================================
	ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	OmStateValidityChecker * om_stat_val_check = new OmStateValidityChecker(si,exploration_altitue_,laser_octomap_flag_);
	si->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Find the appropriate depth
	//=======================================================================
	// Starting position
	collision = false;
	if(increm_path_.size()==0)
		start_pos[0] = start_pos[0] - 2.0;
	start_pos[2] = start_pos[2] + 10.0;
	while(!collision)
	{
		collision = om_stat_val_check->isCollision(start_pos);

		if(collision)
			break;
		else
			start_pos[2] = start_pos[2] + om_stat_val_check->octree_res_;
	}
	start_pos[2] = start_pos[2] - (om_stat_val_check->octree_res_ + exploration_altitue_);

	// Goal position
	collision = false;
	goal_pos[2] = goal_pos[2] + 10.0;
	while(!collision)
	{
		collision = om_stat_val_check->isCollision(goal_pos);

		if(collision)
			break;
		else
			goal_pos[2] = goal_pos[2] + om_stat_val_check->octree_res_;
	}
	goal_pos[2] = goal_pos[2] - (om_stat_val_check->octree_res_ + exploration_altitue_);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];

	//=======================================================================
	// Create a problem instance
	//=======================================================================
	ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

	//=======================================================================
	// Setting optimization objective
	//=======================================================================
//	ob::OptimizationObjective *opt;
//	boost::shared_ptr<ob::OptimizationObjective> newOptimizationObjective;
////	opt = new ob::PathLengthOptimizationObjective(si, std::numeric_limits<double>::epsilon());
//	opt = new ob::StateCostOptimizationObjective(si, std::numeric_limits<double>::epsilon());
////	opt = new ob::BoundedAdditiveOptimizationObjective(si, 40.0);
//	newOptimizationObjective.reset(opt);
//
//	pdef->setOptimizationObjective(newOptimizationObjective);
//
//	ROS_INFO("*** OK specified: ***");

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	pdef->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Set optimization objective
	//=======================================================================
	pdef->setOptimizationObjective(getVisibilityObjective(si, exploration_altitue_, laser_octomap_flag_));

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);

	//=======================================================================
	// Set the problem we are trying to solve for the planner
	//=======================================================================
	planner->setProblemDefinition(pdef);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	planner->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	planner->printProperties(std::cout);// print planner properties
	si->printSettings(std::cout);// print the settings for this space
	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = planner->solve(2.0);

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		path_result_ = pdef->getSolutionPath();
		//geometric_path_result_ = boost::dynamic_pointer_cast<og::PathGeometric>(path_result_);

		ROS_INFO("Path has been found");

		// print the path to screen
		//path_result_->print(std::cout);
	}
	else
		ROS_INFO("Path has not been found");
}

void OmOnlinePlanner::planWithSimpleSetup(std::string planner_name,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	const ob::RealVectorStateSpace::StateType *pos;
	bool collision;

	ROS_INFO("*** Waiting 20 seconds in order to have explored the goal position ***");
	ros::Duration(20).sleep();//Waiting to have a stable version of the map

	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));

	//=======================================================================
	// Finding direction of motion
	//=======================================================================
	// set the bounds for the R^3 part of SE(3)
	if(start_pos[0] > goal_pos[0])
	{
		planning_bounds_[0] = goal_pos[0] - 5.0;
		planning_bounds_[1] = start_pos[0] + 5.0;
	}
	else
	{
		planning_bounds_[0] = start_pos[0] - 5.0;
		planning_bounds_[1] = goal_pos[0] + 5.0;
	}

	if(start_pos[1] > goal_pos[1])
	{
		planning_bounds_[2] = goal_pos[1] - 5.0;
		planning_bounds_[3] = start_pos[1] + 5.0;
	}
	else
	{
		planning_bounds_[2] = start_pos[1] - 5.0;
		planning_bounds_[3] = goal_pos[1] + 5.0;
	}

	if(start_pos[2] > goal_pos[2])
		planning_bounds_[4] = goal_pos[2];
	else
		planning_bounds_[4] = start_pos[2];
	planning_bounds_[5] = 60.0;

	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds_[0]);
	bounds.setHigh(0,planning_bounds_[1]);
	bounds.setLow(1,planning_bounds_[2]);
	bounds.setHigh(1,planning_bounds_[3]);
	bounds.setLow(2,planning_bounds_[4]);
	bounds.setHigh(2,planning_bounds_[5]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	OmStateValidityChecker * om_stat_val_check = new OmStateValidityChecker(si,exploration_altitue_,laser_octomap_flag_);
	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Find the appropriate depth
	//=======================================================================
	// Starting position
	collision = false;
	if(increm_path_.size()==0)
		start_pos[0] = start_pos[0] - 2.0;
	start_pos[2] = start_pos[2] + 10.0;
	while(!collision)
	{
		collision = om_stat_val_check->isCollision(start_pos);

		if(collision)
			break;
		else
			start_pos[2] = start_pos[2] + om_stat_val_check->octree_res_;
	}
	start_pos[2] = start_pos[2] - (om_stat_val_check->octree_res_ + exploration_altitue_);

	// Goal position
	collision = false;
	goal_pos[2] = goal_pos[2] + 10.0;
	while(!collision)
	{
		collision = om_stat_val_check->isCollision(goal_pos);

		if(collision)
			break;
		else
			goal_pos[2] = goal_pos[2] + om_stat_val_check->octree_res_;
	}
	goal_pos[2] = goal_pos[2] - (om_stat_val_check->octree_res_ + exploration_altitue_);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Set optimization objective
	//=======================================================================
	simple_setup_->getProblemDefinition()->setOptimizationObjective(getVisibilityObjective(si, exploration_altitue_, laser_octomap_flag_));

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
//	planner->printProperties(std::cout);// print planner properties
//	si->printSettings(std::cout);// print the settings for this space
//	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve( 2.0 );

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		simple_setup_->simplifySolution();
		simple_setup_->getSolutionPath().interpolate();
		geometric_path_result_.reset(new og::PathGeometric(simple_setup_->getSolutionPath()));

		ROS_INFO("Path has been found with simple_setup");
	}
	else
		ROS_INFO("Path has not been found");
}

ob::PlannerPtr OmOnlinePlanner::selectPlanner(std::string planner_name,ob::SpaceInformationPtr si)
{
	// create a planner for the defined space
	if(planner_name.compare("RRT")==0)
		return ob::PlannerPtr(new og::RRT(si));
	else if(planner_name.compare("RRTstar")==0)
		return ob::PlannerPtr(new og::RRTstar(si));
	else if(planner_name.compare("RRTConnect")==0)
		return ob::PlannerPtr(new og::RRTConnect(si));
	else if(planner_name.compare("LazyRRT")==0)
		return ob::PlannerPtr(new og::LazyRRT(si));
	else if(planner_name.compare("pRRT")==0)
		return ob::PlannerPtr(new og::pRRT(si));
	else if(planner_name.compare("TRRT")==0)
		return ob::PlannerPtr(new og::TRRT(si));
	else if(planner_name.compare("EST")==0)
		return ob::PlannerPtr(new og::EST(si));
	else if(planner_name.compare("BKPIECE1")==0)
		return ob::PlannerPtr(new og::BKPIECE1(si));
	else if(planner_name.compare("KPIECE1")==0)
		return ob::PlannerPtr(new og::KPIECE1(si));
	else if(planner_name.compare("LBKPIECE1")==0)
		return ob::PlannerPtr(new og::LBKPIECE1(si));
	else if(planner_name.compare("PDST")==0)
		return ob::PlannerPtr(new og::PDST(si));
	else if(planner_name.compare("PRM")==0)
		return ob::PlannerPtr(new og::PRM(si));
	else if(planner_name.compare("LazyPRM")==0)
		return ob::PlannerPtr(new og::LazyPRM(si));
	else if(planner_name.compare("PRMstar")==0)
		return ob::PlannerPtr(new og::PRMstar(si));
	else if(planner_name.compare("SPARS")==0)
		return ob::PlannerPtr(new og::SPARS(si));
	else if(planner_name.compare("SPARStwo")==0)
		return ob::PlannerPtr(new og::SPARStwo(si));
	else if(planner_name.compare("SBL")==0)
		return ob::PlannerPtr(new og::SBL(si));
	else if(planner_name.compare("pSBL")==0)
		return ob::PlannerPtr(new og::pSBL(si));
	else
		return ob::PlannerPtr(new og::RRT(si));
}

void OmOnlinePlanner::publishPathMarkers()
{
	const ob::RealVectorStateSpace::StateType *pos;
	geometry_msgs::Point pBound;

	// %Tag(HELIX)%
	// Create the vertices for the points and lines
	std::vector< ob::State * > states = geometric_path_result_->getStates();

	for (uint32_t i = 0; i < geometric_path_result_->getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		pos = states[i]->as<ob::RealVectorStateSpace::StateType>();

		geometry_msgs::Point p;
		p.x = pos->values[0];
		p.y = pos->values[1];
		p.z = pos->values[2];

		if(increm_path_.size()==0 || i!=0)
		{
			std::vector<double> pathPoint(3);
			pathPoint[0] = pos->values[0];
			pathPoint[1] = pos->values[1];
			pathPoint[2] = pos->values[2];
			increm_path_.push_back(pathPoint);
		}
	}
	std::cout << "*** Incremental Path length: " << increm_path_.size() << " ***" << std::endl;

	// %EndTag(HELIX)%

	// %Tag(MARKER_INIT)%
	visualization_msgs::Marker points, line_strip, bound_line_strip;
	points.header.frame_id = line_strip.header.frame_id = bound_line_strip.header.frame_id= "/world";
	points.header.stamp = line_strip.header.stamp = bound_line_strip.header.stamp = ros::Time::now();
	points.ns = line_strip.ns = bound_line_strip.ns = "points_and_lines";
	points.action = line_strip.action = bound_line_strip.action = visualization_msgs::Marker::ADD;
	points.pose.orientation.w = line_strip.pose.orientation.w = bound_line_strip.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	points.id = 0;
	line_strip.id = 1;
	bound_line_strip.id = 2;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	points.type = visualization_msgs::Marker::POINTS;
	line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	bound_line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	points.scale.x = 0.2;
	points.scale.y = 0.2;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	line_strip.scale.x = 0.3;
	bound_line_strip.scale.x = 0.05;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	points.color.g = 1.0f;
	points.color.a = 1.0;

	// Line strip is blue
	line_strip.color.b = 1.0;
	line_strip.color.a = 1.0;

	bound_line_strip.color.r = 1.0;
	bound_line_strip.color.a = 1.0;
	// %EndTag(COLOR)%

	// %Tag(HELIX)%

	std::list< std:: vector<double> >::const_iterator lst_iterator;
	std::vector<double>::const_iterator vct_iterator;
	for (lst_iterator = increm_path_.begin(); lst_iterator != increm_path_.end(); ++lst_iterator)
	{

		geometry_msgs::Point p;
		vct_iterator = lst_iterator->begin();
		p.x = *vct_iterator;
		++vct_iterator;
		p.y = *vct_iterator;
		++vct_iterator;
		p.z = *vct_iterator;

		points.points.push_back(p);
		line_strip.points.push_back(p);

	}
	// %EndTag(HELIX)%

	pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[2];
	pBound.z = planning_bounds_[4];
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[1];
	pBound.y = planning_bounds_[2];
	pBound.z = planning_bounds_[4];
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[1];
	pBound.y = planning_bounds_[3];
	pBound.z = planning_bounds_[4];
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[3];
	pBound.z = planning_bounds_[4];
	bound_line_strip.points.push_back(pBound);

	pBound.x = planning_bounds_[0];
	pBound.y = planning_bounds_[2];
	pBound.z = planning_bounds_[4];
	bound_line_strip.points.push_back(pBound);

	path_marker_pub_.publish(points);
	path_marker_pub_.publish(line_strip);
	path_marker_pub_.publish(bound_line_strip);

}

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}
