/*
 * icra14_state_validity_checker.h
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/objectives/MaximizeMinClearanceObjective.h>

#include "icra14_planners_headers.hpp"

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;


//!  OmStateValidityChecker class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap.
*/
class OmStateValidityChecker : public ob::StateValidityChecker {

public:
	//! OmStateValidityChecker constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	OmStateValidityChecker(const ob::SpaceInformationPtr &si, const double exploration_altitue, const bool laser_octomap_flag);
	//! OmStateValidityChecker destructor.
	/*!
	 * Destroy the octomap.
	 */
	~OmStateValidityChecker();
	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision)
	 */
	virtual bool isValid(const ob::State *state) const;

	virtual bool isCollision(std::vector<double> pos_query) const;

	double octree_res_;
private:
	//ROS
	ros::NodeHandle node_hand_;

	// ROS tf
	tf::Pose last_pose_;

	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;

	//Girona500 info
	double length_g500_;

	double exploration_altitue_;
};

/** Defines an optimization objective which attempts to steer the
    robot away from obstacles. To formulate this objective as a
    minimization of path cost, we can define the cost of a path as a
    summation of the costs of each of the states along the path, where
    each state cost is a function of that state's clearance from
    obstacles.

    The class StateCostIntegralObjective represents objectives as
    summations of state costs, just like we require. All we need to do
    then is inherit from that base class and define our specific state
    cost function by overriding the stateCost() method.
 */
class VisibilityObjective : public ob::StateCostIntegralObjective {

private:
	//ROS
	ros::NodeHandle node_hand_;

	// ROS tf
	tf::Pose last_pose_;

	//Octomap
	AbstractOcTree* tree_;
	OcTree* octree_;
	double octree_res_;

	//Girona500 info
	double length_g500_;

	double exploration_altitue_;
public:
    VisibilityObjective(const ob::SpaceInformationPtr& si, const double exploration_altitue, const bool laser_octomap_flag);

    // Our requirement is to maximize path clearance from obstacles,
    // but we want to represent the objective as a path cost
    // minimization. Therefore, we set each state's cost to be the
    // reciprocal of its clearance, so that as state clearance
    // increases, the state cost decreases.
    ob::Cost stateCost(const ob::State* state) const;
};

/** Return an optimization objective which attempts to steer the robot
    away from obstacles. */
ob::OptimizationObjectivePtr getVisibilityObjective(const ob::SpaceInformationPtr& si, const double exploration_altitue, const bool laser_octomap_flag);
