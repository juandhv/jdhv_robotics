/*
 * icra14_online_map_plan_R3.h
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <auv_msgs/NavSts.h>
#include <std_msgs/Bool.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>

#include <signal.h>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>

#include "icra14_planners_headers.hpp"

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;

//!  OmOnlinePlanner class.
/*!
 * Octomap Offline Planner.
 * Plan and visualize a trajectory given an initial and a final configuration. The map is assume as known and is represented
 * by an octomap.
*/
class OmOnlinePlanner {

public:
	//! Constructor.
	OmOnlinePlanner(std::string planner_name);
	//! Path Planning using OMPL.
	/*!
	 * Path Planning using OMPL and OmStateValidityChecker (over octomaps).
	 * Based on the example of OMPL
	 */
	void planTheHardWay(std::string planner_name,std::vector<double> start_pos,std::vector<double> goal_pos);
	
	void planWithSimpleSetup(std::string planner_name,std::vector<double> start_pos,std::vector<double> goal_pos);
	
	//! Draw the path.
	/*!
	 * Publish the path as markers to be visualized in rviz
	 */
	void publishPathMarkers();

	//! Select the given planner.
	ob::PlannerPtr selectPlanner(std::string planner_name,ob::SpaceInformationPtr si);

	//! Nav Sts
	void navStsSubCallback(const auv_msgs::NavSts &nav_sts_msg);

	void missionFlagSubCallback(const std_msgs::Bool &mission_flag);

	void timerCallback(const ros::TimerEvent &e);

private:

	//ROS
	ros::NodeHandle node_hand_;

	ros::Publisher path_marker_pub_;
	ros::Subscriber nav_sts_sub_;
	ros::Subscriber mission_flag_sub_;

	ros::Timer plannTimer_;

	//OMPL
	ob::PathPtr path_result_;
	boost::shared_ptr<og::PathGeometric> geometric_path_result_;
	og::SimpleSetupPtr simple_setup_;

	//Online planner
	std::vector<double> last_nav_sts_data_pos_;
	std::vector<double> current_nav_sts_data_pos_;
	std::vector<double> planning_bounds_;
	std::list< std::vector<double> > increm_path_;
	std::string planner_name_;
	bool simple_setup_flag_;
	double exploration_altitue_;
	bool laser_octomap_flag_;
	bool rosbags_flag_;

	//Navigation information
	std::vector<double> nav_sts_data_pos_;
	std::vector<double> nav_sts_data_ori_;

	//Flags
	bool nav_sts_available_;
	bool mission_flag_available_;
};

void stopNode(int sig);
