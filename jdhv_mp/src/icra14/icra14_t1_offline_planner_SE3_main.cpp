/*
 * icra14_t1_offline_planner_SE3_main.cpp
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

#include "icra14_offline_planner_SE3.hpp"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "offline_planner_ompl_SE3");
	ROS_INFO_STREAM("Starting offline_planner_ompl_SE3 Node");

//	//TestMap1
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 21.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -20;
//	start_pos[1] = -27;
//	start_pos[2] = 29.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 25.0;
//	goal_pos[1] = 35.0;
//	goal_pos[2] = 28.0;
//
//	//TestMap2
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 20.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = 5.0;
//	start_pos[1] = -5.0;
//	start_pos[2] = 28.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 0.0;
//	goal_pos[1] = 38.0;
//	goal_pos[2] = 28.0;

	//TestMap3
	std::vector<double> planning_bounds(6);
	planning_bounds[0] = -40.0;
	planning_bounds[1] = 40.0;
	planning_bounds[2] = -40.0;
	planning_bounds[3] = 40.0;
	planning_bounds[4] = 20.0;
	planning_bounds[5] = 30.0;

	std::vector<double> start_pos(3);
	start_pos[0] = -35.0;
	start_pos[1] = -35.0;
	start_pos[2] = 28.0;

	std::vector<double> goal_pos(3);
	goal_pos[0] = 4.0;
	goal_pos[1] = 4.0;
	goal_pos[2] = 22.0;

//	//TestMap4
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 20.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -35.0;
//	start_pos[1] = -35.0;
//	start_pos[2] = 28.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 4.0;
//	goal_pos[1] = 4.0;
//	goal_pos[2] = 22.0;
//
//	//TestMap5
//	std::vector<double> planning_bounds(6);
//	planning_bounds[0] = -40.0;
//	planning_bounds[1] = 40.0;
//	planning_bounds[2] = -40.0;
//	planning_bounds[3] = 40.0;
//	planning_bounds[4] = 18.0;
//	planning_bounds[5] = 30.0;
//
//	std::vector<double> start_pos(3);
//	start_pos[0] = -10.0;
//	start_pos[1] = -35.0;
//	start_pos[2] = 25.0;
//
//	std::vector<double> goal_pos(3);
//	goal_pos[0] = 0.0;
//	goal_pos[1] = 35.0;
//	goal_pos[2] = 22.0;

	// Constructor
	//(RRT, RRTstar, RRTConnect, BallTreeRRTstar, LazyRRT, pRRT, TRRT), (EST), (BKPIECE1, KPIECE1, LBKPIECE1), (PDST), (PRM, LazyPRM, PRMstar, SPARS, SPARStwo), (SBL, pSBL)
	OmOfflinePlanner offPlanner(planning_bounds,start_pos,goal_pos);

	return 0;
}
