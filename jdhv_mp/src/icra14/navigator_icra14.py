#!/usr/bin/env python
"""
Created on May 15, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
"""

# Debug control variables
DEBUG_OCTOMAP_VIS = False

# ROS imports
import roslib; roslib.load_manifest('jdhv_mp')
import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String, Bool
from std_srvs.srv import Empty
from visualization_msgs.msg import Marker
import tf

from auv_msgs.msg import NavSts, WorldWaypointReq, BodyVelocityReq, GoalDescriptor
from nav_msgs.msg import Odometry

# cola2 and related imports
from cola2_lib import cola2_ros_lib
from cola2_msgs.msg import WorldWaypointReqGoal, WorldWaypointReqAction, WorldSectionReqGoal, WorldSectionReqAction

# Octomap visualization
if DEBUG_OCTOMAP_VIS:
    import OctomapVisual

# Standard Python imports
from math import *
import numpy as np
import actionlib

STATE_ZERO = 0
STATE_INIT = 1
STATE_TRAJ_INIT_ORI = 2
STATE_TRAJ_POS = 3
STATE_TRAJ_FINAL_ORI = 4

virtFactorPos = 5
virtFactorOri = 1 #2

class CoverageNavigator(object):
    """
    Path follower controller.
    """

    def __init__(self):
        """
        Constructor
            
        Setup of the initial parameters and create the necessary subscribers and publishers.
        First of all, the robot will descend to the specified depth, if provided, otherwise
        will consider 2 meters.
        """
        
        #Waiting for octomap visualization
        rospy.sleep(15.)
        
        rospy.loginfo("+++ Setting up controller params +++")
        
        #=======================================================================
        # Initial conditions
        #=======================================================================
        
        #=======================================================================
        # Default waypoint tolerance values
        #=======================================================================
        self.wp_toler_pos_ = 1.0
        self.wp_toler_ori_ = radians(8.0)

        #=======================================================================
        # Action lib params
        #=======================================================================
        self.wp_act_lib_conf_ = cola2_ros_lib.Config()
        self.wp_act_lib_conf_.trajectory_step = 0.25
        self.wp_act_lib_goal_id_ = 36000
        
        rospy.loginfo("%s: creating action client for waypoints", rospy.get_name())
        self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('world_waypoint_req', WorldWaypointReqAction)
        #self.wp_act_lib_action_client_ = actionlib.SimpleActionClient('absolute_movement', WorldWaypointReqAction)
        rospy.logwarn("%s: waiting for action server for waypoints", rospy.get_name())
        self.wp_act_lib_action_client_.wait_for_server()
        rospy.loginfo("%s: ready to use action client for waypoints", rospy.get_name())
        
        rospy.loginfo("%s: creating action client for sections", rospy.get_name())
        self.sect_act_lib_action_client_ = actionlib.SimpleActionClient('world_section_req', WorldSectionReqAction)
        rospy.logwarn("%s: waiting for action server for sections", rospy.get_name())
        self.sect_act_lib_action_client_.wait_for_server()
        rospy.loginfo("%s: ready to use action client for sections", rospy.get_name())
        
        #=======================================================================
        # Subscribers 
        #=======================================================================
        #Navigation data (feedback)
        self.nav_sts_sub_ = rospy.Subscriber("/pose_ekf_slam/odometry", Odometry, self.navStsSubCallback, queue_size = 1)
        self.nav_sts_available_ = False
        
        #=======================================================================
        # Publishers
        #=======================================================================
        self.wp_cov_traj_pub_ = rospy.Publisher("/jdhv_robotics/coverage_traj", Marker)
        self.mission_flag_pub_ = rospy.Publisher("/jdhv_robotics/mission_flag", Bool)
        
        #=======================================================================
        # Waiting for nav sts
        #=======================================================================
        print "Waiting for nav sts..."
        r = rospy.Rate(5)
        while not rospy.is_shutdown() and not self.nav_sts_available_:
            r.sleep()
        
        #=======================================================================
        # Register handler to be called when rospy process begins shutdown.
        #=======================================================================
        rospy.on_shutdown(self.shutdownHook)
        
        return
    
    def buildTrajList(self):
        """
        Build a trajectory list
            
        List of Waypoints:
        Each waypoint will be specified as a list: [x,y,z,roll,pitch,yaw]
        """
        
        self.prev_waypoint_ = None
        self.traj = {}
        self.traj['list'] = []

#         self.traj['list'].append({'x' : 38.0, 'y' : 30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : atan2(30,38)})
#         self.traj['list'].append({'x' : 38.0, 'y' : 30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : np.pi})
#               
#         self.traj['list'].append({'x' : -38.0, 'y' : 30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})
#         self.traj['list'].append({'x' : -38.0, 'y' : 30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
#               
#         self.traj['list'].append({'x' : -38.0, 'y' : 15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
#         self.traj['list'].append({'x' : -38.0, 'y' : 15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : 0.0})
#               
#         self.traj['list'].append({'x' : 38.0, 'y' : 15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : 0.0})
#         self.traj['list'].append({'x' : 38.0, 'y' : 15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
#               
#         self.traj['list'].append({'x' : 38.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
#         self.traj['list'].append({'x' : 38.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})


        self.traj['list'].append({'x' : 0.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})
        self.traj['list'].append({'x' : 0.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})


        self.traj['list'].append({'x' : -38.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})
        self.traj['list'].append({'x' : -38.0, 'y' : 0.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
         
        self.traj['list'].append({'x' : -38.0, 'y' : -15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
        self.traj['list'].append({'x' : -38.0, 'y' : -15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : 0.0})

        self.traj['list'].append({'x' : 38.0, 'y' : -15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : 0.0})
        self.traj['list'].append({'x' : 38.0, 'y' : -15.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})

        self.traj['list'].append({'x' : 38.0, 'y' : -30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi/2})
        self.traj['list'].append({'x' : 38.0, 'y' : -30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})

        self.traj['list'].append({'x' : -38.0, 'y' : -30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})
        self.traj['list'].append({'x' : -38.0, 'y' : -30.0, 'z' : 3.0, 'roll' : 0.0, 'pitch' : 0.0, 'yaw' : -np.pi})
        
        self.traj_points = Marker()
        self.traj_points.header.frame_id = "/world"
        self.traj_points.header.stamp = rospy.Time.now()
        self.traj_points.ns = 'Coverag_trajectory_points'
        self.traj_points.id = 0
        self.traj_points.type = Marker.SPHERE_LIST
        self.traj_points.action = Marker.ADD
        self.traj_points.pose.position.x = 0.0
        self.traj_points.pose.position.y = 0.0
        self.traj_points.pose.position.z = 0.0
        self.traj_points.pose.orientation.x = 0.0
        self.traj_points.pose.orientation.y = 0.0
        self.traj_points.pose.orientation.z = 0.0
        self.traj_points.pose.orientation.w = 1.0
        self.traj_points.scale.x = 0.8
        self.traj_points.scale.y = 0.8
        self.traj_points.scale.z = 0.8
        self.traj_points.color.a = 0.5
        self.traj_points.color.r = 0.0
        self.traj_points.color.g = 0.1
        self.traj_points.color.b = 1.0
        
        self.traj_line_strip = Marker()
        self.traj_line_strip.header.frame_id = "/world"
        self.traj_line_strip.header.stamp = rospy.Time.now()
        self.traj_line_strip.ns = 'Coverag_trajectory_points'
        self.traj_line_strip.id = 1
        self.traj_line_strip.type = Marker.LINE_STRIP
        self.traj_line_strip.action = Marker.ADD
        self.traj_line_strip.pose.position.x = 0.0
        self.traj_line_strip.pose.position.y = 0.0
        self.traj_line_strip.pose.position.z = 0.0
        self.traj_line_strip.pose.orientation.x = 0.0
        self.traj_line_strip.pose.orientation.y = 0.0
        self.traj_line_strip.pose.orientation.z = 0.0
        self.traj_line_strip.pose.orientation.w = 1.0
        self.traj_line_strip.scale.x = 0.2
        self.traj_line_strip.scale.y = 0.2
        self.traj_line_strip.scale.z = 0.2
        self.traj_line_strip.color.a = 0.5
        self.traj_line_strip.color.r = 0.0
        self.traj_line_strip.color.g = 1.0
        self.traj_line_strip.color.b = 0.1
        
        for WP in self.traj['list']:
            self.traj_points.points.append(Point(WP['x'], WP['y'], WP['z']))
            self.traj_line_strip.points.append(Point(WP['x'], WP['y'], WP['z']))

        return
    
    def navStsSubCallback(self,nav_sts_msg):
        """
        Callback to receive navSts message
        """
        self.nav_sts_data_ = nav_sts_msg
        if self.nav_sts_available_ == False:
            self.nav_sts_available_ = True
        
        return
    
    def moveTo(self, x, y, z, yaw=0.0, los=True):
        """
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
        moveTo: move vehicle to waypoint using COLA2 API (based on Enric's code)
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        """
        rospy.loginfo("%s:\n moveTo(x=%f, y=%f, z=%f, yaw=%f)", rospy.get_name(), x, y, z, yaw)
        
#         # Create a goal to send to the action server
#         goal = WorldWaypointReqGoal()
#         goal.goal.requester = rospy.get_name() + '_virtTargetControl'
#         goal.goal.id = self.wp_act_lib_goal_id_
#         self.wp_act_lib_goal_id_ = self.wp_act_lib_goal_id_ + 1
#         goal.goal.priority = 10
#           
#         goal.mode = ''
#         goal.timeout = 180 # 3 minutes        
#           
#         goal.altitude_mode = False
#   
#         # Goal pose
#         goal.position.north = x
#         goal.position.east = y
#         goal.position.depth = z
#   
#         goal.orientation.roll = 0
#         goal.orientation.pitch = 0
#         goal.orientation.yaw = yaw
#   
#         if los:
#             goal.mode = 'sparus_los'
#             # Set movement type (X-Z-Yaw) using los
#             goal.disable_axis.x = False
#             goal.disable_axis.y = True
#             goal.disable_axis.z = False
#             goal.disable_axis.roll = True
#             goal.disable_axis.pitch = True
#             goal.disable_axis.yaw = False
#         else:
#             # Only Z
#             goal.disable_axis.x = True
#             goal.disable_axis.y = True
#             goal.disable_axis.z = False
#             goal.disable_axis.roll = True
#             goal.disable_axis.pitch = True
#             goal.disable_axis.yaw = True
#   
#         # Set error tolerances
#         goal.position_tolerance.x = self.wp_toler_pos_
#         goal.position_tolerance.y = self.wp_toler_pos_
#         goal.position_tolerance.z = self.wp_toler_pos_
#         goal.orientation_tolerance.roll = self.wp_toler_ori_
#         goal.orientation_tolerance.pitch = self.wp_toler_ori_
#         goal.orientation_tolerance.yaw = self.wp_toler_ori_
#   
#         self.wp_act_lib_action_client_.send_goal(goal)
#         self.wp_act_lib_action_client_.wait_for_result()
#         result = self.wp_act_lib_action_client_.get_result()
        
        #-----
        section_msg = WorldSectionReqGoal()
          
        section_msg.priority = 10
        section_msg.controller_type = WorldSectionReqGoal.LOSCTE
        section_msg.disable_z = False
  
        section_msg.tolerance.x = 1.5
        section_msg.tolerance.y = 1.5
        section_msg.tolerance.z = 1.0
  
        section_msg.initial_surge = 0.3
        #section.use_initial_yaw = true;
        section_msg.final_surge = 0.3
        section_msg.use_final_yaw = True
        section_msg.final_position.z   = z
        #section.timeout = 10.0;
  
        if self.prev_waypoint_ is None:
            section_msg.initial_position.x = self.nav_sts_data_.pose.pose.position.x
            section_msg.initial_position.y = self.nav_sts_data_.pose.pose.position.y
            section_msg.initial_position.z = self.nav_sts_data_.pose.pose.position.z
        else:
            section_msg.initial_position.x = self.prev_waypoint_[0]
            section_msg.initial_position.y = self.prev_waypoint_[1]
            section_msg.initial_position.z = self.prev_waypoint_[2]
            
        section_msg.initial_yaw = yaw # TO-DO
        section_msg.final_position.x = x
        section_msg.final_position.y = y
        section_msg.final_yaw = yaw
  
        self.sect_act_lib_action_client_.send_goal(section_msg)
        self.sect_act_lib_action_client_.wait_for_result()
        result = self.sect_act_lib_action_client_.get_result()
        
    def shutdownHook(self):
        """
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        shutdownHook: gets called on shutdown and cancels all ongoing pilot goals (Enric)
        wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww    
        """
        rospy.loginfo("+++ Canceling goals before shutdown +++")
        rospy.signal_shutdown("manual shutdown")
        self.wp_act_lib_action_client_.cancel_all_goals()
        
    def executeCoverage(self):
        self.wp_cov_traj_pub_.publish(self.traj_points)
        self.wp_cov_traj_pub_.publish(self.traj_line_strip)
        
        self.moveTo(0.0, 0.0, 3.0, 0.0,False)
        
        wpi = 0
        while wpi< len(self.traj['list']):
            self.wp_cov_traj_pub_.publish(self.traj_points)
            self.wp_cov_traj_pub_.publish(self.traj_line_strip)
            
            self.moveTo(self.traj['list'][wpi]['x'], self.traj['list'][wpi]['y'], self.traj['list'][wpi]['z'], self.traj['list'][wpi]['yaw'], False)
            wpi = wpi + 1
            self.moveTo(self.traj['list'][wpi]['x'], self.traj['list'][wpi]['y'], self.traj['list'][wpi]['z'], self.traj['list'][wpi]['yaw'], False)
            
            mission_flag = Bool(True)
            self.mission_flag_pub_.publish(mission_flag)
    
if __name__ == '__main__':
    try:
        rospy.init_node('ICRA14_coverage_navigator')

        #=======================================================================
        # Init: create navigator, go to initial position and create wps list
        #=======================================================================
        cov_navig= CoverageNavigator()
        cov_navig.buildTrajList()
        cov_navig.executeCoverage()
            
    except rospy.ROSInterruptException:
        pass