/*
 * icra14_offline_planner_SE3.cpp
 *
 *  Created on: August 16, 2013
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Path planning (geometric) using algorithms from OMPL and Octomaps as navigation maps.
 */

#include "icra14_offline_planner_SE3.hpp"

OmStateValidityChecker::OmStateValidityChecker(const ob::SpaceInformationPtr &si) :
	ob::StateValidityChecker(si)
{
	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	GetOctomap::Request req;
	GetOctomap::Response resp;

	std::string serv_name = "octomap_binary";
	octree_ = NULL;
	length_g500_ = 1.5;

	ROS_INFO("Requesting the map from %s...", node_hand_.resolveName(serv_name).c_str());

	while(node_hand_.ok() && !ros::service::call(serv_name, req, resp))
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		tree_ = octomap_msgs::msgToMap(resp.map);
		if (tree_){
			octree_ = dynamic_cast<octomap::OcTree*>(tree_);
		}
		octree_res_ = octree_->getResolution();
		if (octree_){
			ROS_INFO("Octomap received (%zu nodes, %f m res)", octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmStateValidityChecker::isValid(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	bool collision(false);
	double node_occupancy;

	// cast the abstract state type to the type we expect
	const ob::SE3StateSpace::StateType *se3state = state->as<ob::SE3StateSpace::StateType>();

	// extract the first component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);

	for(double xi = pos->values[0]-(length_g500_/2.0);xi <= pos->values[0]+(length_g500_/2.0);xi=xi+octree_res_)
		for(double yi = pos->values[1]-(length_g500_/2.0);yi <= pos->values[1]+(length_g500_/2.0);yi=yi+octree_res_)
			for(double zi = pos->values[2]-(length_g500_/2.0);zi <= pos->values[2]+(length_g500_/2.0);zi=zi+octree_res_){
				query.x() = xi;
				query.y() = yi;
				query.z() = zi;
				result = octree_->search (query);

				if(result == NULL){
					collision = true;
					break;
				}
				else{
					node_occupancy = result->getOccupancy();
					if (node_occupancy > 0.4)
					{
						collision = true;
						break;
					}
				}
			}
	return !collision;
}


double OmStateValidityChecker::cost(const ob::State *state) const
{
	point3d query;
	OcTreeNode* result;
	bool collision;
	double cost, bottom_z, zi, node_occupancy;

	// cast the abstract state type to the type we expect
	const ob::SE3StateSpace::StateType *se3state = state->as<ob::SE3StateSpace::StateType>();

	// extract the first component of the state and cast it to what we expect
	const ob::RealVectorStateSpace::StateType *pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);

	// Find the bottom
	zi = pos->values[2];
	collision = false;
	while(!collision)
	{
		query.x() = pos->values[0];
		query.y() = pos->values[1];
		query.z() = zi;
		result = octree_->search (query);
		//print_query_info(query, result);

		if(result == NULL){
			collision = true;
			break;
		}
		else{
			node_occupancy = result->getOccupancy();
			if (node_occupancy > 0.4)
			{
				collision = true;
				break;
			}
		}
		zi = zi + octree_res_;
	}
	bottom_z = zi - octree_res_;
	//

	cost = ((bottom_z - pos->values[2]) / bottom_z) * 10.0;

//	ROS_INFO("Checking cost:");
//	std::cout << "Z:" << pos->values[2] << std::endl;
//	std::cout << "bottom_z:" << bottom_z << std::endl;
//	std::cout << "cost:" << cost << std::endl;

	return cost;
}

OmStateValidityChecker::~OmStateValidityChecker()
{
    delete octree_;
}

OmOfflinePlanner::OmOfflinePlanner(std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos):
	node_hand_()
{
	node_hand_.getParam("planner_name", planner_name_);

	path_marker_pub_ = node_hand_.advertise<visualization_msgs::Marker>("visualization_marker", 10);

	if(simple_setup_flag_)
		planWithSimpleSetup(planner_name_,planning_bounds,start_pos,goal_pos);
	else
		planTheHardWay(planner_name_,planning_bounds,start_pos,goal_pos);

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		OmOfflinePlanner::publishPathMarkers();
		loop_rate.sleep();
	}
}

void OmOfflinePlanner::planTheHardWay(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	const ob::SE3StateSpace::StateType *se3state;
	const ob::RealVectorStateSpace::StateType *pos;
	//const ob::SO2StateSpace::StateType *rot;

	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::SE3StateSpace());

	//=======================================================================
	// Set the bounds for the R^3 part of SE(3)
	//=======================================================================
	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds[0]);
	bounds.setHigh(0,planning_bounds[1]);
	bounds.setLow(1,planning_bounds[2]);
	bounds.setHigh(1,planning_bounds[3]);
	bounds.setLow(2,planning_bounds[4]);
	bounds.setHigh(2,planning_bounds[5]);

	space->as<ob::SE3StateSpace>()->setBounds(bounds);

	//=======================================================================
	// Construct an instance of  space information from this state space
	//=======================================================================
	ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	OmStateValidityChecker * om_stat_val_check = new OmStateValidityChecker(si);
	si->setStateValidityChecker(ob::StateValidityCheckerPtr(om_stat_val_check));

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);
	start.random();
	se3state = start->as<ob::SE3StateSpace::StateType>();
	pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);
	//rot = se3state->as<ob::SO2StateSpace::StateType>(1);

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];
	//rot->value = 0.0;

	// create a random goal state
	ob::ScopedState<> goal(space);
	goal.random();
	se3state = goal->as<ob::SE3StateSpace::StateType>();
	pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);
	//rot = se3state->as<ob::SO2StateSpace::StateType>(1);

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];
	//rot->value = 0.0;

	//=======================================================================
	// Create a problem instance
	//=======================================================================
	ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

	//=======================================================================
	// Setting optimization objective
	//=======================================================================
//	ob::OptimizationObjective *opt;
//	boost::shared_ptr<ob::OptimizationObjective> newOptimizationObjective;
////	opt = new ob::PathLengthOptimizationObjective(si, std::numeric_limits<double>::epsilon());
//	opt = new ob::StateCostOptimizationObjective(si, std::numeric_limits<double>::epsilon());
////	opt = new ob::BoundedAdditiveOptimizationObjective(si, 40.0);
//	newOptimizationObjective.reset(opt);
//
//	pdef->setOptimizationObjective(newOptimizationObjective);
//
//	ROS_INFO("*** OK specified: ***");

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	pdef->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);

	//=======================================================================
	// Set the problem we are trying to solve for the planner
	//=======================================================================
	planner->setProblemDefinition(pdef);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	planner->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	planner->printProperties(std::cout);// print planner properties
	si->printSettings(std::cout);// print the settings for this space
	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = planner->solve(1.0);

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		path_result_ = pdef->getSolutionPath();
		//geometric_path_result_ = boost::dynamic_pointer_cast<og::PathGeometric>(path_result_);

		ROS_INFO("Path has been found");

		// print the path to screen
		//path_result_->print(std::cout);
	}
	else
		ROS_INFO("Path has not been found");
}

void OmOfflinePlanner::planWithSimpleSetup(std::string planner_name,std::vector<double> planning_bounds,std::vector<double> start_pos,std::vector<double> goal_pos)
{
	ROS_INFO("OMPL version: %s", OMPL_VERSION);
	ROS_INFO("*** RigidBodyPlanning3D (C++) without simple setup: ***");

	//=======================================================================
	// Construct the state space we are planning in
	//=======================================================================
	ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));

	//=======================================================================
	// Set the bounds for the R^3 part of SE(3)
	//=======================================================================
	ob::RealVectorBounds bounds(3);
	bounds.setLow(0,planning_bounds[0]);
	bounds.setHigh(0,planning_bounds[1]);
	bounds.setLow(1,planning_bounds[2]);
	bounds.setHigh(1,planning_bounds[3]);
	bounds.setLow(2,planning_bounds[4]);
	bounds.setHigh(2,planning_bounds[5]);

	space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner = selectPlanner(planner_name,si);

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	// Set state validity checking for this space
	simple_setup_->setStateValidityChecker(ob::StateValidityCheckerPtr(new OmStateValidityChecker(si)));

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	const ob::RealVectorStateSpace::StateType *pos;
	ob::ScopedState<> start(space);
	pos = start->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = start_pos[0];
	pos->values[1] = start_pos[1];
	pos->values[2] = start_pos[2];

	// create a goal state
	ob::ScopedState<> goal(space);
	pos = goal->as<ob::RealVectorStateSpace::StateType>();

	pos->values[0] = goal_pos[0];
	pos->values[1] = goal_pos[1];
	pos->values[2] = goal_pos[2];

	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
//	planner->printProperties(std::cout);// print planner properties
//	si->printSettings(std::cout);// print the settings for this space
//	pdef->print(std::cout);// print the problem settings

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve( 1.0 );

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
//		simple_setup_->simplifySolution();
		simple_setup_->getSolutionPath().interpolate();
		geometric_path_result_.reset(new og::PathGeometric(simple_setup_->getSolutionPath()));

		ROS_INFO("Path has been found with simple_setup");
	}
	else
		ROS_INFO("Path has not been found");
}

ob::PlannerPtr OmOfflinePlanner::selectPlanner(std::string planner_name,ob::SpaceInformationPtr si)
{
	// create a planner for the defined space
	if(planner_name.compare("RRT")==0)
		return ob::PlannerPtr(new og::RRT(si));
	else if(planner_name.compare("RRTstar")==0)
		return ob::PlannerPtr(new og::RRTstar(si));
	else if(planner_name.compare("RRTConnect")==0)
		return ob::PlannerPtr(new og::RRTConnect(si));
	else if(planner_name.compare("LazyRRT")==0)
		return ob::PlannerPtr(new og::LazyRRT(si));
	else if(planner_name.compare("pRRT")==0)
		return ob::PlannerPtr(new og::pRRT(si));
	else if(planner_name.compare("TRRT")==0)
		return ob::PlannerPtr(new og::TRRT(si));
	else if(planner_name.compare("EST")==0)
		return ob::PlannerPtr(new og::EST(si));
	else if(planner_name.compare("BKPIECE1")==0)
		return ob::PlannerPtr(new og::BKPIECE1(si));
	else if(planner_name.compare("KPIECE1")==0)
		return ob::PlannerPtr(new og::KPIECE1(si));
	else if(planner_name.compare("LBKPIECE1")==0)
		return ob::PlannerPtr(new og::LBKPIECE1(si));
	else if(planner_name.compare("PDST")==0)
		return ob::PlannerPtr(new og::PDST(si));
	else if(planner_name.compare("PRM")==0)
		return ob::PlannerPtr(new og::PRM(si));
	else if(planner_name.compare("LazyPRM")==0)
		return ob::PlannerPtr(new og::LazyPRM(si));
	else if(planner_name.compare("PRMstar")==0)
		return ob::PlannerPtr(new og::PRMstar(si));
	else if(planner_name.compare("SPARS")==0)
		return ob::PlannerPtr(new og::SPARS(si));
	else if(planner_name.compare("SPARStwo")==0)
		return ob::PlannerPtr(new og::SPARStwo(si));
	else if(planner_name.compare("SBL")==0)
		return ob::PlannerPtr(new og::SBL(si));
	else if(planner_name.compare("pSBL")==0)
		return ob::PlannerPtr(new og::pSBL(si));
	else
		return ob::PlannerPtr(new og::RRT(si));
}

void OmOfflinePlanner::publishPathMarkers()
{
	const ob::SE3StateSpace::StateType *se3state;
	const ob::RealVectorStateSpace::StateType *pos;
	//const ob::SO2StateSpace::StateType *rot;

	// %Tag(MARKER_INIT)%
	visualization_msgs::Marker points, line_strip;
	points.header.frame_id = line_strip.header.frame_id = "/world";
	points.header.stamp = line_strip.header.stamp = ros::Time::now();
	points.ns = line_strip.ns = "points_and_lines";
	points.action = line_strip.action = visualization_msgs::Marker::ADD;
	points.pose.orientation.w = line_strip.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	points.id = 0;
	line_strip.id = 1;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	points.type = visualization_msgs::Marker::POINTS;
	line_strip.type = visualization_msgs::Marker::LINE_STRIP;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	points.scale.x = 0.2;
	points.scale.y = 0.2;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	line_strip.scale.x = 0.3;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	points.color.g = 1.0f;
	points.color.a = 1.0;

	// Line strip is blue
	line_strip.color.b = 1.0;
	line_strip.color.a = 1.0;
	// %EndTag(COLOR)%

	// %Tag(HELIX)%
	// Create the vertices for the points and lines
	std::vector< ob::State * > states = geometric_path_result_->getStates();

	for (uint32_t i = 0; i < geometric_path_result_->getStateCount(); ++i)
	{
		// cast the abstract state type to the type we expect
		se3state = states[i]->as<ob::SE3StateSpace::StateType>();
		// extract the first component of the state and cast it to what we expect
		pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);
		// extract the second component of the state and cast it to what we expect
		//rot = se3state->as<ob::SO2StateSpace::StateType>(1);

		geometry_msgs::Point p;
		p.x = pos->values[0];
		p.y = pos->values[1];
		p.z = pos->values[2];

		points.points.push_back(p);
		line_strip.points.push_back(p);

	}
	// %EndTag(HELIX)%

	path_marker_pub_.publish(points);
	path_marker_pub_.publish(line_strip);
}

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}
