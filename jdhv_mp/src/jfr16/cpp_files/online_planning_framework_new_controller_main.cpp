/*
 * offline_planning_framework_main.cpp
 *
 *  Created on: March 5, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  Framework for planning collision-free paths offline, i.e. the map is assumed to be known
 *  and there is no connection with vehicle's controllers. Iterative planning uses last
 *  known solution path in order to guarantee that new solution paths are always at
 *  least as good as the previous one.
 *
 *  February 2, 2016: Dubins3D included (planning in 3D workspaces)
 */

#include <iostream>
#include <vector>

//standard OMPL
#include <ompl/control/SpaceInformation.h>
#include <ompl/base/MotionValidator.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/objectives/MaximizeMinClearanceObjective.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/config.h>

//extended OMPL
#include "Dubins3DStateSpace.h"
#include "state_validity_checker_octomap_fcl_R2.hpp"
#include "state_validity_checker_octomap_fcl_SE2.hpp"
#include "state_validity_checker_octomap_fcl_SE2_R.hpp"
#include "new_state_sampler.h"
#include "state_cost_objective.hpp"

//ROS
#include <ros/ros.h>
#include <ros/package.h>
//ROS markers rviz
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseArray.h>
#include <cola2_msgs/WorldPathReqGoal.h>
#include <cola2_msgs/WorldPathReqAction.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>
//ROS
#include <ros/ros.h>

//Boost
#include <boost/bind.hpp>

namespace ob = ompl::base;
namespace og = ompl::geometric;

//!  OfflinePlannFramework class.
/*!
 * Offline Planning Framework.
 * Setup a sampling-based planner for offline computation of collision-free paths.
 * C-Space: R2 or Dubins
 * Workspace is represented with Octomaps
*/
class OfflinePlannFramework
{
    public:
		//! Constructor
		OfflinePlannFramework();
		//! Planner setup
		void planWithSimpleSetup();
		//! Periodic callback to solve the query.
		void planningTimerCallback(const ros::TimerEvent &e);
		//! Procedure to visualize the resulting path
		void visualizeRRT(og::PathGeometric geopath);
		//! Procedure to create a mission file using the resulting path
		void createMissionFile(og::PathGeometric geopath);
		//! Procedure to extract Dubins path.
		void extractDubinsPath(og::PathGeometric path, cola2_msgs::WorldPathReqGoal *path_req_msg);
		//! Callback for getting current vehicle position
		void navCallback(const nav_msgs::OdometryConstPtr &odom_msg);
    private:
		//Profiling files
		std::fstream log_file_;

    	//ROS
		ros::NodeHandle node_handler_;
		ros::Timer timer_;
		ros::Publisher rrt_path_pub_, online_traj_point_pub_, online_traj_pub_, cancel_wps_pub_, online_traj_pose_pub_, path_sections_pub_;
		ros::Subscriber nav_sts_sub_;
		boost::shared_ptr<actionlib::SimpleActionClient<cola2_msgs::WorldPathReqAction> > path_act_lib_action_client_;

		tf::Pose last_pose_;

		//OMPL, online planner
		og::SimpleSetupPtr simple_setup_;
		double planning_depth_, timer_period_, solving_time_, surge_speed_, max_vel_yaw_, turning_radius_, ascending_speed_, descending_speed_;
		bool lazy_collision_eval_, use_new_sampler_, motion_cost_interpolation_, nav_sts_available_;
		std::vector<double> planning_bounds_x_, planning_bounds_y_, planning_bounds_z_, start_state_, goal_state_;
		std::string planner_name_, state_space_type_, optimization_objective_, scen_name_test_numb_, log_file_path_;
		std::vector<const ob::State *> path_states_;
};

//!  Constructor.
/*!
 * Load planner parameters from configuration file.
 * Publishers to visualize the resulting path.
*/
OfflinePlannFramework::OfflinePlannFramework()
{
	//=======================================================================
	// Get parameters
	//=======================================================================
	planning_bounds_x_.resize(2);
	planning_bounds_y_.resize(2);
	planning_bounds_z_.resize(2);
	start_state_.resize(4);
	goal_state_.resize(4);

	node_handler_.param("planning_framework/planning_bounds_x", planning_bounds_x_, planning_bounds_x_);
	node_handler_.param("planning_framework/planning_bounds_y", planning_bounds_y_, planning_bounds_y_);
	node_handler_.param("planning_framework/planning_bounds_z", planning_bounds_z_, planning_bounds_z_);
	node_handler_.param("planning_framework/planning_depth", planning_depth_, planning_depth_);
	node_handler_.param("planning_framework/start_state", start_state_, start_state_);
	node_handler_.param("planning_framework/goal_state", goal_state_, goal_state_);
	node_handler_.param("planning_framework/timer_period", timer_period_, timer_period_);
	node_handler_.param("planning_framework/offline_solving_time", solving_time_, solving_time_);
	node_handler_.param("planning_framework/lazy_collision_eval", lazy_collision_eval_, lazy_collision_eval_);
	node_handler_.param("planning_framework/planner_name", planner_name_, planner_name_);
	node_handler_.param("planning_framework/use_new_sampler", use_new_sampler_, use_new_sampler_);
	node_handler_.param("planning_framework/state_space_type", state_space_type_, state_space_type_);
	node_handler_.param("planning_framework/optimization_objective", optimization_objective_, optimization_objective_);
	node_handler_.param("planning_framework/motion_cost_interpolation", motion_cost_interpolation_, motion_cost_interpolation_);
	node_handler_.param("planning_framework/scen_name_test_numb", scen_name_test_numb_, scen_name_test_numb_);
	//node_handler_.param("/pilot/los_cte_max_surge_velocity", surge_speed_, surge_speed_);
	//node_handler_.param("/pilot/path_surge_speed", surge_speed_, surge_speed_);
	node_handler_.param("/planning_framework/surge_speed", surge_speed_, surge_speed_);
	node_handler_.param("planning_framework/ascending_speed", ascending_speed_, ascending_speed_);
	node_handler_.param("planning_framework/descending_speed", descending_speed_, descending_speed_);
	node_handler_.param("/controller/max_velocity_yaw", max_vel_yaw_, max_vel_yaw_);
	turning_radius_ = surge_speed_/0.1;

	//=======================================================================
	// Subscribers
	//=======================================================================
	//Navigation data (feedback)
	nav_sts_sub_ = node_handler_.subscribe("/pose_ekf_slam/odometry", 1, &OfflinePlannFramework::navCallback, this);
	nav_sts_available_ = false;

	//=======================================================================
	// Publishers
	//=======================================================================
	online_traj_point_pub_ = node_handler_.advertise<geometry_msgs::Point> ("/jdhv_robotics/online_traj_point", 1, true);
	online_traj_pub_ = node_handler_.advertise<visualization_msgs::Marker> ("/jdhv_robotics/online_traj", 1, true);
	online_traj_pose_pub_ = node_handler_.advertise<geometry_msgs::PoseArray>("/jdhv_robotics/online_traj_pose", 1, true);
	cancel_wps_pub_ = node_handler_.advertise<std_msgs::Bool> ("/jdhv_robotics/cancel_wps", 1, true);

	//=======================================================================
	// Action lib params
	//=======================================================================
//    self.wp_act_lib_conf_ = cola2_ros_lib.Config()
//    self.wp_act_lib_conf_.trajectory_step = 0.25
//    self.wp_act_lib_goal_id_ = 36000
	ROS_INFO("%s: creating action client for paths", ros::this_node::getName().c_str());
	path_act_lib_action_client_ = boost::shared_ptr<actionlib::SimpleActionClient<
	        cola2_msgs::WorldPathReqAction> >(
	        new actionlib::SimpleActionClient<cola2_msgs::WorldPathReqAction>(
	        "world_path_req", true));
	ROS_WARN("%s: waiting for action server for paths", ros::this_node::getName().c_str());
    path_act_lib_action_client_->waitForServer();
    ROS_INFO("%s: ready to use action client for paths", ros::this_node::getName().c_str());
//    self.is_following_sect = False

	//=======================================================================
	// Waiting for nav sts
	//=======================================================================
	ros::Rate loop_rate(10);
	if(!nav_sts_available_)
		ROS_WARN("%s: waiting for navigation data", ros::this_node::getName().c_str());
	while (!nav_sts_available_ && ros::ok())
	{
		ros::spinOnce();
		loop_rate.sleep();
	}
	ROS_WARN("%s: navigation data received", ros::this_node::getName().c_str());
}

//!  Planner setup.
/*!
 * Setup a sampling-based planner using OMPL.
*/
void OfflinePlannFramework::planWithSimpleSetup()
{
	//=======================================================================
	// Instantiate the state space
	//=======================================================================
	ob::StateSpacePtr space;
	if(state_space_type_.compare("Dubins")==0)
		space = ob::StateSpacePtr(new ob::DubinsStateSpace(turning_radius_));
	else if(state_space_type_.compare("Dubins3D")==0)
		space = ob::StateSpacePtr(new ob::Dubins3DStateSpace(turning_radius_));
	else if(state_space_type_.compare("R2")==0)
		space = ob::StateSpacePtr(new ob::RealVectorStateSpace(2));
	else//R2
		space = ob::StateSpacePtr(new ob::RealVectorStateSpace(2));

	// Set the bounds for the state space
	if(state_space_type_.compare("Dubins3D")==0)
	{
		ob::RealVectorBounds bounds(3);

		bounds.setLow(0, planning_bounds_x_[0]);
		bounds.setHigh(0, planning_bounds_x_[1]);
		bounds.setLow(1, planning_bounds_y_[0]);
		bounds.setHigh(1, planning_bounds_y_[1]);
		bounds.setLow(2, planning_bounds_z_[0]);
		bounds.setHigh(2, planning_bounds_z_[1]);

		space->as<ob::Dubins3DStateSpace>()->setBounds(bounds);
	}
	else if(state_space_type_.compare("Dubins")==0 || state_space_type_.compare("R2")==0)
	{
		ob::RealVectorBounds bounds(2);

		bounds.setLow(0, planning_bounds_x_[0]);
		bounds.setHigh(0, planning_bounds_x_[1]);
		bounds.setLow(1, planning_bounds_y_[0]);
		bounds.setHigh(1, planning_bounds_y_[1]);

		if(state_space_type_.compare("Dubins")==0)
			space->as<ob::DubinsStateSpace>()->setBounds(bounds);
		else if(state_space_type_.compare("R2")==0)
			space->as<ob::RealVectorStateSpace>()->setBounds(bounds);
	}

	//=======================================================================
	// Define a simple setup class
	//=======================================================================
	simple_setup_ = og::SimpleSetupPtr( new og::SimpleSetup(space) );
	ob::SpaceInformationPtr si = simple_setup_->getSpaceInformation();

	//=======================================================================
	// Create and set a motion validator
	//=======================================================================
	if(state_space_type_.compare("Dubins3D")==0)
	{
		ob::MotionValidatorPtr motion_validator;
		motion_validator = ob::MotionValidatorPtr(new ob::Dubins3DMotionValidator(si, surge_speed_, ascending_speed_, descending_speed_));
		si->setMotionValidator(motion_validator);
	}
	else if(state_space_type_.compare("Dubins")==0)
	{
		ob::MotionValidatorPtr motion_validator;
		motion_validator = ob::MotionValidatorPtr(new ob::DubinsMotionValidator(si));
		si->setMotionValidator(motion_validator);
	}

	//=======================================================================
	// Create a planner for the defined space
	//=======================================================================
	ob::PlannerPtr planner;
	if(planner_name_.compare("RRT")==0)
		planner = ob::PlannerPtr(new og::RRT(si));
	if(planner_name_.compare("PRMstar")==0)
		planner = ob::PlannerPtr(new og::PRMstar(si));
	else if(planner_name_.compare("RRTstar")==0)
		planner = ob::PlannerPtr(new og::RRTstar(si));
	else
		planner = ob::PlannerPtr(new og::RRT(si));

	//=======================================================================
	// Set the setup planner
	//=======================================================================
	simple_setup_->setPlanner(planner);

	//=======================================================================
	// Create a start and goal states
	//=======================================================================
	ob::ScopedState<> start(space);

	start[0] = double(start_state_[0]); // x
	start[1] = double(start_state_[1]); // y

	if(state_space_type_.compare("Dubins")==0)
		start[2] = double(start_state_[3]); // yaw
	else if(state_space_type_.compare("Dubins3D")==0)
	{
		start[2] = double(start_state_[2]); // z
		start[3] = double(start_state_[3]); // yaw
	}
	// create a goal state
	ob::ScopedState<> goal(space);

	goal[0] = double(goal_state_[0]); // x
	goal[1] = double(goal_state_[1]); // y

	if(state_space_type_.compare("Dubins")==0)
		goal[2] = double(goal_state_[3]); // yaw
	else if(state_space_type_.compare("Dubins3D")==0)
	{
		goal[2] = double(goal_state_[2]); // z
		goal[3] = double(goal_state_[3]); // yaw
	}
	//=======================================================================
	// Set the start and goal states
	//=======================================================================
	simple_setup_->setStartAndGoalStates(start, goal);

	//=======================================================================
	// Set optimization objective
	//=======================================================================
	if(optimization_objective_.compare("PathLength")==0)//path length Objective
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));
	else if(optimization_objective_.compare("PathLengthClearance")==0)//balanced objective path length and clearance
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective1(si));
	else if(optimization_objective_.compare("RiskZones")==0)//approximate clearance (integral of clearance)
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getRiskZonesObjective(si, motion_cost_interpolation_));
	else if(optimization_objective_.compare("DirVectorsRisk")==0)//approximate risk (integral of risk)
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getDirVectorsRiskObjective(si));
	else
		simple_setup_->getProblemDefinition()->setOptimizationObjective(getPathLengthObjective(si));

	//path clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getClearanceObjective(si));

	//path length and clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective1(si));

	//path length and clearance Objective
	//simple_setup_->getProblemDefinition()->setOptimizationObjective(getBalancedObjective2(si));

	//=======================================================================
	// Perform setup steps for the planner
	//=======================================================================
	simple_setup_->setup();

	//=======================================================================
	// Print information
	//=======================================================================
	//planner->printProperties(std::cout);// print planner properties
	//si->printSettings(std::cout);// print the settings for this space
	//=======================================================================
	// Cleaning profile file
	//=======================================================================
	std::string jdhv_mp_pkg_path = ros::package::getPath("jdhv_mp");
	log_file_path_ = jdhv_mp_pkg_path + "/src/rice/logs/" + "offline" + scen_name_test_numb_ + "_" + optimization_objective_ + ".txt";
	log_file_.open(log_file_path_.c_str(), ios::out | ios::trunc );
	log_file_.close();

	//=======================================================================
	// Activate a timer for incremental planning
	//=======================================================================
	timer_ = node_handler_.createTimer(ros::Duration(timer_period_), &OfflinePlannFramework::planningTimerCallback, this);

	//ros::spin();
}

//!  Periodic callback to solve the query.
/*!
 * Solve the query.
*/
void OfflinePlannFramework::planningTimerCallback(const ros::TimerEvent &e)
{
	if(path_states_.size() > 0)
	{
		double dist_current_start = sqrt(pow(start_state_[0]-last_pose_.getOrigin().getX(), 2.0)+pow(start_state_[1]-last_pose_.getOrigin().getY(), 2.0));
		double dist_nxt_start = sqrt(pow(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getX()-last_pose_.getOrigin().getX(), 2.0)+pow(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getY()-last_pose_.getOrigin().getY(), 2.0));

		if(dist_nxt_start < 2.0)
		{
			//=======================================================================
			// Create a start and goal states
			//=======================================================================
			ob::ScopedState<> start(simple_setup_->getStateSpace());

			start[0] = double(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getX()); // x
			start[1] = double(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getY()); // y

			if(state_space_type_.compare("Dubins")==0)
				start[2] = double(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getYaw()); // yaw
//			else if(state_space_type_.compare("Dubins3D")==0)
//			{
//				start[2] = double(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getZ()); // z
//				start[3] = double(path_states_[path_states_.size()-1]->as<ob::DubinsStateSpace::StateType>()->getYaw()); // yaw
//			}
			//=======================================================================
			// Set the start and goal states
			//=======================================================================
			simple_setup_->clearStartStates();
			simple_setup_->setStartState(start);
			path_states_.pop_back();
		}
	}
	//=======================================================================
	// Set a modified sampler
	//=======================================================================
	if(use_new_sampler_)
		simple_setup_->getSpaceInformation()->getStateSpace()->setStateSamplerAllocator(boost::bind(newAllocStateSampler, _1, path_states_));

	//=======================================================================
	// Set state validity checking for this space
	//=======================================================================
	ob::StateValidityCheckerPtr om_stat_val_check;
	if(state_space_type_.compare("Dubins")==0)
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerSE2(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_));
	else if(state_space_type_.compare("Dubins3D")==0)
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerSE2R(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_, planning_bounds_z_));
	else if(state_space_type_.compare("R2")==0)
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerR2(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_));
	else//R2
		om_stat_val_check = ob::StateValidityCheckerPtr(new OmFclStateValidityCheckerR2(simple_setup_->getSpaceInformation(), planning_depth_, lazy_collision_eval_, planning_bounds_x_, planning_bounds_y_));
	simple_setup_->setStateValidityChecker(om_stat_val_check);

	//=======================================================================
	// Profiling performance
	//=======================================================================
	log_file_.open(log_file_path_.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

	//ompl::tools::Profiler::Clear();
	//ompl::tools::Profiler::Start();

	//=======================================================================
	// Attempt to solve the problem within one second of planning time
	//=======================================================================
	ob::PlannerStatus solved = simple_setup_->solve(solving_time_);

	//=======================================================================
	// Getting data from profiler
	//=======================================================================
	//ompl::tools::Profiler::Stop();
	//ompl::tools::Profiler::Status(log_file_);
	//ompl::tools::Profiler::Status();

	if (solved)
	{
		// get the goal representation from the problem definition (not the same as the goal state)
		// and inquire about the found path
		// simple_setup_->simplifySolution();
		cola2_msgs::WorldPathReqGoal path_req_msg;
		path_req_msg.priority = 10;
		og::PathGeometric path = simple_setup_->getSolutionPath();
		extractDubinsPath(path, &path_req_msg);

		//path_sections_pub_.publish(section_list);
		path_act_lib_action_client_->sendGoal(path_req_msg);

		ROS_INFO("%s: path with cost %f has been found with simple_setup", ros::this_node::getName().c_str(), path.cost(simple_setup_->getProblemDefinition()->getOptimizationObjective()).value());
		path.interpolate(int(path.length()/2.0));
		visualizeRRT(path);
		createMissionFile(path);

		if(use_new_sampler_)
		{
			std::vector<ob::State*> path_states;
			path_states = path.getStates();

			ob::StateSpacePtr space = simple_setup_->getStateSpace();
			path_states_.clear();
			path_states_.reserve(path_states.size()-1);
			//for (std::vector<ob::State *>::const_iterator st = path_states.begin(); st != path_states.end(); ++st)
			for (size_t i = path_states.size()-1; i > 0; i--)
			{
				ob::State *s = space->allocState();
				space->copyState(s, path_states[i]);
				path_states_.push_back(s);
			}
		}
		//=======================================================================
		// Clear previous solution path
		//=======================================================================
		simple_setup_->clear();
	}
	else
		ROS_INFO("%s: path has not been found", ros::this_node::getName().c_str());

	log_file_.close();
}

//! Resulting path visualization.
/*!
 * Visualize resulting path.
*/
void OfflinePlannFramework::visualizeRRT(og::PathGeometric geopath)
{
	// %Tag(MARKER_INIT)%
	geometry_msgs::PoseArray pose_path;
	tf::Quaternion orien_quat;
	visualization_msgs::Marker q_init_goal, visual_rrt, result_path, visual_result_path;
	pose_path.header.frame_id = visual_result_path.header.frame_id = result_path.header.frame_id = q_init_goal.header.frame_id = visual_rrt.header.frame_id =  "/world";
	pose_path.header.stamp = visual_result_path.header.stamp = result_path.header.stamp = q_init_goal.header.stamp = visual_rrt.header.stamp = ros::Time::now();
	q_init_goal.ns = "online_planner_points";
	visual_rrt.ns = "online_planner_rrt";
	result_path.ns = "online_planner_result";
	visual_result_path.ns = "online_planner_result_path";
	visual_result_path.action = result_path.action = q_init_goal.action = visual_rrt.action = visualization_msgs::Marker::ADD;

	visual_result_path.pose.orientation.w = result_path.pose.orientation.w = q_init_goal.pose.orientation.w = visual_rrt.pose.orientation.w = 1.0;
	// %EndTag(MARKER_INIT)%

	// %Tag(ID)%
	q_init_goal.id = 0;
	visual_rrt.id = 1;
	result_path.id = 2;
	visual_result_path.id = 3;
	// %EndTag(ID)%

	// %Tag(TYPE)%
	result_path.type = q_init_goal.type = visualization_msgs::Marker::POINTS;
	visual_rrt.type = visual_result_path.type = visualization_msgs::Marker::LINE_LIST;
	// %EndTag(TYPE)%

	// %Tag(SCALE)%
	// POINTS markers use x and y scale for width/height respectively
	result_path.scale.x = q_init_goal.scale.x = 0.5;
	result_path.scale.y = q_init_goal.scale.y = 0.5;
	result_path.scale.z = q_init_goal.scale.z = 0.5;

	// LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
	visual_rrt.scale.x = 0.05;
	visual_result_path.scale.x = 0.20;
	// %EndTag(SCALE)%

	// %Tag(COLOR)%
	// Points are green
	visual_result_path.color.r = 1.0;
	result_path.color.b = q_init_goal.color.g = 1.0;
	visual_result_path.color.a = result_path.color.a = q_init_goal.color.a = 1.0;

	// Line strip is blue
	visual_rrt.color.b = 1.0;
	visual_rrt.color.a = 1.0;

	const ob::DubinsStateSpace::StateType *state_dubins_2D;
	const ob::RealVectorStateSpace::StateType *state_r2;
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;

	geometry_msgs::Point p;

	ob::PlannerData planner_data(simple_setup_->getSpaceInformation());
	simple_setup_->getPlannerData(planner_data);

	std::vector< unsigned int > edgeList;
	int num_parents;
	//ROS_INFO("%s: number of states in the tree: %d", ros::this_node::getName().c_str(), planner_data.numVertices());
	log_file_ << "Number of tree nodes: " << planner_data.numVertices() << "\n";

	for (unsigned int i = 1 ; i < planner_data.numVertices() ; ++i)
	{
		if (planner_data.getVertex(i).getState() && planner_data.getIncomingEdges(i,edgeList) > 0)
		{
			if(state_space_type_.compare("Dubins")==0)
			{
				state_dubins_2D = planner_data.getVertex(i).getState()->as<ob::DubinsStateSpace::StateType>();
				p.x = state_dubins_2D->getX();
				p.y = state_dubins_2D->getY();
				p.z = planning_depth_;
			}
			else if(state_space_type_.compare("Dubins3D")==0)
			{
				state_dubins_3D = planner_data.getVertex(i).getState()->as<ob::Dubins3DStateSpace::StateType>();
				p.x = state_dubins_3D->getX();
				p.y = state_dubins_3D->getY();
				p.z = state_dubins_3D->getZ();
			}
			else//R2
			{
				state_r2 = planner_data.getVertex(i).getState()->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
				p.z = planning_depth_;
			}
			visual_rrt.points.push_back(p);

			if(state_space_type_.compare("Dubins")==0)
			{
				state_dubins_2D = planner_data.getVertex(edgeList[0]).getState()->as<ob::DubinsStateSpace::StateType>();
				p.x = state_dubins_2D->getX();
				p.y = state_dubins_2D->getY();
				p.z = planning_depth_;
			}
			else if(state_space_type_.compare("Dubins3D")==0)
			{
				state_dubins_3D = planner_data.getVertex(edgeList[0]).getState()->as<ob::Dubins3DStateSpace::StateType>();
				p.x = state_dubins_3D->getX();
				p.y = state_dubins_3D->getY();
				p.z = state_dubins_3D->getZ();
			}
			else
			{
				state_r2 = planner_data.getVertex(edgeList[0]).getState()->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
				p.z = planning_depth_;
			}
			visual_rrt.points.push_back(p);
		}
	}
	std::vector< ob::State * > states = geopath.getStates();

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		// extract the component of the state and cast it to what we expect
		if(state_space_type_.compare("Dubins")==0)
		{
			state_dubins_2D = states[i]->as<ob::DubinsStateSpace::StateType>();
			p.x = state_dubins_2D->getX();
			p.y = state_dubins_2D->getY();
			p.z = planning_depth_;

			//--poses
			geometry_msgs::Pose pose;

			orien_quat.setRPY(0., 0., state_dubins_2D->getYaw());

			pose.position.x = p.x;
			pose.position.y = p.y;
			pose.position.z = p.z;
			pose.orientation.x = orien_quat.getX();
			pose.orientation.y = orien_quat.getY();
			pose.orientation.z = orien_quat.getZ();
			pose.orientation.w = orien_quat.getW();

			pose_path.poses.push_back(pose);

			result_path.points.push_back(p);
		}
		else if(state_space_type_.compare("Dubins3D")==0)
		{
			state_dubins_3D = states[i]->as<ob::Dubins3DStateSpace::StateType>();
			p.x = state_dubins_3D->getX();
			p.y = state_dubins_3D->getY();
			p.z = state_dubins_3D->getZ();

			//--poses
			geometry_msgs::Pose pose;

			orien_quat.setRPY(0., 0., state_dubins_3D->getYaw());

			pose.position.x = p.x;
			pose.position.y = p.y;
			pose.position.z = p.z;
			pose.orientation.x = orien_quat.getX();
			pose.orientation.y = orien_quat.getY();
			pose.orientation.z = orien_quat.getZ();
			pose.orientation.w = orien_quat.getW();

			pose_path.poses.push_back(pose);

			result_path.points.push_back(p);
		}
		else
		{
			state_r2 = states[i]->as<ob::RealVectorStateSpace::StateType>();
			p.x = state_r2->values[0];
			p.y = state_r2->values[1];
			p.z = planning_depth_;
		}
		result_path.points.push_back(p);

		if(i>0)
		{
			if(state_space_type_.compare("R2")==0)
			{
				visual_result_path.points.push_back(p);

				state_r2 = states[i-1]->as<ob::RealVectorStateSpace::StateType>();
				p.x = state_r2->values[0];
				p.y = state_r2->values[1];
				p.z = planning_depth_;
				visual_result_path.points.push_back(p);
			}
		}
	}

	//online_traj_pub_.publish(q_init_goal);
	online_traj_pub_.publish(visual_rrt);
	online_traj_pub_.publish(visual_result_path);
	online_traj_pose_pub_.publish(pose_path);
	online_traj_pub_.publish(result_path);
}

//! Mission file.
/*!
 * Create mission file from resulting path.
*/
void OfflinePlannFramework::createMissionFile(og::PathGeometric geopath)
{
	std::vector< ob::State * > states = geopath.getStates();
	const ob::DubinsStateSpace::StateType *state_dubins_2D;
	const ob::RealVectorStateSpace::StateType *state_r2;
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;

	std::string mission_north_coords = "trajectory/north: [";
	std::string mission_east_coords = "trajectory/east: [";
	std::string mission_depth_coords = "trajectory/z: [";
	std::string altitude_modes = "trajectory/altitude_mode: [";
	std::string wait_times = "trajectory/wait: [";

	for (uint32_t i = 0; i < geopath.getStateCount(); ++i)
	{
		std::ostringstream north_coord;
		std::ostringstream east_coord;
		std::ostringstream depth_coord;

		// extract the component of the state and cast it to what we expect
		if(state_space_type_.compare("Dubins")==0)
		{
			state_dubins_2D = states[i]->as<ob::DubinsStateSpace::StateType>();
			north_coord << state_dubins_2D->getX();
			east_coord << state_dubins_2D->getY();
			depth_coord << planning_depth_;
		}
		else if(state_space_type_.compare("Dubins3D")==0)
		{
			state_dubins_3D = states[i]->as<ob::Dubins3DStateSpace::StateType>();
			north_coord << state_dubins_3D->getX();
			east_coord << state_dubins_3D->getY();
			depth_coord << state_dubins_3D->getZ();
		}
		else// R2 if(state_space_type_.compare("Dubins3D")==0)
		{
			state_r2 = states[i]->as<ob::RealVectorStateSpace::StateType>();
			north_coord << state_r2->values[0];
			east_coord << state_r2->values[1];
			depth_coord << planning_depth_;
		}

		if(i==0)
		{
			mission_north_coords = mission_north_coords + north_coord.str() + ", ";
			mission_east_coords = mission_east_coords + east_coord.str() + ", ";
			mission_depth_coords = mission_depth_coords + "0.0, ";
			altitude_modes = altitude_modes + "False, ";
			wait_times = wait_times + "0.0, ";
		}

		if(i!=0)
		{
			mission_north_coords = mission_north_coords + ", ";
			mission_east_coords = mission_east_coords + ", ";
			mission_depth_coords = mission_depth_coords + ", ";
			altitude_modes = altitude_modes + ", ";
			wait_times = wait_times + ", ";
		}


		mission_north_coords = mission_north_coords + north_coord.str();
		mission_east_coords = mission_east_coords + east_coord.str();
		mission_depth_coords = mission_depth_coords + depth_coord.str();
		altitude_modes = altitude_modes + "False";
		wait_times = wait_times + "0.0";

		if(i==(geopath.getStateCount()-1))
		{
			mission_north_coords = mission_north_coords + ", " + north_coord.str() + "]\n";
			mission_east_coords = mission_east_coords + ", "+ east_coord.str() + "]\n";
			mission_depth_coords = mission_depth_coords + ", 0.0" + "]\n";
			altitude_modes = altitude_modes + ", False" + "]\n";
			wait_times = wait_times + ", 0.0" + "]\n";
		}
	}

	std::string mission_path = ros::package::getPath("jdhv_scenes");
	mission_path = mission_path + "/launch/config/missions/mission.yaml";

	ofstream myfile;
	myfile.open (mission_path.c_str());
	myfile << mission_north_coords;
	myfile << mission_east_coords;
	myfile << mission_depth_coords;
	myfile << altitude_modes;
	myfile << "trajectory/mode: 'sparus_los'\n";
	myfile << "trajectory/time_out: 10\n";
	myfile << wait_times;

	myfile.close();
}

//! Extract Dubins path.
/*!
 * Extract the Dubins path, i.e., arcs and straight segments.
*/
void OfflinePlannFramework::extractDubinsPath(og::PathGeometric path, cola2_msgs::WorldPathReqGoal *path_req_msg)
{
	//TODO generalize function
	//std::cout << "number of states given in the initial solution: " << path.getStateCount() << std::endl;
	std::vector<ob::State*> dubins_path_states;
	dubins_path_states = path.getStates();

	ob::DubinsStateSpace *dubins_2d_state_space;
	ob::Dubins3DStateSpace *dubins_3d_state_space;

	if(state_space_type_.compare("Dubins")==0)
		dubins_2d_state_space = dynamic_cast<ob::DubinsStateSpace*>(simple_setup_->getStateSpace().get());
	else
		dubins_3d_state_space = dynamic_cast<ob::Dubins3DStateSpace*>(simple_setup_->getStateSpace().get());


	ob::DubinsStateSpace::DubinsPath dubins_2d_path;
	ob::Dubins3DStateSpace::DubinsPath dubins_3d_path;

	const ob::State *state_from;
	const ob::State *state_to;

	double seg = dubins_2d_path.length(), phi, v;
	int dubins_path_type_i;
	double state_x, state_y, state_yaw, total_length, initial_z, delta_z;
	std::vector<double> dubins_section_from(3);
	std::vector<double> dubins_section_to(3);

	for (size_t i = 0; i < dubins_path_states.size()-1; i++)
	{
		state_from = path.getState(i);
		state_to = path.getState(i+1);

		state_x = 0.0;
		state_y = 0.0;
		bool dubing_reverse;

		if(state_space_type_.compare("Dubins")==0)
		{
			dubins_2d_path = dubins_2d_state_space->dubins(state_from, state_to);
			seg = dubins_2d_path.length();
			state_yaw = state_from->as<ob::Dubins3DStateSpace::StateType>()->getYaw();
			dubing_reverse = dubins_2d_path.reverse_;
			total_length = dubins_2d_path.length();
		}
		else
		{
			dubins_3d_path = dubins_3d_state_space->dubins(state_from, state_to);
			seg = dubins_3d_path.length();
			state_yaw = state_from->as<ob::Dubins3DStateSpace::StateType>()->getYaw();
			dubing_reverse = dubins_3d_path.reverse_;
			total_length = dubins_3d_path.length();
			initial_z = state_from->as<ob::Dubins3DStateSpace::StateType>()->getZ();
			delta_z = state_to->as<ob::Dubins3DStateSpace::StateType>()->getZ() - initial_z;
		}

//		if(state_space_type_.compare("Dubins")==0)
//		{
//			std::cout << "*** from: [ " << state_from->as<ob::DubinsStateSpace::StateType>()->getX() << ", " << state_from->as<ob::DubinsStateSpace::StateType>()->getY() /*<< ", " << state_from->as<ob::DubinsStateSpace::StateType>()->getZ()*/ << "], " << state_from->as<ob::DubinsStateSpace::StateType>()->getYaw() << std::endl;
//			std::cout << "*** to: [" << state_to->as<ob::DubinsStateSpace::StateType>()->getX() << ", " << state_to->as<ob::DubinsStateSpace::StateType>()->getY() /*<< ", " << state_to->as<ob::DubinsStateSpace::StateType>()->getZ()*/ << "], " << state_to->as<ob::DubinsStateSpace::StateType>()->getYaw() << std::endl;
//			std::cout << "length from state " << i << " to state " << i+1 << " : " << dubins_2d_path.length() << "\n" << std::endl;
//			std::cout << "distance from state " << i << " to state " << i+1 << " : " << dubins_2d_state_space->distance(state_from, state_to) << "\n" << std::endl;
//		}
//		else
//		{
//			std::cout << "*** from: [ " << state_from->as<ob::Dubins3DStateSpace::StateType>()->getX() << ", " << state_from->as<ob::Dubins3DStateSpace::StateType>()->getY() << ", " << state_from->as<ob::Dubins3DStateSpace::StateType>()->getZ() << "], " << state_from->as<ob::Dubins3DStateSpace::StateType>()->getYaw() << std::endl;
//			std::cout << "*** to: [" << state_to->as<ob::Dubins3DStateSpace::StateType>()->getX() << ", " << state_to->as<ob::Dubins3DStateSpace::StateType>()->getY() << ", " << state_to->as<ob::Dubins3DStateSpace::StateType>()->getZ() << "], " << state_to->as<ob::Dubins3DStateSpace::StateType>()->getYaw() << std::endl;
//			std::cout << "length from state " << i << " to state " << i+1 << " : " << dubins_3d_path.length() << "\n" << std::endl;
//			std::cout << "distance from state " << i << " to state " << i+1 << " : " << dubins_3d_state_space->distance(state_from, state_to) << "\n" << std::endl;
//		}



		if (!dubing_reverse)
		{
			for (unsigned int i=0; i<3 && seg>0; ++i)
			{
				if(state_space_type_.compare("Dubins")==0)
				{
					v = std::min(seg, dubins_2d_path.length_[i]);
					dubins_path_type_i = dubins_2d_path.type_[i];
				}
				else
				{
					v = std::min(seg, dubins_3d_path.length_[i]);
					dubins_path_type_i = dubins_3d_path.type_[i];
				}

				phi = state_yaw;
				seg -= v;

				switch(dubins_path_type_i)
				{
				case ob::DubinsStateSpace::DUBINS_LEFT:
					state_x = state_x + sin(phi+v) - sin(phi);
					state_y = state_y - cos(phi+v) + cos(phi);
					state_yaw = phi+v;
					break;
				case ob::DubinsStateSpace::DUBINS_RIGHT:
					state_x = state_x - sin(phi-v) + sin(phi);
					state_y = state_y + cos(phi-v) - cos(phi);
					state_yaw = phi-v;
					break;
				case ob::DubinsStateSpace::DUBINS_STRAIGHT:
					state_x = state_x + v * cos(phi);
					state_y = state_y + v * sin(phi);
					break;
				}

				if(i==0)
				{
					if(state_space_type_.compare("Dubins")==0)
					{
						dubins_section_from[0] = state_from->as<ob::DubinsStateSpace::StateType>()->getX();
						dubins_section_from[1] = state_from->as<ob::DubinsStateSpace::StateType>()->getY();
						dubins_section_from[2] = state_from->as<ob::DubinsStateSpace::StateType>()->getYaw();
					}
					else
					{
						dubins_section_from[0] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getX();
						dubins_section_from[1] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getY();
						dubins_section_from[2] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getYaw();
					}
				}
				else
					dubins_section_from = dubins_section_to;

				if(state_space_type_.compare("Dubins")==0)
				{
					dubins_section_to[0] = state_x*turning_radius_+state_from->as<ob::DubinsStateSpace::StateType>()->getX();
					dubins_section_to[1] = state_y*turning_radius_+state_from->as<ob::DubinsStateSpace::StateType>()->getY();
				}
				else
				{
					dubins_section_to[0] = state_x*turning_radius_+state_from->as<ob::Dubins3DStateSpace::StateType>()->getX();
					dubins_section_to[1] = state_y*turning_radius_+state_from->as<ob::Dubins3DStateSpace::StateType>()->getY();
				}
				dubins_section_to[2] = state_yaw;

//				if(state_space_type_.compare("Dubins")==0)
//				{
//					std::cout << "type: " << dubins_2d_path.type_[i] << std::endl;
//					std::cout << "length_: " << dubins_2d_path.length_[i] << std::endl;
//					std::cout << "from-to:\n section.initial_x = " << dubins_section_from[0] << ";\n section.initial_y = " << dubins_section_from[1] << ";\n section.initial_yaw = " << dubins_section_from[2] << ";" << std::endl;
//					std::cout << " section.final_x = " << dubins_section_to[0] << ";\n section.final_y = " << dubins_section_to[1] << ";\n section.final_yaw = " << dubins_section_to[2] << ";\n" << std::endl;
//				}
//				else
//				{
//					std::cout << "type: " << dubins_3d_path.type_[i] << std::endl;
//					std::cout << "length_: " << dubins_3d_path.length_[i] << std::endl;
//					std::cout << "from-to:\n section.initial_x = " << dubins_section_from[0] << ";\n section.initial_y = " << dubins_section_from[1] << ";\n section.initial_z = " << initial_z << ";\n section.initial_yaw = " << dubins_section_from[2] << ";" << std::endl;
//					std::cout << " section.final_x = " << dubins_section_to[0] << ";\n section.final_y = " << dubins_section_to[1] << ";\n section.final_z = " << initial_z + delta_z*(dubins_3d_path.length_[i]/total_length) << ";\n section.final_yaw = " << dubins_section_to[2] << ";\n" << std::endl;
//				}

				cola2_msgs::Section section;

				section.initial_position.x = dubins_section_from[0];
				section.initial_position.y = dubins_section_from[1];
				if(state_space_type_.compare("Dubins")==0)
					section.initial_position.z = planning_depth_;
				else
					section.initial_position.z = initial_z;
				section.initial_yaw = dubins_section_from[2];
				section.use_initial_yaw = false;

				section.final_position.x = dubins_section_to[0];
				section.final_position.y = dubins_section_to[1];
				if(state_space_type_.compare("Dubins")==0)
					section.final_position.z = planning_depth_;
				else
				{
					section.final_position.z = initial_z + delta_z*(dubins_3d_path.length_[i]/total_length);
					initial_z = section.final_position.z;
				}
				section.final_yaw = dubins_section_to[2];
				section.use_final_yaw = true;
				section.altitude_mode = false;
				section.surge_speed = surge_speed_;

				path_req_msg->data.push_back(section);
			}
		}
		else
		{
			for (unsigned int i=0; i<3 && seg>0; ++i)
			{
				if(state_space_type_.compare("Dubins")==0)
				{
					v = std::min(seg, dubins_2d_path.length_[2-i]);
					dubins_path_type_i = dubins_2d_path.type_[i];
				}
				else
				{
					v = std::min(seg, dubins_3d_path.length_[2-i]);
					dubins_path_type_i = dubins_3d_path.type_[i];
				}

				phi = state_yaw;
				seg -= v;
				switch(dubins_path_type_i)
				{
				case ob::DubinsStateSpace::DUBINS_LEFT:
					state_x = state_x + sin(phi-v) - sin(phi);
					state_y = state_y - cos(phi-v) + cos(phi);
					state_yaw = phi-v;
					break;
				case ob::DubinsStateSpace::DUBINS_RIGHT:
					state_x = state_x - sin(phi+v) + sin(phi);
					state_y = state_y + cos(phi+v) - cos(phi);
					state_yaw = phi+v;
					break;
				case ob::DubinsStateSpace::DUBINS_STRAIGHT:
					state_x = state_x - v * cos(phi);
					state_y = state_y - v * sin(phi);
					break;
				}

				if(i==0)
				{
					if(state_space_type_.compare("Dubins")==0)
					{
						dubins_section_from[0] = state_from->as<ob::DubinsStateSpace::StateType>()->getX();
						dubins_section_from[1] = state_from->as<ob::DubinsStateSpace::StateType>()->getY();
						dubins_section_from[2] = state_from->as<ob::DubinsStateSpace::StateType>()->getYaw();
					}
					else
					{
						dubins_section_from[0] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getX();
						dubins_section_from[1] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getY();
						dubins_section_from[2] = state_from->as<ob::Dubins3DStateSpace::StateType>()->getYaw();
					}
				}
				else
					dubins_section_from = dubins_section_to;

				if(state_space_type_.compare("Dubins")==0)
				{
					dubins_section_to[0] = state_x*turning_radius_+state_from->as<ob::DubinsStateSpace::StateType>()->getX();
					dubins_section_to[1] = state_y*turning_radius_+state_from->as<ob::DubinsStateSpace::StateType>()->getY();
				}
				else
				{
					dubins_section_to[0] = state_x*turning_radius_+state_from->as<ob::Dubins3DStateSpace::StateType>()->getX();
					dubins_section_to[1] = state_y*turning_radius_+state_from->as<ob::Dubins3DStateSpace::StateType>()->getY();
				}
				dubins_section_to[2] = state_yaw;

//				if(state_space_type_.compare("Dubins")==0)
//				{
//					std::cout << "type: " << dubins_2d_path.type_[i] << std::endl;
//					std::cout << "length_: " << dubins_2d_path.length_[i] << std::endl;
//					std::cout << "from-to:\n section.initial_x = " << dubins_section_from[0] << ";\n section.initial_y = " << dubins_section_from[1] << ";\n section.initial_yaw = " << dubins_section_from[2] << ";" << std::endl;
//					std::cout << " section.final_x = " << dubins_section_to[0] << ";\n section.final_y = " << dubins_section_to[1] << ";\n section.final_yaw = " << dubins_section_to[2] << ";\n" << std::endl;
//				}
//				else
//				{
//					std::cout << "type: " << dubins_3d_path.type_[i] << std::endl;
//					std::cout << "length_: " << dubins_3d_path.length_[i] << std::endl;
//					std::cout << "from-to:\n section.initial_x = " << dubins_section_from[0] << ";\n section.initial_y = " << dubins_section_from[1] << ";\n section.initial_z = " << initial_z << ";\n section.initial_yaw = " << dubins_section_from[2] << ";" << std::endl;
//					std::cout << " section.final_x = " << dubins_section_to[0] << ";\n section.final_y = " << dubins_section_to[1] << ";\n section.final_z = " << initial_z + delta_z*(dubins_3d_path.length_[i]/total_length) << ";\n section.final_yaw = " << dubins_section_to[2] << ";\n" << std::endl;
//				}

				cola2_msgs::Section section;

				section.initial_position.x = dubins_section_from[0];
				section.initial_position.y = dubins_section_from[1];
				if(state_space_type_.compare("Dubins")==0)
					section.initial_position.z = planning_depth_;
				else
					section.initial_position.z = initial_z;
				section.initial_yaw = dubins_section_from[2];
				section.use_initial_yaw = false;

				section.final_position.x = dubins_section_to[0];
				section.final_position.y = dubins_section_to[1];
				if(state_space_type_.compare("Dubins")==0)
					section.final_position.z = planning_depth_;
				else
				{
					section.final_position.z = initial_z + delta_z*(dubins_3d_path.length_[i]/total_length);
					initial_z = section.final_position.z;
				}
				section.final_yaw = dubins_section_to[2];
				section.use_final_yaw = true;
				section.altitude_mode = false;
				section.surge_speed = surge_speed_;

				path_req_msg->data.push_back(section);
			}
		}
	}
}

//! Navigation callback.
/*!
 * Callback for getting updated vehicle position
*/
void OfflinePlannFramework::navCallback(const nav_msgs::OdometryConstPtr &odom_msg)
{
	if(!nav_sts_available_)
		nav_sts_available_ = true;
    tf::poseMsgToTF(odom_msg->pose.pose, last_pose_);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "new_offline_approach");

	ROS_INFO("%s: offline planner (C++)", ros::this_node::getName().c_str());
	ROS_INFO("%s: using OMPL version %s", ros::this_node::getName().c_str(), OMPL_VERSION);
	ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
	//	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
	//	   ros::console::notifyLoggerLevelsChanged();
	//	}

	OfflinePlannFramework offline_framework;

	offline_framework.planWithSimpleSetup();

	ros::Rate loop_rate(10);
	while (ros::ok())
	{
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
