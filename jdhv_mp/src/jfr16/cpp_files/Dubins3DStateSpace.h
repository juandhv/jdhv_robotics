/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2010, Rice University
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Rice University nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

#ifndef OMPL_BASE_SPACES_DUBINS3D_STATE_SPACE2_
#define OMPL_BASE_SPACES_DUBINS3D_STATE_SPACE2_

#include <ompl/base/StateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include <ompl/base/MotionValidator.h>
#include <boost/math/constants/constants.hpp>

namespace ompl
{
    namespace base
    {

        /** \brief A state space representing SE(2)xR */
        class Dubins3DStateSpace : public CompoundStateSpace
        {
        public:
        	/** \brief The Dubins path segment type */
        	enum DubinsPathSegmentType { DUBINS_LEFT=0, DUBINS_STRAIGHT=1, DUBINS_RIGHT=2 };
        	/** \brief Dubins path types */
        	static const DubinsPathSegmentType dubinsPathType[6][3];
        	class DubinsPath
        	{
        	public:
        		DubinsPath(const DubinsPathSegmentType* type = dubinsPathType[0],
        				double t=0., double p=std::numeric_limits<double>::max(), double q=0.)
        			: type_(type), reverse_(false)
        		{
        			length_[0] = t;
        			length_[1] = p;
        			length_[2] = q;
        			assert(t >= 0.);
        			assert(p >= 0.);
        			assert(q >= 0.);
        		}
        		double length() const
        		{
        			return length_[0] + length_[1] + length_[2];
        		}

        		/** Path segment types */
				const DubinsPathSegmentType* type_;
        		/** Path segment lengths */
				double length_[3];
        		/** Whether the path should be followed "in reverse" */
				bool reverse_;
        	};

            /** \brief A state in SE(2)xR: (x, y, z, yaw) */
            class StateType : public CompoundStateSpace::StateType
            {
            public:
                StateType() : CompoundStateSpace::StateType()
                {
                }

                /** \brief Get the X component of the state */
                double getX() const
                {
                    return as<RealVectorStateSpace::StateType>(0)->values[0];
                }

                /** \brief Get the Y component of the state */
                double getY() const
                {
                    return as<RealVectorStateSpace::StateType>(0)->values[1];
                }

                /** \brief Get the Z component of the state */
                double getZ() const
                {
                	return as<RealVectorStateSpace::StateType>(0)->values[2];
                }

                /** \brief Get the yaw component of the state. This is
                    the rotation in plane, with respect to the Z
                    axis. */
                double getYaw() const
                {
                    return as<SO2StateSpace::StateType>(1)->value;
                }

                /** \brief Set the X component of the state */
                void setX(double x)
                {
                    as<RealVectorStateSpace::StateType>(0)->values[0] = x;
                }

                /** \brief Set the Y component of the state */
                void setY(double y)
                {
                    as<RealVectorStateSpace::StateType>(0)->values[1] = y;
                }

                /** \brief Set the Y component of the state */
                void setZ(double z)
                {
                	as<RealVectorStateSpace::StateType>(0)->values[2] = z;
                }

                /** \brief Set the X and Y components of the state */
                void setXY(double x, double y)
                {
                    setX(x);
                    setY(y);
                }

                /** \brief Set the X, Y and Z components of the state */
                void setXYZ(double x, double y, double z)
                {
                	setX(x);
                	setY(y);
                	setZ(z);
                }

                /** \brief Set the yaw component of the state. This is
                    the rotation in plane, with respect to the Z
                    axis. */
                void setYaw(double yaw)
                {
                    as<SO2StateSpace::StateType>(1)->value = yaw;
                }

            };


            Dubins3DStateSpace(double turningRadius = 1.0, bool isSymmetric = false)
            	: CompoundStateSpace(), rho_(turningRadius), isSymmetric_(isSymmetric)
            {
                setName("Dubins3D" + getName());
                type_ = STATE_SPACE_SE2;
                addSubspace(StateSpacePtr(new RealVectorStateSpace(3)), 1.0);
                addSubspace(StateSpacePtr(new SO2StateSpace()), 0.5);
                lock();
            }

            virtual bool isMetricSpace() const
            {
            	return false;
            }

            virtual double distance(const State *state1, const State *state2) const;

            virtual void interpolate(const State *from, const State *to, const double t,
            		State *state) const;
            virtual void interpolate(const State *from, const State *to, const double t,
            		bool &firstTime, DubinsPath &path, State *state) const;

            virtual bool hasSymmetricDistance() const
            {
            	return isSymmetric_;
            }

            virtual bool hasSymmetricInterpolate() const
            {
            	return isSymmetric_;
            }

            virtual void sanityChecks() const
            {
            	double zero = std::numeric_limits<double>::epsilon();
            	double eps = std::numeric_limits<float>::epsilon();
            	int flags = ~(STATESPACE_INTERPOLATION | STATESPACE_TRIANGLE_INEQUALITY | STATESPACE_DISTANCE_BOUND);
            	if (!isSymmetric_)
            		flags &= ~STATESPACE_DISTANCE_SYMMETRIC;
            	StateSpace::sanityChecks(zero, eps, flags);
            }

            /** \brief Return the shortest Dubins path from SE(2)xR state state1 to SE(2)xR state state2 */
            DubinsPath dubins(const State *state1, const State *state2) const;

            virtual ~Dubins3DStateSpace()
            {
            }

            /** \copydoc RealVectorStateSpace::setBounds() */
            void setBounds(const RealVectorBounds &bounds)
            {
                as<RealVectorStateSpace>(0)->setBounds(bounds);
            }

            /** \copydoc RealVectorStateSpace::getBounds() */
            const RealVectorBounds& getBounds() const
            {
                return as<RealVectorStateSpace>(0)->getBounds();
            }

            virtual State* allocState() const;
            virtual void freeState(State *state) const;

            virtual void registerProjections();

        protected:
            virtual void interpolate(const State *from, const DubinsPath &path, const double t,
               State *state) const;

            /** \brief Turning radius */
            double rho_;

            /** \brief Whether the distance is "symmetrized"

				If true the distance from state s1 to state s2 is the same as the
				distance from s2 to s1. This is done by taking the \b minimum
				length of the Dubins curves that connect s1 to s2 and s2 to s1. If
				isSymmetric_ is true, then the distance no longer satisfies the
				triangle inequality. */
            bool isSymmetric_;

        };
        /** \brief A Dubins motion validator that only uses the state validity checker.
                    Motions are checked for validity at a specified resolution.

                    This motion validator is almost identical to the DiscreteMotionValidator
                    except that it remembers the optimal DubinsPath between different calls to
                    interpolate. */
        class Dubins3DMotionValidator : public MotionValidator
        {
        public:
        	Dubins3DMotionValidator(SpaceInformation *si, double surgeSpeed, double ascendingSpeed, double descendingSpeed) :
        		MotionValidator(si), surgeSpeed_(surgeSpeed), ascendingSpeed_(ascendingSpeed), descendingSpeed_(descendingSpeed)
        	{
        		defaultSettings();
        	}
        	Dubins3DMotionValidator(const SpaceInformationPtr &si, double surgeSpeed, double ascendingSpeed, double descendingSpeed) :
        		MotionValidator(si), surgeSpeed_(surgeSpeed), ascendingSpeed_(ascendingSpeed), descendingSpeed_(descendingSpeed)
        	{
        		defaultSettings();
        	}
        	virtual ~Dubins3DMotionValidator()
        	{
        	}
        	virtual bool checkMotion(const State *s1, const State *s2) const;
        	virtual bool checkMotion(const State *s1, const State *s2, std::pair<State*, double> &lastValid) const;
        private:
        	Dubins3DStateSpace *stateSpace_;
        	double surgeSpeed_, ascendingSpeed_, descendingSpeed_;
        	void defaultSettings();
        };
    }
}

#endif
