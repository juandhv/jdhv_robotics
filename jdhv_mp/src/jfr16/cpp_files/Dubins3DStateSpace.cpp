/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2010, Rice University
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Rice University nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Ioan Sucan */

#include "Dubins3DStateSpace.h"

#include <ompl/tools/config/MagicConstants.h>
#include <cstring>

#include <ompl/base/SpaceInformation.h>
#include <ompl/util/Exception.h>
#include <queue>

ompl::base::State* ompl::base::Dubins3DStateSpace::allocState() const
{
    StateType *state = new StateType();
    allocStateComponents(state);
    return state;
}

void ompl::base::Dubins3DStateSpace::freeState(State *state) const
{
    CompoundStateSpace::freeState(state);
}

void ompl::base::Dubins3DStateSpace::registerProjections()
{
    class SE2DefaultProjection : public ProjectionEvaluator
    {
    public:

        SE2DefaultProjection(const StateSpace *space) : ProjectionEvaluator(space)
        {
        }

        virtual unsigned int getDimension() const
        {
            return 3;
        }

        virtual void defaultCellSizes()
        {
            cellSizes_.resize(3);
            bounds_ = space_->as<Dubins3DStateSpace>()->getBounds();
            cellSizes_[0] = (bounds_.high[0] - bounds_.low[0]) / magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[1] = (bounds_.high[1] - bounds_.low[1]) / magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[2] = (bounds_.high[2] - bounds_.low[2]) / magic::PROJECTION_DIMENSION_SPLITS;
        }

        virtual void project(const State *state, EuclideanProjection &projection) const
        {
            memcpy(&projection(0), state->as<Dubins3DStateSpace::StateType>()->as<RealVectorStateSpace::StateType>(0)->values, 3 * sizeof(double));
        }
    };

    registerDefaultProjection(ProjectionEvaluatorPtr(dynamic_cast<ProjectionEvaluator*>(new SE2DefaultProjection(this))));
}

//------

using namespace ompl::base;

namespace
{
    const double twopi = 2. * boost::math::constants::pi<double>();
    const double DUBINS_EPS = 1e-6;
    const double DUBINS_ZERO = -1e-9;

    inline double mod2pi(double x)
    {
        if (x<0 && x>DUBINS_ZERO) return 0;
        return x - twopi * floor(x / twopi);
    }

    Dubins3DStateSpace::DubinsPath dubinsLSL(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = 2. + d*d - 2.*(ca*cb +sa*sb - d*(sa - sb));
        if (tmp >= DUBINS_ZERO)
        {
            double theta = atan2(cb - ca, d + sa - sb);
            double t = mod2pi(-alpha + theta);
            double p = sqrt(std::max(tmp, 0.));
            double q = mod2pi(beta - theta);
            assert(fabs(p*cos(alpha + t) - sa + sb - d) < DUBINS_EPS);
            assert(fabs(p*sin(alpha + t) + ca - cb) < DUBINS_EPS);
            assert(mod2pi(alpha + t + q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[0], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubinsRSR(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = 2. + d*d - 2.*(ca*cb + sa*sb - d*(sb - sa));
        if (tmp >= DUBINS_ZERO)
        {
            double theta = atan2(ca - cb, d - sa + sb);
            double t = mod2pi(alpha - theta);
            double p = sqrt(std::max(tmp, 0.));
            double q = mod2pi(-beta + theta);
            assert(fabs(p*cos(alpha - t) + sa - sb - d) < DUBINS_EPS);
            assert(fabs(p*sin(alpha - t) - ca + cb) < DUBINS_EPS);
            assert(mod2pi(alpha - t - q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[1], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubinsRSL(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = d * d - 2. + 2. * (ca*cb + sa*sb - d * (sa + sb));
        if (tmp >= DUBINS_ZERO)
        {
            double p = sqrt(std::max(tmp, 0.));
            double theta = atan2(ca + cb, d - sa - sb) - atan2(2., p);
            double t = mod2pi(alpha - theta);
            double q = mod2pi(beta - theta);
            assert(fabs(p*cos(alpha - t) - 2. * sin(alpha - t) + sa + sb - d) < DUBINS_EPS);
            assert(fabs(p*sin(alpha - t) + 2. * cos(alpha - t) - ca - cb) < DUBINS_EPS);
            assert(mod2pi(alpha - t + q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[2], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubinsLSR(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = -2. + d * d + 2. * (ca*cb + sa*sb + d * (sa + sb));
        if (tmp >= DUBINS_ZERO)
        {
            double p = sqrt(std::max(tmp, 0.));
            double theta = atan2(-ca - cb, d + sa + sb) - atan2(-2., p);
            double t = mod2pi(-alpha + theta);
            double q = mod2pi(-beta + theta);
            assert(fabs(p*cos(alpha + t) + 2. * sin(alpha + t) - sa - sb - d) < DUBINS_EPS);
            assert(fabs(p*sin(alpha + t) - 2. * cos(alpha + t) + ca + cb) < DUBINS_EPS);
            assert(mod2pi(alpha + t - q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[3], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubinsRLR(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = .125 * (6. - d * d  + 2. * (ca*cb + sa*sb + d * (sa - sb)));
        if (fabs(tmp) < 1.)
        {
            double p = twopi - acos(tmp);
            double theta = atan2(ca - cb, d - sa + sb);
            double t = mod2pi(alpha - theta + .5 * p);
            double q = mod2pi(alpha - beta - t + p);
            assert(fabs( 2.*sin(alpha - t + p) - 2. * sin(alpha - t) - d + sa - sb) < DUBINS_EPS);
            assert(fabs(-2.*cos(alpha - t + p) + 2. * cos(alpha - t) - ca + cb) < DUBINS_EPS);
            assert(mod2pi(alpha - t + p - q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[4], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubinsLRL(double d, double alpha, double beta)
    {
        double ca = cos(alpha), sa = sin(alpha), cb = cos(beta), sb = sin(beta);
        double tmp = .125 * (6. - d * d  + 2. * (ca*cb + sa*sb - d * (sa - sb)));
        if (fabs(tmp) < 1.)
        {
            double p = twopi - acos(tmp);
            double theta = atan2(-ca + cb, d + sa - sb);
            double t = mod2pi(-alpha + theta + .5 * p);
            double q = mod2pi(beta - alpha - t + p);
            assert(fabs(-2.*sin(alpha + t - p) + 2. * sin(alpha + t) - d - sa + sb) < DUBINS_EPS);
            assert(fabs( 2.*cos(alpha + t - p) - 2. * cos(alpha + t) + ca - cb) < DUBINS_EPS);
            assert(mod2pi(alpha + t - p + q - beta + .5 * DUBINS_EPS) < DUBINS_EPS);
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[5], t, p, q);
        }
        return Dubins3DStateSpace::DubinsPath();
    }

    Dubins3DStateSpace::DubinsPath dubins(double d, double alpha, double beta)
    {
        if (d<DUBINS_EPS && fabs(alpha-beta)<DUBINS_EPS)
            return Dubins3DStateSpace::DubinsPath(Dubins3DStateSpace::dubinsPathType[0], 0, d, 0);

        Dubins3DStateSpace::DubinsPath path(dubinsLSL(d, alpha, beta)), tmp(dubinsRSR(d, alpha, beta));
        double len, minLength = path.length();

        if ((len = tmp.length()) < minLength)
        {
            minLength = len;
            path = tmp;
        }
        tmp = dubinsRSL(d, alpha, beta);
        if ((len = tmp.length()) < minLength)
        {
            minLength = len;
            path = tmp;
        }
        tmp = dubinsLSR(d, alpha, beta);
        if ((len = tmp.length()) < minLength)
        {
            minLength = len;
            path = tmp;
        }
        tmp = dubinsRLR(d, alpha, beta);
        if ((len = tmp.length()) < minLength)
        {
            minLength = len;
            path = tmp;
        }
        tmp = dubinsLRL(d, alpha, beta);
        if ((len = tmp.length()) < minLength)
            path = tmp;
        return path;
    }
}

const ompl::base::Dubins3DStateSpace::DubinsPathSegmentType
ompl::base::Dubins3DStateSpace::dubinsPathType[6][3] = {
    { DUBINS_LEFT, DUBINS_STRAIGHT, DUBINS_LEFT },
    { DUBINS_RIGHT, DUBINS_STRAIGHT, DUBINS_RIGHT },
    { DUBINS_RIGHT, DUBINS_STRAIGHT, DUBINS_LEFT },
    { DUBINS_LEFT, DUBINS_STRAIGHT, DUBINS_RIGHT },
    { DUBINS_RIGHT, DUBINS_LEFT, DUBINS_RIGHT },
    { DUBINS_LEFT, DUBINS_RIGHT, DUBINS_LEFT }
};

double ompl::base::Dubins3DStateSpace::distance(const State *state1, const State *state2) const
{
    if (isSymmetric_)
        return rho_ * std::min(dubins(state1, state2).length(), dubins(state2, state1).length());
    else
    {
        //return rho_ * dubins(state1, state2).length();
    	double distance_dubins = rho_ * dubins(state1, state2).length();
    	double distance_z = abs(state1->as<Dubins3DStateSpace::StateType>()->getZ() - state2->as<Dubins3DStateSpace::StateType>()->getZ());
    	return sqrt(distance_dubins*distance_dubins + distance_z*distance_z);
    }
}

void ompl::base::Dubins3DStateSpace::interpolate(const State *from, const State *to, const double t, State *state) const
{
    bool firstTime = true;
    DubinsPath path;
    interpolate(from, to, t, firstTime, path, state);
}

void ompl::base::Dubins3DStateSpace::interpolate(const State *from, const State *to, const double t,
    bool &firstTime, DubinsPath &path, State *state) const
{
    if (firstTime)
    {
        if (t>=1.)
        {
            if (to != state)
                copyState(state, to);
            return;
        }
        if (t<=0.)
        {
            if (from != state)
                copyState(state, from);
            return;
        }

        path = dubins(from, to);
        if (isSymmetric_)
        {
            DubinsPath path2(dubins(to, from));
            if (path2.length() < path.length())
            {
                path2.reverse_ = true;
                path = path2;
            }
        }
        firstTime = false;
    }
    double delta_z = t*(to->as<Dubins3DStateSpace::StateType>()->getZ() - from->as<Dubins3DStateSpace::StateType>()->getZ());
    state->as<Dubins3DStateSpace::StateType>()->setZ(from->as<Dubins3DStateSpace::StateType>()->getZ() + delta_z);
    interpolate(from, path, t, state);
}

void ompl::base::Dubins3DStateSpace::interpolate(const State *from, const DubinsPath &path, double t, State *state) const
{
    StateType *s  = allocState()->as<StateType>();
    double seg = t * path.length(), phi, v;

    s->setXY(0., 0.);
    s->setYaw(from->as<StateType>()->getYaw());
    if (!path.reverse_)
    {
        for (unsigned int i=0; i<3 && seg>0; ++i)
        {
            v = std::min(seg, path.length_[i]);
            phi = s->getYaw();
            seg -= v;
            switch(path.type_[i])
            {
                case DUBINS_LEFT:
                    s->setXY(s->getX() + sin(phi+v) - sin(phi), s->getY() - cos(phi+v) + cos(phi));
                    s->setYaw(phi+v);
                    break;
                case DUBINS_RIGHT:
                    s->setXY(s->getX() - sin(phi-v) + sin(phi), s->getY() + cos(phi-v) - cos(phi));
                    s->setYaw(phi-v);
                    break;
                case DUBINS_STRAIGHT:
                    s->setXY(s->getX() + v * cos(phi), s->getY() + v * sin(phi));
                    break;
            }
        }
    }
    else
    {
        for (unsigned int i=0; i<3 && seg>0; ++i)
        {
            v = std::min(seg, path.length_[2-i]);
            phi = s->getYaw();
            seg -= v;
            switch(path.type_[2-i])
            {
                case DUBINS_LEFT:
                    s->setXY(s->getX() + sin(phi-v) - sin(phi), s->getY() - cos(phi-v) + cos(phi));
                    s->setYaw(phi-v);
                    break;
                case DUBINS_RIGHT:
                    s->setXY(s->getX() - sin(phi+v) + sin(phi), s->getY() + cos(phi+v) - cos(phi));
                    s->setYaw(phi+v);
                    break;
                case DUBINS_STRAIGHT:
                    s->setXY(s->getX() - v * cos(phi), s->getY() - v * sin(phi));
                    break;
            }
        }
    }
    state->as<StateType>()->setX(s->getX() * rho_ + from->as<StateType>()->getX());
    state->as<StateType>()->setY(s->getY() * rho_ + from->as<StateType>()->getY());
    getSubspace(1)->enforceBounds(s->as<SO2StateSpace::StateType>(1));
    state->as<StateType>()->setYaw(s->getYaw());
    freeState(s);
}

ompl::base::Dubins3DStateSpace::DubinsPath ompl::base::Dubins3DStateSpace::dubins(const State *state1, const State *state2) const
{
    const StateType *s1 = static_cast<const StateType*>(state1);
    const StateType *s2 = static_cast<const StateType*>(state2);
    double x1 = s1->getX(), y1 = s1->getY(), th1 = s1->getYaw();
    double x2 = s2->getX(), y2 = s2->getY(), th2 = s2->getYaw();
    double dx = x2 - x1, dy = y2 - y1, d = sqrt(dx*dx + dy*dy) / rho_, th = atan2(dy, dx);
    double alpha = mod2pi(th1 - th), beta = mod2pi(th2 - th);
    return ::dubins(d, alpha, beta);
}

//---

void ompl::base::Dubins3DMotionValidator::defaultSettings()
{
    stateSpace_ = dynamic_cast<Dubins3DStateSpace*>(si_->getStateSpace().get());
    if (!stateSpace_)
        throw Exception("No state space for motion validator");
}

bool ompl::base::Dubins3DMotionValidator::checkMotion(const State *s1, const State *s2, std::pair<State*, double> &lastValid) const
{
    /* assume motion starts in a valid configuration so s1 is valid */
	//std::cout << "checking motion" << std::endl;
    bool result = true, firstTime = true;
    Dubins3DStateSpace::DubinsPath path;
    int nd = stateSpace_->validSegmentCount(s1, s2);

    if (nd > 1)
    {
        /* temporary storage for the checked state */
        State *test = si_->allocState();

        for (int j = 1 ; j < nd ; ++j)
        {
            stateSpace_->interpolate(s1, s2, (double)j / (double)nd, firstTime, path, test);
            if (!si_->isValid(test))
            {
                lastValid.second = (double)(j - 1) / (double)nd;
                if (lastValid.first)
                    stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
                result = false;
                break;
            }
        }
        si_->freeState(test);
    }

    if (result)
        if (!si_->isValid(s2))
        {
            lastValid.second = (double)(nd - 1) / (double)nd;
            if (lastValid.first)
                stateSpace_->interpolate(s1, s2, lastValid.second, firstTime, path, lastValid.first);
            result = false;
        }

    if (result)
        valid_++;
    else
        invalid_++;

    return result;
}

bool ompl::base::Dubins3DMotionValidator::checkMotion(const State *s1, const State *s2) const
{
	double dubins_distance = stateSpace_->distance(s1,s2);
	double dubins_time = dubins_distance / surgeSpeed_;
	double distance_z = s2->as<Dubins3DStateSpace::StateType>()->getZ() - s1->as<Dubins3DStateSpace::StateType>()->getZ();
	double velocity_z = distance_z / dubins_time;

	if(distance_z < 0.0 && velocity_z < -ascendingSpeed_) //ascending
	{
		//std::cout << "not valid ascending" << std::endl;
		return false;
	}
  	else if(velocity_z > descendingSpeed_)//descending
	{
		//std::cout << "not valid descending" << std::endl;
		return false;
	}

    /* assume motion starts in a valid configuration so s1 is valid */
    if (!si_->isValid(s2))
    {
    	//std::cout << "s2 not valid" << std::endl;
        return false;
    }

    bool result = true, firstTime = true;
    Dubins3DStateSpace::DubinsPath path;
    int nd = stateSpace_->validSegmentCount(s1, s2);

    /* initialize the queue of test positions */
    std::queue< std::pair<int, int> > pos;
    if (nd >= 2)
    {
        pos.push(std::make_pair(1, nd - 1));

        /* temporary storage for the checked state */
        State *test = si_->allocState();

        /* repeatedly subdivide the path segment in the middle (and check the middle) */
        while (!pos.empty())
        {
            std::pair<int, int> x = pos.front();

            int mid = (x.first + x.second) / 2;
            stateSpace_->interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

            if (!si_->isValid(test))
            {
            	//std::cout << "not valid while interpolating" << std::endl;
                result = false;
                break;
            }

            pos.pop();

            if (x.first < mid)
                pos.push(std::make_pair(x.first, mid - 1));
            if (x.second > mid)
                pos.push(std::make_pair(mid + 1, x.second));
        }

        si_->freeState(test);
    }

    if (result)
        valid_++;
    else
        invalid_++;

    return result;
}
