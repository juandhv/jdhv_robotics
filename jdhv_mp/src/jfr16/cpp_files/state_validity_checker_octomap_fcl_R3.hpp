/*
 * state_validity_checker_octomap_fcl_R3.hpp
 *
 *  Created on: January 11, 2017
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (R3 state) is collision-free.
 *  The workspace is represented by an octomap and collision check is done with FCL.
 */

#ifndef OMPL_CONTRIB_STATE_VALIDITY_CHECKER_FCL_OCTOMAP_R3_
#define OMPL_CONTRIB_STATE_VALIDITY_CHECKER_FCL_OCTOMAP_R3_

//ROS
#include <ros/ros.h>
//ROS markers rviz
#include <visualization_msgs/Marker.h>
// ROS messages
#include <nav_msgs/Odometry.h>
// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

//Standard libraries
#include <cstdlib>
#include <cmath>
#include <string>

//Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>

//OMPL
#include <ompl/config.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/tools/debug/Profiler.h>

//Boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

//Eigen
#include <Eigen/Dense>

//FCL
#include <fcl/shape/geometric_shapes.h>
#include <fcl/shape/geometric_shapes_utility.h>
#include <fcl/narrowphase/narrowphase.h>
#include <fcl/octree.h>
#include <fcl/traversal/traversal_node_octree.h>
#include <fcl/broadphase/broadphase.h>
#include <fcl/shape/geometric_shape_to_BVH_model.h>
#include <fcl/math/transform.h>
#include <fcl/collision.h>
#include <fcl/collision_node.h>
#include <fcl/distance.h>

#include <iostream>

//ROS-Octomap interface
using octomap_msgs::GetOctomap;
//Standard namespace
using namespace std;
//Octomap namespace
using namespace octomap;
//OMPL namespaces
namespace ob = ompl::base;
namespace og = ompl::geometric;


//!  OmFclStateValidityCheckerR3 class.
/*!
  Octomap State Validity checker.
  Extension of an abstract class used to implement the state validity checker over an octomap using FCL.
*/
class OmFclStateValidityCheckerR3 : public ob::StateValidityChecker {

public:
	//! OmFclStateValidityCheckerR3 constructor.
	/*!
	 * Besides of initializing the private attributes, it loads the octomap.
	 */
	OmFclStateValidityCheckerR3(const ob::SpaceInformationPtr &si, const bool lazy_collision_eval, std::vector<double> planning_bounds_x, std::vector<double> planning_bounds_y, std::vector<double> planning_bounds_z);

	//! OmFclStateValidityCheckerR3 destructor.
	/*!
	 * Destroy the octomap.
	 */
	~OmFclStateValidityCheckerR3();

	//! State validator.
	/*!
	 * Function that verifies if the given state is valid (i.e. is free of collision) using FCL
	 */
	virtual bool isValid(const ob::State *state) const;

	//! State clearance.
	/*!
	 * Returns the minimum distance from the given robot state and the environment
	 */
	virtual double clearance(const ob::State *state) const;
	
	virtual bool isValidPoint(const ob::State *state) const;
private:
	//ROS
	ros::NodeHandle node_hand_;

	//Octomap
	octomap::AbstractOcTree* abs_octree_;
	octomap::OcTree* octree_;
	double octree_min_x_, octree_min_y_, octree_min_z_;
	double octree_max_x_, octree_max_y_, octree_max_z_;
	std::vector<double> planning_bounds_x_, planning_bounds_y_, planning_bounds_z_;

	//FCL
	fcl::OcTree* tree_;
	fcl::CollisionObject* tree_obj_;
	std::shared_ptr<fcl::Box> vehicle_solid_;

	bool lazy_collision_eval_;
};

#endif
