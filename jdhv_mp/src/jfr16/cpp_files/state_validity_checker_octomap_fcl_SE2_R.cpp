/*
 * state_validity_checker_octomap_fcl_SE2_R.cpp
 *
 *  Created on: February 3, 2015
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *      
 *  State checker. Check is a given configuration (SE2xR state) is collision-free.
 *  The workspace is represented by an octomap and collision check is done with FCL.
 */

#include "state_validity_checker_octomap_fcl_SE2_R.hpp"

OmFclStateValidityCheckerSE2R::OmFclStateValidityCheckerSE2R(const ob::SpaceInformationPtr &si, const double planning_depth, const bool lazy_collision_eval, const bool lazy_risk_eval, std::vector<double> planning_bounds_x, std::vector<double> planning_bounds_y, std::vector<double> planning_bounds_z) :
	ob::StateValidityChecker(si)
{
	GetOctomap::Request req;
	GetOctomap::Response resp;
	std::string serv_name;

	planning_depth_ = planning_depth;
	lazy_collision_eval_ = lazy_collision_eval;
	lazy_risk_eval_ = lazy_risk_eval;
	planning_bounds_x_ = planning_bounds_x;
	planning_bounds_y_ = planning_bounds_y;
	planning_bounds_z_ = planning_bounds_z;

	serv_name = "/laser_octomap/get_binary";
	octree_ = NULL;

	ROS_DEBUG("%s: requesting the map to %s...", ros::this_node::getName().c_str(), node_hand_.resolveName(serv_name).c_str());

	while((node_hand_.ok() && !ros::service::call(serv_name, req, resp)) || resp.map.data.size()==0)
	{
		ROS_WARN("Request to %s failed; trying again...", node_hand_.resolveName(serv_name).c_str());
		usleep(1000000);
	}
	if (node_hand_.ok()){ // skip when CTRL-C
		abs_octree_ = octomap_msgs::msgToMap(resp.map);
		//std::cout << std::endl;
		if (abs_octree_){
			octree_ = dynamic_cast<octomap::OcTree*>(abs_octree_);
			tree_ = new fcl::OcTree(std::shared_ptr<const octomap::OcTree>(octree_));
			tree_obj_ = new fcl::CollisionObject((std::shared_ptr<fcl::CollisionGeometry>(tree_)));

			vehicle_solid_.reset(new fcl::Box(1.6, 0.5, 0.5));
		}

		octree_res_ = octree_->getResolution();
		octree_->getMetricMin(octree_min_x_, octree_min_y_, octree_min_z_);
		octree_->getMetricMax(octree_max_x_, octree_max_y_, octree_max_z_);

		if (octree_){
			ROS_DEBUG("%s: Octomap received (%zu nodes, %f m res)", ros::this_node::getName().c_str(), octree_->size(), octree_->getResolution());
		} else{
			ROS_ERROR("Error reading OcTree from stream");
		}
	}
}

bool OmFclStateValidityCheckerSE2R::isValid(const ob::State *state) const
{
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;

	//ompl::tools::Profiler::Begin("collision");

	// extract the component of the state and cast it to what we expect
	state_dubins_3D = state->as<ob::Dubins3DStateSpace::StateType>();

	if(lazy_collision_eval_ && (state_dubins_3D->getX() < octree_min_x_ || state_dubins_3D->getY() < octree_min_y_ || state_dubins_3D->getZ() < octree_min_z_ || state_dubins_3D->getX() > octree_max_x_ || state_dubins_3D->getY() > octree_max_y_ || state_dubins_3D->getZ() > octree_max_z_))
	{
		//ompl::tools::Profiler::End("collision");
		return true;
	}

	if(state_dubins_3D->getX() < planning_bounds_x_[0] || state_dubins_3D->getY() < planning_bounds_y_[0] || state_dubins_3D->getZ() < planning_bounds_z_[0] || state_dubins_3D->getX() > planning_bounds_x_[1] || state_dubins_3D->getY() > planning_bounds_y_[1] || state_dubins_3D->getZ() > planning_bounds_z_[1])
	{
		//ompl::tools::Profiler::End("collision");
		return false;
	}

	//FCL
	fcl::Transform3f vehicle_tf;
	vehicle_tf.setIdentity();
	vehicle_tf.setTranslation(fcl::Vec3f(state_dubins_3D->getX(), state_dubins_3D->getY(), state_dubins_3D->getZ()));
	fcl::Quaternion3f qt0;
	qt0.fromEuler(0.0, 0.0, state_dubins_3D->getYaw());
	vehicle_tf.setQuatRotation(qt0);

	fcl::CollisionObject vehicle_co(vehicle_solid_, vehicle_tf);
	fcl::CollisionRequest collision_request;
	fcl::CollisionResult collision_result;

	fcl::collide(tree_obj_, &vehicle_co, collision_request, collision_result);

	//std::cout << "Collision (FCL): " << collision_result.isCollision() << std::endl;

	if(collision_result.isCollision())
	{
		//ompl::tools::Profiler::End("collision");
		return	false;
	}
	else
	{
		//ompl::tools::Profiler::End("collision");
		return	true;
	}
}

double OmFclStateValidityCheckerSE2R::clearance(const ob::State *state) const
{
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;
	double minDist = std::numeric_limits<double>::infinity ();

	//ompl::tools::Profiler::Begin("clearance");

	// extract the component of the state and cast it to what we expect
	state_dubins_3D = state->as<ob::Dubins3DStateSpace::StateType>();

	if(lazy_risk_eval_ && (state_dubins_3D->getX() < octree_min_x_ || state_dubins_3D->getY() < octree_min_y_ || state_dubins_3D->getZ() < octree_min_z_ || state_dubins_3D->getX() > octree_max_x_ || state_dubins_3D->getY() > octree_max_y_ || state_dubins_3D->getZ() > octree_max_z_))
	{
		//ompl::tools::Profiler::End("clearance");
		return minDist;
	}

	//FCL
	fcl::Transform3f vehicle_tf;
	vehicle_tf.setIdentity();
	vehicle_tf.setTranslation(fcl::Vec3f(state_dubins_3D->getX(), state_dubins_3D->getY(), state_dubins_3D->getZ()));
	fcl::Quaternion3f qt0;
	qt0.fromEuler(0.0, 0.0, state_dubins_3D->getYaw());
	vehicle_tf.setQuatRotation(qt0);

	fcl::CollisionObject vehicle_co(vehicle_solid_, vehicle_tf);
	fcl::DistanceRequest distanceRequest;
	fcl::DistanceResult distanceResult;

	fcl::distance(tree_obj_, &vehicle_co, distanceRequest, distanceResult);

	//std::cout << "Distance (FCL): " << distanceResult.min_distance << std::endl;

	if (distanceResult.min_distance < minDist)
		minDist = distanceResult.min_distance;

	//ompl::tools::Profiler::End("clearance");

	return minDist;
}

double OmFclStateValidityCheckerSE2R::checkRiskZones(const ob::State *state) const
{
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;
	double clearance_cost = 1.0;

	//ompl::tools::Profiler::Begin("RiskZones");

	// extract the component of the state and cast it to what we expect
	state_dubins_3D = state->as<ob::Dubins3DStateSpace::StateType>();

	if(lazy_risk_eval_ && (state_dubins_3D->getX() < octree_min_x_ || state_dubins_3D->getY() < octree_min_y_ || state_dubins_3D->getZ() < octree_min_z_ || state_dubins_3D->getX() > octree_max_x_ || state_dubins_3D->getY() > octree_max_y_ || state_dubins_3D->getZ() > octree_max_z_))
	{
		//ompl::tools::Profiler::End("RiskZones");
		return clearance_cost;
	}

	//FCL
	fcl::Transform3f vehicle_tf;
	vehicle_tf.setIdentity();
	vehicle_tf.setTranslation(fcl::Vec3f(state_dubins_3D->getX(), state_dubins_3D->getY(), state_dubins_3D->getZ()));
	fcl::Quaternion3f qt0;
	qt0.fromEuler(0.0, 0.0, state_dubins_3D->getYaw());
	vehicle_tf.setQuatRotation(qt0);

	std::shared_ptr<fcl::Box> box0(new fcl::Box(1.6+1.0, 0.5+1.0, 0.5+1.0));

	fcl::CollisionObject box0_co(box0, vehicle_tf);
	fcl::CollisionRequest collision_request;
	fcl::CollisionResult collision_result;

	fcl::collide(tree_obj_, &box0_co, collision_request, collision_result);

	if(collision_result.isCollision())
		clearance_cost = 15.0;
	else
	{
		std::shared_ptr<fcl::Box> box1(new fcl::Box(1.6+1.5, 0.5+1.5, 0.5+1.5));
		fcl::CollisionObject box1_co(box1, vehicle_tf);
		collision_result.clear();

		fcl::collide(tree_obj_, &box1_co, collision_request, collision_result);
		if(collision_result.isCollision())
			clearance_cost = 10.0;
		else
		{
			std::shared_ptr<fcl::Box> box2(new fcl::Box(1.6+3.0, 0.5+3.0, 0.5+3.0));
			fcl::CollisionObject box2_co(box2, vehicle_tf);
			collision_result.clear();

			fcl::collide(tree_obj_, &box2_co, collision_request, collision_result);
			if(collision_result.isCollision())
				clearance_cost = 2.0;
		}
	}

	//ompl::tools::Profiler::End("RiskZones");

	return clearance_cost;
}

double OmFclStateValidityCheckerSE2R::checkDirVectorsRisk(const ob::State *state) const
{
	octomap::OcTreeNode* result;
	octomap::point3d query;
	double node_occupancy;
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D;
	double risk = 1.0;

	//ompl::tools::Profiler::Begin("DirVectorsRisk");

	// extract the component of the state and cast it to what we expect
	state_dubins_3D = state->as<ob::Dubins3DStateSpace::StateType>();

	if(lazy_risk_eval_ && (state_dubins_3D->getX() < octree_min_x_ || state_dubins_3D->getY() < octree_min_y_ || state_dubins_3D->getZ() < octree_min_z_ || state_dubins_3D->getX() > octree_max_x_ || state_dubins_3D->getY() > octree_max_y_ || state_dubins_3D->getZ() > octree_max_z_))
	{
		//ompl::tools::Profiler::End("DirVectorsRisk");
		return risk;
	}

	for(double delta_i = 0.0; delta_i <= 1.5; delta_i=delta_i+octree_res_)
	{
		//----

		query.x() = state_dubins_3D->getX()+(0.8+delta_i)*cos((float)state_dubins_3D->getYaw());
		query.y() = state_dubins_3D->getY()+(0.8+delta_i)*sin((float)state_dubins_3D->getYaw());
		query.z() = state_dubins_3D->getZ();
		result = octree_->search (query);

		if(result != NULL){
			node_occupancy = result->getOccupancy();
			if (node_occupancy > 0.4){
				if(delta_i<1.0)
					risk = 15.0;
				else if(delta_i<1.5)
					risk = 10.0;
				else
					risk = 2.0;

				//ompl::tools::Profiler::End("DirVectorsRisk");
				return risk;
			}
		}

		//----

		query.x() = state_dubins_3D->getX()-(0.25+delta_i)*sin((float)state_dubins_3D->getYaw());
		query.y() = state_dubins_3D->getY()+(0.25+delta_i)*cos((float)state_dubins_3D->getYaw());
		query.z() = state_dubins_3D->getZ();
		result = octree_->search (query);

		if(result != NULL){
			node_occupancy = result->getOccupancy();
			if (node_occupancy > 0.4){
				if(delta_i<1.0)
					risk = 15.0;
				else if(delta_i<1.5)
					risk = 10.0;
				else
					risk = 2.0;

				//ompl::tools::Profiler::End("DirVectorsRisk");
				return risk;
			}
		}

		//----

		query.x() = state_dubins_3D->getX()+(0.25+delta_i)*sin((float)state_dubins_3D->getYaw());
		query.y() = state_dubins_3D->getY()-(0.25+delta_i)*cos((float)state_dubins_3D->getYaw());
		query.z() = state_dubins_3D->getZ();
		result = octree_->search (query);

		if(result != NULL){
			node_occupancy = result->getOccupancy();
			if (node_occupancy > 0.4){
				if(delta_i<1.0)
					risk = 15.0;
				else if(delta_i<1.5)
					risk = 10.0;
				else
					risk = 2.0;

				//ompl::tools::Profiler::End("DirVectorsRisk");
				return risk;
			}
		}
	}

	//ompl::tools::Profiler::End("DirVectorsRisk");
	return risk;
}

bool OmFclStateValidityCheckerSE2R::isValidPoint(const ob::State *state) const
{
	OcTreeNode* result;
	point3d query;
	double node_occupancy;

	// extract the component of the state and cast it to what we expect
	const ob::Dubins3DStateSpace::StateType *state_dubins_3D = state->as<ob::Dubins3DStateSpace::StateType>();

	query.x() = state_dubins_3D->getX();
	query.y() = state_dubins_3D->getY();
	query.z() = state_dubins_3D->getZ();

	result = octree_->search (query);

	if(result == NULL){
		return false;
	}
	else{
		node_occupancy = result->getOccupancy();
		if (node_occupancy <= 0.4)
			return true;
	}
	return false;
}

OmFclStateValidityCheckerSE2R::~OmFclStateValidityCheckerSE2R()
{
    delete octree_;
//    delete tree_;
//    delete tree_obj_;
}
