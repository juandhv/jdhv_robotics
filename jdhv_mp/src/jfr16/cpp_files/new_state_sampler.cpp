/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, Rice University
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Rice University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Javier V. Gómez*/

#include "new_state_sampler.h"

ob::StateSamplerPtr newAllocStateSampler(const ob::StateSpace *space, const std::vector<const ob::State *> &start_states)
{
	NewStateSampler *sampler = new NewStateSampler(space, space->allocDefaultStateSampler());
	sampler->setStatesToSample(start_states);
	return ob::StateSamplerPtr(sampler);
}

void NewStateSampler::sampleUniform(ob::State *state)
{
	if (!statesToSample_.empty())
	{
		getNextSample(state);
		//std::cout << "reusing state: " << state->as<ob::RealVectorStateSpace::StateType>()->values[0] << ", " << state->as<ob::RealVectorStateSpace::StateType>()->values[1] << std::endl;
	}
	else //if(!reusing_)
		sampler_->sampleUniform(state);
//	else
//		std::cout << "reusing state: " << state->as<ob::RealVectorStateSpace::StateType>()->values[0] << ", " << state->as<ob::RealVectorStateSpace::StateType>()->values[1] << std::endl;
}

void NewStateSampler::sampleUniformNear(ob::State *state, const ob::State *near, const double distance)
{
	if (!statesToSample_.empty())
		getNextSample(state);
	else
		sampler_->sampleUniformNear(state, near, distance);
}

void NewStateSampler::sampleGaussian(ob::State *state, const ob::State *mean, const double stdDev)
{
	if (!statesToSample_.empty())
		getNextSample(state);
	else
		sampler_->sampleGaussian(state, mean, stdDev);
}

void NewStateSampler::setStatesToSample(const std::vector<const ob::State *> &states)
{
	//boost::mutex::scoped_lock slock(statesLock_);
	for (size_t i = 0; i < statesToSample_.size(); ++i)
		space_->freeState(statesToSample_[i]);
	statesToSample_.clear();

	statesToSample_.reserve(states.size());
	//std::cout << "state to be recovered: " << states.size() << std::endl;
	// push in reverse order, so that the states are popped in order in getNextSample()
	//for (std::vector<const ob::State *>::const_iterator st = states.begin(); st != states.end(); ++st)
	reusing_ = false;
	for (size_t i = 0; i < states.size(); i++)
	{
		reusing_ = true;
		ob::State *s = space_->allocState();
		space_->copyState(s, states[i]);
		statesToSample_.push_back(s);
		//std::cout << "recovering state: " << s->as<ob::RealVectorStateSpace::StateType>()->values[0] << ", " << s->as<ob::RealVectorStateSpace::StateType>()->values[1] << std::endl;
	}
}

void NewStateSampler::getNextSample(ob::State *state)
{
	//boost::mutex::scoped_lock slock(statesLock_);
	space_->copyState(state, statesToSample_.back());
	space_->freeState(statesToSample_.back());
	statesToSample_.pop_back();
}

void NewStateSampler::clear()
{
	//boost::mutex::scoped_lock slock(statesLock_);
	for (size_t i = 0; i < statesToSample_.size(); ++i)
		space_->freeState(statesToSample_[i]);
	statesToSample_.clear();
	sampler_.reset();
}

//-------

oc::ControlSamplerPtr newAllocControlSampler(const oc::ControlSpace *cspace, const std::vector<const oc::Control *> &start_controls)
{
	NewControlSampler *csampler = new NewControlSampler(cspace, cspace->allocDefaultControlSampler());
	csampler->setControlsToSample(start_controls);
	return oc::ControlSamplerPtr(csampler);
}

void NewControlSampler::sample(oc::Control *control)
{
	if (!controlsToSample_.empty())
		getNextControl(control);
	else
		csampler_->sample(control);
}

void NewControlSampler::sample(oc::Control *control, const ob::State *state)
{
	if (!controlsToSample_.empty())
		getNextControl(control);
	else
		csampler_->sample(control, state);
}

void NewControlSampler::sampleNext (oc::Control *control, const oc::Control *previous)
{
	if (!controlsToSample_.empty())
		getNextControl(control);
	else
		csampler_->sampleNext(control, previous);
}

void NewControlSampler::sampleNext (oc::Control *control, const oc::Control *previous, const ob::State *state)
{
	if (!controlsToSample_.empty())
		getNextControl(control);
	else
		csampler_->sampleNext(control, previous, state);
}

void NewControlSampler::getNextControl(oc::Control *control)
{
	//boost::mutex::scoped_lock slock(statesLock_);
	space_->copyControl(control, controlsToSample_.back());
	space_->freeControl(controlsToSample_.back());
	controlsToSample_.pop_back();
}

void NewControlSampler::setControlsToSample(const std::vector<const oc::Control *> &controls)
{
	//boost::mutex::scoped_lock slock(statesLock_);
	for (size_t i = 0; i < controlsToSample_.size(); ++i)
		space_->freeControl(controlsToSample_[i]);
	controlsToSample_.clear();

	controlsToSample_.reserve(controls.size());
	// push in reverse order, so that the states are popped in order in getNextSample()
	//for (std::vector<const oc::Control *>::const_iterator ct = controls.begin(); ct != controls.end(); ++ct)
	for(size_t i = 0; i < controls.size(); i++)
	{
		oc::Control *c = space_->allocControl();
		space_->copyControl(c, controls[i]);
		controlsToSample_.push_back(c);
	}
}

void NewControlSampler::clear()
{
	//boost::mutex::scoped_lock slock(statesLock_);
	for (size_t i = 0; i < controlsToSample_.size(); ++i)
		space_->freeControl(controlsToSample_[i]);
	controlsToSample_.clear();
	csampler_.reset();
}

//

oc::DirectedControlSamplerPtr newAllocDirectedControlSampler(const oc::SpaceInformation *si)
{
	NewDirectedControlSampler *dcsampler = new NewDirectedControlSampler(si);
	return oc::DirectedControlSamplerPtr(dcsampler);
}


NewDirectedControlSampler::NewDirectedControlSampler(const oc::SpaceInformation *si) :
		DirectedControlSampler(si), cs_(si->allocControlSampler()), numControlSamples_(1)
{
}

NewDirectedControlSampler::~NewDirectedControlSampler()
{
}

unsigned int NewDirectedControlSampler::sampleTo(oc::Control *control, const ob::State *source, ob::State *dest)
{
	return getBestControl(control, source, dest, NULL);
}

unsigned int NewDirectedControlSampler::sampleTo(oc::Control *control, const oc::Control *previous, const ob::State *source, ob::State *dest)
{
	return getBestControl(control, source, dest, previous);
}

unsigned int NewDirectedControlSampler::getBestControl (oc::Control *control, const ob::State *source, ob::State *dest, const oc::Control *previous)
{
	// Sample the first control
	if (previous)
		cs_->sampleNext(control, previous, source);
	else
		cs_->sample(control, source);

	const unsigned int minDuration = si_->getMinControlDuration();
	const unsigned int maxDuration = si_->getMaxControlDuration();

	unsigned int steps = cs_->sampleStepCount(minDuration, maxDuration);
	// Propagate the first control, and find how far it is from the target state
	ob::State *bestState   = si_->allocState();
	steps = propagateWhileValid(source, dest, steps, bestState);

	si_->copyState(dest, bestState);
	si_->freeState(bestState);

	return steps;
}

unsigned int NewDirectedControlSampler::propagateWhileValid(const ob::State *state, const ob::State *dest, int steps, ob::State *result) const
{
	if (steps == 0)
	{
		if (result != state)
			si_->copyState(result, state);
		return 0;
	}

	double stepSize_ = si_->getPropagationStepSize();

	double signedStepSize = steps > 0 ? stepSize_ : -stepSize_;
	steps = abs(steps);

	// perform the first step of propagation
	const oc::StatePropagatorPtr statePropagator = si_->getStatePropagator();
	DynUnicycle(state, dest, signedStepSize, result);

	// if we found a valid state after one step, we can go on
	if (si_->isValid(result))
	{
		ob::State *temp1 = result;
		ob::State *temp2 = si_->allocState();
		ob::State *toDelete = temp2;
		unsigned int r = steps;

		// for the remaining number of steps
		for (int i = 1 ; i < steps ; ++i)
		{
			DynUnicycle(temp1, dest, signedStepSize, temp2);
			if (si_->isValid(temp2))
				std::swap(temp1, temp2);
			else
			{
				// the last valid state is temp1;
				r = i;
				break;
			}
		}

		// if we finished the for-loop without finding an invalid state, the last valid state is temp1
		// make sure result contains that information
		if (result != temp1)
			si_->copyState(result, temp1);

		// free the temporary memory
		si_->freeState(toDelete);

		return r;
	}
	// if the first propagation step produced an invalid step, return 0 steps
	// the last valid state is the starting one (assumed to be valid)
	else
	{
		if (result != state)
			si_->copyState(result, state);
		return 0;
	}
	return 0;
}

void DynUnicycle(const ob::State *start, const ob::State *desired, const double duration, ob::State *result)
{
	//start state
	const ob::SE2StateSpace::StateType *se2state = start->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
	const double* lin_v_state = start->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values;
	const double* pos = se2state->as<ob::RealVectorStateSpace::StateType>(0)->values;
	const double rot = se2state->as<ob::SO2StateSpace::StateType>(1)->value;
	//const double* ctrl = control->as<oc::RealVectorControlSpace::ControlType>()->values;

	//desired state
	const ob::SE2StateSpace::StateType *se2desired = desired->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0);
	const double* lin_v_desired = desired->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values;
	const double* pos_desired = se2desired->as<ob::RealVectorStateSpace::StateType>(0)->values;
	const double rot_desired = se2desired->as<ob::SO2StateSpace::StateType>(1)->value;

	//control
	Eigen::MatrixXf K(2,4);
	//
//	K << -2.0, -3.0, 0.0, 0.0,
//	     0.0, 0.0, -12.0, -7.0;

	//LQR
	K << -1, -2.2361, 0.0, 0.0,
		 0.0, 0.0, -1.0, -2.2361;

	Eigen::MatrixXf X(4,1);
	X(0,0) = pos[0];
	X(1,0) = lin_v_state[0]*cos(rot);
	X(2,0) = pos[1];
	X(3,0) = lin_v_state[0]*sin(rot);

	Eigen::MatrixXf Xd(4,1);
	Xd(0,0) = pos_desired[0];
	Xd(1,0) = lin_v_desired[0]*cos(rot_desired);
	Xd(2,0) = pos_desired[1];
	Xd(3,0) = lin_v_desired[0]*sin(rot_desired);

	Eigen::MatrixXf U(2,1);
	U = K*(X-Xd);

	double u1 = U(0,0)*cos(rot) + U(1,0)*sin(rot);
	double u2 = (-U(0,0)*sin(rot) + U(1,0)*cos(rot))/lin_v_state[0];
	//

	double w_max = 0.2;
	if(abs(u2) > w_max)
		if(u2 < 0)
			u2 = -w_max;
		else
			u2 = w_max;

	double a_max = 0.05;
	if(abs(u1) > a_max)
		if(u1 < 0)
			u1 = -a_max;
		else
			u1 = a_max;

//	if((lin_v_state[0] >= 0.4 && u1 > 0.0) || (lin_v_state[0] <= 0.3 && u1 < 0.0))
//		u1 = 0;

    result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setXY(
        pos[0] + lin_v_state[0] * duration * cos(rot),
        pos[1] + lin_v_state[0] * duration * sin(rot));
    result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->setYaw(
        rot    + u2 * duration);
    result->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0] = lin_v_state[0] + u1 * duration;

//    std::cout << std::endl << "duration:" << duration << std::endl;
//    std::cout << "x: " << pos[0] << " y: " << pos[1] << " rot: " << rot << " v: " << lin_v_state[0] << std::endl;
//    std::cout << "X: " << X << std::endl;
//    std::cout << "Xd: " << Xd << std::endl;
//    std::cout << "v: " << result->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(1)->values[0] << std::endl;

    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    // Normalize orientation value between 0 and 2*pi
    ob::SO2StateSpace::StateType* so2 = result->as<ob::CompoundStateSpace::StateType>()->as<ob::SE2StateSpace::StateType>(0)->as<ob::SO2StateSpace::StateType>(1);
    SO2.enforceBounds(so2);

    //covariance
	Eigen::MatrixXf start_cov(4,4);
	Eigen::MatrixXf result_cov(4,4);
	Eigen::MatrixXf A(4,4);
	Eigen::MatrixXf B(4,2);
	Eigen::MatrixXf Acl(4,4);
	Eigen::MatrixXf Ad(4,4);
	Eigen::MatrixXf Pw(4,4);
	A << 0.0, 1.0, 0.0, 0.0,
		 0.0, 0.0, 0.0, 0.0,
		 0.0, 0.0, 0.0, 1.0,
		 0.0, 0.0, 0.0, 0.0;

	B << 0.0, 0.0,
	     1.0, 0.0,
	     0.0, 0.0,
	     0.0, 1.0;

	Acl = A + B*K;

//	Ad = Eigen::MatrixXf::Identity(4, 4) + Acl*duration;
//	Ad = Ad.inverse();

	//Ad (K)
//	Ad << 0.9999, 0.0099, 0.0, 0.0,
//		-0.0197, 0.9703, 0.0, 0.0,
//		0.0, 0.0, 0.9994, 0.0097,
//		0.0, 0.0, -0.1159, 0.9318;

	//Ad LQR (dt=0.01)
	Ad << 1.0, 0.0099, 0.0, 0.0,
		-0.0099, 0.9778, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0099,
		0.0, 0.0, -0.0099, 0.9778;
	//Ad LQR (dt=0.1)
//		Ad << 0.9954,    0.0895,         0,         0,
//			  -0.0895,    0.7953,         0,         0,
//				    0,         0,    0.9954,    0.0895,
//				    0,         0,   -0.0895,    0.7953;

//	std::cout << "Acl: " << Acl << std::endl;
//	std::cout << "Ad: " << Ad << std::endl;

	Pw = 0.01*Eigen::MatrixXf::Identity(4, 4);
//	Pw << 0.002, 0.001, 0.0, 0.0,
//		  0.0, 0.0, 0.0001, 0.0,
//		  0.001, 0.002, 0.0, 0.0,
//		  0.0, 0.0, 0.0, 0.0001;

    int cov_ind = 0;
    for(int i=0; i<4 ; i++)
    {
    	for(int j=0; j<4 ; j++)
		{
    		start_cov(i,j) = start->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind];
    		cov_ind++;
		}
    }

    result_cov = Ad*start_cov*Ad.transpose() + Pw;

    cov_ind = 0;
    for(int i=0; i<4 ; i++)
    {
    	for(int j=0; j<4 ; j++)
    	{
    		result->as<ob::CompoundStateSpace::StateType>()->as<ob::RealVectorStateSpace::StateType>(2)->values[cov_ind] = result_cov(i,j);
    		cov_ind++;
    	}
    }
}

