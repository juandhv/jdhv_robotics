#!/usr/bin/env python
from __future__    import division
from numpy         import linspace, meshgrid, zeros, isfinite, isnan, uint32
from osgeo         import gdal
from scipy.ndimage import filters
import Image
import numpy
import sys
#import ipdb

import pyproj

import os

################################################################################
def delaunayRegularGrid(shape):
    """
    Obtains the triangles to represent the grid in a 3D mesh based on triangular
    faces. The input is the shape (dimensions) of the grid.
    It assumes that the matrices representing the grid will be read as vectors.
    """
    xsize = shape[1]
    ysize = shape[0]
    trian = zeros((2*(xsize-1)*(ysize-1), 3), dtype=uint32)
    n = 0 # index
    for j in range(ysize-1):
        for i in range(xsize-1):
            # First triangle of a grid square
            trian[n,:] = [i+j*xsize,  i+j*xsize+1,    i+j*xsize+xsize]
            n += 1
            # Second triangle of a grid square
            trian[n,:] = [i+j*xsize+1,i+j*xsize+xsize,i+j*xsize+1+xsize]
            n += 1

    return trian

################################################################################
def writeOBJ(x,y,z,trian,siz):
    '''
    Takes vertex positions (x,y,z) and the triangle mesh (trian)
    and creates a OBJ file.
    '''
    sx = float(siz[0])
    sy = float(siz[1])
    # Indices start at 1, not at 0
    trian += 1
    
    # Output file
    fh = file(name + '.obj','w')
    
    # Header with material file used
    line = 'mtllib %s.mtl\n' % name
    fh.write(line)
    
    # All vertices [v %x %y %z]
    for i in range(x.shape[0]):
        fh.write('v %s %s %s\n' % (x[i],y[i],z[i]))
        
    # Vertice normals [vn %x %y %z]
    fh.write('vn 0.0 0.0 -1.0\n')
        
    # Texture coordinates [vt %0-1 %0-1]
    xmi = x.min()
    ymi = y.min()
    xm = x.max() - xmi
    ym = y.max() - ymi
    for i in range(x.shape[0]):
        fh.write('vt %s %s\n' % ( (x[i]-xmi)/xm, 1-(y[i]-ymi)/ym))
        
    # Material to use for face color
    line = 'usemtl colored_%s\n' % name
    fh.write(line)
    
    # All faces
    for i in range(trian.shape[0]):
        fh.write('f %s/%s/1 %s/%s/1 %s/%s/1\n' % (trian[i,0],trian[i,0],trian[i,1],trian[i,1],trian[i,2],trian[i,2]))
        
    # Close file
    fh.close()

################################################################################
#def write_scripts():
#    # Script to ease mission replay (offline)
#    fo = file('../start_replay.sh','w')
#    fo.write('#!/bin/sh\n')
#    fo.write('cp missionReplay/UTMmap.obj  ~/ros_workspace/underwater_simulation/UWSim/data/scenes/\n')
#    fo.write('cp missionReplay/UTMmapc.ppm ~/ros_workspace/underwater_simulation/UWSim/data/scenes/\n')
#    fo.write('cp missionReplay/UTMmap.tif  ~/ros_workspace/playMission/src/\n')
#    fo.write('roslaunch UWSim replay_mission.launch &\n')           # open simulator with correct map
#    fo.write('sleep 30s\n')                                         # wait for the simulator to be correctly setup
#    fo.write('roslaunch playMission translator_mission.launch &\n') # translator from rosbag to simulator (UTM to simulator coordinates)
#    fo.write('rosbag play *.bag -r 20 -q')                          # play the bagfiles in the same folder
#    fo.close()
#    
#    # Script to ease mission play (online)
#    fo = file('../start_play.sh','w')
#    fo.write('#!/bin/sh\n')
#    fo.write('cp missionReplay/UTMmap.obj  ~/ros_workspace/underwater_simulation/UWSim/data/scenes/\n')
#    fo.write('cp missionReplay/UTMmapc.ppm ~/ros_workspace/underwater_simulation/UWSim/data/scenes/\n')
#    fo.write('cp missionReplay/UTMmap.tif  ~/ros_workspace/playMission/src/\n')
#    fo.write('roslaunch UWSim replay_mission.launch &\n') # open simulator with correct map
#    fo.write('sleep 30s\n')                               # wait for the simulator to be correctly setup
#    fo.write('roslaunch playMission play_mission.launch') # listener to USBL messages through UDP
#    fo.close()

################################################################################
def write_material():
    '''
    Writes a .mtl file for .obj files.
    '''
    fo = file(name + '.mtl','w')
    fo.write('newmtl colored_%s\n' % name)
    fo.write('illum 4\n')
    fo.write('Kd 0.70 0.70 0.70\n')
    fo.write('Ka 0.00 0.00 0.00\n')
    fo.write('Tf 1.00 1.00 1.00\n')
    fo.write('map_Kd %s.ppm\n' % name)
    fo.close()

################################################################################
def write_texture(data, zmin, zmax):
    """
    Create a texture for the map, mapping the sea with blue colors and land
    with red and green colors.
    """
#    zmax = data.max()
#    zmin = data.min()
    im = numpy.zeros(data.shape + (3,), dtype=numpy.uint8)
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if zmin <= data[i,j] <= zmax:
                # Underwater -> paint blue (0->255, zmin->0)
                im[i,j,0] = int( (data[i,j]-zmin) / (zmax-zmin) * 255. )
                im[i,j,1] = 0
                im[i,j,2] = int( (1 - (data[i,j]-zmin) / (zmax-zmin)) * 255. )
            else:
                # NaN values
                im[i,j,0] = 0
                im[i,j,1] = 0
                im[i,j,2] = 0
    # Write the output texture
    im = Image.fromarray(im)
    im = im.transpose(Image.FLIP_TOP_BOTTOM)
    im.save(("%s.ppm") % name)
    
################################################################################
if __name__ == '__main__':
    
    print 'Generating 3D mesh...'
    
    if len(sys.argv) < 2:
        print '\nUsage:\n\t%s file.grd [offsetN/offsetE]\n' % sys.argv[0] 
    
    else:
        # Parameters
        name = sys.argv[1].split('.')[0]
        
        # Read the grid file
        fh  = gdal.Open(sys.argv[1])
        gt  = fh.GetGeoTransform()
        md  = fh.GetMetadata()
        zz  = fh.ReadAsArray()
        e_scale = abs(gt[1])
        n_scale = abs(gt[5])
        e_size  = fh.RasterXSize
        n_size  = fh.RasterYSize
        e0 = gt[0]
        n0 = gt[3]

        # Create x and y meshgrid
        east   = linspace(0, e_scale*(e_size-1), e_size)
        north  = linspace(0, n_scale*(n_size-1), n_size)
        #north = north[::-1] # flip vector
        
        # Project to UTM
        if md['x#long_name'].startswith('Longitude'):
            print 'Projecting data to UTM...'
            zone = int(round((e0 + 180) / 6.))
            p = pyproj.Proj(proj='utm', zone=zone, ellps='WGS84')
            eastn, trash  = p(east, [north[0],]*len(east))
            trash, northn = p([east[0],]*len(north), north)
            xx, yy = meshgrid(eastn,northn)
            xx -= xx.min()
            yy -= yy.min()
        
        # Degrees
        elif md['x#long_name'].startswith('Easting'):
            print 'Already UTM data provided...'
            xx, yy = meshgrid(east,north)
        # Error
        else:
            print '>> No valid projection specified in grid file.'

        # Minimum and maximum depth (avoid NaN values)
        zmin = abs(zz[isfinite(zz)]).min()
        zmax = abs(zz[isfinite(zz)]).max()
        zz[isnan(zz)] = zmax + 10.0 #TODO
        zz = abs(zz)
        
        # Generate custom texture
        os.system(('mbm_grdtiff -I %s.grd -G2 -S0/1 -D0/0 -W1/1 -Q -O temp') % name)
        os.system(('csh temp_tiff.cmd'))
        os.system(('convert temp.tif -fill black -opaque white -resize 512x512\! %s.ppm') % name)
        os.system(('rm %s.grd.* temp_tiff.cmd temp.tif' % name))
        
        # Statistics
        print 'Readed grid:'
        print ' Size:',  zz.shape
        print ' Min north:', xx.min()
        print ' Min east: ', yy.min()
        print ' Min depth:', zmin
        print '  Max north:', xx.max()
        print '  Max east: ', yy.max()
        print '  Max depth:', zmax
        print '   North origin:', gt[3]
        print '   East origin: ', gt[0]

        # Resize grids
        if e_size > 1024 or n_size > 1024:
            im = Image.fromarray(xx)
            xx = numpy.array(im.resize((1024,1024)))
            im = Image.fromarray(yy)
            yy = numpy.array(im.resize((1024,1024)))
            im = Image.fromarray(zz)
            zz = numpy.array(im.resize((1024,1024)))

        # Filter for bad data (spikes)
        #zz = filters.median_filter(zz,7)

        # Extract points
        x = xx.ravel()
        y = yy.ravel()
        z = zz.ravel()
        
        # Apply offset
        if len(sys.argv) >= 3:
            off = sys.argv[2].split('/')
            x += float(off[1])
            y += float(off[0])

        # Delaunay, assuming regular grid
        trian = delaunayRegularGrid(zz.shape)
            
        # Export 3D model
        print 'Generating mesh...'
        writeOBJ(x, y, z, trian, zz.shape)
        
        # Write extra needed files
        write_material()
        
        
        print 'Mesh done.'
