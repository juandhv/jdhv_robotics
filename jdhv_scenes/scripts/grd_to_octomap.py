#!/usr/bin/env python
"""
@author: Guillem
"""
from __future__    import division
from numpy         import linspace, meshgrid, zeros, isfinite, isnan, uint32
from osgeo         import gdal
from scipy.ndimage import filters
import Image
import numpy
import sys
# import ipdb
import pyproj
import os

################################################################################
if __name__ == '__main__':
    
    # Check for valid inputs
    if len(sys.argv) < 2:
        print '\nUsage:\n\t%s file.grd\n' % sys.argv[0] 
    
    else:
        # Parameters
        name = sys.argv[1].split('.')[0]
        
        # Read the grid file
        fh  = gdal.Open(sys.argv[1])
        gt  = fh.GetGeoTransform()
        md  = fh.GetMetadata()
        zz  = fh.ReadAsArray()
        e_scale = abs(gt[1])
        n_scale = abs(gt[5])
        e_size  = fh.RasterXSize
        n_size  = fh.RasterYSize
        e0 = gt[0]
        n0 = gt[3]

        # Create x and y meshgrid
        east   = linspace(0, e_scale*(e_size-1), e_size)
        north  = linspace(0, n_scale*(n_size-1), n_size)
        
        # Transform to UTM if necessary to have distances in meters
        if md['x#long_name'].startswith('Longitude'):
            print 'Projecting data to UTM...'
            zone = int(round((e0 + 180) / 6.))
            p = pyproj.Proj(proj='utm', zone=zone, ellps='WGS84')
            eastn, trash  = p(east, [north[0],]*len(east))
            trash, northn = p([east[0],]*len(north), north)
            yy, xx = meshgrid(eastn,northn)
            xx -= xx.min() # north
            yy -= yy.min() # east
            north = northn
            east  = eastn
        elif md['x#long_name'].startswith('Easting'):
            print 'Already UTM data provided...'
            yy, xx = meshgrid(east,north)
        else:
            print '>> No valid projection specified in grid file.'

        # Minimum and maximum depth (avoid NaN values)
        zmin = abs(zz[isfinite(zz)]).min()
        zmax = abs(zz[isfinite(zz)]).max()
        zz[isnan(zz)] = zmax + 10.0 #TODO
        zz = abs(zz)
        
        # Statistics
        print 'Readed grid:'
        print ' Size:',  zz.shape
        print ' Min north:', xx.min()
        print ' Min east: ', yy.min()
        print ' Min depth:', zmin
        print '  Max north:', xx.max()
        print '  Max east: ', yy.max()
        print '  Max depth:', zmax
        print '   North origin:', gt[3]
        print '   East origin: ', gt[0]

        # Filter for bad data (spikes)
        zz = filters.median_filter(zz,3)

        # Extract points
        x = xx.ravel()
        y = yy.ravel(); y = y[::-1]
        z = zz.ravel()
            
        # Generate laser scan data
        print 'Generating scan nodes...'
        res = max(north[1]-north[0], east[1]-east[0])
        fe = file(name + '.pt' , 'w')
        fe.write(('%.3f %.3f\n' % (res, zmax+res)))
        for i in range(len(x)):
            if zmin <= z[i] <= zmax:
                fe.write(('%.3f %.3f %.3f\n') % (x[i], y[i], z[i]))
        fe.close()
        
        # Generate bt file
        print 'Generating bt file...'
        os.system(('rosrun tests_octomaps pt2octomap %s.pt') % (name))
        
        
        print 'Done.'
