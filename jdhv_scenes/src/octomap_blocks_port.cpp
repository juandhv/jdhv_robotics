/*
 * OctoMap - An Efficient Probabilistic 3D Mapping Framework Based on Octrees
 * http://octomap.github.com/
 *
 * Copyright (c) 2009-2013, K.M. Wurm and A. Hornung, University of Freiburg
 * All rights reserved.
 * License: New BSD
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the University of Freiburg nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <octomap/octomap.h>
#include <octomap/OcTree.h>

using namespace std;
using namespace octomap;


void print_query_info(point3d query, OcTreeNode* node) {
  if (node != NULL) {
    cout << "occupancy probability at " << query << ":\t " << node->getOccupancy() << endl;
  }
  else 
    cout << "occupancy probability at " << query << ":\t is unknown" << endl;    
}

int main(int argc, char** argv) {

  cout << endl;
  cout << "generating an octomap of concrete blocks scenario" << endl;

  OcTree map (0.5);  // create empty tree with resolution 0.1


  // insert some measurements of occupied cells

  double depth_offset = 8.0;
  double x_offset = 50.0;
  double y_offset = 10.0;
  double rotation_angle = -1.57;//-1.8648943470859733;

  for (double x=-50.0; x<=50.0; x=x+0.1) {
    for (double y=-50.0; y<=50.0; y=y+0.1) {
      for (double z=0.0; z<=0.1; z=z+0.1) {
    	  double vx = x+x_offset;
    	  double vy = y+y_offset;
    	  point3d endpoint (vx*cos(rotation_angle)-vy*sin(rotation_angle)+10.0, vx*sin(rotation_angle)+vy*cos(rotation_angle), z+depth_offset-1.0);
    	  point3d origin (vx*cos(rotation_angle)-vy*sin(rotation_angle)+10.0, vx*sin(rotation_angle)+vy*cos(rotation_angle), 0.0);
    	  map.insertRay(origin,endpoint);
      }
    }
  }

  printf("\n ok");
  for (int obst_i=0; obst_i<6; obst_i++)
	  for (double x=0.0; x<=12.5; x=x+0.1) {
		  for (double y=-7.0; y<=7.0; y=y+0.1) {
			  if(x==0.0 || (x>12.39 && x<12.41) || y==-7.0 || (y>6.99 && y <7.01)){
				  for (double z=8.0; z>=0.0; z=z-0.1) {
					  double vx = x+x_offset-50.0+(obst_i*17.0);
					  double vy = (y+y_offset);
					  point3d endpoint (vx*cos(rotation_angle)-vy*sin(rotation_angle)+10.0, vx*sin(rotation_angle)+vy*cos(rotation_angle), z+depth_offset-9.0);
					  point3d origin (x+x_offset-50.0+(obst_i*17.0), y+y_offset, 0.0);
					  //map.insertRay(origin,endpoint);
					  map.updateNode(endpoint, true); // integrate 'occupied' measurement
				  }
			  }
			  else
			  {
				  double z=0.0;
				  double vx = x+x_offset-50.0+(obst_i*17.0);
				  double vy = (y+y_offset);
				  point3d endpoint (vx*cos(rotation_angle)-vy*sin(rotation_angle)+10.0, vx*sin(rotation_angle)+vy*cos(rotation_angle), z+depth_offset-9.0);
				  //map.insertRay(origin,endpoint);
				  map.updateNode(endpoint, true); // integrate 'occupied' measurement
			  }
//				  map.updateNode(endpoint, true); // integrate 'occupied' measurement
//			  }
		  }
	  }


//  // insert some measurements of free cells
//
//  for (int x=-30; x<30; x++) {
//    for (int y=-30; y<30; y++) {
//      for (int z=-30; z<30; z++) {
//        point3d endpoint ((double) x*0.02f-1.0f, (double) y*0.02f-1.0f, (double) z*0.02f-1.0f);
//        tree.updateNode(endpoint, false);  // integrate 'free' measurement
//      }
//    }
//  }

  cout << endl;
  cout << "performing some queries:" << endl;
  
  point3d query (0., 0., 0.);
  OcTreeNode* result = map.search (query);
  print_query_info(query, result);

  query = point3d(-1.,-1.,-1.);
  result = map.search (query);
  print_query_info(query, result);

  query = point3d(1.,1.,1.);
  result = map.search (query);
  print_query_info(query, result);


  cout << endl;
  //map.writeBinary("/home/juandhv/Blocks.bt");
  map.writeBinary("/home/juandhv/Dropbox/PhD/3D_ModelsMaps/BlocksNoRotation.bt");
  cout << "wrote example BlocksNoRotation.bt" << endl << endl;
  //cout << "now you can use octovis to visualize: octovis Iros1.bt"  << endl;
  //cout << "Hint: hit 'F'-key in viewer to see the freespace" << endl  << endl;

}
