/*
Created on Jun 25, 2013

@author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)

Based on pt2octomap (Guillem)

Based on a obj file (mesh) to and octmap (.bt)
The function takes the mesh vertices and inserts rays with an origin from the zero-Z level to the vertex,
doing so the octomap creates the occupied and free voxels. Besides it completes the surfaces in order to
avoid empty voxels.
 */

#include <octomap/octomap.h>
#include <boost/algorithm/string.hpp>

#include <iostream>
#include <fstream>
#include <cstdio>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>

using namespace boost;
using namespace std;

//#define DEBUG_MSG


void print( vector <string> & v )
{
	for (size_t n = 0; n < v.size(); n++)
		cout << "\"" << v[ n ] << "\"\n";
	cout << endl;
}

/// MAIN =======================================================================
int main(int argc, char** argv){

	double seamountDepth = 15.0;

	// Show some help
	if(argc < 2)
	{
		std::cout << std::endl << "\tUsage:\n\t\t" << argv[0] << " <file.obj>" << std::endl << std::endl;
		return 0;
	}

	// Variables
	double res=0.5;
	octomath::Vector3 origin, end;
	octomap::OcTreeKey key;
	vector <string> fields;
	vector <double> X,Y,Z;
	vector <double>::iterator it;
	double Xi, Yi, Zi, Xmax=0.0, Ymax=0.0, Zmax=0.0;

	// Open file for reading
	std::cout << std::endl << "Obj file: " << argv[1] << std::endl;
	std::ifstream objFile;
	objFile.open(argv[1]);
	std::string str;
	while (std::getline(objFile, str))
	{
		split( fields, str, is_any_of( " " ) );
		if(strcmp(fields[0].c_str(),"v")==0){

			it = X.begin();
			Xi =  atof(fields[1].c_str());
			X.insert(it,Xi);
			if(Xi>Xmax){
				Xmax = Xi;
			}

			it = Y.begin();
			Yi =  atof(fields[2].c_str());
			Y.insert(it,Yi);
			if(Yi>Ymax){
				Ymax = Yi;
			}

			it = Z.begin();
			Zi =  atof(fields[3].c_str());
			Z.insert(it,Zi);
			if(Zi>Zmax){
				Zmax = Zi;
			}
#ifdef DEBUG_MSG
			cout << endl << "Reading --> Xi:" << Xi << ", Yi:" << Yi << ", Zi:" << Zi << endl;
#endif
		}
	}
#ifdef DEBUG_MSG
	cout << endl << "Xmax" << Xmax << endl;
	cout << endl << "Ymax" << Ymax << endl;
	cout << endl << "Zmax" << Zmax << endl;
#endif
	// Init octree
	octomap::OcTree map(res);
	map.setProbHit(0.7);
	map.setProbMiss(0.4);
	map.setClampingThresMin(0.1192);
	map.setClampingThresMax(0.971);

	for (unsigned i=0; i < X.size(); i++) {
			Xi = X[i]; Yi = -Y[i]; Zi =-Z[i]+seamountDepth;

			origin.x() = Xi;
			origin.y() = Yi;
			origin.z() = 0.0;

			end.x() = Xi;
			end.y() = Yi;
			end.z() = Zi;

			map.insertRay(origin,end);

//			for (double j=0; j <= Z[i]; j=j+res) {
//				Xi = X[i]; Yi = -Y[i]; Zi =-j+seamountDepth;
//				end.x() = Xi;
//				end.y() = Yi;
//				end.z() = Zi;
//				map.updateNode(end, true); // mark occupied
//			}
	}

	// Simplify the map
	map.prune();

	// Output file
	std::string fname(argv[1]);
	string fname2;
	fname2 = fname.substr(0, fname.length()-3);
	map.writeBinary(fname2 + "bt");

	// Exit main function without errors
	return 0;
}
