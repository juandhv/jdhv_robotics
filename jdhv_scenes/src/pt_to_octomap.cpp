#include <octomap/octomap.h>

#include <iostream>
#include <fstream>
#include <cstdio>
#include <sstream>
#include <algorithm>

/// MAIN =======================================================================
int main(int argc, char** argv){

    // Show some help
    if(argc < 2)
    {
        std::cout << std::endl << "\tUsage:\n\t\t" << argv[0] << " <file.pt>" << std::endl << std::endl;
        return 0;
    }

    // Variables
    double res, maxz;
    double x, y, z;
    octomath::Vector3 end;
    octomap::OcTreeKey key;

    // Open file for reading
    std::ifstream fh(argv[1]);
    fh >> res >> maxz;
    std::cout << std::endl << "Resolution: " << res << std::endl;
    std::cout << "Max depth: " << maxz << std::endl;

    // Init octree
    octomap::OcTree map(res);
    map.setProbHit(0.7);
    map.setProbMiss(0.4);
    map.setClampingThresMin(0.1192);
    map.setClampingThresMax(0.971);

    // Compute maxz again
    bool init(false);

    while(fh >> x >> y >> z)
    {
        end.x() = x;
        end.y() = y;
        end.z() = z;

        if(!init)
        {
            end.z() = maxz;

            map.coordToKeyChecked(end, key);//map.genKey(end, key);
            end = map.keyToCoord(key,map.getTreeDepth());//map.genCoords(key, map.getTreeDepth(), end);

            maxz = end.z() - res/2.0;
            end.x() = x;
            end.y() = y;
            end.z() = z;

            std::cout << "Max depth: " << maxz << std::endl;
            init = true;
        }

        map.updateNode(end, true); // mark occupied
        map.coordToKeyChecked(end, key);//map.genKey(end, key);
        end = map.keyToCoord(key,map.getTreeDepth());//map.genCoords(key, map.getTreeDepth(), end);
        z = end.z() + res;
        while(z < maxz)
        {
            end.z() = z;
            map.updateNode(end, true);
            z += res;
        }
    }

    // Simplify the map
    map.prune();

    // Output file
    std::string fname(argv[1]);
    fname = fname.substr(0, fname.length()-3);
    map.writeBinary(fname + ".bt");

    // Exit main function without errors
    return 0;
}
