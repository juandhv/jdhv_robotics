/*
 * laser_octomap_multibeam_echosounders.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: juandhv (Juan David Hernandez Vega, juandhv@eia.udg.edu)
 *
 *  Purpose: Merge data from simulated echosounders and create a laser_scan msg.
 *  Based on laser_octomap.cpp (Guillem)
 */

// Boost
#include <boost/shared_ptr.hpp>

// ROS
#include <ros/ros.h>

// ROS LaserScan tools
#include <laser_geometry/laser_geometry.h>

// ROS messages
#include <geometry_msgs/Pose.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/PointCloud.h>
#include <std_msgs/ColorRGBA.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/MarkerArray.h>

// ROS services
#include <std_srvs/Empty.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

// Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/GetOctomap.h>
typedef octomap_msgs::GetOctomap OctomapSrv;
#include <octomap_ros/conversions.h>

#include <signal.h>

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}

/// CLASS DEFINITION ===========================================================
class LaserOctomap
{
    public:
        // Constructor and destructor
        LaserOctomap();
        virtual ~LaserOctomap();

        // Callbacks
        void mbCallback(const sensor_msgs::LaserScanConstPtr& scan);
        void echos11SubCallback(const sensor_msgs::Range &echos11_data);
        void echos12SubCallback(const sensor_msgs::Range &echos12_data);
        void echos13SubCallback(const sensor_msgs::Range &echos13_data);
        void echos14SubCallback(const sensor_msgs::Range &echos14_data);
        void echos15SubCallback(const sensor_msgs::Range &echos15_data);
        void odomCallback(const nav_msgs::OdometryConstPtr& scan);
        void timerCallback(const ros::TimerEvent& e);
        void missionFlagSubCallback(const std_msgs::Bool &mission_flag);

        // Services
        bool octomapBinarySrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
        bool octomapFullSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
        bool getOctomapBinarySrv(OctomapSrv::Request  &req, OctomapSrv::GetOctomap::Response &res);


        // Octree visualization
        void publishMap();
        std_msgs::ColorRGBA heightMapColor(double h);

    private:
        // Other functions
        void filter_single_outliers(sensor_msgs::LaserScan &scan, std::vector<bool> &rngflags);

        // ROS
        ros::NodeHandle node_hand_;
        ros::Publisher octree_pub_, octomap_pub_;
        ros::Subscriber nav_sts_sub_, mb_scan_sub_, es1_scan_sub_, es2_scan_sub_, es3_scan_sub_, es4_scan_sub_, es5_scan_sub_, mission_flag_sub_;
        ros::ServiceServer octomap_binary_service_, octomap_full_service_;
        ros::ServiceServer get_octomap_binary_service_, get_BBx_binary_service_;
        ros::Timer timer_;

        // ROS tf
        tf::Pose last_pose_;
        tf::StampedTransform robot_to_mb_;
        tf::StampedTransform robot_to_echo11_, robot_to_echo12_, robot_to_echo13_, robot_to_echo14_, robot_to_echo15_;
        tf::TransformListener tf_listener_;

        // Names
        std::string fixed_frame_, robot_frame_, multibeam_frame_, echosounder11_frame_, echosounder12_frame_, echosounder13_frame_, echosounder14_frame_, echosounder15_frame_, offline_octomap_path_;

        // LaserScan => (x,y,z)
        laser_geometry::LaserProjection mb_projector_;

        // ROS Messages
        sensor_msgs::PointCloud cloud_;

        // Octree
        octomap::OcTree* octree_;
        double octree_resol_;

        // Map visualization
        bool man_color_;
        double min_z_;
        double max_z_;
        double min_depth_;
        //filter data

        // Flags
        bool add_max_range_measures_;
        bool apply_filter_;
        bool initialized_;
        bool mission_flag_available_;
        bool nav_sts_available_;
        bool add_rays_;
        bool use_echosounders_;
};

/// Constructor and destructor =================================================
LaserOctomap::LaserOctomap():
    node_hand_(),
    fixed_frame_("/world"),
    robot_frame_("/girona500"),
    multibeam_frame_("/multibeam"),
    offline_octomap_path_(""),
    octree_(NULL),
    octree_resol_(1.0),
    man_color_(false),
    min_z_(0.0),
    max_z_(0.0),
    add_max_range_measures_(false),
    add_rays_(false),
    apply_filter_(false),
    initialized_(false),
    mission_flag_available_(true),
	use_echosounders_(false),
	min_depth_(0.5)
{
    // ROS parameters
    node_hand_.param("laser_octomap_multibeam_echosounders/resolution", octree_resol_, octree_resol_);
    node_hand_.param("laser_octomap_multibeam_echosounders/manual_color", man_color_, man_color_);
    node_hand_.param("laser_octomap_multibeam_echosounders/zmin_color", min_z_, min_z_);
    node_hand_.param("laser_octomap_multibeam_echosounders/zmax_color", max_z_, max_z_);
    node_hand_.param("laser_octomap_multibeam_echosounders/apply_filter", apply_filter_, apply_filter_);
    node_hand_.param("laser_octomap_multibeam_echosounders/add_max_ranges", add_max_range_measures_, add_max_range_measures_);
    node_hand_.param("laser_octomap_multibeam_echosounders/add_rays", add_rays_, add_rays_);
    node_hand_.param("laser_octomap_multibeam_echosounders/mission_flag_available", mission_flag_available_, mission_flag_available_);

    node_hand_.param("laser_octomap_multibeam_echosounders/fixed_frame", fixed_frame_, fixed_frame_);
    node_hand_.param("laser_octomap_multibeam_echosounders/robot_frame", robot_frame_, robot_frame_);
    node_hand_.param("laser_octomap_multibeam_echosounders/multibeam_frame", multibeam_frame_, multibeam_frame_);
    node_hand_.param("laser_octomap_multibeam_echosounders/offline_octomap_path", offline_octomap_path_, offline_octomap_path_);
    node_hand_.param("laser_octomap_multibeam_echosounders/use_echosounders", use_echosounders_, use_echosounders_);
    node_hand_.param("laser_octomap_multibeam_echosounders/min_depth", min_depth_, min_depth_);

    // Transforms TF and catch the static transform from vehicle to multibeam sensor
    tf_listener_.setExtrapolationLimit(ros::Duration(0.2));
    int count(0);
    ros::Time t;
    std::string err = "cannot find transform from robot_frame to multibeam_frame";
    tf_listener_.getLatestCommonTime(robot_frame_, multibeam_frame_, t, &err);

    while (ros::ok() && !initialized_)
    {
        try
        {
            tf_listener_.lookupTransform(robot_frame_, multibeam_frame_, t, robot_to_mb_);
            initialized_ = true;
        }
        catch (std::exception e)
        {
        	tf_listener_.waitForTransform(robot_frame_, multibeam_frame_, ros::Time::now(), ros::Duration(1.0));
            tf_listener_.getLatestCommonTime(robot_frame_, multibeam_frame_, t, &err);
            count++;
            ROS_WARN("Cannot find transform from %s to %s", robot_frame_.c_str(), multibeam_frame_.c_str());
        }
        if (count > 10)
        {
            ROS_ERROR("No transform. Aborting...");
            exit(-1);
        }
    }
    ROS_WARN("Transform from %s to %s OK", robot_frame_.c_str(), multibeam_frame_.c_str());


    // Transforms TF and catch the static transform from vehicle to echosounder sensor
    initialized_ = false;
    echosounder11_frame_ = "/echosounder11";

    tf_listener_.setExtrapolationLimit(ros::Duration(0.2));
    count = 0;
    err = "cannot find transform from robot_frame to echosounders_frame";
    tf_listener_.getLatestCommonTime(robot_frame_, echosounder11_frame_, t, &err);

    while (ros::ok() && !initialized_)
    {
    	try
    	{
    		tf_listener_.lookupTransform(robot_frame_, echosounder11_frame_, t, robot_to_echo11_);
    		initialized_ = true;
    	}
    	catch (std::exception e)
    	{
    		tf_listener_.waitForTransform(robot_frame_, echosounder11_frame_, ros::Time::now(), ros::Duration(1.0));
    		tf_listener_.getLatestCommonTime(robot_frame_, echosounder11_frame_, t, &err);
    		count++;
    		ROS_WARN("Cannot find transform from %s to %s", robot_frame_.c_str(), echosounder11_frame_.c_str());
    	}
    	if (count > 10)
    	{
    		ROS_ERROR("No transform. Aborting...");
    		exit(-1);
    	}
    }
    ROS_WARN("Transform from %s to %s OK", robot_frame_.c_str(), echosounder11_frame_.c_str());

    echosounder12_frame_ = "/echosounder12";
    echosounder13_frame_ = "/echosounder13";
    echosounder14_frame_ = "/echosounder14";
    echosounder15_frame_ = "/echosounder15";

    tf_listener_.getLatestCommonTime(robot_frame_, echosounder12_frame_, t, &err);
    tf_listener_.lookupTransform(robot_frame_, echosounder12_frame_, t, robot_to_echo12_);

    tf_listener_.getLatestCommonTime(robot_frame_, echosounder13_frame_, t, &err);
    tf_listener_.lookupTransform(robot_frame_, echosounder13_frame_, t, robot_to_echo13_);

    tf_listener_.getLatestCommonTime(robot_frame_, echosounder14_frame_, t, &err);
    tf_listener_.lookupTransform(robot_frame_, echosounder14_frame_, t, robot_to_echo14_);

    tf_listener_.getLatestCommonTime(robot_frame_, echosounder15_frame_, t, &err);
    tf_listener_.lookupTransform(robot_frame_, echosounder15_frame_, t, robot_to_echo15_);

    // Octree
    if(offline_octomap_path_.size()>0)
    	octree_ = new octomap::OcTree(offline_octomap_path_);
    else
    	octree_ = new octomap::OcTree(octree_resol_);
    octree_->setProbHit(0.7);
    octree_->setProbMiss(0.4);
    octree_->setClampingThresMin(0.1192);
    octree_->setClampingThresMax(0.971);

    //=======================================================================
    // Subscribers
    //=======================================================================
    //Mission Flag (feedback)
    mission_flag_sub_ = node_hand_.subscribe("/jdhv_robotics/mission_flag", 2, &LaserOctomap::missionFlagSubCallback, this);
    // Waiting for mission flag
    ros::Rate loop_rate(10);
    if(!mission_flag_available_)
    	ROS_WARN("Waiting for mission_flag");
    while (ros::ok() && !mission_flag_available_)
    {
    	ros::spinOnce();
    	loop_rate.sleep();
    }
    //Navigation data (feedback)
    nav_sts_sub_ = node_hand_.subscribe("/pose_ekf_slam/odometry", 1, &LaserOctomap::odomCallback, this);
    nav_sts_available_ = false;
    while (ros::ok() && !nav_sts_available_)
    {
    	ros::spinOnce();
    	loop_rate.sleep();
    }

    if(offline_octomap_path_.length()==0)
    {
    	//multibeam
    	mb_scan_sub_ = node_hand_.subscribe("/multibeam_scan", 1, &LaserOctomap::mbCallback, this);

    	//Echosounders
    	if(use_echosounders_)
    	{
    		es1_scan_sub_ = node_hand_.subscribe("/echosounders/e11", 1, &LaserOctomap::echos11SubCallback, this);
    		es2_scan_sub_ = node_hand_.subscribe("/echosounders/e12", 1, &LaserOctomap::echos12SubCallback, this);
    		es3_scan_sub_ = node_hand_.subscribe("/echosounders/e13", 1, &LaserOctomap::echos13SubCallback, this);
    		es4_scan_sub_ = node_hand_.subscribe("/echosounders/e14", 1, &LaserOctomap::echos14SubCallback, this);
    		//es5_scan_sub_ = node_hand_.subscribe("/echosounders/e15", 1, &LaserOctomap::echos15SubCallback, this);
    	}
    }

    //=======================================================================
    // Publishers
    //=======================================================================
    octree_pub_ = node_hand_.advertise<visualization_msgs::MarkerArray> ("/octomap_map", 2, true);
    octomap_pub_ = node_hand_.advertise<octomap_msgs::Octomap>("octomap_map_plugin", 2, true);
    
    //=======================================================================
    // Services
    //=======================================================================
    octomap_binary_service_ = node_hand_.advertiseService("/laser_octomap/save_binary", &LaserOctomap::octomapBinarySrv, this);
    octomap_full_service_ = node_hand_.advertiseService("/laser_octomap/save_full", &LaserOctomap::octomapFullSrv, this);
    get_octomap_binary_service_ = node_hand_.advertiseService("/laser_octomap/get_binary", &LaserOctomap::getOctomapBinarySrv, this);

    // Timer for publishing
    timer_ = node_hand_.createTimer(ros::Duration(3.0), &LaserOctomap::timerCallback, this);

    // Info
    ROS_INFO("%s: node initialized_.\n\tResolution = %f\n\tManual color = %d\n\tminZ = %f\n\tmaxZ = %f\n\tfilter = %d\n\tmax_ranges = %d \n", ros::this_node::getName().c_str(), octree_resol_, man_color_, min_z_, max_z_, apply_filter_, add_max_range_measures_);
    ROS_INFO("%s: \n\tFixed frame = %s\n\tRobot frame = %s\n\tMultibeam frame = %s\n", ros::this_node::getName().c_str(), fixed_frame_.c_str(), robot_frame_.c_str(), multibeam_frame_.c_str());
}

LaserOctomap::~LaserOctomap()
{
	std::cout << "octree has been deleted" << std::endl;
    delete octree_;
}

void LaserOctomap::missionFlagSubCallback(const std_msgs::Bool &mission_flag)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

/// Callbacks ==================================================================
void LaserOctomap::mbCallback(const sensor_msgs::LaserScanConstPtr &scan)
{
	ros::Time t;
	std::string err = "cannot find transform from robot_frame to multibeam_frame";
	tf_listener_.getLatestCommonTime(robot_frame_, multibeam_frame_, t, &err);
	tf_listener_.lookupTransform(robot_frame_, multibeam_frame_, t, robot_to_mb_);

    // Editable message
    sensor_msgs::LaserScan mbscan = *scan;

    // Max range flags
    std::vector<bool> rngflags;
    rngflags.resize(mbscan.ranges.size());
    for (int i=0; i<mbscan.ranges.size(); i++)
    {
        rngflags[i] = false;
        if (!ros::ok())
        	break;
    }

    // Use zero ranges as max ranges
    double newmax = mbscan.range_max*0.90;
    if (add_max_range_measures_)
    {
        // Flag readings as maxrange
        for (int i=0; i<mbscan.ranges.size(); i++)
        {
            if ( mbscan.ranges[i] == 0.0 || mbscan.ranges[i] >= mbscan.range_max )
            {
                mbscan.ranges[i] = newmax;
                rngflags[i] = true;
            }
            if (!ros::ok())
            	break;
        }
    }

    // Apply single outliers removal filter
    if (apply_filter_)
    {
        // Copy message for editing, filter and project to (x,y,z)
        sensor_msgs::LaserScan mbscan = *scan;
        filter_single_outliers(mbscan, rngflags);
    }

    // Project to (x,y,z)
    mb_projector_.projectLaser(mbscan, cloud_, mbscan.range_max);

    // Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
    tf::Vector3 orig(0, 0, 0);
    orig = last_pose_ * robot_to_mb_ * orig;
    octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

    // Trick for max ranges not occupied
    newmax *= 0.90;
    for (unsigned i=0; i<cloud_.points.size(); i++)
    {
        // Transform readings
        tf::Vector3 scanpt(cloud_.points[i].x, cloud_.points[i].y, cloud_.points[i].z);
        scanpt = last_pose_ * robot_to_mb_ * scanpt;
        octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

        // Insert readings
        if(scanpt.getZ()>0.0)
        	octree_->insertRay(origin, end, newmax);

        if (!ros::ok())
        	break;
    }
}

/// Callbacks ==================================================================
void LaserOctomap::echos11SubCallback(const sensor_msgs::Range &echos11_data)
{
	//Data from echosounder
	double min_range_data = echos11_data.min_range;
	double max_range_data = echos11_data.max_range;
	double range_data = echos11_data.range;
	double newmax = max_range_data*0.95;

	// Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
	tf::Vector3 orig(0.0, 0.0, 0.0);
	orig = last_pose_ * robot_to_echo11_ * orig;
	octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

	tf::Vector3 scanpt(range_data, 0.0, 0.0);
	scanpt = last_pose_ * robot_to_echo11_ * scanpt;
	octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

	if(end.z() >= 0.5)
	{
		if(add_rays_)
		{
			if(max_range_data==range_data)
				octree_->insertRay(origin, end, max_range_data*0.99);
			else if(range_data!=0.0)
				octree_->insertRay(origin, end);
		}
		else
			octree_->updateNode(end, true); // integrate 'occupied' measurement
	}
}

void LaserOctomap::echos12SubCallback(const sensor_msgs::Range &echos12_data)
{
	//Data from echosounder
	double min_range_data = echos12_data.min_range;
	double max_range_data = echos12_data.max_range;
	double range_data = echos12_data.range;
	double newmax = max_range_data*0.95;

	// Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
	tf::Vector3 orig(0.0, 0.0, 0.0);
	orig = last_pose_ * robot_to_echo12_ * orig;
	octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

	tf::Vector3 scanpt(range_data, 0.0, 0.0);
	scanpt = last_pose_ * robot_to_echo12_ * scanpt;
	octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

	if(end.z() >= 0.5)
	{
		if(add_rays_)
		{
			if(max_range_data==range_data)
				octree_->insertRay(origin, end, max_range_data*0.99);
			else if(range_data!=0.0)
				octree_->insertRay(origin, end);
		}
		else
			octree_->updateNode(end, true); // integrate 'occupied' measurement
	}
}

void LaserOctomap::echos13SubCallback(const sensor_msgs::Range &echos13_data)
{
	//Data from echosounder
	double min_range_data = echos13_data.min_range;
	double max_range_data = echos13_data.max_range;
	double range_data = echos13_data.range;
	double newmax = max_range_data*0.95;

	// Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
	tf::Vector3 orig(0.0, 0.0, 0.0);
	orig = last_pose_ * robot_to_echo13_ * orig;
	octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

	tf::Vector3 scanpt(range_data, 0.0, 0.0);
	scanpt = last_pose_ * robot_to_echo13_ * scanpt;
	octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

	if(end.z() >= 0.5)
	{
		if(add_rays_)
		{
			if(max_range_data==range_data)
				octree_->insertRay(origin, end, max_range_data*0.99);
			else if(range_data!=0.0)
				octree_->insertRay(origin, end);
		}
		else
			octree_->updateNode(end, true); // integrate 'occupied' measurement
	}
}

void LaserOctomap::echos14SubCallback(const sensor_msgs::Range &echos14_data)
{
	//Data from echosounder
	double min_range_data = echos14_data.min_range;
	double max_range_data = echos14_data.max_range;
	double range_data = echos14_data.range;
	double newmax = max_range_data*0.95;

	// Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
	tf::Vector3 orig(0.0, 0.0, 0.0);
	orig = last_pose_ * robot_to_echo14_ * orig;
	octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

	tf::Vector3 scanpt(range_data, 0.0, 0.0);
	scanpt = last_pose_ * robot_to_echo14_ * scanpt;
	octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

	if(end.z() >= 0.5)
	{
		if(add_rays_)
		{
			if(max_range_data==range_data)
				octree_->insertRay(origin, end, max_range_data*0.99);
			else if(range_data!=0.0)
				octree_->insertRay(origin, end);
		}
		else
			octree_->updateNode(end, true); // integrate 'occupied' measurement
	}
}

void LaserOctomap::echos15SubCallback(const sensor_msgs::Range &echos15_data)
{
	//Data from echosounder
	double min_range_data = echos15_data.min_range;
	double max_range_data = echos15_data.max_range;
	double range_data = echos15_data.range;
	double newmax = max_range_data*0.95;

	// Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
	tf::Vector3 orig(0.0, 0.0, 0.0);
	orig = last_pose_ * robot_to_echo15_ * orig;
	octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());

	tf::Vector3 scanpt(range_data, 0.0, 0.0);
	scanpt = last_pose_ * robot_to_echo15_ * scanpt;
	octomap::point3d end(scanpt.getX(), scanpt.getY(), scanpt.getZ());

	if(end.z() >= 0.5)
	{
		if(add_rays_)
		{
			if(max_range_data==range_data)
				octree_->insertRay(origin, end, max_range_data*0.99);
			else if(range_data!=0.0)
				octree_->insertRay(origin, end);
		}
		else
			octree_->updateNode(end, true); // integrate 'occupied' measurement
	}
}

void LaserOctomap::odomCallback(const nav_msgs::OdometryConstPtr &scan)
{
	if(!nav_sts_available_)
		nav_sts_available_ = true;
    tf::poseMsgToTF(scan->pose.pose, last_pose_);
}

void LaserOctomap::timerCallback(const ros::TimerEvent &e)
{
	// Declare message
	octomap_msgs::Octomap msg;
	octomap_msgs::binaryMapToMsg(*octree_, msg);
	msg.header.stamp = ros::Time::now();
	msg.header.frame_id = fixed_frame_;
	octomap_pub_.publish(msg);
    //publishMap();
}

/// Services ===================================================================
bool LaserOctomap::octomapBinarySrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    // Saves current octree_ in home folder
    std::string fpath(getenv("HOME"));
    octree_->writeBinary(fpath + "/map_laser_octomap.bt");
    return true;
}

bool LaserOctomap::octomapFullSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    // Saves current octree_ in home folder (full probabilities)
    std::string fpath(getenv("HOME"));
    octree_->write(fpath + "/map_laser_octomap.ot");
    return true;
}

bool LaserOctomap::getOctomapBinarySrv(OctomapSrv::Request  &req, OctomapSrv::GetOctomap::Response &res)
{
	ROS_INFO("%s: sending binary map data on service request", ros::this_node::getName().c_str());
    res.map.header.frame_id = "/world";
    res.map.header.stamp = ros::Time::now();

    if (!octomap_msgs::binaryMapToMsg(*octree_, res.map))
        return false;

    return true;
}

/// Map visualization ==========================================================
void LaserOctomap::publishMap()
{    
    // Declare message and resize
    visualization_msgs::MarkerArray occupiedNodesVis;
    occupiedNodesVis.markers.resize(octree_->getTreeDepth()+1);

    // Octree limits for height map
    if (!man_color_)
    {
        double minX, minY, maxX, maxY;
        octree_->getMetricMin(minX, minY, min_z_);
        octree_->getMetricMax(maxX, maxY, max_z_);
    }

    // Traverse all leafs in the tree:
    for (octomap::OcTree::iterator it = octree_->begin(octree_->getTreeDepth()), end = octree_->end(); it != end; ++it)
    {
        if (octree_->isNodeOccupied(*it))
        {
            double x = it.getX();
            double y = it.getY();
            double z = it.getZ();

            // Create marker:
            unsigned idx = it.getDepth();
            assert(idx < occupiedNodesVis.markers.size());

            geometry_msgs::Point cubeCenter;
            cubeCenter.x = x;
            cubeCenter.y = y;
            cubeCenter.z = z;

            occupiedNodesVis.markers[idx].points.push_back(cubeCenter);

            // Color according to height
            double h = (1.0 - std::min(std::max((cubeCenter.z-min_z_)/(max_z_ - min_z_), 0.0), 1.0)); //*m_colorFactor = 0.8;
            occupiedNodesVis.markers[idx].colors.push_back(heightMapColor(h));
        }

        if (!ros::ok())
        	break;
    }
    // Finish Headers and options
    for (unsigned i= 0; i < occupiedNodesVis.markers.size(); ++i)
    {
        double size = octree_->getNodeSize(i);

        occupiedNodesVis.markers[i].header.frame_id = fixed_frame_;
        occupiedNodesVis.markers[i].header.stamp = ros::Time::now();
        occupiedNodesVis.markers[i].ns = fixed_frame_;
        occupiedNodesVis.markers[i].id = i;
        occupiedNodesVis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
        occupiedNodesVis.markers[i].scale.x = size;
        occupiedNodesVis.markers[i].scale.y = size;
        occupiedNodesVis.markers[i].scale.z = size;
        occupiedNodesVis.markers[i].color.r = 0.5;
        occupiedNodesVis.markers[i].color.g = 0.5;
        occupiedNodesVis.markers[i].color.b = 0.5;
        occupiedNodesVis.markers[i].color.a = 1.0;

        if (occupiedNodesVis.markers[i].points.size() > 0)
            occupiedNodesVis.markers[i].action = visualization_msgs::Marker::ADD;
        else
            occupiedNodesVis.markers[i].action = visualization_msgs::Marker::DELETE;

        if (!ros::ok())
        	break;
    }
    // Publish it
    octree_pub_.publish(occupiedNodesVis);
}

std_msgs::ColorRGBA LaserOctomap::heightMapColor(double h)
{
  std_msgs::ColorRGBA color;
  color.a = 1.0;
  // blend over HSV-values (more colors)

  double s = 1.0;
  double v = 1.0;

  h -= floor(h);
  h *= 6;
  int i;
  double m, n, f;

  i = floor(h);
  f = h - i;
  if (!(i & 1))
    f = 1 - f; // if i is even
  m = v * (1 - s);
  n = v * (1 - s * f);

  switch (i) {
    case 6:
    case 0:
      color.r = v; color.g = n; color.b = m;
      break;
    case 1:
      color.r = n; color.g = v; color.b = m;
      break;
    case 2:
      color.r = m; color.g = v; color.b = n;
      break;
    case 3:
      color.r = m; color.g = n; color.b = v;
      break;
    case 4:
      color.r = n; color.g = m; color.b = v;
      break;
    case 5:
      color.r = v; color.g = m; color.b = n;
      break;
    default:
      color.r = 1; color.g = 0.5; color.b = 0.5;
      break;
  }

  return color;
}

/// OTHER FUNCTION (private)
void LaserOctomap::filter_single_outliers(sensor_msgs::LaserScan &scan, std::vector<bool> &rngflags)
{
    int n(scan.ranges.size());
    double thres(scan.range_max / 10.0);
    std::vector<int> zeros;
    for (int i=1; i<n-1; i++)
    {
        if( (std::abs(scan.ranges[i-1]-scan.ranges[i]) > thres) &&
            (std::abs(scan.ranges[i+1]-scan.ranges[i]) > thres)  )
        {
            scan.ranges[i] = 0.0;
            zeros.push_back(i);
        }

        if (!ros::ok())
        	break;
    }

    // Delete unnecessary flags
    for (std::vector<int>::reverse_iterator rit = zeros.rbegin(); rit<zeros.rend(); rit++)
    {
        rngflags.erase(rngflags.begin() + *rit);

        if (!ros::ok())
        	break;
    }
}

/// MAIN NODE FUNCTION =========================================================
int main(int argc, char** argv){

	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	// Init ROS node
	ros::init(argc, argv, "laser_octomap");
	ros::NodeHandle private_nh("~");

	// Constructor
	LaserOctomap mapper;

	// Spin
	ros::spin();

	// Exit main function without errors
	return 0;
}
