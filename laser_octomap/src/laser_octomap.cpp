// Boost
#include <boost/shared_ptr.hpp>

// ROS
#include <ros/ros.h>

// ROS LaserScan tools
#include <laser_geometry/laser_geometry.h>

// ROS messages
#include <geometry_msgs/Pose.h>
#include <message_filters/subscriber.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <std_msgs/ColorRGBA.h>
#include <std_msgs/Bool.h>
#include <visualization_msgs/MarkerArray.h>

// ROS services
#include <std_srvs/Empty.h>

// ROS tf
#include <tf/message_filter.h>
#include <tf/transform_listener.h>

// Octomap
#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
//#include <octomap_msgs/OctomapBinary.h>
#include <octomap_msgs/GetOctomap.h>
typedef octomap_msgs::GetOctomap OctomapSrv;
//#include <laser_octomap/BoundingBoxQuery.h>
//typedef laser_octomap::BoundingBoxQuery BBXSrv;
#include <octomap_ros/conversions.h>

#include <signal.h>

void stopNode(int sig)
{
	ros::shutdown();
	exit(0);
}

/// CLASS DEFINITION ===========================================================
class LaserOctomap
{
    public:
        // Constructor and destructor
        LaserOctomap();
        virtual ~LaserOctomap();

        // Callbacks
        void mbCallback(const sensor_msgs::LaserScanConstPtr& scan);
        void odomCallback(const nav_msgs::OdometryConstPtr& scan);
        void timerCallback(const ros::TimerEvent& e);
        void missionFlagSubCallback(const std_msgs::Bool &mission_flag);

        // Services
        bool octomapBinarySrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
        bool octomapFullSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
        bool getOctomapBinarySrv(OctomapSrv::Request  &req, OctomapSrv::GetOctomap::Response &res);
//        bool getBBxBinarySrv(BBXSrv::Request &req, BBXSrv::Response &res);


        // Octree visualization
        void publishMap();
        std_msgs::ColorRGBA heightMapColor(double h);

    private:
        // Other functions
        void filter_single_outliers(sensor_msgs::LaserScan &scan, std::vector<bool> &rngflags);

        // ROS
        ros::NodeHandle node_hand_;
        ros::Publisher octreePub, octomap_pub_;
        ros::Subscriber odomSub_, mb_scan_sub_, mission_flag_sub_;
        ros::ServiceServer octomapBinaryService_, octomapFullService_;
        ros::ServiceServer getOctomapBinaryService_, getBBxBinaryService_;
        ros::Timer timer_;

        // ROS tf
        tf::Pose last_pose_;
        tf::StampedTransform robot2Mb_;
        tf::TransformListener tfListener_;

        // Names
        std::string fixedFrame_, robotFrame_, multibeamFrame_, offline_octomap_path_;

        // LaserScan => (x,y,z)
        laser_geometry::LaserProjection mb_projector_;

        // ROS Messages
        //geometry_msgs::Pose pose;
        sensor_msgs::PointCloud cloud_;

        //Mapping
        double planning_depth_;

        // Octree
        octomap::OcTree* octree_;
        double octree_resol_;

        // Map visualization
        bool manColor_;
        double minZ_;
        double maxZ_;

        // Flags
        bool addMaxRangeMeasures_;
        bool applyFilter_;
        bool initialized_;
        bool mission_flag_available_;
        bool nav_sts_available_;
        bool publish_enabled_;
        bool visualize_free_space_;
        bool projection_2d_;
};

/// Constructor and destructor =================================================
LaserOctomap::LaserOctomap():
    node_hand_(),
    fixedFrame_("/world"),
    robotFrame_("/girona500"),
    multibeamFrame_("/multibeam"),
    offline_octomap_path_(""),
    octree_(NULL),
    octree_resol_(1.0),
    manColor_(false),
    minZ_(0.0),
    maxZ_(0.0),
    addMaxRangeMeasures_(false),
    applyFilter_(false),
    initialized_(false),
    mission_flag_available_(true),
    publish_enabled_(false),
	visualize_free_space_(false),
	projection_2d_(false),
	planning_depth_(-1.0)
{
    // ROS parameters
    node_hand_.param("laser_octomap/resolution", octree_resol_, octree_resol_);
    node_hand_.param("laser_octomap/manual_color", manColor_, manColor_);
    node_hand_.param("laser_octomap/zmin_color", minZ_, minZ_);
    node_hand_.param("laser_octomap/zmax_color", maxZ_, maxZ_);
    node_hand_.param("laser_octomap/apply_filter", applyFilter_, applyFilter_);
    node_hand_.param("laser_octomap/add_max_ranges", addMaxRangeMeasures_, addMaxRangeMeasures_);
    node_hand_.param("laser_octomap/mission_flag_available", mission_flag_available_, mission_flag_available_);
    node_hand_.param("laser_octomap/publish_enabled", publish_enabled_, publish_enabled_);
    node_hand_.param("laser_octomap/fixed_frame", fixedFrame_, fixedFrame_);
    node_hand_.param("laser_octomap/robot_frame", robotFrame_, robotFrame_);
    node_hand_.param("laser_octomap/multibeam_frame", multibeamFrame_, multibeamFrame_);
    node_hand_.param("laser_octomap/offline_octomap_path", offline_octomap_path_, offline_octomap_path_);
    node_hand_.param("laser_octomap/visualize_free_space", visualize_free_space_, visualize_free_space_);
    node_hand_.param("laser_octomap/projection_2d", projection_2d_, projection_2d_);

    node_hand_.param("planning_framework/planning_depth", planning_depth_, -1.0);

    // Transforms TF and catch the static transform from vehicle to multibeam sensor
    tfListener_.setExtrapolationLimit(ros::Duration(0.2));
    int count(0);
    ros::Time t;
    std::string err = "cannot find transform from robot_frame to multibeam_frame";
    tfListener_.getLatestCommonTime(robotFrame_, multibeamFrame_, t, &err);

    while (ros::ok() && !initialized_)
    {
        try
        {
            tfListener_.lookupTransform(robotFrame_, multibeamFrame_, t, robot2Mb_);
            initialized_ = true;
        }
        catch (std::exception e)
        {
        	tfListener_.waitForTransform(robotFrame_, multibeamFrame_, ros::Time::now(), ros::Duration(1.0));
            tfListener_.getLatestCommonTime(robotFrame_, multibeamFrame_, t, &err);
            count++;
            ROS_WARN("Cannot find transform from %s to %s", robotFrame_.c_str(), multibeamFrame_.c_str());
        }
        if (count > 10)
        {
            ROS_ERROR("No transform. Aborting...");
            exit(-1);
        }
    }
    ROS_WARN("Transform from %s to %s OK", robotFrame_.c_str(), multibeamFrame_.c_str());
    
    // Octree
    if(offline_octomap_path_.size()>0)
    	octree_ = new octomap::OcTree(offline_octomap_path_);
    else
    	octree_ = new octomap::OcTree(octree_resol_);
    octree_->setProbHit(0.7);
    octree_->setProbMiss(0.4);
    octree_->setClampingThresMin(0.1192);
    octree_->setClampingThresMax(0.971);

    // Publishers
    octreePub = node_hand_.advertise<visualization_msgs::MarkerArray> ("/octomap_map", 2, true);
    octomap_pub_ = node_hand_.advertise<octomap_msgs::Octomap>("octomap_map_plugin", 2, true);

    //=======================================================================
    // Subscribers
    //=======================================================================
    //Mission Flag (feedback)
    mission_flag_sub_ = node_hand_.subscribe("/jdhv_robotics/mission_flag", 2, &LaserOctomap::missionFlagSubCallback, this);
    // Waiting for mission flag
    ros::Rate loop_rate(10);
    if(!mission_flag_available_)
    	ROS_WARN("Waiting for mission_flag");
    while (ros::ok() && !mission_flag_available_)
    {
    	ros::spinOnce();
    	loop_rate.sleep();
    }
    //Navigation data (feedback)
    odomSub_ = node_hand_.subscribe("/prius1/world_model/odometry/filtered", 1, &LaserOctomap::odomCallback, this);
    nav_sts_available_ = false;
    if(!nav_sts_available_)
    	ROS_WARN("Waiting for odometry");
    while (ros::ok() && !nav_sts_available_)
    {
    	ros::spinOnce();
    	loop_rate.sleep();
    }
    ROS_WARN("Odometry received");

    if(offline_octomap_path_.length()==0)
	    mb_scan_sub_ = node_hand_.subscribe("/prius1/ibeo_lux_merged/scan", 1, &LaserOctomap::mbCallback, this);

    // Services
    octomapBinaryService_ = node_hand_.advertiseService("/laser_octomap/save_binary", &LaserOctomap::octomapBinarySrv, this);
    octomapFullService_ = node_hand_.advertiseService("/laser_octomap/save_full", &LaserOctomap::octomapFullSrv, this);
    getOctomapBinaryService_ = node_hand_.advertiseService("/laser_octomap/get_binary", &LaserOctomap::getOctomapBinarySrv, this);
//    getBBxBinaryService_ = node_hand_.advertiseService("/laser_octomap/get_bbx_binary", &LaserOctomap::getBBxBinarySrv, this);

    // Timer for publishing
    if (publish_enabled_)
    {
        timer_ = node_hand_.createTimer(ros::Duration(3.0), &LaserOctomap::timerCallback, this);
    }

    // Info
    ROS_INFO("Laser Octomap node initialized_.\n\tResolution = %f\n\tManual color = %d\n\tminZ = %f\n\tmaxZ = %f\n\tfilter = %d\n\tmax_ranges = %d \n", octree_resol_, manColor_, minZ_, maxZ_, applyFilter_, addMaxRangeMeasures_);
    ROS_INFO("\n\tFixed frame = %s\n\tRobot frame = %s\n\tMultibeam frame = %s\n", fixedFrame_.c_str(), robotFrame_.c_str(), multibeamFrame_.c_str());
}

LaserOctomap::~LaserOctomap()
{
	std::cout << "octree has been deleted" << std::endl;
    delete octree_;
}

void LaserOctomap::missionFlagSubCallback(const std_msgs::Bool &mission_flag)
{
	if(!mission_flag_available_)
		mission_flag_available_ = true;
}

/// Callbacks ==================================================================
void LaserOctomap::mbCallback(const sensor_msgs::LaserScanConstPtr &scan)
{
	//ROS_WARN("data received");
	ros::Time t;
	std::string err = "cannot find transform from robot_frame to multibeam_frame";
	tfListener_.getLatestCommonTime(robotFrame_, multibeamFrame_, t, &err);
	tfListener_.lookupTransform(robotFrame_, multibeamFrame_, t, robot2Mb_);

    // Editable message
    sensor_msgs::LaserScan mbscan = *scan;

    // Max range flags
    std::vector<bool> rngflags;
    rngflags.resize(mbscan.ranges.size());
    for (int i=0; i<mbscan.ranges.size(); i++)
    {
        rngflags[i] = false;
        if (!ros::ok())
        	break;
    }

    // Use zero ranges as max ranges
    double newmax = mbscan.range_max*0.95;
    if (addMaxRangeMeasures_)
    {
        // Flag readings as maxrange
        for (int i=0; i<mbscan.ranges.size(); i++)
        {
            if ( mbscan.ranges[i] == 0.0 || mbscan.ranges[i] >= mbscan.range_max )
            {
                mbscan.ranges[i] = newmax;
                rngflags[i] = true;
            }
            if (!ros::ok())
            	break;
        }
    }

    // Apply single outliers removal filter
    if (applyFilter_)
    {
        // Copy message for editing, filter and project to (x,y,z)
        sensor_msgs::LaserScan mbscan = *scan;
        filter_single_outliers(mbscan, rngflags);
    }

    // Project to (x,y,z)
    mb_projector_.projectLaser(mbscan, cloud_, mbscan.range_max);

    // Compute origin of sensor in world frame (filters scan->ranges[i] > 0.0)
    tf::Vector3 orig(0, 0, 0);
    orig = last_pose_ * robot2Mb_ * orig;
    octomap::point3d origin(orig.getX(), orig.getY(), orig.getZ());
    //std::cout << "origin: " << orig.getX() << ", " << orig.getY() << "," << orig.getZ() << std::endl;

    // Trick for max ranges not occupied
    newmax *= 0.90;
    for (unsigned i=0; i<cloud_.points.size(); i++)
    {
        // Transform readings
        tf::Vector3 scanpt(cloud_.points[i].x, cloud_.points[i].y, cloud_.points[i].z);
        scanpt = last_pose_ * robot2Mb_ * scanpt;
        octomap::point3d end;

        if(projection_2d_)
        	if(planning_depth_<0.0)
        		end = octomap::point3d(scanpt.getX(), scanpt.getY(), scanpt.getZ());
        	else
        		end = octomap::point3d(scanpt.getX(), scanpt.getY(), planning_depth_);
        else
        	end = octomap::point3d(scanpt.getX(), scanpt.getY(), scanpt.getZ());


        // Insert readings
        //if(scanpt.getZ()>0.0)
        	octree_->insertRay(origin, end, newmax);

        if (!ros::ok())
        	break;
    }
}

void LaserOctomap::odomCallback(const nav_msgs::OdometryConstPtr &odom_msg)
{
	if(!nav_sts_available_)
		nav_sts_available_ = true;
    tf::poseMsgToTF(odom_msg->pose.pose, last_pose_);
}

void LaserOctomap::timerCallback(const ros::TimerEvent &e)
{
	// Declare message
	octomap_msgs::Octomap msg;
	octomap_msgs::binaryMapToMsg(*octree_, msg);
	msg.header.stamp = ros::Time::now();
	msg.header.frame_id = fixedFrame_;
	octomap_pub_.publish(msg);
	if(visualize_free_space_)
		publishMap();
}

/// Services ===================================================================
bool LaserOctomap::octomapBinarySrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    // Saves current octree_ in home folder
    std::string fpath(getenv("HOME"));
    octree_->writeBinary(fpath + "/map_laser_octomap.bt");
    return true;
}

bool LaserOctomap::octomapFullSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    // Saves current octree_ in home folder (full probabilities)
    std::string fpath(getenv("HOME"));
    octree_->write(fpath + "/map_laser_octomap.ot");
    return true;
}

bool LaserOctomap::getOctomapBinarySrv(OctomapSrv::Request  &req, OctomapSrv::GetOctomap::Response &res)
{
	ROS_INFO("%s: sending binary map data on service request", ros::this_node::getName().c_str());
    res.map.header.frame_id = "/world";
    res.map.header.stamp = ros::Time::now();

    if (!octomap_msgs::binaryMapToMsg(*octree_, res.map))
        return false;

    return true;
}

//bool LaserOctomap::getBBxBinarySrv(BBXSrv::Request &req, BBXSrv::Response &res)
//{
//    // Request
//    octomap::point3d min = octomap::pointMsgToOctomap(req.min);
//    octomap::point3d max = octomap::pointMsgToOctomap(req.max);
//
//    // New octree map
//    octomap::point3d pos;
//    octomap::OcTreeKey key;
//    octomap::OcTree box(octree_->getResolution());
//    box.setProbHit(octree_->getProbHit());
//    box.setProbMiss(octree_->getProbMiss());
//    box.setClampingThresMin(octree_->getClampingThresMin());
//    box.setClampingThresMax(octree_->getClampingThresMax());
//
//    for(octomap::OcTree::leaf_bbx_iterator it = octree_->begin_leafs_bbx(min,max), end=octree_->end_leafs_bbx(); it!= end; ++it)
//    {
//        if (it->getOccupancy() > 0.5)
//        {
//            pos = it.getCoordinate();  // get coordinates
//            box.updateNode(pos, true); // mark occupied
//        }
//    }
//
//    ROS_INFO("Sending BBX map data on service request");
//    res.map.header.frame_id = "/world";
//    res.map.header.stamp = ros::Time::now();
//
//    if (!octomap_msgs::binaryMapToMsgData(box, res.map.data))
//        return false;
//
//    return true;
//}
//
//    ROS_INFO("Sending BBX map data on service request");
//    res.map.header.frame_id = fixedFrame_;
//    res.map.header.stamp = ros::Time::now();
//
//    if (!octomap_msgs::binaryMapToMsgData(box, res.map.data))
//        return false;
//
//    return true;
//}

/// Map visualization ==========================================================
void LaserOctomap::publishMap()
{    
    // Declare message and resize
    visualization_msgs::MarkerArray occupiedNodesVis;
    occupiedNodesVis.markers.resize(octree_->getTreeDepth()+1);

    // Octree limits for height map
    if (!manColor_)
    {
        double minX, minY, maxX, maxY;
        octree_->getMetricMin(minX, minY, minZ_);
        octree_->getMetricMax(maxX, maxY, maxZ_);
    }

    // Traverse all leafs in the tree:
    for (octomap::OcTree::iterator it = octree_->begin(octree_->getTreeDepth()), end = octree_->end(); it != end; ++it)
    {
        if (!octree_->isNodeOccupied(*it))
        {
            double x = it.getX();
            double y = it.getY();
            double z = it.getZ();

            // Create marker:
            unsigned idx = it.getDepth();
            assert(idx < occupiedNodesVis.markers.size());

            geometry_msgs::Point cubeCenter;
            cubeCenter.x = x;
            cubeCenter.y = y;
            cubeCenter.z = z;

            occupiedNodesVis.markers[idx].points.push_back(cubeCenter);

            // Color according to height
            double h = (1.0 - std::min(std::max((cubeCenter.z-minZ_)/(maxZ_ - minZ_), 0.0), 1.0)); //*m_colorFactor = 0.8;
            //occupiedNodesVis.markers[idx].colors.push_back(heightMapColor(h));
        }

        if (!ros::ok())
        	break;
    }
    // Finish Headers and options
    for (unsigned i= 0; i < occupiedNodesVis.markers.size(); ++i)
    {
        double size = octree_->getNodeSize(i);

        occupiedNodesVis.markers[i].header.frame_id = fixedFrame_;
        occupiedNodesVis.markers[i].header.stamp = ros::Time::now();
        occupiedNodesVis.markers[i].ns = fixedFrame_;
        occupiedNodesVis.markers[i].id = i;
        occupiedNodesVis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
        occupiedNodesVis.markers[i].scale.x = size;
        occupiedNodesVis.markers[i].scale.y = size;
        occupiedNodesVis.markers[i].scale.z = size;
        occupiedNodesVis.markers[i].color.r = 0.5;
        occupiedNodesVis.markers[i].color.g = 0.5;
        occupiedNodesVis.markers[i].color.b = 0.5;
        occupiedNodesVis.markers[i].color.a = 1.0;

        if (occupiedNodesVis.markers[i].points.size() > 0)
            occupiedNodesVis.markers[i].action = visualization_msgs::Marker::ADD;
        else
            occupiedNodesVis.markers[i].action = visualization_msgs::Marker::DELETE;

        if (!ros::ok())
        	break;
    }
    // Publish it
    octreePub.publish(occupiedNodesVis);
}

std_msgs::ColorRGBA LaserOctomap::heightMapColor(double h)
{
  std_msgs::ColorRGBA color;
  color.a = 1.0;
  // blend over HSV-values (more colors)

  double s = 1.0;
  double v = 1.0;

  h -= floor(h);
  h *= 6;
  int i;
  double m, n, f;

  i = floor(h);
  f = h - i;
  if (!(i & 1))
    f = 1 - f; // if i is even
  m = v * (1 - s);
  n = v * (1 - s * f);

  switch (i) {
    case 6:
    case 0:
      color.r = v; color.g = n; color.b = m;
      break;
    case 1:
      color.r = n; color.g = v; color.b = m;
      break;
    case 2:
      color.r = m; color.g = v; color.b = n;
      break;
    case 3:
      color.r = m; color.g = n; color.b = v;
      break;
    case 4:
      color.r = n; color.g = m; color.b = v;
      break;
    case 5:
      color.r = v; color.g = m; color.b = n;
      break;
    default:
      color.r = 1; color.g = 0.5; color.b = 0.5;
      break;
  }

  return color;
}

/// OTHER FUNCTION (private)
void LaserOctomap::filter_single_outliers(sensor_msgs::LaserScan &scan, std::vector<bool> &rngflags)
{
    int n(scan.ranges.size());
    double thres(scan.range_max / 10.0);
    std::vector<int> zeros;
    for (int i=1; i<n-1; i++)
    {
        if( (std::abs(scan.ranges[i-1]-scan.ranges[i]) > thres) &&
            (std::abs(scan.ranges[i+1]-scan.ranges[i]) > thres)  )
        {
            scan.ranges[i] = 0.0;
            zeros.push_back(i);
        }

        if (!ros::ok())
        	break;
    }

    // Delete unnecessary flags
    for (std::vector<int>::reverse_iterator rit = zeros.rbegin(); rit<zeros.rend(); rit++)
    {
        rngflags.erase(rngflags.begin() + *rit);

        if (!ros::ok())
        	break;
    }
}

/// MAIN NODE FUNCTION =========================================================
int main(int argc, char** argv){

	//=======================================================================
	// Override SIGINT handler
	//=======================================================================
	signal(SIGINT, stopNode);

	// Init ROS node
	ros::init(argc, argv, "laser_octomap");
	ros::NodeHandle private_nh("~");

	// Constructor
	LaserOctomap mapper;

	// Spin
	ros::spin();

	// Exit main function without errors
	return 0;
}
